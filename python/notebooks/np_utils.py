"""
Functions used to configure and capture / verify data for Numpy runs

"""
import math
import numpy as np
from io_utils import *

def build_run_id(size, iters, optimizer, run_type):
    return str(size) + '_' + str(iters) + '_' + run_type + '_' + optimizer + '_'
"""
def np_global_params(size, learning_rate, iters, optimizer):
    np_run_id = build_run_id(size, learning_rate, optimizer, 'np')
    tf_run_id = build_run_id(size, learning_rate, optimizer, 'tf')
    return {'learning_rate': learning_rate, 'iters': iters, 'np_run_id': np_run_id, 'tf_run_id': tf_run_id}
"""

def np_conv_layer(stride, pads):
    return {'layer': 'conv', 'stride': stride, 'pads': pads}

def np_pool_layer(stride, fsize, mode ='max'):
    return {'layer': 'pool', "stride": stride, "fsize": fsize, 'mode': mode}

def np_fully_connected_layer():
    return {'layer': 'fcn'}


def load_global_params_and_data(size, iters, optimizer, learning_rate):
    np_run_id = build_run_id(size, iters, optimizer, 'np')
    tf_run_id = build_run_id(size, iters, optimizer, 'tf')
    global_params = {'learning_rate': learning_rate, 'iters': iters, 'np_run_id': np_run_id, 'tf_run_id': tf_run_id}
    X = loadH5(tf_run_id + 'X')
    Y = loadH5(tf_run_id + 'Y')
    return global_params, X, Y

def np_assign_layer_values(params):
    global_params, params_CNN, params_FCN = params
    np_run_id = global_params['np_run_id']
    tf_run_id = global_params['tf_run_id']

    for index, params in enumerate(params_CNN):
        layer_id = str(index + 1)
        params['layer_id'] = layer_id
        if params['layer'] == 'conv':
            W = loadH5(tf_run_id + 'W' + layer_id)
            params['W'] = W
            B = np.zeros((1,1,1,W.shape[3]))
            params['B'] = B
            
    layer_id = str(len(params_CNN) + 1)
    params_FCN['layer_id'] = layer_id
    W = loadH5(tf_run_id + 'W' + layer_id)
    params_FCN['W'] = W
    B = loadH5(tf_run_id + 'B' + layer_id)
    params_FCN['B'] = B
    return  global_params, params_CNN, params_FCN

def persist_layer(np_run_id, params, i):
    for k, v in params.items():
        if type(v).__name__ == 'ndarray':
            fName = np_run_id + k + params['layer_id'] + '_' + str(i)
            if 'W' in k:
                if v.ndim == 4:
                    persistNPByChannel(fName, v)
                else:
                    persist(fName, v)
            else:
                persist(fName, v)
    

def persist_updated_values(params, i):
    global_params, params_CNN, params_FCN = params
    np_run_id = global_params['np_run_id']
 
    for params in params_CNN:
        persist_layer(np_run_id, params, i)
            
    persist_layer(np_run_id, params_FCN, i)
    
def verify_updated_values(params):
    global_params, params_CNN, params_FCN = params
    np_run_id = global_params['np_run_id']
    tf_run_id = global_params['tf_run_id']
    iters = int(global_params['iters'])
    for i in range(iters):
 
        for index, params in enumerate(params_CNN):
            if params['layer'] == 'conv':
            
                name = 'W' + params['layer_id']
                np_v = loadH5(np_run_id + 'd' + name + '_' + str(i))
                tf_v = loadH5(tf_run_id + 'd' + name + '_' + str(i))
                np.testing.assert_allclose(tf_v, np_v, rtol=1e-3, atol=0, err_msg='d' + name + ' on iteration ' + str(i))
                np_v = loadH5(np_run_id + name + '_' + str(i))
                tf_v = loadH5(tf_run_id + name + '_' + str(i))
                np.testing.assert_allclose(tf_v, np_v, rtol=1e-3, atol=0, err_msg= name + ' on iteration ' + str(i))
            
           
        name = 'B' + params_FCN['layer_id']
        np_v = loadH5(np_run_id + 'd' + name + '_' + str(i))
        tf_v = loadH5(tf_run_id + 'd' + name + '_' + str(i))
        np.testing.assert_allclose(tf_v, np_v, rtol=1e-3, atol=0, err_msg='d' + name + ' on iteration ' + str(i))
        np_v = loadH5(np_run_id + name + '_' + str(i))
        tf_v = loadH5(tf_run_id + name + '_' + str(i))
        np.testing.assert_allclose(tf_v, np_v, rtol=1e-3, atol=0, err_msg= name + ' on iteration ' + str(i))
     
        name = 'W' + params_FCN['layer_id']
        np_v = loadH5(np_run_id + 'd' + name + '_' + str(i))
        tf_v = loadH5(tf_run_id + 'd' + name + '_' + str(i))
        np.testing.assert_allclose(tf_v, np_v, rtol=1e-3, atol=0, err_msg='d' + name + ' on iteration ' + str(i))
        np_v = loadH5(np_run_id + name + '_' + str(i))
        tf_v = loadH5(tf_run_id + name + '_' + str(i))
        np.testing.assert_allclose(tf_v, np_v, rtol=1e-3, atol=0, err_msg= name + ' on iteration ' + str(i))

 