"""
Functions used to configure and capture data from Tensorflow runs

"""
import math
import numpy as np
import tensorflow as tf

from io_utils import *

"""
Create and initialise configuration functions
"""
def tf_global_params(X, learning_rate, iters, optimizer, mini_batch_size = 64):
    run_id = str(X.shape[0]) + '_' + str(iters) + '_tf_' + optimizer + '_'
    return {'learning_rate': learning_rate, 'iters': iters, 'optimizer': optimizer, 'run_id': run_id, 'mini_batch_size': mini_batch_size}

def tf_conv_layer(padType, stride, channelsIn, fSize, channelsOut):
    return {'layer': 'conv', 'stride': stride, 'padType': padType, "fSize": fSize, 'channelsIn': channelsIn, 'channelsOut' : channelsOut}

def tf_pool_layer(stride, fSize):
    return {'layer': 'pool', "stride": stride, "fSize": fSize}

def tf_fully_connected_layer():
    return {'layer': 'fcn'}

def tf_assign_layer_values(params):
    global_params, params_CNN, params_FCN = params
    tf.set_random_seed(1)

    for index, params in enumerate(params_CNN):
        layer_id = str(index + 1)
        params['layer_id'] = layer_id
        if params['layer'] == 'conv': 
            fSize = params['fSize']
            channelsIn = params['channelsIn']
            channelsOut = params['channelsOut']
            W = tf.get_variable('W' + layer_id, [fSize, fSize, channelsIn, channelsOut], initializer =                         tf.contrib.layers.xavier_initializer(seed = 0))
            params['W'] = W
    
    if type(params_FCN).__name__ == 'dict':
        params_FCN['layer_id'] = str(len(params_CNN) + 1)
    elif type(params_FCN).__name__ == 'list':
        for index, params in enumerate(params_CNN):
            params['layer_id'] = str(len(params_CNN) + index + 1)
    return global_params, params_CNN, params_FCN

"""
Persist the values of trainable variables before each iteration
"""
def persist_CNN_initial(run_id, params_CNN, sess):
    for params in params_CNN:
        if params['layer'] == 'conv':
            name = 'W' + params['layer_id']
            W = get_trainable_var(name + ':0', sess)
            if W is not None:
                persistNPByChannel(run_id + name, W) 
                
def persist_FCN_layer_initial(run_id, params, sess):
    W = get_trainable_var('fully_connected/weights:0', sess)
    name = 'W' + params['layer_id']
    persist(run_id + name, W)
    B = get_trainable_var('fully_connected/biases:0', sess)
    name = 'B' + params['layer_id']
    persist(run_id + name, B)
    
def persist_initial(run_id, params_CNN, params_FCN, sess):
    persist_CNN_initial(run_id, params_CNN, sess)
    persist_FCN_layer_initial(run_id, params_FCN, sess)

"""
Persist the gradients and values for trainable variables after each iteration
"""
    
def persist_CNN_after(run_id, params_CNN, i, gv):
    for params in params_CNN:
        if params['layer'] == 'conv':
            name = 'W' + params['layer_id']
            dW, W = get_gradient_and_value(name + ':0', gv)
            if dW is not None:
                persistNPByChannel(run_id + 'd' + name + '_' + str(i), dW) 
                persistNPByChannel(run_id + name + '_' + str(i), W) 

                
def persist_FCN_layer_after(run_id, params, i, gv):
    dW, W = get_gradient_and_value('fully_connected/weights:0', gv)
    name = 'W' + params['layer_id']
    persist(run_id + 'd' + name + '_' + str(i), dW) 
    persist(run_id + name + '_' + str(i), W) 
    dB, B = get_gradient_and_value('fully_connected/biases:0', gv)
    name = 'B' + params['layer_id']
    persist(run_id + 'd' + name + '_' + str(i), dB) 
    persist(run_id + name + '_' + str(i), B) 

    
def persist_after(run_id, params_CNN, params_FCN, i, gv):
    persist_CNN_after(run_id, params_CNN, i, gv)
    persist_FCN_layer_after(run_id, params_FCN, i, gv)

def persist_costs(run_id, costs):
    persist(run_id + 'Costs', np.array(costs))
    
"""
Helper functions to access trianable data
"""
def list_trainable_vars(sess):
    variables_names = [v.name for v in tf.trainable_variables()]
    values = sess.run(variables_names)
    for k, v in zip(variables_names, values):
        print ("Trainable Variable: ", k)
        print ("Trainable Shape: ", v.shape)

def list_global_vars(sess):
    variables_names = [v.name for v in tf.global_variables()]
    values = sess.run(variables_names)
    for k, v in zip(variables_names, values):
        print ("Global Variable: ", k)
        print ("Global Shape: ", v.shape)
        
def list_gradients(gv):
    variables_names = [v.name for v in tf.trainable_variables()]
    grads = gv[0]

    for k, v in zip(variables_names, grads):
        print ("Trainable Variable: ", k)
        g,v = v
        print ("Grad Shape: ", g.shape)
        print ("Var Shape: ", v.shape)
        
def get_trainable_var(name, sess):
    variables_names = [v.name for v in tf.trainable_variables()]
    values = sess.run(variables_names)
    val = None
    for k, v in zip(variables_names, values):
        if k == name:
            val = v

    return val

def get_gradient_and_value(name, gv):
    variables_names = [v.name for v in tf.trainable_variables()]
    grad = None
    val = None
    grads = gv[0]

    for k, v in zip(variables_names, grads):
        if k == name:
            grad, val = v
    
    return grad, val
