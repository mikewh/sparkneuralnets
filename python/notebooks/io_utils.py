"""
Functions to persist and load test data, both as h5 (for python) and strings (for scala framework).
TODO: Naming conventions need to be 'pythonised'
"""
import math
import numpy as np
import h5py
import ast
import re

DEFAULT_PATH = 'C:/Temp/MLData/sparkneuralnets/2018_03_14/python/testData/'

def h5fName(fName, path = DEFAULT_PATH):
    return path + fName + '.h5'

def closeH5(fName, path = DEFAULT_PATH):
    try:
        h5f = h5py.File(h5fName(fName, path),'r')
        h5f.close()
    except OSError:
        print('Failed to close ' + h5fName(fName, path))

def persistH5(fName, nparr, path = DEFAULT_PATH):
    try:
        h5f = h5py.File(h5fName(fName, path), 'w')
        h5f.create_dataset(fName, data=nparr)
        h5f.close()
    except OSError:
        print('Failed to persist ' + h5fName(fName, path))
        closeH5(fName, path)
        
def loadH5(fName, path = DEFAULT_PATH):
    try:
        h5f = h5py.File(h5fName(fName, path),'r')
        ds = np.array(h5f[fName][:])
        h5f.close()
        return ds
    except OSError:
        print('Failed to load ' + h5fName(fName, path))
        closeH5(fName, path)

def npStrFName(fName, path = DEFAULT_PATH):
    return path + fName + '.txt'

def closeNpStr(fName, path = DEFAULT_PATH):
    try:
        f = open(npStrFName(fName, path),'r')
        f.close()
    except OSError:
        print('Failed to close ' + npStrFName(fName, path))
        
def persistNpStr(fName, nparr, path = DEFAULT_PATH):
    numE = 1
    for i in range(nparr.ndim):
        numE = numE * nparr.shape[i]
    
    np.set_printoptions(threshold=numE)
    try:
        f = open(npStrFName(fName, path),'w')
        f.write(np.array_str(nparr))
        f.close()
    except OSError:
        print('Failed to persist ' + npStrFName(fName, path))
        closeNpStr(fName)
    
def persistNPArr(fName, nparr, path = DEFAULT_PATH):
    persistH5(fName, nparr, path)
    persistNpStr(fName, nparr, path)
    
def persistNpStrByChannel(fName, nparr, path = DEFAULT_PATH):
    numE = nparr.shape[0] * nparr.shape[1] * nparr.shape[2] * nparr.shape[3]
    
    np.set_printoptions(threshold=numE)
    try:
        f = open(npStrFName(fName, path),'w')
        f.write('[\n')
        for cOut in range(0, nparr.shape[3]):
            f.write('[\n')
            for wnnc in nparr[:,:,:, cOut]:
                f.write(np.array_str(wnnc))
                f.write('\n')
        
            f.write('\n]')
        f.write('\n]')
        f.close()
    except OSError:
        print('Failed to persist ' + npStrFName(fName, path))
        closeNpStr(fName, path)

def persistNPByChannel(fName, nparr, path = DEFAULT_PATH):
    persistH5(fName, nparr, path)
    persistNpStrByChannel(fName, nparr, path)
    
def persist(fName, nparr, path = DEFAULT_PATH):
    persistH5(fName, nparr, path)
    if nparr.ndim == 4:
        persistNpStrByChannel(fName, nparr, path)
    else:
        persistNpStr(fName, nparr, path)

def persistRunData(sess , rId, runData, epoch, feedDict, updated, path = DEFAULT_PATH):
    for key, val in runData.items():
        v = sess.run(val, feed_dict=feedDict)
        persist(rId + updated + key + "_" + str(epoch),v, path)

def readString(fName, path = DEFAULT_PATH):
    try:
        f = open(npStrFName(fName),'r')
        contents = f.read()
        f.close()
        return contents
    except OSError:
        print('Failed to read ' + npStrFName(fName, path))
        closeNpStr(fName, path)

def stringToNPArr(s):
    s = re.sub('\n',r'',s)
    s = re.sub('0. ', r'0.0 ', s)
    s = re.sub('(\d) +(-|\d)', r'\1,\2', s)
    s = re.sub('\] *\[', r'],[', s)
    return np.array(ast.literal_eval(s))
    
def loadNPArrFromChannelStr(fName):
    s = readString(fName)
    return NPArrFromChannelStr(s)

def NPArrFromChannelStr(s):
    na= stringToNPArr(s)
    nac = np.zeros((na.shape[1],na.shape[2],na.shape[3], na.shape[0]))
    for channel in range(na.shape[0]):
        nac[:,:,:,channel] = na[channel,:,:,:]

    return nac    