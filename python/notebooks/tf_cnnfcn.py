"""
Core functions to:
- generate test data from Tensorflow CNN FCN runs, optionally persisting all trainable data
- run full local Tensorflow train and predict 

"""
import math
import time

import numpy as np
import tensorflow as tf
from tensorflow.python.framework import ops
import matplotlib.pyplot as plt

from tf_utils import *

def cnn_layer_forward(params):
    A_Prev = params['A_Prev']
    if params['layer'] == 'conv':
        strides = [params["stride"]] * 4
        Z = tf.nn.conv2d(A_Prev, params['W'], strides = strides, padding = params["padType"])
        params['Z'] = Z
        A = tf.nn.relu(Z)
        params['A'] = A
    else:
        fsize = params["fSize"]
        stride = params["stride"]

        A = tf.nn.max_pool(A_Prev, ksize = [1,fsize,fsize,1], strides = [1,stride,stride,1], padding = 'SAME')
        params['A'] = A
    
    return params

def cnn_forward(X, params_CNN):
    A_Prev = X
    for params in params_CNN:
        params['A_Prev'] = A_Prev
        params = cnn_layer_forward(params)
        A_Prev = params['A']
    
    Al = params_CNN[-1]['A']
    Fal = tf.contrib.layers.flatten(Al)

    return Fal, params_CNN

def fcn_Forward(Fal, Y, params_FCN):
    Z = tf.contrib.layers.fully_connected(Fal, Y.shape[1].value, activation_fn=None)
    params_FCN['Z'] = Z
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits = Z,  labels = Y))
    return cost, params_FCN

def cnn_fcn_Once(X, Y, params_CNN, params_FCN):
    Fal, params_CNN = cnn_forward(X, params_CNN)
    cost, params_FCN = fcn_Forward(Fal, Y, params_FCN)
    return cost, params_CNN, params_FCN

def tf_train_test_data(Xtrain, Ytrain, params, record = False):
    global_params, params_CNN, params_FCN = params
    run_id = global_params['run_id']
    if record == True:
        persistNPArr(run_id + 'X', Xtrain)
        persist(run_id + 'Y', Ytrain)
       
    (_, h, w, c) = Xtrain.shape  
    X = tf.placeholder(tf.float32, [None,  h, w, c], name = "X")
    Y = tf.placeholder(tf.float32, [None, Ytrain.shape[1]], name = "Y")
    
    costs = []
    cost, params_CNN, params_FCN = cnn_fcn_Once(X, Y, params_CNN, params_FCN)

    optimizer = tf.train.GradientDescentOptimizer(learning_rate = global_params['learning_rate'])
    grads_and_vars = optimizer.compute_gradients(cost,tf.trainable_variables())
    r = optimizer.apply_gradients(grads_and_vars)

    init = tf.global_variables_initializer()
     
    with tf.Session() as sess:
        sess.run(init)
        feedDict = {X:Xtrain, Y: Ytrain}
        if record == True:
            persist_initial(run_id, params_CNN, params_FCN, sess) 
        for i in range(global_params['iters']):
                
            gv = sess.run([grads_and_vars, cost, r], feed_dict=feedDict)
            if record == True:
                persist_after(run_id, params_CNN, params_FCN, i, gv) 
            lastCost = gv[1]
            costs.append(lastCost)
    if record == True:
        persist_costs(run_id, costs)
    return costs 

def tf_train_and_predict(Xtrain, Ytrain, Xtest, Ytest, params):
    startTs = time.time()
    global_params, params_CNN, params_FCN = params
    run_id = global_params['run_id']
    mini_batch_size = global_params['mini_batch_size']
       
    (m, h, w, c) = Xtrain.shape
    numBatches = int(m / mini_batch_size)
 
    image = tf.placeholder(tf.float32, [h, w, c])
    label = tf.placeholder(tf.float32, [Ytrain.shape[1]])
    X, Y = tf.train.shuffle_batch(
          [image, label],
          batch_size=mini_batch_size,
          num_threads=4,
          capacity=50000,
          min_after_dequeue=10000,
          allow_smaller_final_batch=True)
    
    costs = []
    cost, params_CNN, params_FCN = cnn_fcn_Once(X, Y, params_CNN, params_FCN)
    
    if global_params['optimizer'] == 'adam':
        optimizer = tf.train.AdamOptimizer(learning_rate = global_params['learning_rate'])
    else:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate = global_params['learning_rate'])
        
    grads_and_vars = optimizer.compute_gradients(cost,tf.trainable_variables())
    r = optimizer.apply_gradients(grads_and_vars)

    init = tf.global_variables_initializer()
     
    with tf.Session() as sess:
        sess.run(init)
        for i in range(global_params['iters']):
            mbCost = 0
            for _ in range(numBatches):    
                gv = sess.run([grads_and_vars, cost, r], feed_dict={X:Xtrain, Y: Ytrain})
                mbCost += gv[1] / numBatches
                
            print ("Cost after epoch %i: %f" % (i, mbCost))
            
            costs.append(mbCost)
    
        trainSeconds = int(time.time() - startTs)
        print('Training complete in %i seconds' % trainSeconds)
        plt.plot(np.squeeze(costs))
        plt.ylabel('cost')
        plt.xlabel('iterations')
        plt.title("Run Id:" + run_id)
        plt.show()
        Z = params_FCN['Z']
        
        accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(Z, 1), tf.argmax(Y, 1)), "float"))
        train_accuracy = accuracy.eval({X: Xtrain, Y: Ytrain})
        print("Train Accuracy:", train_accuracy)
        test_accuracy = accuracy.eval({X: Xtest, Y: Ytest})
        print("Test Accuracy:", test_accuracy)

    return costs 
