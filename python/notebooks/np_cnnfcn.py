"""
Core functions to run a pure numpy CNN FCN configuration

"""
import numpy as np
from np_utils import *

def softmax(x):
    x_exp = np.exp(x)
    x_sum = np.sum(x_exp, axis = 1, keepdims = True)
    return x_exp / x_sum

def sigmoid(Z):
    return 1/(1+np.exp(-Z))

def relu(Z):
    return np.maximum(0,Z)


def cross_entropy_cost(AL, Y):
     return -np.sum(Y * np.log(AL))

def backward_softmax(dA, Z):
    return dA

def update_weights_and_biases(params, learning_rate):
    W = params['W']
    B = params['B']
    dW = params['dW']
    dB = params['dB']
    W = W - learning_rate * dW
    B = B - learning_rate * dB
    params['W'] = W
    params['B'] = B
    return params
    
    
def relu_backward(dA, cache):
    Z = cache
    dZ = np.array(dA, copy=True) 
    dZ[Z <= 0] = 0
    
    return dZ

def sigmoid_backward(dA, cache):
    
    Z = cache
    
    s = 1/(1+np.exp(-Z))
    dZ = dA * s * (1-s)
    
    assert (dZ.shape == Z.shape)
    
    return dZ


def flatten(X):
    return X.reshape(X.shape[0], -1)

def unflatten(X, unflattened_shape):
    return X.reshape(unflattened_shape)

def zero_pad(X, pads):
    (pTop, pBottom, pLeft, pRight) = pads
    X_pad = np.pad(X, ((0,0), (pTop, pBottom), (pLeft, pRight), (0,0)), 'constant', constant_values = (0,0))
    return X_pad

def conv_forward(A_prev, params):
    W = params['W']
    # Tensorflow appears to always use zero biases thru  tf.nn.conv2d
    B = np.zeros((1,1,1,W.shape[3]))
    
    (m, n_H_prev, n_W_prev, n_C_prev) = A_prev.shape
    (f, f, n_C_prev, n_C) = W.shape
    
    stride = params["stride"]
    pads = params["pads"]
    (pTop, pBottom, pLeft, pRight) = pads
    
    n_H = int(((n_H_prev - f + pTop + pBottom) / stride) + 1)
    n_W = int(((n_W_prev - f + pLeft + pRight) / stride) + 1)
    
    Z = np.zeros((m, n_H, n_W, n_C))
    
    A_prev_pad = zero_pad(A_prev, pads)
    
    for i in range(m):                                  
        a_prev_pad = A_prev_pad[i,:,:,:]                      
        for h in range(0, n_H,stride):                           
            for w in range(0, n_W,stride):                       
                for c in range(n_C):                   
                    
                    vert_start = h
                    vert_end = h + f
                    horiz_start = w
                    horiz_end = w + f
                    
                    a_slice_prev = a_prev_pad[vert_start:vert_end,horiz_start:horiz_end,:]
                    def conv_forward_slice(W, B):
                        return np.sum(a_slice_prev * W + B)
                   
                    Z[i, h, w, c] = conv_forward_slice(W[:,:,:,c], B[:,:,:,c])
                                  
    return Z

def pool_forward(A_prev, params):

    (m, n_H_prev, n_W_prev, n_C_prev) = A_prev.shape
    
    f = params["fsize"]
    stride = params["stride"]
    
    n_H = int(1 + (n_H_prev - f) / stride)
    n_W = int(1 + (n_W_prev - f) / stride)
    n_C = n_C_prev
    
    A = np.zeros((m, n_H, n_W, n_C))              
    
    for i in range(m):                         
        for h in range(0, n_H):                     
            for w in range(0, n_W):                 
                for c in range (n_C):            
                    
                    vert_start = h * stride
                    vert_end = vert_start + f
                    horiz_start = w * stride
                    horiz_end = horiz_start + f
                    
                    a_prev_slice = A_prev[i, vert_start:vert_end,horiz_start:horiz_end,c]
                    
                    if params['mode'] == 'max':
                        A[i, h, w, c] = np.max(a_prev_slice)
                    else:
                        A[i, h, w, c] = np.mean(a_prev_slice)
    
    return A

def cnn_layer_forward(params):
    A_Prev = params['A_Prev']
    if params['layer'] == 'conv':
        Z = conv_forward(A_Prev, params)
        params['Z'] = Z
        A = relu(Z)
        params['A'] = A
    else:
        A = pool_forward(A_Prev, params)
        params['A'] = A
    
    return params

def cnn_forward(X, global_params, params_CNN):
    A_Prev = X
    for params in params_CNN:
        params['A_Prev'] = A_Prev
        params = cnn_layer_forward(params)
        A_Prev = params['A']
    return params_CNN

def conv_backward(dZ, params):
    A_prev = params['A_Prev']
    W = params['W']
    B = params['B']
    
    (m, n_H_prev, n_W_prev, n_C_prev) = A_prev.shape
    
    (f, f, n_C_prev, n_C) = W.shape
    
    stride = params["stride"]
    pads = params["pads"]
    (pTop, pBottom, pLeft, pRight) = pads
    
    (m, n_H, n_W, n_C) = dZ.shape
    
    dA_prev = np.zeros((m, n_H_prev, n_W_prev, n_C_prev))                           
    dW = np.zeros(W.shape)
    dB = np.zeros(B.shape)

    A_prev_pad = zero_pad(A_prev, pads)
    dA_prev_pad = zero_pad(dA_prev, pads)
    
    for i in range(m):                      
        
        a_prev_pad = A_prev_pad[i,:,:,:]
        da_prev_pad = dA_prev_pad[i,:,:,:]
        
        for h in range(0, n_H,stride):                   
            for w in range(0, n_W,stride):               
                for c in range(n_C):          
                    
                    vert_start = h
                    vert_end = h + f
                    horiz_start = w
                    horiz_end = w + f
                    
                    a_slice = a_prev_pad[vert_start:vert_end,horiz_start:horiz_end,:]

                    da_prev_pad[vert_start:vert_end, horiz_start:horiz_end, :] += W[:,:,:,c] * dZ[i, h, w, c]
                    dW[:,:,:,c] += a_slice * dZ[i, h, w, c]
                    dB[:,:,:,c] += dZ[i, h, w, c]
                    
        dA_prev[i, :, :, :] = da_prev_pad[pTop:-pBottom, pLeft:-pRight, :]
    
    params['dA'] = dA_prev
    params['dW'] = dW
    params['dB'] = dB
    return params



def pool_backward(dA, params):
    A_Prev = params['A_Prev']
    
    stride = params["stride"]
    f = params["fsize"]
    
    (m, n_H_prev, n_W_prev, n_C_prev) = A_Prev.shape
    (m, n_H, n_W, n_C) = dA.shape
    
    dA_prev = np.zeros((m, n_H_prev, n_W_prev, n_C_prev))
    
    for i in range(m):                       
        
        a_prev = A_Prev[i,:,:,:]
        
        for h in range(0, n_H):                   
            for w in range(0, n_W):               
                for c in range(n_C):           
                    
                    vert_start = h * stride
                    vert_end = vert_start + f
                    horiz_start = w * stride
                    horiz_end = horiz_start + f
                    
                    if params['mode'] == 'max':
                        
                        a_prev_slice = A_Prev[i, vert_start:vert_end,horiz_start:horiz_end,c]
                        mask = (a_prev_slice == np.max(a_prev_slice))
                        dA_prev[i, vert_start: vert_end, horiz_start: horiz_end, c] += mask * dA[i, h, w, c]
                        
                    else:
                        
                        da = dA[i, h, w, c]
                        average = da / (f * f)
                        dA_prev[i, vert_start: vert_end, horiz_start: horiz_end, c] += np.full((f, f), average)
                        
    params['dA'] = dA_prev
    return params

def cnn_layer_backward(params): 
    A_Prev = params['A_Prev']
    dA_Prev = params['dA_Prev']
    if params['layer'] == 'conv':
        Z = params['Z']
        dZ = relu_backward(dA_Prev, Z)
        return conv_backward(dZ, params)

    elif params['layer'] == 'pool':
        return pool_backward(dA_Prev, params)

def cnn_backwards(dA_fcn, params_CNN):
    dA_Prev = dA_fcn
    for params in reversed(params_CNN):
        params['dA_Prev'] = dA_Prev
        params = cnn_layer_backward(params)
        dA_Prev = params['dA']
    
    return params_CNN

def fcn_layer_forward(A_prev, W, b, activation):
    Z = np.dot(A_prev,W) + b
    if activation == "sigmoid":
        A = sigmoid(Z)
    elif activation == "relu":
        A = relu(Z)
    elif activation == "softmax":
        A = softmax(Z)
    else:
        A = Z
        
    return A,Z           

def dA_last(AL, Y):
    return AL - Y

def fcn_layer_backward(dA, APrev, A, Z, W, b, activation):
    dZ = backward_softmax(dA,Z)
    m = APrev.shape[0]
    dW = np.dot(APrev.T,dZ) / m
    db = np.sum(dZ, axis = 0, keepdims = False) / m
    dA_prev = np.dot(dZ,W.T)
    return dA_prev, dW, db

def fcn_once(X, Y, params):
    W = params['W']
    B = params['B']
    A,Z = fcn_layer_forward(X, W, B, 'softmax')
    params['Z'] = Z
    params['A'] = A
    cost = cross_entropy_cost(A, Y)
    dA = dA_last(A, Y)
    params['dA'] = dA
    dA_Prev, dW, dB = fcn_layer_backward(dA, X, A, Z, W, B,  'softmax')
    params['dA_Prev'] = dA_Prev
    params['dW'] = dW
    params['dB'] = dB

    return cost, params


def cnn_fcn_once(X, Y, params, i, record = False):
    global_params, params_CNN, params_FCN = params
    learning_rate = global_params['learning_rate']
        
    params_CNN = cnn_forward(X, global_params, params_CNN)

    AL = params_CNN[-1]['A']
    FAL = flatten(AL)
    if record == True:
        fName = global_params['np_run_id'] + 'FAL_' + str(i)
        persist(fName, FAL)


    cost, params_FCN = fcn_once(FAL, Y, params_FCN)
    params_FCN = update_weights_and_biases(params_FCN, learning_rate)

    dA_Prev = params_FCN['dA_Prev']
    dA_fcn = unflatten(dA_Prev, AL.shape)
    params_CNN = cnn_backwards(dA_fcn, params_CNN)
    for index, params in enumerate(params_CNN):
        if params['layer'] == 'conv':
              params_CNN[index] = update_weights_and_biases(params, learning_rate) 

    if record == True:
        persist_updated_values((global_params, params_CNN, params_FCN), i)
    
    return cost, params_CNN, params_FCN



def np_train(X, Y, params, record = False):
    global_params, _, _ = params
    iters = int(global_params['iters'])
    costs = []
    for i in range(iters):
        cost, _, _  = cnn_fcn_once(X, Y, params, i, record)
        costs.append(cost)
    
    return costs