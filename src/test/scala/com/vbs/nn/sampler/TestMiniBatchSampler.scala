/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.sampler

import breeze.linalg.DenseMatrix

class TestMiniBatchSampler(ys: DenseMatrix[Double], xs: DenseMatrix[Double], batchSize: Int, numBatches: Int, samplerType: String) 
  extends MiniBatchSampler(ys, xs, batchSize, numBatches, samplerType) {
  val testBatches = scala.collection.mutable.ListBuffer[MiniBatchSampler]()
  var callCount = 0
  def addMiniBatch(sampler: MiniBatchSampler): Unit = {
    testBatches += (sampler)
    ()
  }
  
  override def miniBatches(): Seq[(DenseMatrix[Double], DenseMatrix[Double])] = {
    require (callCount < testBatches.size)
    val sample = testBatches(callCount)
    callCount += 1
    sample.miniBatches()
  }

}