/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.sampler
import java.io.File
import scala.io.Source
import org.junit.Test
import org.junit.Assert._

import com.vbs.nn.utils.MatrixUtils._

class MiniBatchSamplerTest {

  val path = new File(getClass.getResource("/").getPath).getAbsolutePath + "/optimise_data/"
  val xTrain = Source.fromFile(path + "train_x.txt").toSeq.mkString
  val yTrain = Source.fromFile(path + "train_y.txt").toSeq.mkString
  val ys = toDenseMatrix(yTrain)
  val xs = toDenseMatrix(xTrain)

  @Test def createSingleMiniBatch(): Unit = {
    val sampler = MiniBatchSampler(ys, xs, xs.cols, xs.cols, "random")
    val r = sampler.miniBatches()
    assertEquals(1, r.size)
    val shy = r(0)._1
    assertEquals(ys.rows, shy.rows)
    assertEquals(ys.cols, shy.cols)
    val shx = r(0)._2
    assertEquals(xs.rows, shx.rows)
    assertEquals(xs.cols, shx.cols)
  }

  @Test def createMiniBatches(): Unit = {
    val sampler =  MiniBatchSampler(ys, xs, 64, xs.cols, "random")
    val r = sampler.miniBatches()
    assertEquals(5, r.size)
    for (i <- 0 until 4) {
      val shy = r(i)._1
      assertEquals(ys.rows, shy.rows)
      assertEquals(64, shy.cols)
      val shx = r(i)._2
      assertEquals(xs.rows, shx.rows)
      assertEquals(64, shx.cols)
    }
    val shy = r(4)._1
    assertEquals(ys.rows, shy.rows)
    assertEquals(44, shy.cols)
    val shx = r(4)._2
    assertEquals(xs.rows, shx.rows)
    assertEquals(44, shx.cols)

  }
}