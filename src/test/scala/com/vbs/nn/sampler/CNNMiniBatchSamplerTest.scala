/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.sampler
import org.junit.Test
import org.junit.Assert._

import com.vbs.nn.cnn.CNNTestUtils
import com.vbs.nn.tf.TensorflowRegression

class CNNMiniBatchSamplerTest {

  @Test def sequentialSample(): Unit = {
    val sampleX = TensorflowRegression.loadSampleX()
    val sampleY = TensorflowRegression.loadSampleY()

    var sampler = CNNMiniBatchSampler(sampleY, sampleX, sampleX.size, 1, "sequential", true)
    var miniBatch = sampler.miniBatches()
    assertEquals(1, miniBatch.size)
    assertEquals(sampleY, miniBatch(0)._1)
    assertEquals(sampleX.size, miniBatch(0)._2.size)

    sampleX.zip(miniBatch(0)._2).foreach(mcis => CNNTestUtils.assertMCIEquals(mcis._1, mcis._2))

    sampler = CNNMiniBatchSampler(sampleY, sampleX, 1, sampleX.size, "sequential", true)
    miniBatch = sampler.miniBatches()
    assertEquals(2, miniBatch.size)
    val yb1 = miniBatch(0)._1
    assertEquals(6, yb1.rows)
    assertEquals(1, yb1.cols)
    assertEquals(sampleY(::, 0 until 1), yb1)

    val xb1 = miniBatch(0)._2
    assertEquals(1, xb1.size)
    CNNTestUtils.assertMCIEquals(sampleX(0), xb1(0))

    val yb2 = miniBatch(1)._1
    assertEquals(6, yb2.rows)
    assertEquals(1, yb2.cols)
    assertEquals(sampleY(::, 1 until 2), yb2)

    val xb2 = miniBatch(1)._2
    assertEquals(1, xb2.size)
    CNNTestUtils.assertMCIEquals(sampleX(1), xb2(0))

  }

}