/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn
import org.junit.Test
import org.junit.Assert._

import java.io.File
import scala.io.Source
import com.vbs.nn.params.Parameters
import com.vbs.nn.model.FullyConnectedModel._
import com.vbs.nn.sampler.MiniBatchSampler
import com.vbs.nn.utils.MatrixUtils._
import org.junit.Ignore

/**
 * Tests regularization and dropout used in FCNs
 */
class RegularizationAndDropoutTrainingTests {
  val PRECISION = 0.0000001
  val path = new File(getClass.getResource("/").getPath).getAbsolutePath + "/reg2ddata/"
  val xTrain = Source.fromFile(path + "train_x.txt").toSeq.mkString
  val yTrain = Source.fromFile(path + "train_y.txt").toSeq.mkString
  val xTest = Source.fromFile(path + "test_x.txt").toSeq.mkString
  val yTest = Source.fromFile(path + "test_y.txt").toSeq.mkString
  val ys = toDenseMatrix(yTrain)
  val xs = toDenseMatrix(xTrain)

  @Test def noRegularizationOrDropout(): Unit = {
    val params = getBasicParameters()
      .setWeightInitializer("static_params")
      .setIterations(30000)

    setWeightsAndBiases(params)

    val (trainAccuracy, testAccuracy) = trainAndPredict(params)
    assertEquals(0.947867298578, trainAccuracy, PRECISION)
    assertEquals(0.915, testAccuracy, PRECISION)

  }

  @Test def l2Regularization(): Unit = {
    val params = getBasicParameters()
      .setWeightInitializer("static_params")
      .setIterations(30000)
      .setWeightRegularizerParams("l2", 0.7)

    setWeightsAndBiases(params)

    val (trainAccuracy, testAccuracy) = trainAndPredict(params)
    assertEquals(0.938388625592, trainAccuracy, PRECISION)
    assertEquals(0.93, testAccuracy, PRECISION)

  }

  @Ignore def dropout(): Unit = {
    val params = getBasicParameters()
      .setWeightInitializer("static_params")
      .setIterations(30000)
      .setWeightRegularizerParams("l2", 0.7)
      .setKeepProbabilities(0.86, 0.86)
    setWeightsAndBiases(params)

    val (trainAccuracy, testAccuracy) = trainAndPredict(params)

  }

  def trainAndPredict(params: Parameters): (Double, Double) = {

    val sampler = MiniBatchSampler(ys, xs, xs.cols, xs.cols, "sequential")

    val ts = System.currentTimeMillis()
    val lastCost = FullyConnectedNetwork.train(params, sampler)
    val pys = FullyConnectedNetwork.predict(params, xs)
    val pyst = FullyConnectedNetwork.predict(params, toDenseMatrix(xTest))
    val costs = params.getCosts().get.toArray
    println(s"Runtime: ${System.currentTimeMillis() - ts}ms, costs.size: ${costs.size}, last cost: $lastCost")
    val (trainAccuracy, testAccuracy) = trainingAndTestAccuracy(params, (ys, pys), (toDenseMatrix(yTest), pyst), binaryDecode(0.5)(_))
    println(s"Training accuracy=$trainAccuracy")
    println(s"Test accuracy=$testAccuracy")
    (trainAccuracy, testAccuracy)

  }

  def getBasicParameters(): Parameters = {
    Parameters()
      .setLearningRate(0.3)
      .setLayerDims(2, 20, 3, 1)
      .setWeightInitializer("xavier")
      .setActivations("relu", "relu", "sigmoid")
      .setCostFunction("crossentropy")
      .setOptimizer("simple")

  }

  def setWeightsAndBiases(p: Parameters): Unit = {
    val s = Source.fromFile(path + "params.txt").toSeq.mkString
    val pw = Parameters(s)
    val wb = pw.getLayerWeightsAndBiases().get
    p.setWeightsAndBiases(wb)
    ()

  }

}