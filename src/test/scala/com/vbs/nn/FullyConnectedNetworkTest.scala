/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn
import org.junit.Test
import org.junit.Assert._
import breeze.linalg.DenseMatrix
import com.vbs.nn.params.Parameters

import com.vbs.nn.fcn.ActivationFunctions._
import com.vbs.nn.fcn.BackPropagationFunctions._
import com.vbs.nn.params.LGradients

import FullyConnectedNetwork._


import com.vbs.nn.fcn.ActivationsTest

import com.vbs.nn.params.OptimizedParamsSetSimple
import com.vbs.nn.params.OptimizerParamsSimple

class FullyConnectedNetworkTest {
  import FullyConnectedNetworkTest._
  
  
  val PRECISION = 0.0000001

 /**
   * Test each component individually, then as a single training call
   */
  @Test def reluSigmoidMultiLayerComponents(): Unit = {
    val (p, xs, ys) = getReluSigmoidMultiLayerTestData()
    val weightsAndBiases = p.getLayerWeightsAndBiases().get

    val activationFunctions = p.activationFunctions
    val feedForwardFcn = feedForward(activationFunctions)(_, _, _)

    val outputsAndActivations = feedForwardFcn(xs, weightsAndBiases, None)
    val al = outputsAndActivations.last._2
    val costFunction = p.costFunction
    val cost = costFunction(al, ys)
    val backPropOutputFunction = backwardOutputLayer(_,_)
    val backPropLayerFunctions = p.backPropationFunctions()

    val gradients = new Array[LGradients](weightsAndBiases.size)
    val activationsLast = outputsAndActivations.last._2
    val dALast = backPropOutputFunction(activationsLast, ys)
    gradients(weightsAndBiases.size - 1) = linearBackwardL(dALast, outputsAndActivations(outputsAndActivations.size - 2)._2, weightsAndBiases.last, 0.0)

    val idx = weightsAndBiases.size - 2
    val aPrev = outputsAndActivations(idx - 1)._2
    var dA = gradients(idx + 1).dActivationsPrev
    gradients(idx) = backPropLayerFunctions(idx)(dA, aPrev, outputsAndActivations(idx), weightsAndBiases(idx), 0.0)
    dA = gradients(1).dActivationsPrev
    gradients(0) = backPropLayerFunctions(0)(dA, xs, outputsAndActivations(0), weightsAndBiases(0), 0.0)

    val backPropFunction = backPropagate(backPropLayerFunctions, backPropOutputFunction)(_, _, _, _, _, _)
    val gradients2 = backPropFunction(xs, ys, outputsAndActivations, weightsAndBiases, 0.0, None)

    gradients.zip(gradients2).foreach(gg => assertEquals(gg._1, gg._2))
    
    val optimizerParams = OptimizerParamsSimple(weightsAndBiases)

    val (cost1, optimizedParamsSet) = trainMiniBatch(p,optimizerParams,  xs, ys)
    assertEquals(cost, cost1, PRECISION)
    val opsSimple = optimizedParamsSet.asInstanceOf[OptimizedParamsSetSimple]
    val gradsAndUpdatedWeights = opsSimple.params.map(op => (op.gradients, op.weightsAndBiases))
    gradients.zip(gradsAndUpdatedWeights.map(_._1)).foreach(gg => assertEquals(gg._1, gg._2))
    weightsAndBiases.zip(gradsAndUpdatedWeights.map(_._2)).foreach(ww => assertNotEquals(ww._1, ww._2))

  }

}

object FullyConnectedNetworkTest {
  
  
  def getReluSigmoidMultiLayerTestData(): (Parameters, DenseMatrix[Double], DenseMatrix[Double]) = {
    val (xs, weightsAndBiases) = ActivationsTest.inputsAndWeightsTestData()
    val ys = DenseMatrix((0.0, 1.0, 0.0, 0.0))
    val p = Parameters()
      .setIterations(1)
      .setLearningRate(0.008)
      .setLayerDims(784, 397, 10)
      .setWeightInitializer("static_xavier")
      .setActivations("relu", "relu", "sigmoid")
      .setCostFunction("crossentropy")
      .setAdam(0.75, 0.95, 1e-8)
    p.setWeightsAndBiases(weightsAndBiases)
    
    (p, xs, ys)
  }
}