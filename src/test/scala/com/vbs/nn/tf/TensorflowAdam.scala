/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.tf
import org.junit.Assert._
import org.junit.Test

import com.vbs.nn.cnn.CNNTestUtils.DEFAULT_PRECISION
import com.vbs.nn.utils.MatrixTestUtils
import com.vbs.nn.params.Parameters
import com.vbs.nn.FullyConnectedNetwork
import com.vbs.nn.sampler.MiniBatchSampler
import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.PoolingLayerParameters
import com.vbs.nn.cnn.params.FilterWeightsAndBiasesL
import com.vbs.nn.params.LWeightsAndBiases
import breeze.linalg.DenseVector


/**
 * Regression tests using Tensorflow generated data to verify Adam optimizers
 */
class TensorflowAdam {
  val learningRate = 0.009
  val beta1 = 0.9
  val beta2 = 0.999
  val epsilon = 1e-08
  val callCount = 1

  @Test def fcnAdamTf(): Unit = {

    val sampleY = TensorflowRegression.loadSampleY()
    val flattenedOutput = TensorflowRegression.getH5DataSet("2_1_np_adam_FAL_0.txt")

     val (_, fcnParams) = TensorflowAdam.loadSampleRunParams()

    val sampler = MiniBatchSampler(sampleY, flattenedOutput, flattenedOutput.cols, flattenedOutput.cols, "sequential")
    val (cost, optimizedparamsLast) = FullyConnectedNetwork.trainImpl(fcnParams, sampler)
    assertEquals(8.40054, cost, DEFAULT_PRECISION)
    
    val uBiases = fcnParams.getBiases().get(0)
    val uWeights = fcnParams.getWeights().get(0)
    val expectedFCNWeights = TensorflowRegression.getH5DataSet("2_1_tf_adam_W5_0.txt")
    val expectedFCNBiases = TensorflowRegression.getH5Vector("2_1_tf_adam_B5_0.txt")
    assertEquals(0.9921875, MatrixTestUtils.contentSimilarity(expectedFCNWeights, uWeights, DEFAULT_PRECISION), DEFAULT_PRECISION)

    MatrixTestUtils.assertVectorsEqual(expectedFCNBiases, uBiases, DEFAULT_PRECISION)
  }
}

object TensorflowAdam {
  
  def loadTestCNNWeights(): Seq[FilterWeightsAndBiasesL] = {
    val w1 = TensorflowRegression.getH5MultiChannel("2_1_tf_adam_W1.txt")
    val b1 = DenseVector.zeros[Double](w1.size)
    val w2 = TensorflowRegression.getH5MultiChannel("2_1_tf_adam_W3.txt")
    val b2 = DenseVector.zeros[Double](w2.size)
    Seq(FilterWeightsAndBiasesL(w1, b1), FilterWeightsAndBiasesL(w2, b2))
  }

  def loadTestFCNWeights(): LWeightsAndBiases = {
    val w = TensorflowRegression.getH5DataSet("2_1_tf_adam_W5.txt")
    val b = DenseVector.zeros[Double](w.rows)
    LWeightsAndBiases(w, b)

  }
  def loadSampleRunParams(): (CNNParameters, Parameters) = {
    val globalParams = Parameters()
      .setIterations(1)
      .setLearningRate(0.009)
      .setAdam(0.9, 0.999, 1e-08)


    val filterWeightsAndBiases = loadTestCNNWeights()
    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8),
        ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4))
      .setFilterWeightsAndBiases(filterWeightsAndBiases)

    val weightsAndBiases = loadTestFCNWeights()
    val fcnParams = Parameters(globalParams)
      .setLayerDims(64, 6)
      .setActivations("softmax")
      .setCostFunction("crossentropytf")
      .setWeightsAndBiases(Array((weightsAndBiases)))
    (cnnParams, fcnParams)
  }   
}