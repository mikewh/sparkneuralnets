/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.tf
import scala.io.Source

import org.junit.Assert._
import org.junit.Test

import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

import com.vbs.nn.FullyConnectedNetwork
import com.vbs.nn.cnn.CNNTestUtils
import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.PoolingLayerParameters
import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.Parameters
import com.vbs.nn.sampler.MiniBatchSampler
import com.vbs.nn.utils.FCNDataCaptureImplementation
import com.vbs.nn.utils.MatrixTestUtils
import com.vbs.nn.utils.MatrixUtils

import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.cnn.utils.FeedForwardLayerIOCapturer
import com.vbs.nn.cnn.utils.BackPropLinearLayerIOCapturer
import com.vbs.nn.CNNFCNLinkedNetwork
import com.vbs.nn.cnn.Convolution
import com.vbs.nn.cnn.utils.BackPropNonLinearLayerIOCapturer
import com.vbs.nn.cnn.utils.PoolingIOCapturer
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.io.NumpyArrayLoader
import com.vbs.nn.cnn.params.FilterWeightsAndBiasesL
import com.vbs.nn.model.MultiChannelDataSetDM

class TensorflowRegression {

  @Test def fcnOnlyMultiIterations(): Unit = {
    val xs = TensorflowRegression.getH5DataSet("1_1_np_simple_FAL_0.txt")
    val ys = TensorflowRegression.getH5DataSet("1_1_tf_simple_Y.txt")

    val globalParams = Parameters()
      .setIterations(5)
      .setLearningRate(0.009)
      .setOptimizer("simple")

    val w = TensorflowRegression.getH5DataSet("1_1_tf_simple_W5.txt")
    val b = DenseVector.zeros[Double](w.rows)
    val weightsAndBiases = LWeightsAndBiases(w, b)
    val fcnParams = Parameters(globalParams)
      .setLayerDims(64, 6)
      .setActivations("softmax")
      .setCostFunction("crossentropyTF")
      .setWeightsAndBiases(Array((weightsAndBiases)))
    val fcnImpls = new FCNDataCaptureImplementation(fcnParams)
    fcnParams.setFCNImplementations(fcnImpls)

    val sampler = MiniBatchSampler(ys, xs, xs.cols, xs.cols, "sequential")
    val (cost, optimizedparamsLast) = FullyConnectedNetwork.trainImpl(fcnParams, sampler)
    assertEquals(-19.410877, cost, CNNTestUtils.DEFAULT_PRECISION)

  }

  /**
   * Verify all weights, gradients and layer outputs after one iteration for a single item
   */
  @Test def oneIteration(): Unit = {
    val xs = TensorflowRegression.getH5MultiChannel("1_1_tf_simple_X.txt")
    val ys = TensorflowRegression.getH5DataSet("1_1_tf_simple_Y.txt")

    val globalParams = Parameters()
      .setIterations(1)
      .setLearningRate(0.009)
      .setOptimizer("simple")
      .setSamplerSpec("sequential", 1, 1)

    val clp1 = ConvolutionLayerParameters("same", 1, 3, 4, 8)
    val clp1Debugger = new BackPropLinearLayerIOCapturer(1, clp1)
    val plp2 = PoolingLayerParameters(8, 8)
    val plp2Debugger = new PoolingIOCapturer(2, plp2)

    val clp3 = ConvolutionLayerParameters("same", 1, 8, 2, 16)
    val clp3FeedForwardDebugger = new FeedForwardLayerIOCapturer(3, clp3)
    val clp3BackPropDebugger = new BackPropLinearLayerIOCapturer(3, clp3)
    val clp3BackPropNonLinearDebugger = new BackPropNonLinearLayerIOCapturer(3, clp3)

    val plp4 = PoolingLayerParameters(4, 4)
    val plp4Debugger = new PoolingIOCapturer(4, plp4)

    val filterWeightsAndBiases = TensorflowRegression.loadTestCNNWeights()
    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(clp1,
        plp2,
        clp3,
        plp4)
      .setFilterWeightsAndBiases(filterWeightsAndBiases)
      .createCNNSampler(ys, xs)

    val weightsAndBiases = TensorflowRegression.loadTestFCNWeights()
    val fcnParams = Parameters(globalParams)
      .setLayerDims(64, 6)
      .setActivations("softmax")
      .setCostFunction("crossentropyTF")
      .setWeightsAndBiases(Array((weightsAndBiases)))
    val fcnImpls = new FCNDataCaptureImplementation(fcnParams)
    fcnParams.setFCNImplementations(fcnImpls)

    val cost = CNNFCNLinkedNetwork.train(cnnParams, fcnParams)

    println(s"Cost $cost after ${globalParams.getIterations()} iterations")
    assertEquals(-3.78975, cost, CNNTestUtils.DEFAULT_PRECISION)

    val z3 = fcnImpls.feedForwardOutputsAndActivations(0).last._1
    val eZ3 = TensorflowRegression.getH5DataSet("1_1_np_simple_Z5_0.txt")
    MatrixTestUtils.assertMatricesEqual(eZ3, z3, CNNTestUtils.DEFAULT_PRECISION)

    val uw3 = fcnParams.getWeights().get(0)
    val euw3 = TensorflowRegression.getH5DataSet("1_1_np_simple_W5_0.txt")
    MatrixTestUtils.assertMatricesEqual(euw3, uw3, CNNTestUtils.DEFAULT_PRECISION)

    val ub3 = fcnParams.getBiases().get(0)
    val eub3 = TensorflowRegression.getH5Vector("1_1_np_simple_B5_0.txt")
    MatrixTestUtils.assertVectorsEqual(eub3, ub3, CNNTestUtils.DEFAULT_PRECISION)

    val dFp2 = fcnImpls.backPropGradients(0)(0).dActivationsPrev
    val edFp2 = TensorflowRegression.getH5DataSet("1_1_np_simple_dA_Prev5_0.txt")
    MatrixTestUtils.assertMatricesEqual(edFp2, dFp2, CNNTestUtils.DEFAULT_PRECISION)

    val dP20 = plp4Debugger.dAs(0)
    val edP2 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_dA_Prev4_0.txt")
    val edP20 = TensorflowRegression.mapToChannels(0, edP2)
    CNNTestUtils.assertMCIEquals(edP20, dP20)

    val dAC20 = plp4Debugger.outputs(0)
    val edAC2 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_dA4_0.txt")
    val edAC20 = TensorflowRegression.mapToChannels(0, edAC2)
    CNNTestUtils.assertMCIEquals(edAC20, dAC20)

    val a20 = plp4Debugger.prevLayerOutputs(0)
    CNNTestUtils.assertMCIEquals(clp3FeedForwardDebugger.activationsA(0), a20)

    val ez2 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_Z3_0.txt")
    val ez20 = TensorflowRegression.mapToChannels(0, ez2)
    CNNTestUtils.assertMCIEquals(ez20, clp3FeedForwardDebugger.outputsZ(0))

    val ea2 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_A3_0.txt")
    val ea20 = TensorflowRegression.mapToChannels(0, ea2)
    CNNTestUtils.assertMCIEquals(a20, ea20)

    CNNTestUtils.assertMCIEquals(clp3BackPropNonLinearDebugger.dAs(0), dAC20)
    CNNTestUtils.assertMCIEquals(clp3BackPropNonLinearDebugger.prevLayerOutputs(0), ez20)

    val dz20 = clp3BackPropDebugger.dZs(0)
    CNNTestUtils.assertMCIEquals(clp3BackPropNonLinearDebugger.dZs(0), dz20)

    val reluZ20 = Convolution.backPropReluLayer(dAC20, a20)
    CNNTestUtils.assertMCIEquals(reluZ20, dz20, CNNTestUtils.DEFAULT_PRECISION)

    val db2 = clp3BackPropDebugger.dBiases(0)
    val eb2 = TensorflowRegression.getVectorFromH5MultiChannel("1_1_np_simple_dB3_0.txt")
    MatrixTestUtils.assertVectorsEqual(eb2, db2, CNNTestUtils.DEFAULT_PRECISION)
    
    val dW2 = clp3BackPropDebugger.dWeights(0)
    val edW2 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_dW3_0.txt")
    CNNTestUtils.assertMCIEquals(edW2(0), dW2(0), CNNTestUtils.DEFAULT_PRECISION)

    val dAC10 = plp2Debugger.outputs(0)
    val edAC1 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_dA2_0.txt")
    val edAC10 = TensorflowRegression.mapToChannels(0, edAC1)
    CNNTestUtils.assertMCIEquals(edAC10, dAC10)

    val db1 = clp1Debugger.dBiases(0)
    val eb1 = TensorflowRegression.getVectorFromH5MultiChannel("1_1_np_simple_dB1_0.txt")
    MatrixTestUtils.assertVectorsEqual(eb1, db1, CNNTestUtils.DEFAULT_PRECISION)
    val dW1 = clp1Debugger.dWeights(0)
    val edW1 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_dW1_0.txt")
    CNNTestUtils.assertMCIEquals(edW1(0), dW1(0), CNNTestUtils.DEFAULT_PRECISION)

    val uW2 = cnnParams.getFilterWeightsAndBiases().get(1).weights
    val euW2 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_W3_0.txt")

    for (i <- 0 until uW2.size) CNNTestUtils.assertMCIEquals(euW2(i), uW2(i), CNNTestUtils.DEFAULT_PRECISION)

    val uW1 = cnnParams.getFilterWeightsAndBiases().get(0).weights
    val euW1 = TensorflowRegression.getH5MultiChannel("1_1_np_simple_W1_0.txt")

    for (i <- 0 until uW1.size) CNNTestUtils.assertMCIEquals(euW1(i), uW1(i), CNNTestUtils.DEFAULT_PRECISION)

  }

  @Test def multiIteration(): Unit = {
    val xs = TensorflowRegression.getH5MultiChannel("1_1_tf_simple_X.txt")
    val ys = TensorflowRegression.getH5DataSet("1_1_tf_simple_Y.txt")

    val globalParams = Parameters()
      .setIterations(5)
      .setLearningRate(0.009)
      .setOptimizer("simple")
      .setSamplerSpec("sequential", 1, 1)


    val filterWeightsAndBiases = TensorflowRegression.loadTestCNNWeights()
    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8),
        ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4))
      .setFilterWeightsAndBiases(filterWeightsAndBiases)
      .createCNNSampler(ys, xs)

    val weightsAndBiases = TensorflowRegression.loadTestFCNWeights()
    val fcnParams = Parameters(globalParams)
      .setLayerDims(64, 6)
      .setActivations("softmax")
      .setCostFunction("crossentropyTF")
      .setWeightsAndBiases(Array((weightsAndBiases)))

    val cost = CNNFCNLinkedNetwork.train(cnnParams, fcnParams)

    println(s"Cost $cost after ${globalParams.getIterations()} iterations")
    assertEquals(-76.1968, cost, 0.0001)

  }
}

/**
 * Used by any tests that require Tensroflow generated test data
 */
object TensorflowRegression {
  val PATH = "C:/Temp/MLData/sparkneuralnets/2018_03_14/python/testData/"
 
  def loadTestCNNWeights(): Seq[FilterWeightsAndBiasesL] = {
    val w1 = getH5MultiChannel("1_1_tf_simple_W1.txt")
    val b1 = DenseVector.zeros[Double](w1.size)
    val w2 = getH5MultiChannel("1_1_tf_simple_W3.txt")
    val b2 = DenseVector.zeros[Double](w2.size)
    Seq(FilterWeightsAndBiasesL(w1, b1), FilterWeightsAndBiasesL(w2, b2))
  }

  def loadTestFCNWeights(): LWeightsAndBiases = {
    val w = TensorflowRegression.getH5DataSet("1_1_tf_simple_W5.txt")
    val b = DenseVector.zeros[Double](w.rows)
    LWeightsAndBiases(w, b)

  }
  
  def loadSampleY(): DenseMatrix[Double] = TensorflowRegression.getH5DataSet("2_1_tf_adam_Y.txt")
  def loadSampleX(): DenseMatrix4d = TensorflowRegression.getH5MultiChannel("2_1_tf_adam_X.txt")

  def getH5DataSet(name: String): DenseMatrix[Double] = MatrixUtils.toDenseMatrix(Source.fromFile(s"${PATH}$name").toSeq.mkString).t
  def getH5Vector(name: String): DenseVector[Double] = MatrixUtils.toDenseVector(Source.fromFile(s"${PATH}$name").toSeq.mkString)
  
  def getVectorFromH5MultiChannel(name: String): DenseVector[Double] = 
    DenseVector(MultiChannelDataSetDM(getH5MultiChannel(name)).toArray)

  def getH5MultiChannel(name: String): DenseMatrix4d = NumpyArrayLoader.loadMultiChannelDataSet(s"${PATH}$name")

  def getH5MatrixSeqs(name: String): DenseMatrix4d = MatrixUtils.toDenseMatrix4d(Source.fromFile(s"${PATH}$name").toSeq.mkString)

  def mapToChannels(row: Int, dmss: DenseMatrix4d): DenseMatrix3d = {
    for (channel <- 0 until dmss.size) yield {
      val dms = dmss(channel).
        map(dm => dm(row, ::).t).
        map(_.toArray).flatten.toArray
      new DenseMatrix(dmss(channel).size, dmss(channel)(0).cols, dms)
    }
  }

}