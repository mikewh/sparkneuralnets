/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.utils
import org.junit.Test
import org.junit.Assert._
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

import com.vbs.nn.params.OptimizedParamsSetSimple

import com.vbs.nn.params.LGradients
import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.FullyConnectedNetwork._
import com.vbs.nn.FullyConnectedNetworkTest._

import GradientChecker._
import MatrixTestUtils._
import com.vbs.nn.params.OptimizerParamsSimple

class GradientCheckerTest {
  def rounded(x: Int)(n: Double): Double = {
    BigDecimal(n).setScale(x, BigDecimal.RoundingMode.HALF_UP).toDouble
  }


  val PRECISION = 0.0000001

  val r2 = rounded(2)(_)

  @Test def componentByComponent(): Unit = {
    val xs = DenseMatrix((1.62434536, -0.61175641, -0.52817175),
      (-1.07296862, 0.86540763, -2.3015387),
      (1.74481176, -0.7612069, 0.3190391),
      (-0.24937038, 1.46210794, -2.06014071))

    val ys = DenseMatrix((1.0, 1.0, 0.0))

    val w1 = DenseMatrix((-0.3224172, -0.38405435, 1.13376944, -1.09989127),
      (-0.17242821, -0.87785842, 0.04221375, 0.58281521),
      (-1.10061918, 1.14472371, 0.90159072, 0.50249434),
      (0.90085595, -0.68372786, -0.12289023, -0.93576943),
      (-0.26788808, 0.53035547, -0.69166075, -0.39675353))
    val b1 = DenseVector(-0.6871727, -0.84520564, -0.67124613, -0.0126646, -1.11731035)

    val w2 = DenseMatrix((0.2344157, 1.65980218, 0.74204416, -0.19183555, -0.88762896),
      (-0.74715829, 1.6924546, 0.05080775, -0.63699565, 0.19091548),
      (2.10025514, 0.12015895, 0.61720311, 0.30017032, -0.35224985))
    val b2 = DenseVector(-1.1425182, -0.34934272, -0.20889423)

    val w3 = DenseMatrix((0.58662319, 0.83898341, 0.93110208))
    val b3 = DenseVector(0.28558733)

    val (p, _, _) = getReluSigmoidMultiLayerTestData()
    val weightsAndBiases = Array(LWeightsAndBiases(w1, b1), LWeightsAndBiases(w2, b2), LWeightsAndBiases(w3, b3))
    p.setWeightsAndBiases(weightsAndBiases)
    val optimizerParams = OptimizerParamsSimple(weightsAndBiases)

    val (cost, optimizedParamsSet) = trainMiniBatch(p,optimizerParams,  xs, ys)
    assertEquals(2.40783338983, cost, PRECISION)
    val opsSimple = optimizedParamsSet.asInstanceOf[OptimizedParamsSetSimple]
    val gradients = opsSimple.params.map(_.gradients)
    val dw3 = DenseMatrix((0.0, 0.0, 2.24404238))
    val db3 = DenseVector(0.21225753)
    val grads3 = gradients(2)
    assertMatricesEqual(dw3, grads3.dWeights, PRECISION)
    assertVectorsEqual(db3, grads3.dBiases, PRECISION)

    val dw2 = DenseMatrix((0.0, 0.0, 0.0, 0.0, 0.0),
      (0.0, 0.0, 0.0, 0.0, 0.0),
      (0.91580165, 0.02451548, -0.10797954, 0.90281891, 0.0))
    val db2 = DenseVector(0.0, 0.0, 0.19763343)
    val grads2 = gradients(1)
    assertMatricesEqual(dw2, grads2.dWeights, PRECISION)
    assertVectorsEqual(db2, grads2.dBiases, PRECISION)

    val dw1 = DenseMatrix((-0.37347779, -1.47903216,  0.17596143, -1.33685036),
       (-0.01967514, -0.08573553,  0.01188465, -0.07674312),
       ( 0.03916037, -0.05539735,  0.04872715, -0.09359393),
       (-0.05337778, -0.21138458,  0.02514856, -0.19106384),
       ( 0.0       ,  0.0       ,  0.0       ,  0.0       ))
    val db1 = DenseVector( 0.63290787, 0.0372514 ,-0.06401301,0.09045575,0.0       )
    val grads1 = gradients(0)
    assertMatricesEqual(dw1, grads1.dWeights, PRECISION)
    assertVectorsEqual(db1, grads1.dBiases, PRECISION)

    val diff = gradientDifference(p, optimizerParams, gradients, xs, ys, 1e-7)
    assertEquals(1.18855520355e-07, diff, PRECISION)

  }

  @Test def applyEpsilonByIndex(): Unit = {
    val weightsAndBiases = generateWeights(Array(5, 4, 3))
    var r = applyEpsilonToElement(weightsAndBiases, 23, 64.0)
    assertEquals(weightsAndBiases(0).weights, r(0).weights)
    assertNotEquals(weightsAndBiases(0).biases, r(0).biases)
    assertEquals(66.4, r(0).biases.toArray.last, 0.01)
    assertEquals(weightsAndBiases(1).weights, r(1).weights)
    assertEquals(weightsAndBiases(1).biases, r(1).biases)

    r = applyEpsilonToElement(weightsAndBiases, 24, 64.0)
    assertEquals(weightsAndBiases(0).weights, r(0).weights)
    assertEquals(weightsAndBiases(0).biases, r(0).biases)
    assertNotEquals(weightsAndBiases(1).weights, r(1).weights)
    assertEquals(66.5, r(1).weights(0, 0), 0.01)
    assertEquals(weightsAndBiases(1).biases, r(1).biases)
  }

  @Test def differenceReluSigmoidMultiLayer(): Unit = {
    val (p, xs, ys) = getReluSigmoidMultiLayerTestData()

    val optimizerParams = OptimizerParamsSimple(p.getLayerWeightsAndBiases().get)

    val (cost, optimizedParamsSet) = trainMiniBatch(p,optimizerParams,  xs, ys)
    val opsSimple = optimizedParamsSet.asInstanceOf[OptimizedParamsSetSimple]
    val gradients = opsSimple.params.map(_.gradients)
 
    val diff = gradientDifference(p, optimizerParams, gradients, xs, ys, 1e-7)
    assertTrue(s"$diff",gradientCheck(p, optimizerParams, gradients, xs, ys))

  }

  def generateGradients(layerDims: Array[Int], initialValue: Double = 0.0): Array[LGradients] = {
    generateWeights(layerDims, initialValue).map(lwb => LGradients(null, lwb.weights, lwb.biases))
  }

  def generateWeights(layerDims: Array[Int], initialValue: Double = 0.0): Array[LWeightsAndBiases] = {
    var elemVal = initialValue
    def initWeights(w: DenseMatrix[Double]): Unit = {
      for (r <- 0 until w.rows) {
        for (c <- 0 until w.cols) w(r, c) = {
          elemVal = r2(elemVal + 0.1)
          elemVal
        }
      }
    }
    def initBiases(b: DenseVector[Double]): Unit = {
      for (i <- 0 until b.size) b(i) = {
        elemVal = r2(elemVal + 0.1)
        elemVal
      }
    }
    val r = for (idx <- 1 until layerDims.size) yield {
      val w = DenseMatrix.zeros[Double](layerDims(idx), layerDims(idx - 1))
      initWeights(w)
      val b = DenseVector.zeros[Double](layerDims(idx))
      initBiases(b)
      LWeightsAndBiases(w, b)
    }
    r.toArray

  }
}