/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.utils
import org.junit.Assert._

import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

object MatrixTestUtils {

  def assertMatricesEqual(dm1: DenseMatrix[Double], dm2: DenseMatrix[Double], precision: Double): Unit = {
    assertEquals("rows not equal", dm1.rows, dm2.rows)
    assertEquals("cols not equal", dm1.cols, dm2.cols)
    for (r <- 0 until dm1.rows) {
      for (c <- 0 until dm1.cols) assertEquals(s"Mismatch at ($r,$c)", dm1(r, c), dm2(r, c), precision)
    }
  }

  def assertVectorsEqual(dv1: DenseVector[Double], dv2: DenseVector[Double], precision: Double, moreInfo: String = ""): Unit = {
    assertEquals("size not equal", dv1.size, dv2.size)
    for (i <- 0 until dv1.size) assertEquals(s"$moreInfo Mismatch at $i", dv1(i), dv2(i), precision)
  }
  
  def assertVectorsEqual(dvs1: Seq[DenseVector[Double]], dvs2: Seq[DenseVector[Double]], precision: Double): Unit = {
    assertEquals("size not equal", dvs1.size, dvs2.size)
    for (i <- 0 until dvs1.size) assertVectorsEqual(dvs1(i), dvs2(i), precision, i.toString)
    
  }

  def sameDims(dm1: DenseMatrix[Double], dm2: DenseMatrix[Double]): Boolean =
    dm1.rows == dm2.rows && dm1.cols == dm2.cols

  def assertMatrixArrayEquals(dma1: Seq[DenseMatrix[Double]], dma2: Seq[DenseMatrix[Double]], precision: Double): Unit = {
    assertEquals("arrays not of equal size", dma1.size, dma2.size)

    def matricesEqual(i: Int): Unit = {
      assertEquals("rows not equal", dma1(i).rows, dma2(i).rows)
      assertEquals("cols not equal", dma1(i).cols, dma2(i).cols)
      for (r <- 0 until dma1(i).rows) {
        for (c <- 0 until dma1(i).cols) assertEquals(s"Mismatch at ($i, $r,$c)", dma1(i)(r, c), dma2(i)(r, c), precision)
      }
    }

    for (i <- 0 until dma1.size) matricesEqual(i)
  }

  def assertMatrixArraysEquals(dmaa1: Array[Array[DenseMatrix[Double]]], dmaa2: Seq[Seq[DenseMatrix[Double]]], precision: Double): Unit = {
    assertEquals("arrays not of equal size", dmaa1.size, dmaa2.size)
    for (i <- 0 until dmaa1.size) assertMatrixArrayEquals(dmaa1(i), dmaa2(i), precision)

  }

  def contentSimilarity(dm1: DenseMatrix[Double], dm2: DenseMatrix[Double], precision: Double): Double = {
    dm1.size == dm2.size match {
      case false => 0.0
      case true  => contentSimilarity(dm1.toArray, dm2.toArray, precision)
    }
  }

  def contentSimilarity(ar1: Seq[Double], ar2: Seq[Double], precision: Double): Double = {
    val r = (for (i <- 0 until ar1.length) yield {
      (ar2.find(x2 => Math.abs(ar1(i) - x2) <= precision))
    }).filter(_.isDefined)
    r.size.toDouble / ar1.length
  }

  def contentSimilaritiesFlattened(dmaa1: Seq[Seq[DenseMatrix[Double]]], dmaa2: Seq[Seq[DenseMatrix[Double]]], precision: Double): Double = {
    val ar1 = dmaa1.map(dma => dma.map(_.toArray)).flatten.flatten
    val ar2 = dmaa2.map(dma => dma.map(_.toArray)).flatten.flatten
    contentSimilarity(ar1, ar2, precision)
  }

  def contentSimilarities(dma1: Seq[DenseMatrix[Double]], dma2: Seq[DenseMatrix[Double]], precision: Double): Seq[Double] =
    dma1.zip(dma2).map(dmas => contentSimilarity(dmas._1, dmas._2, precision))

  def printIndicesWhere[T](dm: DenseMatrix[T], fcn: (T) => Boolean): Unit = {
    val r = indicesWhere(dm, fcn)
    if (!r.isEmpty) println(r.mkString(" "))
  }

  def indicesWhere[T](dm: DenseMatrix[T], fcn: (T) => Boolean): Seq[(Int, Int)] = {
    for (r <- 0 until dm.rows; c <- 0 until dm.cols; if (fcn(dm(r, c)))) yield ((r, c))
  }

}