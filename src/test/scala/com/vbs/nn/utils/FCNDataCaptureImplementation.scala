/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.utils
import org.junit.Assert._
import com.vbs.nn.cnn.CNNTestUtils.DEFAULT_PRECISION

import com.vbs.nn.DefaultFCNImplementations
import com.vbs.nn.params.Parameters
import scala.collection.mutable.ArrayBuffer
import com.vbs.nn.fcn.ActivationFunctions.OutputsAndActivationsL
import com.vbs.nn.params.LGradients
import com.vbs.nn.params.LWeightsAndBiases
import breeze.linalg.DenseMatrix
import com.vbs.nn.fcn.BackPropagationFunctions
import com.vbs.nn.fcn.ActivationFunctions
import com.vbs.nn.fcn.ActivationFunctions.ActivationFunction
import com.vbs.nn.params.OptimizedParamsSet
import breeze.linalg.DenseVector

/**
 * Captures data within an FCN
 */
class FCNDataCaptureImplementation(params: Parameters) extends DefaultFCNImplementations(params) {

  val feedForwardWeightsAndBiases = new ArrayBuffer[Array[LWeightsAndBiases]]()
  val feedForwardOutputsAndActivations = new ArrayBuffer[Array[OutputsAndActivationsL]]()

  val backPropGradients = new ArrayBuffer[Array[LGradients]]()
  
  def executeFeedForward(activationFunctions: Array[ActivationFunction]) (xs: DenseMatrix[Double],
                                                                  weightsAndBiases: Array[LWeightsAndBiases],
                                                                  dropouts: Option[(Array[Double], Array[DenseMatrix[Double]])]): Array[OutputsAndActivationsL] = {
    feedForwardWeightsAndBiases += weightsAndBiases
    val oAndA = ActivationFunctions.feedForward(params.activationFunctions())(xs, weightsAndBiases, dropouts)
    feedForwardOutputsAndActivations += oAndA
    oAndA
  }
                                                                    

  def executeBackPropagate(layerFunctions: Array[BackPropagationFunctions.BackPropagationLayerFunction],
                           outputFunction: BackPropagationFunctions.BackPropagationOutputFunction)(inputs: DenseMatrix[Double],
                                                                                                   targets: DenseMatrix[Double],
                                                                                                   outputsAndActivations: Array[OutputsAndActivationsL],
                                                                                                   weightsAndBiases: Array[LWeightsAndBiases],
                                                                                                   lambda: Double,
                                                                                                   dropouts: Option[(Array[Double], Array[DenseMatrix[Double]])]): Array[LGradients] = {
    val gradients =
      BackPropagationFunctions.backPropagate(params.backPropationFunctions(), BackPropagationFunctions.backwardOutputLayer)(inputs, targets, outputsAndActivations, weightsAndBiases, lambda, dropouts)
    backPropGradients += gradients
    gradients
  }
  
  override def feedForward = {
    executeFeedForward(params.activationFunctions()) (_,_,_)
  }
  
  override def backPropagate = {
    executeBackPropagate(params.backPropationFunctions(), BackPropagationFunctions.backwardOutputLayer)(_, _, _, _, _, _)
  }
  
  def verifyLastActivation(aLast: DenseMatrix[Double], index: Int): Unit = {
    MatrixTestUtils.assertMatricesEqual(feedForwardOutputsAndActivations(index).last._2, aLast, DEFAULT_PRECISION)
  }
  
  def verifyLastGradients(eWeightsGrads: Seq[DenseMatrix[Double]], eBiasGrads: Seq[DenseVector[Double]], 
      optimizedParams: OptimizedParamsSet): Unit = {
    val aGrads = optimizedParams.gradients
    MatrixTestUtils.assertVectorsEqual(eBiasGrads, aGrads.map(_.dBiases), DEFAULT_PRECISION)
  }
  
  def verifyBiases(eBiases: Seq[DenseVector[Double]]): Unit = {
    val aBiases = params.getBiases() match {
      case Some(biases) => biases
      case None => {
        fail("Missing params.getBiases()")
        Array(DenseVector.zeros[Double](1))
      }
    }
    MatrixTestUtils.assertVectorsEqual(eBiases, aBiases, DEFAULT_PRECISION)
  } 
  
  def verifyLastWeights(eWeights: Seq[DenseMatrix[Double]]): Unit = {
    val aWeights = params.getWeights() match {
      case Some(wts) => wts
      case None => {
        fail("Missing params.getWeights()")
        Array(DenseMatrix.zeros[Double](1,1))
      }
    }
    MatrixTestUtils.assertMatrixArrayEquals(eWeights, aWeights, DEFAULT_PRECISION)
  }
  
  def weightValueSimilarities(eWeights: Seq[DenseMatrix[Double]]): Seq[Double] = params.getWeights() match {
    case None => Seq()
    case Some(wts) => MatrixTestUtils.contentSimilarities(eWeights, wts, DEFAULT_PRECISION)
  }

}