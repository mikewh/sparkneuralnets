/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn
import org.junit.Test
import org.junit.Assert._

import java.io.File
import scala.io.Source
import com.vbs.nn.params.Parameters
import com.vbs.nn.model.FullyConnectedModel._
import com.vbs.nn.sampler.MiniBatchSampler
import com.vbs.nn.utils.MatrixUtils._
import breeze.linalg.DenseMatrix
import com.vbs.nn.sampler.TestMiniBatchSampler
import com.vbs.nn.model.FullyConnectedModel

/**
 * Tests momentum and adam optimizers used in FCNs
 */
class MomentumAndAdamTrainingTests {

  val PRECISION = 0.0000001

  val path = new File(getClass.getResource("/").getPath).getAbsolutePath + "/optimise_data/"
  val xTrain = Source.fromFile(path + "epoch_1_x.txt").toSeq.mkString
  val yTrain = Source.fromFile(path + "epoch_1_y.txt").toSeq.mkString
  val ys = DenseMatrix.horzcat(toDenseMatrixArray(yTrain): _*)
  val xs = DenseMatrix.horzcat(toDenseMatrixArray(xTrain): _*)

  @Test def pureGradientDescentSmall(): Unit = {
    val params = getBasicParameters()
      .setOptimizer("simple")
    setWeightsAndBiases(params)
    val sampler = MiniBatchSampler(ys, xs, 64, xs.cols, "sequential")

    val lastCost = FullyConnectedNetwork.train(params, sampler)
    val costs = params.getCosts().get.toArray
    costs foreach println

    assertEquals(0.3052948989382396, lastCost, PRECISION)
  }

  @Test def testSampler(): Unit = {
    val xFull = Source.fromFile(path + "train_x.txt").toSeq.mkString
    val yFull = Source.fromFile(path + "train_y.txt").toSeq.mkString
    val ys = toDenseMatrix(yFull)
    val xs = toDenseMatrix(xFull)

    var sampler = MiniBatchSampler(ys, xs, 64, xs.cols, "sequential")
    var mb1 = sampler.miniBatches()
    assertEquals(5, mb1.size)
    var mb2 = sampler.miniBatches()
    mb1.zip(mb2).foreach(mbs => assertEquals(mbs._1, mbs._2))

    sampler = MiniBatchSampler(ys, xs, 64, xs.cols, "random")
    mb1 = sampler.miniBatches()
    assertEquals(5, mb1.size)
    mb2 = sampler.miniBatches()
    mb1.zip(mb2).foreach(mbs => assertNotEquals(mbs._1, mbs._2))
  }

  @Test def pureGradientDescentMedium(): Unit = {
    val params = getBasicParameters()
      .setOptimizer("simple")
      .setIterations(2)
    setWeightsAndBiases(params)
    val testSampler = new TestMiniBatchSampler(ys, xs, 64, xs.cols, "sequential")
    testSampler.addMiniBatch(MiniBatchSampler(ys, xs, 64, xs.cols, "sequential"))
    val xTrain2 = Source.fromFile(path + "epoch_2_x.txt").toSeq.mkString
    val yTrain2 = Source.fromFile(path + "epoch_2_y.txt").toSeq.mkString
    val ys2 = DenseMatrix.horzcat(toDenseMatrixArray(yTrain2): _*)
    val xs2 = DenseMatrix.horzcat(toDenseMatrixArray(xTrain2): _*)
    testSampler.addMiniBatch(MiniBatchSampler(ys2, xs2, 64, xs.cols, "sequential"))

    val lastCost = FullyConnectedNetwork.train(params, testSampler)
    val costs = params.getCosts().get.toArray
    costs foreach println

    assertEquals(0.38763791549292664, lastCost, PRECISION)
  }

  @Test def momentumSmall(): Unit = {
    val params = getBasicParameters()
      .setMomentum(0.9)
    setWeightsAndBiases(params)
    val sampler = MiniBatchSampler(ys, xs, 64, xs.cols, "sequential")

    val lastCost = FullyConnectedNetwork.train(params, sampler)

    assertEquals(0.30530861509428603, lastCost, PRECISION)
  }

  @Test def pureGradientDescentFull(): Unit = {
    import com.vbs.nn.utils.MatrixUtils._

    val params = getBasicParameters()
      .setOptimizer("simple")
      .setIterations(10000)
    setWeightsAndBiases(params)
    val xFull = Source.fromFile(path + "train_x.txt").toSeq.mkString
    val yFull = Source.fromFile(path + "train_y.txt").toSeq.mkString
    val ys = toDenseMatrix(yFull)
    val xs = toDenseMatrix(xFull)

    val sampler = MiniBatchSampler(ys, xs, 64, xs.cols, "random")

    val ts = System.currentTimeMillis()
    val lastCost = FullyConnectedNetwork.train(params, sampler)
    val pys = FullyConnectedNetwork.predict(params, xs)
    val costs = params.getCosts().get.toArray
    println(s"Runtime: ${System.currentTimeMillis() - ts}ms, costs.size: ${costs.size}, last cost: $lastCost")
    val reportedCosts = for (i <- 0 until costs.size if i % 5000 == 0) yield ((i / 5, costs(i)))
    reportedCosts.foreach(rp => println(s"Cost after iteration ${rp._1}: ${rp._2}"))
    val trainAccuracy = FullyConnectedModel.getAccuracy(ys, pys, binaryDecode(0.5)(_))
    println(s"training accuracy $trainAccuracy")
    assertEquals(0.79666666666666, trainAccuracy, PRECISION)

  }

  @Test def momentumFull(): Unit = {
    import com.vbs.nn.utils.MatrixUtils._

    val params = getBasicParameters()
      .setIterations(10000)
      .setMiniBatchSize(64)
      .setMomentum(0.9)
    setWeightsAndBiases(params)
    val xFull = Source.fromFile(path + "train_x.txt").toSeq.mkString
    val yFull = Source.fromFile(path + "train_y.txt").toSeq.mkString
    val ys = toDenseMatrix(yFull)
    val xs = toDenseMatrix(xFull)

    val sampler = MiniBatchSampler(ys, xs, 64, xs.cols, "random")

    val ts = System.currentTimeMillis()
    val lastCost = FullyConnectedNetwork.train(params, sampler)
    val pys = FullyConnectedNetwork.predict(params, xs)
    val costs = params.getCosts().get.toArray
    println(s"Runtime: ${System.currentTimeMillis() - ts}ms, costs.size: ${costs.size}, last cost: $lastCost")
    val reportedCosts = for (i <- 0 until costs.size if i % 5000 == 0) yield ((i / 5, costs(i)))
    reportedCosts.foreach(rp => println(s"Cost after iteration ${rp._1}: ${rp._2}"))
    val trainAccuracy = FullyConnectedModel.getAccuracy(ys, pys, binaryDecode(0.5)(_))
    println(s"training accuracy $trainAccuracy")
    assertEquals(0.79666666666666, trainAccuracy, PRECISION)

  }

  @Test def adamFull(): Unit = {
    import com.vbs.nn.utils.MatrixUtils._

    val params = getBasicParameters()
      .setIterations(10000)
      .setMiniBatchSize(64)
      .setAdam(0.9, 0.999, 1e-8)
    setWeightsAndBiases(params)
    val xFull = Source.fromFile(path + "train_x.txt").toSeq.mkString
    val yFull = Source.fromFile(path + "train_y.txt").toSeq.mkString
    val ys = toDenseMatrix(yFull)
    val xs = toDenseMatrix(xFull)

    val sampler = MiniBatchSampler(ys, xs, 64, xs.cols, "random")

    val ts = System.currentTimeMillis()
    val lastCost = FullyConnectedNetwork.train(params, sampler)
    val pys = FullyConnectedNetwork.predict(params, xs)
    val costs = params.getCosts().get.toArray
    println(s"Runtime: ${System.currentTimeMillis() - ts}ms, costs.size: ${costs.size}, last cost: $lastCost")
    val reportedCosts = for (i <- 0 until costs.size if i % 5000 == 0) yield ((i / 5, costs(i)))
    reportedCosts.foreach(rp => println(s"Cost after iteration ${rp._1}: ${rp._2}"))
    val trainAccuracy = FullyConnectedModel.getAccuracy(ys, pys, binaryDecode(0.5)(_))
    println(s"training accuracy $trainAccuracy")
    assertEquals(0.94, trainAccuracy, PRECISION)
  }

  def graph(name: String, costs: Seq[Double]): Unit = {
    val gPath = "C:/Temp/MLData/sparkneuralnets/optimise_data/"
    val f = produceGraph(costs)
    f.saveas(s"${gPath}graph_${name}_${System.currentTimeMillis()}.png")

  }

  def getBasicParameters(): Parameters = {
    Parameters()
      .setLearningRate(0.0007)
      .setLayerDims(2, 5, 2, 1)
      .setWeightInitializer("static_params")
      .setActivations("relu", "relu", "sigmoid")
      .setCostFunction("crossentropytf")
      .setIterations(1)
  }

  def setWeightsAndBiases(p: Parameters): Unit = {
    val s = Source.fromFile(path + "weightsandbiases.txt").toSeq.mkString
    val pw = Parameters(s)
    val wb = pw.getLayerWeightsAndBiases().get
    p.setWeightsAndBiases(wb)
    ()

  }

}