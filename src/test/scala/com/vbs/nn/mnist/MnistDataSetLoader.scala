/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.mnist
import org.junit.Test
import org.junit.Ignore
import org.junit.Assert._
import com.vbs.nn.io.CSVDataSetPreprocessor
import com.vbs.nn.io.MatrixIO
import breeze.linalg.DenseMatrix
import com.vbs.nn.GlobalParams

/**
 * Used to load full data sets from Numpy strings and store them as a single array of doubles for rapid subsequent retrieval.
 */
class MnistDataSetLoader {

  @Test def transformAndPersistAsDenseMatrices(): Unit = {
    val (xs, ys) = MnistDataSetLoader.persistDataAsMatrices(s"${MnistDataSetLoader.PATH}small")

    val persistedXS = MatrixIO.load(s"${MnistDataSetLoader.PATH}small_xs")
    assertEquals(xs, persistedXS)
    val persistedYS = MatrixIO.load(s"${MnistDataSetLoader.PATH}small_ys")
    assertEquals(ys, persistedYS)

  }
  
  @Ignore def persistTrainingData(): Unit = {
    val (xs, ys) = MnistDataSetLoader.persistDataAsMatrices(s"${MnistDataSetLoader.PATH}train")
    val persistedXS = MatrixIO.load(s"${MnistDataSetLoader.PATH}train_xs")
    assertEquals(xs, persistedXS)
  }
  
  @Ignore def persistTestData(): Unit = {
    val (xs, ys) = MnistDataSetLoader.persistDataAsMatrices(s"${MnistDataSetLoader.PATH}test")
    val persistedXS = MatrixIO.load(s"${MnistDataSetLoader.PATH}test_xs")
    assertEquals(xs, persistedXS)
  }

}

object MnistDataSetLoader {
  val PATH = GlobalParams.EXTERNAL_BASE_PATH + "mnist/"

  def persistDataAsMatrices(fName: String): (DenseMatrix[Double], DenseMatrix[Double]) = {
    CSVDataSetPreprocessor(fName).
      setLabelSpec(true, 10).
      setImageNormaliser().
      persistDenseMatrices()
  }
  
  def getTrainingData(): (DenseMatrix[Double], DenseMatrix[Double]) = {
    val xs = MatrixIO.load(PATH + "train_xs")
    val ys = MatrixIO.load(PATH + "train_ys")
    (xs, ys)
    
  }
  
  def getTestData(): (DenseMatrix[Double], DenseMatrix[Double]) = {
    val xs = MatrixIO.load(PATH + "test_xs")
    val ys = MatrixIO.load(PATH + "test_ys")
    (xs, ys)
    
  }

}