/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.mnist
import org.junit.Test
import com.vbs.nn.params.Parameters
import com.vbs.nn.model.FullyConnectedModel
import org.junit.Ignore
import com.vbs.nn.model.ModelReporter

/**
 * Training run for a full data set
 */
//@Ignore
class MnistTrainer {

  @Test def mNistAdamTrain(): Unit = {
    val params = Parameters()
      .setIterations(4)
      .setLearningRate(0.001)
      .setLayerDims(784, 397, 10)
      .setWeightInitializer("xavier")
      .setActivations("relu", "softmax")
      .setCostFunction("crossentropy")
      .setAdam(0.7, 0.95, 1e-8)
      .setSamplerSpec("random", 50)

    val ts = System.currentTimeMillis()
    val (xs, ys) = MnistDataSetLoader.getTrainingData()
    val (xst, yst) =  MnistDataSetLoader.getTestData()
    println(s"Load time: ${System.currentTimeMillis() - ts}ms")
    val id = FullyConnectedModel.run(params, ys, xs, yst, xst, MnistDataSetLoader.PATH + "model/", "image_mnist_full")
    println(s"Run $id finished")

  }

  @Test def produceReports(): Unit = {
    ModelReporter.produceReports(MnistDataSetLoader.PATH + "model/")

  }  
}