/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import org.junit.Test
import org.junit.Assert._

import breeze.linalg.DenseMatrix

class PaddingTest {
  
  @Test def padValidSize(): Unit = {
    val (pTop, pLeft, pBottom, pRight) = Padding.getValidPaddingSize(64,4,1)
    assertEquals(0, pTop)
    assertEquals(0, pLeft)
    assertEquals(0, pBottom)
    assertEquals(0, pRight)
  }
  
  @Test def padSameSize(): Unit = {
    val padding = Padding.getPadding(64,4,1, "same")
    assertEquals(1, padding.pTop)
    assertEquals(1, padding.pLeft)
    assertEquals(2, padding.pBottom)
    assertEquals(2, padding.pRight)
    
    val (newH, newW) = padding.layerDims(64, 64)
    assertEquals(64, newH)
    assertEquals(64, newW)
    
    val arr = (1 to 64 * 64).toArray.map(_.toDouble)
    val dm = (new DenseMatrix[Double](64, 64, arr)).t
    val paddedDM = padding.padInput(dm)
    assertEquals(67, paddedDM.rows)
    assertEquals(67, paddedDM.cols)
    
    assertEquals(1, paddedDM(1,1).toInt)
    assertEquals(64, paddedDM(1,64).toInt)
    assertEquals(64 * 64, paddedDM(64,64).toInt)
    
    paddedDM(::,0).foreach(x => assertEquals(0, x.toInt))
    paddedDM(::,65).foreach(x => assertEquals(0, x.toInt))
    paddedDM(::,66).foreach(x => assertEquals(0, x.toInt))
    
    paddedDM.t(::,0).foreach(x => assertEquals(0, x.toInt))
    paddedDM.t(::,65).foreach(x => assertEquals(0, x.toInt))
    paddedDM.t(::,66).foreach(x => assertEquals(0, x.toInt))
    
    val depadded = padding.dePad(paddedDM)
    assertEquals(dm, depadded)

  }

}