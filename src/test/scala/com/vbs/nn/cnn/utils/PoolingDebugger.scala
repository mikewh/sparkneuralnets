/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.utils
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

import scala.collection.mutable.ArrayBuffer
import com.vbs.nn.cnn.params.PoolingLayerParameters
import com.vbs.nn.cnn.Pooling

/**
 * Captures the inputs and outputs of a call to Pooling.backPropPoolLayer
 */
class PoolingIOCapturer(val layerIdx: Int, poolingLayerParams: PoolingLayerParameters) {
  private var _fSize: Int = 0
  private var _stride: Int = 0
  def fSize = _fSize
  def stride = _stride

  val dAs = new ArrayBuffer[DenseMatrix3d]()
  val prevLayerOutputs = new ArrayBuffer[DenseMatrix3d]()
  val outputs = new ArrayBuffer[DenseMatrix3d]()

  def captureInput(dA: DenseMatrix3d, prevLayerOutput: DenseMatrix3d,
                   fSize: Int, stride: Int): Unit = {
    _fSize = fSize
    _stride = stride
    dAs += dA
    prevLayerOutputs += prevLayerOutput
    ()
  }

  def captureOutput(output: DenseMatrix3d): Unit = {
    outputs += output
    ()
  }
  poolingLayerParams.setBackPropPoolLayerFcn(PoolingDebugger.backPropPoolLayer(captureInput, captureOutput))

}

/**
 * Wraps calls to back prop through a pooling layer to capture input and output
 */
object PoolingDebugger {
  def backPropPoolLayer(inputCapturer: (DenseMatrix3d, DenseMatrix3d, Int, Int) => Unit,
                        outputCapturer: DenseMatrix3d => Unit)(dA: DenseMatrix3d, prevLayerOutput: DenseMatrix3d,
                                                                     fSize: Int, stride: Int, maxPooling: Boolean): DenseMatrix3d = {
    inputCapturer(dA, prevLayerOutput, fSize, stride)
    val r = Pooling.backPropPoolLayer(dA, prevLayerOutput, fSize, stride, maxPooling)
    outputCapturer(r)
    r
  }

}