/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.utils
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import breeze.linalg.DenseVector
import scala.collection.mutable.ArrayBuffer
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.Convolution
import com.vbs.nn.cnn.Padding

/**
 * Captures the inputs and outputs of a call to Convolution.feedForwardLayer
 */
class FeedForwardLayerIOCapturer(val layerIdx: Int, cnnLayerParams: ConvolutionLayerParameters) {

  val prevLayerOutputs = new ArrayBuffer[DenseMatrix3d]()

  val outputsZ = new ArrayBuffer[DenseMatrix3d]()
  val activationsA = new ArrayBuffer[DenseMatrix3d]()

  def captureInput(prevLayerOutput: DenseMatrix3d): Unit = {
    prevLayerOutputs += prevLayerOutput
    ()
  }

  def captureOutput(z: DenseMatrix3d, a: DenseMatrix3d): Unit = {
    outputsZ += z
    activationsA += a
    ()
  }

  cnnLayerParams.setFeedForwardLayerFcn(ConvolutionDebugger.feedForwardLayer(captureInput, captureOutput))

}

/**
 * Captures the inputs and outputs of a call to Convolution.backPropReluLayer
 */
class BackPropNonLinearLayerIOCapturer(val layerIdx: Int, cnnLayerParams: ConvolutionLayerParameters) {

  val dAs = new ArrayBuffer[DenseMatrix3d]()
  val prevLayerOutputs = new ArrayBuffer[DenseMatrix3d]()

  val dZs = new ArrayBuffer[DenseMatrix3d]()

  def captureInput(dA: DenseMatrix3d, prevLayerOutput: DenseMatrix3d): Unit = {
    dAs += dA
    prevLayerOutputs += prevLayerOutput
    ()
  }

  def captureOutput(dZ: DenseMatrix3d): Unit = {
    dZs += dZ
    ()
  }
  cnnLayerParams.setBackPropNonLinearFcn(ConvolutionDebugger.backPropLayerNonLinear(captureInput, captureOutput))

}

/**
 * Captures the inputs and outputs of a call to Convolution.backPropLayerLinear
 */
class BackPropLinearLayerIOCapturer(val layerIdx: Int, cnnLayerParams: ConvolutionLayerParameters) {

  val dZs = new ArrayBuffer[DenseMatrix3d]()
  val prevLayerOutputs = new ArrayBuffer[DenseMatrix3d]()

  val daPrevs = new ArrayBuffer[DenseMatrix3d]()
  val dWeights = new ArrayBuffer[DenseMatrix4d]()
  val dBiases = new ArrayBuffer[DenseVector[Double]]()

  def captureInput(dZ: DenseMatrix3d, prevLayerOutput: DenseMatrix3d): Unit = {
    dZs += dZ
    prevLayerOutputs += prevLayerOutput
    ()
  }

  def captureOutput(daPrevsL: DenseMatrix3d, dWeightsL: DenseMatrix4d, dBiasesL: DenseVector[Double]): Unit = {
    daPrevs += daPrevsL
    dWeights += dWeightsL
    dBiases += dBiasesL
    ()
  }

  cnnLayerParams.setBackPropLinearFcn(ConvolutionDebugger.backPropLayerLinear(captureInput, captureOutput))

}

/**
 * Wraps calls to feed forward and back propagation through a layer to capture input and output
 */
object ConvolutionDebugger {

  def feedForwardLayer(captureInput: (DenseMatrix3d) => Unit,
                       captureOutput: (DenseMatrix3d, DenseMatrix3d) => Unit)(prevLayerOutput: DenseMatrix3d, padding: Padding,
                                                                              filterWeights: DenseMatrix4d, filterBiases: DenseVector[Double],
                                                                              activationFcn: Convolution.ActivationFunction = Convolution.relu): Convolution.CNNOutputsAndActivations = {
    captureInput(prevLayerOutput)
    val (z, a) = Convolution.feedForwardLayer(prevLayerOutput, padding, filterWeights, filterBiases, activationFcn)
    captureOutput(z, a)
    (z, a)
  }

  def backPropLayerNonLinear(captureInput: (DenseMatrix3d, DenseMatrix3d) => Unit,
                             captureOutput: (DenseMatrix3d) => Unit)(dA: DenseMatrix3d, prevLayerOutput: DenseMatrix3d): DenseMatrix3d = {
    captureInput(dA, prevLayerOutput)
    val dZ = Convolution.backPropReluLayer(dA, prevLayerOutput)
    captureOutput(dZ)

    dZ
  }

  def backPropLayerLinear(captureInput: (DenseMatrix3d, DenseMatrix3d) => Unit,
                          captureOutput: (DenseMatrix3d, DenseMatrix4d, DenseVector[Double]) => Unit)(dZ: DenseMatrix3d,
                                                                                                      prevLayerOutput: DenseMatrix3d,
                                                                                                      fSize: Int,
                                                                                                      padding: Padding,
                                                                                                      filterWeights: DenseMatrix4d,
                                                                                                      filterBiases: DenseVector[Double]): (DenseMatrix3d, DenseMatrix4d, DenseVector[Double]) = {
    captureInput(dZ, prevLayerOutput)
    val r = Convolution.backPropLayerLinear(dZ, prevLayerOutput, fSize, padding, filterWeights, filterBiases)
    captureOutput(r._1, r._2, r._3)
    r
  }

}