/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.utils
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import breeze.linalg.DenseVector
import scala.collection.mutable.ArrayBuffer
import com.vbs.nn.cnn.CNNBackPropagate
import scala.collection.GenSeq
import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.cnn.CNNToFCNInterface
import breeze.linalg.DenseMatrix

/**
 * Captures the inputs and outputs of a call to CNNToFCNInterface.flattenBatch
 */
class FlattenBatchIOCapturer(cnnParams: CNNParameters) {
  val unflattened = new ArrayBuffer[GenSeq[DenseMatrix3d]]()
  val outputs = new ArrayBuffer[DenseMatrix[Double]]()

  def captureInput(lastLayerOutputs: GenSeq[DenseMatrix3d]): Unit = {
    unflattened += lastLayerOutputs
    ()
  }
  def captureOutput(output: DenseMatrix[Double]): Unit = {
    outputs += output
    ()
  }

  cnnParams.setCNNFlattenBatchFcn(FlattenDebugger.flattenBatch(captureInput, captureOutput))

}

/**
 * Captures the inputs and outputs of a call to CNNBackPropagate.unflattenFCNOutputs
 */
class UnflattenIOCapturer(cnnParams: CNNParameters) {
  val flattened = new ArrayBuffer[DenseVector[Double]]()
  val outputs = new ArrayBuffer[DenseMatrix3d]()

  def captureInput(v: DenseVector[Double]): Unit = {
    flattened += v
    ()
  }

  def captureOutput(output: DenseMatrix3d): Unit = {
    outputs += output
    ()
  }

  cnnParams.setCNNUnflattenFCNOutputsFcn(FlattenDebugger.unflattenFCNOutputs(captureInput, captureOutput))

}

/**
 * Wraps calls to flatten and back unflatten to capture input and output
 */
object FlattenDebugger {

  def unflattenFCNOutputs(captureInput: DenseVector[Double] => Unit,
                          captureOutput: DenseMatrix3d => Unit)(fcnLayer0Activations: DenseVector[Double],

                                                                cnnLayerOutputs: GenSeq[(DenseMatrix3d, DenseMatrix3d)]): DenseMatrix3d = {
    captureInput(fcnLayer0Activations)
    val r = CNNBackPropagate.unflattenFCNOutputs(fcnLayer0Activations, cnnLayerOutputs)
    captureOutput(r)
    r
  }

  def flattenBatch(captureInput: GenSeq[DenseMatrix3d] => Unit,
                   captureOutput: DenseMatrix[Double] => Unit)(lastLayerOutputs: GenSeq[DenseMatrix3d]): DenseMatrix[Double] = {
    captureInput(lastLayerOutputs)
    val r = CNNToFCNInterface.flattenBatch(lastLayerOutputs)
    captureOutput(r)
    r
  }

}