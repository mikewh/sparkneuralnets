/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import java.io.File

import org.junit.Assert._
import org.junit.Test

import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.utils.MatrixTestUtils
import com.vbs.nn.utils.MatrixUtils

import breeze.linalg.DenseVector
import breeze.stats.mean
import ConvolutionTest._
import breeze.linalg.DenseMatrix
import com.vbs.nn.io.NumpyArrayLoader

class ConvolutionTest {

  @Test def convSingleStep(): Unit = {
    var s = """
      [
 [[ 1.62434536 -1.07296862  1.74481176 -0.24937038]
 [-0.3224172  -1.09989127  0.04221375  1.14472371]
 [ 0.90085595 -0.93576943 -0.69166075 -0.84520564]
 [-1.11731035  0.74204416 -0.74715829 -0.63699565]]
[[-0.61175641  0.86540763 -0.7612069   1.46210794]
 [-0.38405435 -0.17242821  0.58281521  0.90159072]
 [-0.68372786 -0.26788808 -0.39675353 -0.67124613]
 [ 0.2344157  -0.19183555  1.6924546   0.19091548]]
[[-0.52817175 -2.3015387   0.3190391  -2.06014071]
 [ 1.13376944 -0.87785842 -1.10061918  0.50249434]
 [-0.12289023  0.53035547 -0.6871727  -0.0126646 ]
 [ 1.65980218 -0.88762896  0.05080775  2.10025514]]
      ]
      """

    val aSlice = MatrixUtils.toDenseMatrixArray(s)

    s = """
      [
[[ 0.12015895 -0.35224985 -0.20889423  0.93110208]
 [-0.75439794 -0.29809284  1.13162939 -1.39649634]
 [ 0.16003707 -2.02220122  0.23009474 -0.20075807]
 [ 0.19829972  0.37756379  1.19891788 -0.63873041]]
[[ 0.61720311 -1.1425182   0.58662319  0.28558733]
 [ 1.25286816  0.48851815  1.51981682 -1.44411381]
 [ 0.87616892 -0.30620401  0.76201118  0.18656139]
 [ 0.11900865  0.12182127  0.18515642  0.42349435]]
[[ 0.30017032 -0.34934272  0.83898341  0.88514116]
 [ 0.51292982 -0.07557171  2.18557541 -0.50446586]
 [ 0.31563495  0.82797464 -0.22232814  0.41005165]
 [-0.67066229  1.12948391 -0.37528495  0.07734007]]      
      ]
      """
    val weights = MatrixUtils.toDenseMatrixArray(s)

    val bias = -0.34385368

    assertEquals(-23.1602122025, Convolution.convolveSlice(aSlice, weights, bias), CNNTestUtils.DEFAULT_PRECISION)

  }

  @Test def convolveOneImage(): Unit = {
    val prevOutputs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}A_prev.txt")
    assertEquals(10, prevOutputs.size)
    val (filterWeights, filterBiases, padding) = getTestWeightsBiasesAndPadding()
    val newZ = Convolution.convolveLayer(prevOutputs(0), padding, filterWeights, filterBiases)
    val expectedZs = getExpectedOutputsForImage0()

    MatrixTestUtils.assertMatricesEqual(expectedZs(0), newZ(0), CNNTestUtils.DEFAULT_PRECISION)
    MatrixTestUtils.assertMatricesEqual(expectedZs(1), newZ(1), CNNTestUtils.DEFAULT_PRECISION)
    MatrixTestUtils.assertMatricesEqual(expectedZs(2), newZ(7), CNNTestUtils.DEFAULT_PRECISION)
  }

  @Test def convolveOneLayer(): Unit = {
    val prevOutputs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}A_prev.txt")
    assertEquals(10, prevOutputs.size)
    val (filterWeights, filterBiases, padding) = getTestWeightsBiasesAndPadding()
    val layerOutputs = Convolution.convolveLayerBatch(prevOutputs, padding, filterWeights, filterBiases)
    val lMeans = layerOutputs.map(mci => mci.map(mean(_))).flatten
    val lMean = lMeans.sum / lMeans.size
    assertEquals(0.155859324889, lMean, CNNTestUtils.DEFAULT_PRECISION)

  }

  @Test def reluOneImage(): Unit = {
    val outputs = getExpectedOutputsForImage0()
    val r = Convolution.relu(outputs)
    outputs.zip(r).foreach(dm12 => {
      val oa = dm12._1.toArray
      val ra = dm12._2.toArray
      for (i <- 0 until oa.size) {
        if (oa(i) < 0.0) assertEquals(0.0, ra(i), 0.1) else assertEquals(oa(i), ra(i), CNNTestUtils.DEFAULT_PRECISION)
      }
    })

    val prevOutput = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}A_prev.txt").head
    val (filterWeights, filterBiases, padding) = getTestWeightsBiasesAndPadding()
    val activations = Convolution.feedForwardLayer(prevOutput, padding, filterWeights, filterBiases)
    assertEquals(8, activations._1.size) // Number of filters
    assertEquals(7, activations._1(0).rows) // Height
    assertEquals(7, activations._1(0).cols) // Width
    MatrixTestUtils.assertMatricesEqual(outputs(0), activations._1(0), CNNTestUtils.DEFAULT_PRECISION)
    MatrixTestUtils.assertMatricesEqual(r(0), activations._2(0), CNNTestUtils.DEFAULT_PRECISION)
    MatrixTestUtils.assertMatricesEqual(outputs(1), activations._1(1), CNNTestUtils.DEFAULT_PRECISION)
    MatrixTestUtils.assertMatricesEqual(r(1), activations._2(1), CNNTestUtils.DEFAULT_PRECISION)
    MatrixTestUtils.assertMatricesEqual(outputs(2), activations._1(7), CNNTestUtils.DEFAULT_PRECISION)
    MatrixTestUtils.assertMatricesEqual(r(2), activations._2(7), CNNTestUtils.DEFAULT_PRECISION)

  }
  
  @Test def flattenMCI(): Unit = {
    val dm1 = new DenseMatrix(3,3, ((1 to 9).toArray).map(_.toDouble))
    val dm2 = new DenseMatrix(3,3, ((10 to 19).toArray).map(_.toDouble))
    val mci = Seq(dm1, dm2)

    val mcif = CNNToFCNInterface.flatten(mci)
    val expectedMcif = Seq(1.0, 10.0, 4.0, 13.0, 7.0, 16.0, 2.0, 11.0, 5.0, 14.0, 8.0, 17.0, 3.0, 12.0, 6.0, 15.0, 9.0, 18.0)
    assertEquals(expectedMcif, mcif)
    val umci = CNNToFCNInterface.unflatten(3, 3, 2, new DenseVector(mcif.toArray))
    assertEquals(dm1, umci(0))
    assertEquals(dm2, umci(1))
  }
}

object ConvolutionTest {
  val BASE_PATH = new File(getClass.getResource("/").getPath).getAbsolutePath + "/hsigns/"

  def getExpectedOutputsForImage0(): DenseMatrix3d = {
    val s0 = """
      [[  4.46948222   4.46948222   4.46948222   4.46948222   4.46948222
    4.46948222   4.46948222]
 [  4.46948222   6.32884429   9.13423461  10.17273978   8.22617471
    9.89898634   4.46948222]
 [  4.46948222   0.94313066  -1.52923828   6.69500425   6.1869032
    4.99839908   4.46948222]
 [  4.46948222   6.62307301   1.95518268  -0.27784887   4.506057
    5.29401078   4.46948222]
 [  4.46948222   0.69115422   2.70055392   6.04481291   0.57326069
   -1.62342146   4.46948222]
 [  4.46948222   7.19355673   3.50975099   4.73727349   9.09937269
    5.4505174    4.46948222]
 [  4.46948222   4.46948222   4.46948222   4.46948222   4.46948222
    4.46948222   4.46948222]]
      """

    val s1 = """
[[-1.78187764 -1.78187764 -1.78187764 -1.78187764 -1.78187764 -1.78187764
  -1.78187764]
 [-1.78187764 -4.13804325  2.89618341 -2.13261119  0.39935266  0.85083911
  -1.78187764]
 [-1.78187764  0.14516575 -2.48665817  4.58134663 -4.4302973   2.20266179
  -1.78187764]
 [-1.78187764 -3.25243507  0.04275378  0.38535275  0.66797322 -5.94263974
  -1.78187764]
 [-1.78187764  1.00358106 -7.65429911  2.51555201  0.86729214 -4.72894505
  -1.78187764]
 [-1.78187764 -3.16200326 -3.62183949 -2.58615159 -4.09324696 -5.94550649
  -1.78187764]
 [-1.78187764 -1.78187764 -1.78187764 -1.78187764 -1.78187764 -1.78187764
  -1.78187764]]
        """
    val s2 = """
[[-0.8089203  -0.8089203  -0.8089203  -0.8089203  -0.8089203  -0.8089203
  -0.8089203 ]
 [-0.8089203  -0.54611451 -2.75288402  4.69151547 -3.73920041  3.88285761
  -0.8089203 ]
 [-0.8089203  -0.42518546  1.90839255 -2.28097921  8.53248941 -2.13827742
  -0.8089203 ]
 [-0.8089203  -1.7870014  -2.20998251 -2.32273443  0.53691735 -0.08146865
  -0.8089203 ]
 [-0.8089203  -0.84929576 -1.50647616 -1.70926019  0.43495775 -4.41895077
  -0.8089203 ]
 [-0.8089203  -1.77286696 -2.92136019  2.56978474 -4.68360122 -2.66536032
  -0.8089203 ]
 [-0.8089203  -0.8089203  -0.8089203  -0.8089203  -0.8089203  -0.8089203
  -0.8089203 ]]
        """
    Seq(MatrixUtils.toDenseMatrix(s0), MatrixUtils.toDenseMatrix(s1), MatrixUtils.toDenseMatrix(s2))

  }

  def getTestWeightsBiasesAndPadding(): (Seq[DenseMatrix3d], DenseVector[Double], Padding) = {
    val (filterWeights, filterBiases) = getSliceWeightsAndBiases()
    val padding = Padding(2, 2, 2, 2, 1, 1, Padding.newDimsPadded(2, 2, 2, 2, filterWeights(0)(0).rows, filterWeights(0)(0).rows, 1, 1))
    (filterWeights, filterBiases, padding)

  }

  def getSliceWeightsAndBiases(): (Seq[DenseMatrix3d], DenseVector[Double]) = {
    var s = """
[
[[ 0.5154138  -0.25898285]
 [ 0.63658341  1.01120706]]
[[ 1.04008915  0.16986926]
 [ 0.68400133  0.85328219]]
[[ 0.52887975  1.44287693]
 [-2.22711263 -1.39881282]]
 ]      
      """
    val weights = new Array[DenseMatrix3d](8)
    weights(0) = MatrixUtils.toDenseMatrixArray(s)

    s = """
[
[[-1.11487105  0.1892932 ]
 [ 1.40925339 -1.47656266]]
[[-0.91844004 -1.16400797]
 [-0.35340998 -0.13971173]]
[[-2.23708651 -0.53968156]
 [-1.6993336   0.08176782]]
 ]      
      """
    weights(1) = MatrixUtils.toDenseMatrixArray(s)

    s = """
[
[[-0.76730983 -0.56378873]
 [ 1.62091229 -0.14319575]]
[[-0.10534471  0.69336623]
 [-1.78791289  1.38631426]]
[[-1.1077125   0.12837699]
 [-0.27584606 -0.45994283]]
]
      """
    weights(2) = MatrixUtils.toDenseMatrixArray(s)

    s = """
[
[[ 0.67457071  0.08968641]
 [-0.80618482  1.03298378]]
[[ 0.63019567 -0.75806733]
 [ 0.36184732  0.54812958]]
[[-0.01771832  1.76041518]
 [ 1.22895559  0.64435367]]
 ]
      """
    weights(3) = MatrixUtils.toDenseMatrixArray(s)

    s = """
[
[[ 1.46089238 -0.6011568 ]
 [-0.25167421 -0.22241403]]
[[-0.4148469  -0.8088472 ]
 [-0.42449279 -1.63744959]]
[[-1.71939447  0.96653925]
 [ 1.30970591  0.37167029]]
 ]
      """
    weights(4) = MatrixUtils.toDenseMatrixArray(s)

    s = """
[
[[ 0.5924728   0.55607351]
 [ 0.38271517  1.47016034]]
[[ 0.45194604  0.55743945]
 [-0.73153098  3.9586027 ]]
[[ 0.057121    0.71304905]
 [-1.15498263  1.85300949]]
 ]
      """
    weights(5) = MatrixUtils.toDenseMatrixArray(s)

    s = """
[
[[ 1.19783084  1.69380911]
 [-0.28899734 -0.87000822]]
[[-1.57915629  0.18103874]
 [-1.56573815  0.64864364]]
[[-0.79954749  1.30620607]
 [-0.1776322   0.14225137]]
 ]
      """
    weights(6) = MatrixUtils.toDenseMatrixArray(s)

    s = """
[
[[ 1.70459417  0.19686978]
 [-0.39181624  0.36919047]]
[[-0.82862798  1.10717545]
 [ 1.01382247  0.10734329]]
[[-0.2915946  -0.60460297]
 [-1.51045638  0.51350548]]
 ]
      """
    weights(7) = MatrixUtils.toDenseMatrixArray(s)

    s = """
[
[[ 1.70459417  0.19686978]
 [-0.39181624  0.36919047]]
[[-0.82862798  1.10717545]
 [ 1.01382247  0.10734329]]
[[-0.2915946  -0.60460297]
 [-1.51045638  0.51350548]]
 ]
      """
    weights(7) = MatrixUtils.toDenseMatrixArray(s)

    val biases = DenseVector(Array(0.37245685, -0.1484898, -0.1834002, 1.1010002, 0.78002714, -0.6294416, -1.1134361, -0.06741002))
    (weights, biases)
  }

}