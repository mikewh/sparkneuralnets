/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import org.junit.Test
import org.junit.Assert._

import scala.collection.GenSeq

import com.vbs.nn.CNNFCNLinkedNetwork
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

import com.vbs.nn.FullyConnectedNetwork
import breeze.linalg.DenseMatrix
import com.vbs.nn.tf.TensorflowRegression
import com.vbs.nn.tf.TensorflowAdam

class CNNFeedForwardTest {

  @Test def feedForwardCost(): Unit = {
    val (_, sampleY) = CNNFeedForwardTest.loadSampleData()

    val (_, fcnParams) = TensorflowAdam.loadSampleRunParams()

    val fcnOptimizerParams = FullyConnectedNetwork.initialise(fcnParams)

    val (flattenedOutput, cnnLayerOutputs) = CNNFeedForwardTest.feedForwardCNN()
    val (cost, fcnOptimizedParamsSet) = FullyConnectedNetwork.trainMiniBatch(fcnParams, fcnOptimizerParams, flattenedOutput, sampleY)
    assertEquals(8.40054, cost, CNNTestUtils.DEFAULT_PRECISION)

  }
  
}

object CNNFeedForwardTest {
  
  def feedForwardCNN(): (DenseMatrix[Double], GenSeq[Seq[(DenseMatrix3d, DenseMatrix3d)]]) = {
    val (sampleX, _) = CNNFeedForwardTest.loadSampleData()

    val (cnnParams, _) = TensorflowAdam.loadSampleRunParams()

    CNNFCNLinkedNetwork.initialise(cnnParams)
    val layerParams = cnnParams.getCNNLayerParams()

    val cnnLayerOutputs = CNNFeedForward.feedForwardCNNBatch(sampleX, layerParams)
    val flattenedOutput = CNNFeedForward.flattenCNNOutputs(cnnLayerOutputs)
    (flattenedOutput, cnnLayerOutputs)
  }
    
  
  
  def loadSampleData(): (GenSeq[DenseMatrix3d], DenseMatrix[Double]) = 
    (TensorflowRegression.loadSampleX().par, TensorflowRegression.loadSampleY())
}