/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import breeze.linalg.DenseMatrix

object PoolingTestUtils {
  def maskMaxPoolLayerBatch(dAs: DenseMatrix4d, prevLayerOutputs: DenseMatrix4d,
                        fSize: Int, stride: Int): DenseMatrix4d = {
    for (i <- 0 until dAs.size) yield (maskMaxPoolLayer(dAs(i), prevLayerOutputs(i), fSize, stride))
  }

  def maskMaxPoolLayer(dA: DenseMatrix3d, prevLayerOutput: DenseMatrix3d,
                       fSize: Int, stride: Int): DenseMatrix3d = {
    val h = dA(0).rows
    val w = dA(0).cols
    val channels = dA.size

    val masked = prevLayerOutput.map(dm => DenseMatrix.zeros[Double](dm.rows, dm.cols))

    for (rowStart <- 0 until h by stride) {
      for (colStart <- 0 until w by stride) {
        for (channel <- 0 until channels) {
          val prevSlice = prevLayerOutput(channel)(rowStart until rowStart + fSize, colStart until colStart + fSize)
          val dAPrevDelta = Pooling.maskMaxPool(prevSlice)
          for (r <- 0 until fSize) {
            for (c <- 0 until fSize) {
              masked(channel)(rowStart + r, colStart + c) = dAPrevDelta(r, c)
            }
          }
        }

      }
    }
    masked
  }
}