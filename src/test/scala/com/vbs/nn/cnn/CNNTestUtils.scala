/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import org.junit.Assert._
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.utils.MatrixTestUtils

object CNNTestUtils {
   val DEFAULT_PRECISION = 0.000001
 
  def assertMCIDims(mci: DenseMatrix3d, h: Int, w: Int, channels: Int): Unit = {
    assertEquals(s"Height ${toDims(mci)}", h, mci(0).rows)
    assertEquals(s"Width ${toDims(mci)}", w, mci(0).cols)
    assertEquals(s"Channels ${toDims(mci)}", channels, mci.size)
  }
  
  def toDims(mci: DenseMatrix3d): String = s"(${mci(0).rows}, ${mci(0).cols}, ${mci.size})"
  def printDims(mci: DenseMatrix3d): Unit = println(toDims(mci))
  
  def assertMCIEquals(mci1: DenseMatrix3d, mci2: DenseMatrix3d, precision: Double): Unit = {
    assertEquals("channels not equal",mci1.size,mci2.size)
    for (c <- 0 until mci1.size) {
    assertEquals("rows not equal", mci1(c).rows, mci2(c).rows)
    assertEquals("cols not equal",  mci1(c).cols,  mci2(c).cols)
    for (r <- 0 until  mci1(c).rows) {
      for (h <- 0 until  mci1(c).cols) assertEquals(s"Mismatch at ($r, $h, $c)",  mci1(c)(r, h),  mci2(c)(r, h), precision)
    }
      
    }
    mci1.zip(mci2).foreach(dms => MatrixTestUtils.assertMatricesEqual(dms._1, dms._2, precision))
  }
    
  def assertMCIEquals(mci1: DenseMatrix3d, mci2: DenseMatrix3d): Unit = {
    assertMCIEquals(mci1, mci2, DEFAULT_PRECISION)
  }
}