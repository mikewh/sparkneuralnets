/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import org.junit.Assert._
import org.junit.Test

import breeze.stats.mean

import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.io.NumpyArrayLoader
import com.vbs.nn.utils.MatrixTestUtils
import com.vbs.nn.utils.MatrixUtils

import ConvolutionTest.BASE_PATH
import ConvolutionTest.getTestWeightsBiasesAndPadding
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

class BackPropConvolutionTest {

  @Test def backPropReluOneImage(): Unit = {
    val prevOutput = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}A_prev.txt").head
    val (filterWeights, filterBiases, padding) = getTestWeightsBiasesAndPadding()

    // We're using the outputs as a convenient source of data. This would be the gradients of the outputs
    val oAndA = Convolution.feedForwardLayer(prevOutput, padding, filterWeights, filterBiases)
    val dANext = oAndA._1
    val prevLayerOutput = oAndA._2
    val dA = Convolution.backPropReluLayer(dANext, prevLayerOutput)
    for (i <- 0 until prevLayerOutput.size) {
      for (r <- 0 until prevLayerOutput(0).rows) {
        for (c <- 0 until prevLayerOutput(0).cols) {
          if (prevLayerOutput(i)(r, c) > 0.0) assertEquals(dANext(i)(r, c), dA(i)(r, c), CNNTestUtils.DEFAULT_PRECISION)
          else assertEquals(0.0, dA(i)(r, c), CNNTestUtils.DEFAULT_PRECISION)
        }
      }
    }

  }

  @Test def updateOneDAPrevSlice(): Unit = {
    var s = """
[[[ 2.30363282 -1.15751926  0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 2.84519823  4.51957199  0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]][[ 4.64865998  0.75922762  0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 3.05713177  3.81372956  0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]][[ 2.36381862  6.44891277  0.0         0.0         0.0         0.0         0.
   0.0       ]
 [-9.95404031 -6.25196902  0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0         0.0         0.
   0.0       ]]]        """
    val expectedDaPrevPad = MatrixUtils.toDenseMatrixArray(s)
    val dAPrevPad = expectedDaPrevPad.map(dm => DenseMatrix.zeros[Double](dm.rows, dm.cols))

    s = """
[[[ 0.5154138  -0.25898285]
 [ 0.63658341  1.01120706]][[ 1.04008915  0.16986926]
 [ 0.68400133  0.85328219]][[ 0.52887975  1.44287693]
 [-2.22711263 -1.39881282]]]
        """

    val channelWeights = MatrixUtils.toDenseMatrixArray(s)
    val dZSlice = 4.46948221814

    s = """
[[[ 2.30363282 -1.15751926]
 [ 2.84519823  4.51957199]][[ 4.64865998  0.75922762]
 [ 3.05713177  3.81372956]][[ 2.36381862  6.44891277]
 [-9.95404031 -6.25196902]]]
          """
    val expectedChannelWeightsDelta = MatrixUtils.toDenseMatrixArray(s)
    val channelWeightsDelta = channelWeights.map(_ :* dZSlice)
    MatrixTestUtils.assertMatricesEqual(expectedChannelWeightsDelta(0), channelWeightsDelta(0), CNNTestUtils.DEFAULT_PRECISION)

    Convolution.updateDAPrevSlice(dAPrevPad, channelWeightsDelta, 0, 0, 2)

    expectedDaPrevPad.zip(dAPrevPad).foreach(dms => MatrixTestUtils.assertMatricesEqual(dms._1, dms._2, CNNTestUtils.DEFAULT_PRECISION))

  }

  @Test def updateWeightsAndBiasesGradients(): Unit = {
    var s = """[
[[ 1.62434536 -1.07296862]
 [-0.3224172  -1.09989127]][[-0.61175641  0.86540763]
 [-0.38405435 -0.17242821]][[-0.52817175 -2.3015387 ]
 [ 1.13376944 -0.87785842]]      
      ]"""
    val prevSlice = MatrixUtils.toDenseMatrixArray(s)

    s = """[
[[  0.           1.53196991]
 [ 15.80673351  15.87355198]][[  0.          -0.57696623]
 [ 11.42720556   7.95490741]][[  0.          -0.49813497]
 [-46.00623236 -36.99779417]]      
      ]"""

    val dW = MatrixUtils.toDenseMatrixArray(s)
    val dZSlice = -1.52923828264

    s = """[
[[ -2.48401111   3.1727946 ]
 [ 16.29978625  17.55554782]][[  0.93552133  -1.9003807 ]
 [ 12.01451618   8.21859122]][[  0.80770046   3.02146611]
 [-47.740036   -35.65533947]]      ]"""
    val expectedDw = MatrixUtils.toDenseMatrixArray(s)

    val sliceWeightsDelta = prevSlice.map(_ :* dZSlice)
    val dWeightsDelta = dW.zip(sliceWeightsDelta).map(mcis => mcis._1 + mcis._2)
    expectedDw.zip(dWeightsDelta).foreach(dms => MatrixTestUtils.assertMatricesEqual(dms._1, dms._2, CNNTestUtils.DEFAULT_PRECISION))
  }

  @Test def backPropOneLinearLayer(): Unit = {

    val prevLayerOutputs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}A_prev.txt")
    val dZs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}Z.txt")
    val (filterWeights, filterBiases, padding) = getTestWeightsBiasesAndPadding()

    val cnnGrads = Convolution.backPropLinearLayerBatch(dZs, prevLayerOutputs, padding, filterWeights, filterBiases)

    BackPropConvolutionTest.verifyBackPropOneLayer(cnnGrads.dActivationsPrev, cnnGrads.dWeights, cnnGrads.dBiases)

  }

  @Test def backPropOneLinearLayerFold(): Unit = {
    val prevLayerOutputs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}A_prev.txt")
    val dZs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}Z.txt")
    assertEquals(dZs.size, prevLayerOutputs.size)
    val zippedParallelArgs = dZs.zip(prevLayerOutputs).par
    val (filterWeights, filterBiases, padding) = getTestWeightsBiasesAndPadding()
    val filterSize = filterWeights(0)(0).rows

    val dWeights = filterWeights.map(Convolution.zeroMCI).toArray
    val dBiases = DenseVector.zeros[Double](filterBiases.size)

    val dActivationsPrev = (zippedParallelArgs.map { da =>
      val dZ = da._1
      val prevLayerOutput = da._2
 
      val (aPrev, dLweights, dLBiases) = Convolution.backPropLayerLinear(dZ, prevLayerOutput, filterSize, padding, filterWeights, filterBiases)
      assertEquals(dWeights.size, dLweights.size)
      for (channel <- 0 until dWeights.size) {
        assertEquals(dWeights(channel)(0).size, dLweights(channel)(0).size)
        for (i <- 0 until dWeights(channel).size) {
          dWeights(channel)(i) :+= dLweights(channel)(i)
        }
      }
      dBiases :+= dLBiases
      aPrev
    }).seq

    BackPropConvolutionTest.verifyBackPropOneLayer(dActivationsPrev, dWeights, dBiases)

  }

}

object BackPropConvolutionTest {

  def verifyBackPropOneLayer(dActivationsPrev: Seq[DenseMatrix3d], dWeights: Seq[DenseMatrix3d], dBiases: DenseVector[Double]): Unit = {

    def mciMean(mcis: Seq[DenseMatrix3d]): Double = {
      val lMeans = mcis.map(mci => mci.map(mean(_))).flatten
      lMeans.sum / lMeans.size
    }

    var lMean = mciMean(dActivationsPrev)
    assertEquals(9.60899067587, lMean, CNNTestUtils.DEFAULT_PRECISION)

    lMean = mciMean(dWeights)
    assertEquals(10.5817412755, lMean, CNNTestUtils.DEFAULT_PRECISION)

    lMean = mean(dBiases)
    assertEquals(76.3710691956, lMean, CNNTestUtils.DEFAULT_PRECISION)

  }
  
}