/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.par
import org.junit.Test
import org.junit.Assert._

import com.vbs.nn.cnn.params.ConvolutionLayerParameters

import com.vbs.nn.cnn.params.PoolingLayerParameters
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.sampler.CNNMiniBatchSampler
import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.params.Parameters
import com.vbs.nn.CNNFCNLinkedNetwork
import com.vbs.nn.cnn.CNNTestUtils
import com.vbs.nn.cnn.CNNFeedForward
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.cnn.params.CNNLayerParameters
import scala.collection.GenSeq
import breeze.linalg.DenseMatrix
import com.vbs.nn.params.OptimizedParamsSetAdam
import com.vbs.nn.params.OptimizerParamsAdam
import com.vbs.nn.params.OptimizerParamsMomentum
import com.vbs.nn.params.OptimizedParamsSetMomentum
import com.vbs.nn.FullyConnectedNetwork
import com.vbs.nn.cnn.CNNBackPropagate
import com.vbs.nn.hsigns.HSignsDataSetLoader

class CNNHSignsTimingTests {

  @Test def feedForwardTiming(): Unit = {
    val ts = System.currentTimeMillis()

    val (xs, ys) = HSignsDataSetLoader.getTrainingData()
    println(s"Loaded ${xs.size} training samples in ${System.currentTimeMillis() - ts}ms")

    val globalParams = Parameters()
      .setIterations(1)
      .setLearningRate(0.009)
      .setAdam(0.9, 0.999, 1e-08)

    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8, true),
        new ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4, true))
      .setFilterWeightsAndBiasesInitializer("static_xavier")
    CNNFCNLinkedNetwork.initialise(cnnParams)

    val layerParams = cnnParams.getCNNLayerParams()
    val cnnSamplerS = CNNMiniBatchSampler(ys, xs, 4, 50, "static_random", false)
    val samplesS = cnnSamplerS.miniBatches().map(_._2)
    val seqTime = feedForward(samplesS, layerParams)
    println(s"feedForwardTiming seq: $seqTime")
    val cnnSamplerP = CNNMiniBatchSampler(ys, xs, 4, 50, "static_random", true)
    val samplesP = cnnSamplerP.miniBatches().map(_._2)
    val parTime = feedForward(samplesP, layerParams)
    println(s"feedForwardTiming par: $parTime")
  }

  def feedForward(samples: Seq[GenSeq[DenseMatrix3d]], layerParams: Seq[CNNLayerParameters]): Long = {
    val ts = System.currentTimeMillis()
    samples.foreach(sample => CNNFeedForward.feedForwardCNNBatch(sample, layerParams))
    System.currentTimeMillis() - ts

  }

  @Test def backPropTiming(): Unit = {
    val ts = System.currentTimeMillis()

    val (xs, ys) = HSignsDataSetLoader.getTrainingData()
    println(s"Loaded ${xs.size} training samples in ${System.currentTimeMillis() - ts}ms")
    val parTime = backPropagate(xs, ys, true)
    println(s"backPropTiming par: $parTime")
    val seqTime = backPropagate(xs, ys, false)
    println(s"backPropTiming none: $seqTime")

  }

  def backPropagate(xs: DenseMatrix4d, ys: DenseMatrix[Double], par: Boolean): Long = {

    val globalParams = Parameters()
      .setIterations(1)
      .setLearningRate(0.009)
      .setAdam(0.9, 0.999, 1e-08)
      .setParallel(par)
      .setSamplerSpec("static_random", 16, 20)

    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8, true),
        ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4, true))
      .setFilterWeightsAndBiasesInitializer("static_xavier")
      .createCNNSampler(ys, xs)

    val fcnParams = Parameters(globalParams)
      .setLayerDims(64, 6)
      .setLearningRate(0.009)
      .setActivations("softmax")
      .setCostFunction("crossentropyTF")
      .setWeightInitializer("static_xavier")
    val samples = cnnParams.miniBatches()
    val layerParams = cnnParams.getCNNLayerParams()

    CNNFCNLinkedNetwork.initialise(cnnParams)
    val fcnOptimizerParams = FullyConnectedNetwork.initialise(fcnParams)

    var totalTime = 0l
    samples.foreach(sample => {
      val (sampleY, sampleX) = (sample._1, sample._2)
      val cnnLayerOutputs = cnnParams.feedForwardCNNFcn()(sampleX, layerParams)
      val flattenedOutput = CNNFeedForward.flattenCNNOutputs(cnnLayerOutputs, cnnParams.getCNNFlattenBatchFcn())

      val (cost, fcnOptimizedParamsSet) = FullyConnectedNetwork.trainMiniBatch(fcnParams, fcnOptimizerParams, flattenedOutput, sampleY)
      fcnOptimizerParams.weightsAndBiases = fcnOptimizedParamsSet.weightsAndBiases
      fcnOptimizedParamsSet match {
        case opsm: OptimizedParamsSetMomentum => fcnOptimizerParams.asInstanceOf[OptimizerParamsMomentum].velocities = opsm.velocities
        case opsa: OptimizedParamsSetAdam => {
          val opa = fcnOptimizerParams.asInstanceOf[OptimizerParamsAdam]
          opa.avgGradients = opsa.avgGradients
          opa.avgSqGradients = opsa.avgSqGradients
        }
        case _ =>
      }
      val ts = System.currentTimeMillis()
      val cnnLayerOutputsP = par match {
        case true => cnnLayerOutputs.par
        case false       => cnnLayerOutputs
      }
      val (actvationPrevGradients, weightsAndBiasesGradients) =
        CNNBackPropagate.backPropageCNNBatch(sampleX, fcnOptimizedParamsSet.gradients(0), layerParams, cnnLayerOutputsP, cnnParams.getCNNUnflattenFCNOutputsFcn())
      totalTime += System.currentTimeMillis() - ts

    })

    totalTime
  }

  @Test def trainingTimeAndConsistency(): Unit = {

    val (xs, ys) = HSignsDataSetLoader.getTrainingData()
    val yv = (for (col <- 0 until ys.cols) yield (ys(::, col))).toSeq

    println(s"Loaded ${xs.size} training samples")

    def trainSpark(): Unit = {

      val globalParams = Parameters()
        .setIterations(2)
        .setLearningRate(0.009)
        .setAdam(0.9, 0.999, 1e-08)
        .setSamplerSpec("static_random", 16, 2)

      val cnnParams = CNNParameters(globalParams)
        .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
          PoolingLayerParameters(8, 8, true),
          ConvolutionLayerParameters("same", 1, 8, 2, 16),
          PoolingLayerParameters(4, 4, true))
        .setFilterWeightsAndBiasesInitializer("static_xavier")
        .setSparkSession("SparkHsignsTest", 20)
        .createCNNSampler(yv, xs)

      val fcnParams = Parameters(globalParams)
        .setLayerDims(64, 6)
        .setLearningRate(0.009)
        .setActivations("softmax")
        .setCostFunction("crossentropyTF")
        .setWeightInitializer("static_xavier")
      val ts = System.currentTimeMillis()
      val cost = CNNFCNLinkedNetwork.train(cnnParams, fcnParams)
      println(s"Train spark, cost $cost in ${System.currentTimeMillis() - ts}ms")
      assertEquals(1.5932274170472653, cost, CNNTestUtils.DEFAULT_PRECISION)

    }

    def train(par: String): Unit = {

      val globalParams = Parameters()
        .setIterations(2)
        .setLearningRate(0.009)
        .setAdam(0.9, 0.999, 1e-08)
        .setParallel(par != "none")
        .setSamplerSpec("static_random", 16, 2)

      val cnnParams = CNNParameters(globalParams)
        .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
          PoolingLayerParameters(8, 8, true),
          ConvolutionLayerParameters("same", 1, 8, 2, 16),
          PoolingLayerParameters(4, 4, true))
        .setFilterWeightsAndBiasesInitializer("static_xavier")
        .createCNNSampler(ys, xs)

      val fcnParams = Parameters(globalParams)
        .setLayerDims(64, 6)
        .setLearningRate(0.009)
        .setActivations("softmax")
        .setCostFunction("crossentropyTF")
        .setWeightInitializer("static_xavier")
      val ts = System.currentTimeMillis()
      val cost = CNNFCNLinkedNetwork.train(cnnParams, fcnParams)
      println(s"Train ${cnnParams.getParallel()}, cost $cost in ${System.currentTimeMillis() - ts}ms")
      assertEquals(1.7878655289, cost, CNNTestUtils.DEFAULT_PRECISION)
    }
    train("scala")
    train("none")
    trainSpark

  }

}
