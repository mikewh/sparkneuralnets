/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import org.junit.Test
import org.junit.Assert._

class CNNLayerParametersTest {
  
  @Test def convolutionLayerParameters(): Unit = {
    val convParams = ConvolutionLayerParameters("same",1,3, 4, 8)
    val s = convParams.toString
    val r = CNNLayerParameters.fromString(s).asInstanceOf[ConvolutionLayerParameters]
    assertEquals(s, r.toString)
  }
  
  @Test def poolingLayerParameters(): Unit = {
    val poolParams = PoolingLayerParameters(4,1)
    val s = poolParams.toString
    val r = CNNLayerParameters.fromString(s).asInstanceOf[PoolingLayerParameters]
    assertEquals(s, r.toString)
  }
  
  @Test def layerParamSequence(): Unit = {
    val cnnLayerParams = Seq(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8),
        ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4))
    val sBlock = CNNParameters.toSequenceBlock("cnnLayerParams", cnnLayerParams.map(_.toString))
    val (s, blocks) = CNNParameters.extractSequenceBlocks(sBlock)
    assertEquals(1, blocks.size)
    val (name, valueStr) = blocks(0)
    val r = CNNLayerParameters.toLayerParams(valueStr)
    assertEquals(4, r.size)
    cnnLayerParams.zip(r).foreach(ea => assertEquals(ea._1.toString, ea._2.toString))
    
  }
  
}