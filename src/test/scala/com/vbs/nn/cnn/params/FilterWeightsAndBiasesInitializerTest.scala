/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import org.junit.Test
import org.junit.Assert._
import FilterWeightsAndBiasesInitializer._
import com.vbs.nn.cnn.CNNTestUtils._

class FilterWeightsAndBiasesInitializerTest {
  val PRECISION = 0.000001

  @Test def initialization(): Unit = {
    val filterwb = generateFilterWeightsAndBiasesL(4, 3, 8, "static_gaussian")
    assertEquals(8, filterwb.weights.size)
    assertEquals(8, filterwb.biases.size)
    filterwb.weights.foreach(mciWeights => assertMCIDims(mciWeights, 4, 4, 3))
    
    val filterWb1 = generateFilterWeightsAndBiasesL(4, 4, 3, 8, "static_gaussian", false)
    filterwb.weights.zip(filterWb1.weights).foreach(f12 => assertMCIEquals(f12._1, f12._2, PRECISION))
    
  }

}