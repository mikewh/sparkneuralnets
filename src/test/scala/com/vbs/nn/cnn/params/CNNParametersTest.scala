/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import org.junit.Assert._
import org.junit.Test
import com.vbs.nn.params.Parameters
import com.vbs.nn.CNNFCNLinkedNetwork


class CNNParametersTest {

  @Test def createCNNParams(): Unit = {
    val (globalParams, cnnParams, fcnParams) = CNNParametersTest.getTestParams()
    CNNFCNLinkedNetwork.initialise(cnnParams)

    assertEquals(4, cnnParams.getCNNLayerParams().size)
    assertEquals(1, cnnParams.getIterations())
    cnnParams.setIterations(2)
    assertEquals(2, cnnParams.getIterations())
    assertEquals(1, globalParams.getIterations())
    assertEquals("static_gaussian", cnnParams.getFilterWeightsAndBiasesInitializer())

    val convLayers = cnnParams.getCNNLayerParams().collect { case convLayer: ConvolutionLayerParameters => convLayer }
    convLayers.foreach(convLayer => assertNotNull(convLayer.getFilterWeightsAndBiases()))
    assertTrue(cnnParams.getFilterWeightsAndBiases().isDefined)

    assertEquals(3, fcnParams.getIterations())
    assertEquals(2, cnnParams.getIterations())
    assertEquals(1, globalParams.getIterations())

  }

  @Test def toAndFromString(): Unit = {
    val (globalParams, cnnParams, _) = CNNParametersTest.getTestParams()
    CNNFCNLinkedNetwork.initialise(cnnParams)
    val cnnLayerParams = cnnParams.getCNNLayerParams()
    val filterWeightsAndBiases = cnnParams.getFilterWeightsAndBiases().get
    val s = cnnParams.toString
    val r = CNNParameters(s)
    assertEquals(1, r.getIterations())
    assertEquals("adam", r.getOptimizer())
    assertEquals("static_gaussian", r.getFilterWeightsAndBiasesInitializer())
    val rLayerParams = r.getCNNLayerParams()
    assertEquals(4, rLayerParams.size)
    cnnLayerParams.zip(rLayerParams).foreach(ea => assertEquals(ea._1.toString, ea._2.toString))
    
    assertTrue(r.getFilterWeightsAndBiases().isDefined)

    val rWeightsAndBiases = r.getFilterWeightsAndBiases().get
    assertEquals(2, rWeightsAndBiases.size)
    filterWeightsAndBiases.zip(rWeightsAndBiases).foreach(ea => assertEquals(ea._1.toString, ea._2.toString))
  }

}
object CNNParametersTest {
  def getTestParams(): (Parameters, CNNParameters, Parameters) = {
    val globalParams = Parameters()
      .setIterations(1)
      .setLearningRate(0.009)
      .setAdam(0.9, 0.999, 1e-08)

    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8),
        ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4))
      .setFilterWeightsAndBiasesInitializer("static_gaussian")

    val fcnParams = Parameters(globalParams)
      .setLayerDims(64, 6)
      .setActivations("softmax")
      .setCostFunction("crossentropy")
      .setIterations(3)
      .setWeightInitializer("static_gaussian")
    (globalParams, cnnParams, fcnParams)

  }
}

