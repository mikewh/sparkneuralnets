/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import org.junit.Test
import org.junit.Assert._
import com.vbs.nn.cnn.CNNTestUtils
import com.vbs.nn.utils.MatrixTestUtils

class FilterWeightsAndBiasesLTest {
  
  @Test def toAndFromString(): Unit = {
    val filterWb = FilterWeightsAndBiasesInitializer.generateFilterWeightsAndBiasesL(4, 4, 3, 8, "static_gaussian", false)
    val s = filterWb.toString
    
    val r = FilterWeightsAndBiasesL.fromString(s)
    assertEquals(filterWb.weights.size, r.weights.size)
    for (i <- 0 until r.weights.size) CNNTestUtils.assertMCIEquals(filterWb.weights(i), r.weights(i), CNNTestUtils.DEFAULT_PRECISION)
    MatrixTestUtils.assertVectorsEqual(filterWb.biases, r.biases, CNNTestUtils.DEFAULT_PRECISION)
  }
  
}