/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import java.io.File

import org.junit.Test
import org.junit.Assert._
import breeze.stats.mean

import com.vbs.nn.utils.MatrixTestUtils
import com.vbs.nn.utils.MatrixUtils
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.io.NumpyArrayLoader
import com.vbs.nn.io.NumpyArrayLoaderTest

class PoolingTest {
  val PRECISION = 0.000001
  val BASE_PATH = new File(getClass.getResource("/").getPath).getAbsolutePath + "/hsigns/"

  @Test def pooling(): Unit = {
    val prevOutputs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}A_prev.txt").take(2)
    var outputs = Pooling.poolLayerBatch(prevOutputs, 4, 1, Pooling.maxPool).output
    var expectedZ = MatrixUtils.toDenseMatrixArray("[[[ 1.74481176 ]] [[  1.6924546 ]] [[  2.10025514]]]")
    expectedZ.zip(outputs(0)).foreach(ea => MatrixTestUtils.assertMatricesEqual(ea._1, ea._2, PRECISION))

    expectedZ = MatrixUtils.toDenseMatrixArray("[[[ 1.19891788 ]] [[  1.51981682 ]] [[  2.18557541]]]")
    expectedZ.zip(outputs(1)).foreach(ea => MatrixTestUtils.assertMatricesEqual(ea._1, ea._2, PRECISION))

    outputs = Pooling.poolLayerBatch(prevOutputs, 4, 1, Pooling.averagePool).output
    expectedZ = MatrixUtils.toDenseMatrixArray("[[[ -0.09498456 ]] [[  0.11180064 ]] [[  -0.14263511]]]")
    expectedZ.zip(outputs(0)).foreach(ea => MatrixTestUtils.assertMatricesEqual(ea._1, ea._2, PRECISION))

    expectedZ = MatrixUtils.toDenseMatrixArray("[[[ -0.09525108 ]] [[  0.28325018 ]] [[  0.33035185]]]")
    expectedZ.zip(outputs(1)).foreach(ea => MatrixTestUtils.assertMatricesEqual(ea._1, ea._2, PRECISION))
  }

  @Test def maskPool(): Unit = {
    val dm = Pooling.maskMaxPool(MatrixUtils.toDenseMatrix("[[1 3] [4 2]]"))
    val expected = MatrixUtils.toDenseMatrix("[[0 0] [1 0]]")
    MatrixTestUtils.assertMatricesEqual(expected, dm, PRECISION)
  }

  @Test def distributePool(): Unit = {
    val dm = Pooling.distributePool(30.0, 3, 4)
    assertEquals(3, dm.rows)
    assertEquals(4, dm.cols)
    dm.toArray.foreach(x => assertEquals(2.5, x, PRECISION))
  }

  @Test def backPropPoolingOneImage(): Unit = {
    var s = """[
 [[ 1.62434536 -0.52817175  0.86540763]
 [ 1.74481176  0.3190391   1.46210794]
 [-0.3224172   1.13376944 -0.17242821]
 [ 0.04221375 -1.10061918  0.90159072]
 [ 0.90085595 -0.12289023 -0.26788808]]
 [[-0.61175641 -1.07296862 -2.3015387 ]
 [-0.7612069  -0.24937038 -2.06014071]
 [-0.38405435 -1.09989127 -0.87785842]
 [ 0.58281521  1.14472371  0.50249434]
 [-0.68372786 -0.93576943  0.53035547]]
       ]"""
    val aPrev = MatrixUtils.toDenseMatrixArray(s)
    val activations = Pooling.poolLayerBatch(Seq(aPrev), 2, 1, Pooling.maxPool).output

    s = """[   
[[ 1.74481176  1.46210794]
 [ 1.74481176  1.46210794]
 [ 1.13376944  1.13376944]
 [ 0.90085595  0.90159072]]
 [[-0.24937038 -0.24937038]
 [-0.24937038 -0.24937038]
 [ 1.14472371  1.14472371]
 [ 1.14472371  1.14472371]]   
       ]"""

    val expectedActivations = MatrixUtils.toDenseMatrixArray(s)
    expectedActivations.zip(activations(0)).foreach(ea => MatrixTestUtils.assertMatricesEqual(ea._1, ea._2, PRECISION))

    s = """[   
[[-0.31011677  1.0388246 ]
 [ 0.44136444 -0.13644474]
 [ 0.01740941 -0.51709446]
 [ 0.24879916  0.49521132]]
 [[-2.43483776  2.18697965]
 [-0.10015523 -0.11905419]
 [-1.12201873 -0.99702683]
 [-0.29664115 -0.17470316]]  
       ]"""
    val dA = MatrixUtils.toDenseMatrixArray(s)
    var dAPrev = Pooling.backPropPoolLayer(dA, aPrev, 2, 1, true)

    s = """[   
[[ 0.          0.          0.        ]
 [ 0.13124767  0.          0.90237986]
 [ 0.         -0.49968505  0.        ]
 [ 0.          0.          0.49521132]
 [ 0.24879916  0.          0.        ]]
 [[ 0.          0.          0.        ]
 [ 0.         -0.46706754  0.        ]
 [ 0.          0.          0.        ]
 [ 0.         -2.59038987  0.        ]
 [ 0.          0.          0.        ]]  
       ]"""

    var expectedDAPrev = MatrixUtils.toDenseMatrixArray(s)
    expectedDAPrev.zip(dAPrev).foreach(ea => MatrixTestUtils.assertMatricesEqual(ea._1, ea._2, PRECISION))

    dAPrev = Pooling.backPropPoolLayer(dA, aPrev, 2, 1, false)
    s = """[   
[[-0.07752919  0.18217696  0.25970615]
 [ 0.03281192  0.25840688  0.22559496]
 [ 0.11469346 -0.04869134 -0.1633848 ]
 [ 0.06655214  0.06108136 -0.00547078]
 [ 0.06219979  0.18600262  0.12380283]]
 [[-0.60870944 -0.06196453  0.54674491]
 [-0.63374825 -0.11676688  0.51698136]
 [-0.30554349 -0.58456374 -0.27902025]
 [-0.35466497 -0.64759747 -0.2929325 ]
 [-0.07416029 -0.11783608 -0.04367579]]  
       ]"""

    expectedDAPrev = MatrixUtils.toDenseMatrixArray(s)
    expectedDAPrev.zip(dAPrev).foreach(ea => MatrixTestUtils.assertMatricesEqual(ea._1, ea._2, PRECISION))
  }
  
  @Test def backPropPoolingBatch(): Unit = {
    val prevLayerOutputs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}A_prev_pool.txt")
    //val outputs = Pooling.poolLayerBatch(prevOutputs, 2, 1, Pooling.maxPool)
    val dAs = NumpyArrayLoader.loadMultiChannelDataSet(s"${BASE_PATH}dA_pool.txt")
    
    
    def mciMean(mcis: DenseMatrix4d): Double = {
      val lMeans = mcis.map(mci => mci.map(mean(_))).flatten
      lMeans.sum / lMeans.size
    }
    
    var dAPrevs = Pooling.backPropPoolLayerBatch(dAs, prevLayerOutputs, 2, 1, true)
    var lMeans = mciMean(dAPrevs)
    assertEquals(0.0777140814556, lMeans, PRECISION)
    
    dAPrevs = Pooling.backPropPoolLayerBatch(dAs, prevLayerOutputs, 2, 1, false)
    lMeans = mciMean(dAPrevs)
    assertEquals(0.0777140814556, lMeans, PRECISION)
  }

}

object PoolingTest {
  def getAPrev(): DenseMatrix4d = {
    val s = """
[
[[[ 1.62434536 -0.61175641]
  [-0.52817175 -1.07296862]
  [ 0.86540763 -2.3015387 ]]

 [[ 1.74481176 -0.7612069 ]
  [ 0.3190391  -0.24937038]
  [ 1.46210794 -2.06014071]]

 [[-0.3224172  -0.38405435]
  [ 1.13376944 -1.09989127]
  [-0.17242821 -0.87785842]]

 [[ 0.04221375  0.58281521]
  [-1.10061918  1.14472371]
  [ 0.90159072  0.50249434]]

 [[ 0.90085595 -0.68372786]
  [-0.12289023 -0.93576943]
  [-0.26788808  0.53035547]]]

][[[-0.69166075 -0.39675353]
  [-0.6871727  -0.84520564]
  [-0.67124613 -0.0126646 ]]

 [[-1.11731035  0.2344157 ]
  [ 1.65980218  0.74204416]
  [-0.19183555 -0.88762896]]

 [[-0.74715829  1.6924546 ]
  [ 0.05080775 -0.63699565]
  [ 0.19091548  2.10025514]]

 [[ 0.12015895  0.61720311]
  [ 0.30017032 -0.35224985]
  [-1.1425182  -0.34934272]]

 [[-0.20889423  0.58662319]
  [ 0.83898341  0.93110208]
  [ 0.28558733  0.88514116]]]

][[[-0.75439794  1.25286816]
  [ 0.51292982 -0.29809284]
  [ 0.48851815 -0.07557171]]

 [[ 1.13162939  1.51981682]
  [ 2.18557541 -1.39649634]
  [-1.44411381 -0.50446586]]

 [[ 0.16003707  0.87616892]
  [ 0.31563495 -2.02220122]
  [-0.30620401  0.82797464]]

 [[ 0.23009474  0.76201118]
  [-0.22232814 -0.20075807]
  [ 0.18656139  0.41005165]]

 [[ 0.19829972  0.11900865]
  [-0.67066229  0.37756379]
  [ 0.12182127  1.12948391]]]

][[[ 1.19891788  0.18515642]
  [-0.37528495 -0.63873041]
  [ 0.42349435  0.07734007]]

 [[-0.34385368  0.04359686]
  [-0.62000084  0.69803203]
  [-0.44712856  1.2245077 ]]

 [[ 0.40349164  0.59357852]
  [-1.09491185  0.16938243]
  [ 0.74055645 -0.9537006 ]]

 [[-0.26621851  0.03261455]
  [-1.37311732  0.31515939]
  [ 0.84616065 -0.85951594]]

 [[ 0.35054598 -1.31228341]
  [-0.03869551 -1.61577235]
  [ 1.12141771  0.40890054]]]

][[[-0.02461696 -0.77516162]
  [ 1.27375593  1.96710175]
  [-1.85798186  1.23616403]]

 [[ 1.62765075  0.3380117 ]
  [-1.19926803  0.86334532]
  [-0.1809203  -0.60392063]]

 [[-1.23005814  0.5505375 ]
  [ 0.79280687 -0.62353073]
  [ 0.52057634 -1.14434139]]

 [[ 0.80186103  0.0465673 ]
  [-0.18656977 -0.10174587]
  [ 0.86888616  0.75041164]]

 [[ 0.52946532  0.13770121]
  [ 0.07782113  0.61838026]
  [ 0.23249456  0.68255141]]]

] 
           """
    NumpyArrayLoaderTest.toMultiChannelData(s)
  }

  def getActivations(): DenseMatrix4d = {
    val s = """
[
[[[ 1.74481176 -0.24937038]
  [ 1.46210794 -0.24937038]]

 [[ 1.74481176 -0.24937038]
  [ 1.46210794 -0.24937038]]

 [[ 1.13376944  1.14472371]
  [ 1.13376944  1.14472371]]

 [[ 0.90085595  1.14472371]
  [ 0.90159072  1.14472371]]]

][[[ 1.65980218  0.74204416]
  [ 1.65980218  0.74204416]]

 [[ 1.65980218  1.6924546 ]
  [ 1.65980218  2.10025514]]

 [[ 0.30017032  1.6924546 ]
  [ 0.30017032  2.10025514]]

 [[ 0.83898341  0.93110208]
  [ 0.83898341  0.93110208]]]

][[[ 2.18557541  1.51981682]
  [ 2.18557541 -0.07557171]]

 [[ 2.18557541  1.51981682]
  [ 2.18557541  0.82797464]]

 [[ 0.31563495  0.87616892]
  [ 0.31563495  0.82797464]]

 [[ 0.23009474  0.76201118]
  [ 0.18656139  1.12948391]]]

][[[ 1.19891788  0.69803203]
  [ 0.42349435  1.2245077 ]]

 [[ 0.40349164  0.69803203]
  [ 0.74055645  1.2245077 ]]

 [[ 0.40349164  0.59357852]
  [ 0.84616065  0.31515939]]

 [[ 0.35054598  0.31515939]
  [ 1.12141771  0.40890054]]]

][[[ 1.62765075  1.96710175]
  [ 1.27375593  1.96710175]]

 [[ 1.62765075  0.86334532]
  [ 0.79280687  0.86334532]]

 [[ 0.80186103  0.5505375 ]
  [ 0.86888616  0.75041164]]

 [[ 0.80186103  0.61838026]
  [ 0.86888616  0.75041164]]]

]      
      """
    NumpyArrayLoaderTest.toMultiChannelData(s)

  }
}