/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import org.junit.Assert._
import org.junit.Test

import com.vbs.nn.params.LGradients
import com.vbs.nn.params.Parameters
import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.OptimizedParamsSimple
import com.vbs.nn.params.StaticWeightsAndBiasesGenerator._

import OptimizerFunctions._
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.params.OptimizedParamsSetMomentum
import com.vbs.nn.utils.MatrixUtils._
import com.vbs.nn.utils.MatrixTestUtils._
import com.vbs.nn.params.LayerDeltas

class OptimizerFunctionsTest {
  val PRECISION = 0.0000001

  @Test def updateSingleWeightsSimple(): Unit = {

    val (wb1, grads1) = layer1TestData
    val optimizedParams = optimizeSimpleL(0.5, wb1, grads1)
    assertEquals(grads1, optimizedParams.gradients)
    verifyLayerSimple("layer1", wb1, grads1, optimizedParams)

  }
  @Test def updateSingleWeightsSimple1(): Unit = {

    val (wb, grads) = layer1TestData
    val (uW, uB) = optimizeSimpleL(0.5, wb.weights, wb.biases, grads.dWeights, grads.dBiases)
    for (r <- 0 until wb.weights.rows) {
      for (c <- 0 until wb.weights.cols) assertEquals(uW(r, c), wb.weights(r, c) - 0.5 * grads.dWeights(r, c), PRECISION)
    }
    for (i <- 0 until wb.biases.size) assertEquals(uB(i), wb.biases(i) - 0.5 * grads.dBiases(i), PRECISION)

  }

  @Test def multiLayerSimple(): Unit = {
    val (wbs, grads) = multiLayerTestData()
    val optimizedParamSet = optimizeSimple(0.5, wbs, grads)
    verifyLayerSimple("layer1", wbs(0), grads(0), optimizedParamSet.params(0))
    verifyLayerSimple("layer2", wbs(1), grads(1), optimizedParamSet.params(1))

  }

  @Test def multiLayerMomentum(): Unit = {
    val (wbs, grads, vs) = momentumTestData()
    val optimizedParamSet: OptimizedParamsSetMomentum = optimizeMomentum(0.01, 0.9, wbs, grads, vs)

    val expectedWts = toDenseMatrixArray("""[
[[ 1.62544598 -0.61290114 -0.52907334]
 [-1.07347112  0.86450677 -2.30085497]]
[[ 0.31930698 -0.24990073  1.4627996 ]
 [-2.05974396 -0.32173003 -0.38320915]
 [ 1.13444069 -1.0998786  -0.1713109 ]]       
      ]""")
    val wts = optimizedParamSet.params.map(p => p.weightsAndBiases.weights)
    expectedWts.zip(wts).foreach(ea => assertMatricesEqual(ea._1, ea._2, PRECISION))

    val expectedBiases = toDenseVectorArray("""
      [[1.74493465 -0.76027113] [-0.87809283 0.04055394 0.58207317]]
      """)
    val biases = optimizedParamSet.params.map(p => p.weightsAndBiases.biases)
    expectedBiases.zip(biases).foreach(ea => assertVectorsEqual(ea._1, ea._2, PRECISION))

    val expectedVs = toDenseMatrixArray("""[
[[-0.11006192  0.11447237  0.09015907]
 [ 0.05024943  0.09008559 -0.06837279]]
 [[-0.02678881  0.05303555 -0.06916608]
 [-0.03967535 -0.06871727 -0.08452056]
 [-0.06712461 -0.00126646 -0.11173103]]
      ]""")

    val vws = optimizedParamSet.params.map(p => p.velocities.wDeltas)
    expectedVs.zip(vws).foreach(ea => assertMatricesEqual(ea._1, ea._2, PRECISION))

    val expectedvB = toDenseVectorArray("""
      [[-0.01228902 -0.09357694] [0.02344157 0.16598022 0.07420442]]
      """)
    val vbs = optimizedParamSet.params.map(p => p.velocities.bDeltas)
    expectedvB.zip(vbs).foreach(ea => assertVectorsEqual(ea._1, ea._2, PRECISION))

  }

  @Test def multiLayerAdam(): Unit = {
    val (wbs, grads) = adamTestData()
    val v = initializeDeltas(wbs)
    val s = initializeDeltas(wbs)

    val optimizedParamSet = optimizeAdam(0.01, 0.9, 0.999, 1e-8, 2, wbs, grads, v, s)
    val wts = optimizedParamSet.params.map(p => p.weightsAndBiases.weights)

    val expectedWts = toDenseMatrixArray("""[
 [[ 1.63178673 -0.61919778 -0.53561312]
 [-1.08040999  0.85796626 -2.29409733]]
[[ 0.32648046 -0.25681174  1.46954931]
 [-2.05269934 -0.31497584 -0.37661299]
 [ 1.14121081 -1.09244991 -0.16498684]] 
           ]""")

    expectedWts.zip(wts).foreach(ea => assertMatricesEqual(ea._1, ea._2, PRECISION))

    val expectedBiases = toDenseVectorArray("""[  
  [ 1.75225313 -0.75376553]  
  [-0.88529979 0.03477238 0.57537385]
      ]""")
    val biases = optimizedParamSet.params.map(p => p.weightsAndBiases.biases)
    expectedBiases.zip(biases).foreach(ea => assertVectorsEqual(ea._1, ea._2, PRECISION))

    val expectedAvgGradsW = toDenseMatrixArray("""[  
[[-0.11006192  0.11447237  0.09015907]
 [ 0.05024943  0.09008559 -0.06837279]]
[[-0.02678881  0.05303555 -0.06916608]
 [-0.03967535 -0.06871727 -0.08452056]
 [-0.06712461 -0.00126646 -0.11173103]] 
                       ]""")

    val uAvgGradsW = optimizedParamSet.params.map(p => p.avgGradients.wDeltas)
    expectedAvgGradsW.zip(uAvgGradsW).foreach(ea => assertMatricesEqual(ea._1, ea._2, PRECISION))

    val expectedAvgGradsB = toDenseVectorArray("""[
[-0.01228902 -0.09357694]
[ 0.02344157 0.16598022 0.07420442]
              ]""")
    val uAvgGradsB = optimizedParamSet.params.map(p => p.avgGradients.bDeltas)
    expectedAvgGradsB.zip(uAvgGradsB).foreach(ea => assertVectorsEqual(ea._1, ea._2, PRECISION))

    val expectedAvgSqGradsW = toDenseMatrixArray("""[  
[[ 0.00121136  0.00131039  0.00081287]
 [ 0.0002525   0.00081154  0.00046748]]
[[  7.17640232e-05   2.81276921e-04   4.78394595e-04]
 [  1.57413361e-04   4.72206320e-04   7.14372576e-04]
 [  4.50571368e-04   1.60392066e-07   1.24838242e-03]]
                        ]""")

    val uAvgSqGradsW = optimizedParamSet.params.map(p => p.avgSqGradients.wDeltas)
    expectedAvgSqGradsW.zip(uAvgSqGradsW).foreach(ea => assertMatricesEqual(ea._1, ea._2, PRECISION))

    val expectedAvgSqGradsB = toDenseVectorArray("""[
[  1.51020075e-05 8.75664434e-04]
[  5.49507194e-05  2.75494327e-03 5.50629536e-04]
              ]""")
    val uAvgSqGradsB = optimizedParamSet.params.map(p => p.avgSqGradients.bDeltas)
    expectedAvgSqGradsB.zip(uAvgSqGradsB).foreach(ea => assertVectorsEqual(ea._1, ea._2, PRECISION))

  }

  def multiLayerTestData(): (Array[LWeightsAndBiases], Array[LGradients]) = {
    val wbs = generateWeightsAndBiases(Array(4, 3, 2), "xavier")
    val dwb = generateWeightsAndBiases(Array(4, 3, 2), "xavier", 2, true).map(wb => (wb.weights, wb.biases))
    val aPrevs = wbs.map(wb => DenseMatrix.ones[Double](wb.weights.cols, 4))
    val grads = aPrevs.zip(dwb).map(adwb => LGradients(adwb._1, adwb._2._1, adwb._2._2))
    (wbs, grads)

  }
  def momentumTestData(): (Array[LWeightsAndBiases], Array[LGradients], Array[LayerDeltas]) = {
    var s = """
      weights:[
      [[ 1.62434536, -0.61175641, -0.52817175],
       [-1.07296862,  0.86540763, -2.3015387 ]]
      [[ 0.3190391 , -0.24937038,  1.46210794],
       [-2.06014071, -0.3224172 , -0.38405435],
       [ 1.13376944, -1.09989127, -0.17242821]]
      ]
      |
      biases:[
      [ 1.74481176 -0.7612069 ]
      [-0.87785842 0.04221375 0.58281521]
      ]
      """
    val wbs = Parameters(s).getLayerWeightsAndBiases().get

    s = """
      weights:[
      [[-1.10061918,  1.14472371,  0.90159072],
       [ 0.50249434,  0.90085595, -0.68372786]]
       
      [[-0.26788808,  0.53035547, -0.69166075],
       [-0.39675353, -0.6871727 , -0.84520564],
       [-0.67124613, -0.0126646 , -1.11731035]]
      ]
      |
      biases:[
      [-0.12289023 -0.93576943 ]
      [0.2344157  1.65980218 0.74204416]
      ]
      """
    val dwb = Parameters(s).getLayerWeightsAndBiases().get.map(wb => (wb.weights, wb.biases))
    val aPrevs = wbs.map(wb => DenseMatrix.ones[Double](wb.weights.cols, 4))
    val grads = aPrevs.zip(dwb).map(adwb => LGradients(adwb._1, adwb._2._1, adwb._2._2))

    (wbs, grads, initializeDeltas(wbs))

  }

  def adamTestData(): (Array[LWeightsAndBiases], Array[LGradients]) = {
    var s = """
      weights:[
      [[ 1.62434536, -0.61175641, -0.52817175],
       [-1.07296862,  0.86540763, -2.3015387 ]]
      [[ 0.3190391 , -0.24937038,  1.46210794],
       [-2.06014071, -0.3224172 , -0.38405435],
       [ 1.13376944, -1.09989127, -0.17242821]]
       ]
       |
      biases:[
      [ 1.74481176 -0.7612069 ]
      [-0.87785842 0.04221375 0.58281521]
            ]            
      """
    val wbs = Parameters(s).getLayerWeightsAndBiases().get

    // Grads:
    s = """
      weights:[
      [[-1.10061918,  1.14472371,  0.90159072],
       [ 0.50249434,  0.90085595, -0.68372786]]
      [[-0.26788808,  0.53035547, -0.69166075],
       [-0.39675353, -0.6871727 , -0.84520564],
       [-0.67124613, -0.0126646 , -1.11731035]]
       ]
       |
      biases:[
      [ -0.12289023 -0.93576943 ]
      [0.2344157  1.65980218 0.74204416]
            ]            
      """
    val dwb = Parameters(s).getLayerWeightsAndBiases().get.map(wb => (wb.weights, wb.biases))
    val aPrevs = wbs.map(wb => DenseMatrix.ones[Double](wb.weights.cols, 4))
    val grads = aPrevs.zip(dwb).map(adwb => LGradients(adwb._1, adwb._2._1, adwb._2._2))

    (wbs, grads)
  }

  def layer1TestData(): (LWeightsAndBiases, LGradients) = {
    val w = DenseMatrix((2.0, 3.0, 4.0), (5.0, 6.0, 7.0))
    val b = DenseVector(8.0, 9.0)

    val dW = DenseMatrix((0.2, 0.3, 0.4), (0.5, 0.6, 0.7))
    val db = DenseVector(0.8, 0.9)
    val aPrev = DenseMatrix.ones[Double](dW.cols, 4)
    (LWeightsAndBiases(w, b), LGradients(aPrev, dW, db))
  }

  def verifyLayerSimple(s: String, wb: LWeightsAndBiases, grads: LGradients, optimizedParams: OptimizedParamsSimple): Unit = {
    for (r <- 0 until wb.weights.rows) {
      for (c <- 0 until wb.weights.cols) assertEquals(s, optimizedParams.weightsAndBiases.weights(r, c), wb.weights(r, c) - 0.5 * grads.dWeights(r, c), PRECISION)
    }
    for (i <- 0 until wb.biases.size) assertEquals(optimizedParams.weightsAndBiases.biases(i), wb.biases(i) - 0.5 * grads.dBiases(i), PRECISION)

  }

}