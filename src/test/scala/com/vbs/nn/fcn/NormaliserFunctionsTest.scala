/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import org.junit.Assert._
import org.junit.Test

import breeze.linalg.DenseMatrix

class NormaliserFunctionsTest {
  
  @Test def imageNormaliserInplaceAndReturned(): Unit = {
    val dm = new DenseMatrix(2,3, (1 to 6).toArray).map(_.toDouble)
    val r = NormaliserFunctions.normaliseImage(dm)
    val ra = r.toArray
    for(i <- 1 to ra.size) assertEquals(i.toDouble / 255, ra(i - 1), 0.001)
    // check In place
    assertEquals(dm,r)
  }
  
  @Test def meanNormaliserInplaceAndReturned(): Unit = {
    val dm = new DenseMatrix(2,3, (1 to 6).toArray).map(_.toDouble)
    val dmc = dm.copy

    val r = NormaliserFunctions.normaliseMean(dm)
    assertNotEquals(dmc, r)
    assertEquals(dm,r)
  }
  
}