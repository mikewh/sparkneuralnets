/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import org.junit.Test
import org.junit.Assert._
import BackPropagationFunctions._
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import breeze.linalg._
import breeze.numerics._

import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.LGradients
import com.vbs.nn.params.ActivationParams
import ActivationFunctions._

/**
 * Tests back propagation through an FCN layer by layer and as a single call
 */
class BackPropagationTest {

  val PRECISION = 0.0000001

  /**
   * Replicate all steps of the calculation using test data provided by x (note activation values are not as would be generated)
   */
  @Test def firstPrinciples(): Unit = {
    val (targets, outputsAndActivations, weightsAndBiases) = getDefaultBackPropTestData()
    val aLast = outputsAndActivations(2)._2
    val dALast = sigmoidBackwardOutputLayer(aLast, targets)
    assertEquals(-0.5590876, dALast(0, 0), PRECISION)
    assertEquals(1.77465392, dALast(0, 1), PRECISION)

    val zLast = outputsAndActivations(2)._1
    val correctActivationlast = sigmoid(zLast)
    val dz2 = dALast :* (correctActivationlast :* (1.0 - correctActivationlast))
    assertEquals(-0.1261204, dz2(0, 0), PRECISION)
    assertEquals(0.42987761, dz2(0, 1), PRECISION)

    val dZ2 = sigmoidBackwardL(dALast, (null, correctActivationlast))
    assertEquals(dz2, dZ2)

    val a2 = outputsAndActivations(1)._2
    val wb2 = weightsAndBiases(1)
    val grads2_1 = linearBackwardL(dZ2, a2, wb2, 0.0)
    verifyGrads2(grads2_1)
    val params = new ActivationParams()

    params.activationName = "sigmoid"
    val sigmoidFcn = getLayerBackPropFunction(params)
    val grads2 = sigmoidFcn(dALast, outputsAndActivations(1)._2, (outputsAndActivations(2)._1, correctActivationlast), weightsAndBiases(1), 0.0)
    verifyGrads2(grads2)

  }

  /**
   * Test each layer separately
   */
  @Test def layerByLayer(): Unit = {
    val (targets, outputsAndActivations, weightsAndBiases) = getDefaultBackPropTestData()
    val aLast = outputsAndActivations.last._2
    val dALast = sigmoidBackwardOutputLayer(aLast, targets)

    val params = new ActivationParams()
    params.activationName = "sigmoidr"
    val sigmoidFcn = getLayerBackPropFunction(params)
    val grads2 = sigmoidFcn(dALast, outputsAndActivations(1)._2, outputsAndActivations(2), weightsAndBiases(1), 0.0)
    verifyGrads2(grads2)

    val da2 = grads2.dActivationsPrev
    params.activationName = "relu"
    val reluFcn = getLayerBackPropFunction(params)
    val grads1 = reluFcn(da2, outputsAndActivations(0)._2, outputsAndActivations(1), weightsAndBiases(0), 0.0)
    verifyGrads1(grads1)

  }


  def verifyGrads2(grads: LGradients): Unit = {
    val da = grads.dActivationsPrev
    assertEquals(0.12913162, da(0, 0), PRECISION)
    assertEquals(-0.44014127, da(0, 1), PRECISION)
    assertEquals(-0.14175655, da(1, 0), PRECISION)
    assertEquals(0.48317296, da(1, 1), PRECISION)
    assertEquals(0.01663708, da(2, 0), PRECISION)
    assertEquals(-0.05670698, da(2, 1), PRECISION)

    val dw = grads.dWeights
    assertEquals(-0.39202432, dw(0, 0), PRECISION)
    assertEquals(-0.13325855, dw(0, 1), PRECISION)
    assertEquals(-0.04601089, dw(0, 2), PRECISION)

    assertEquals(0.15187861, grads.dBiases(0), PRECISION)

  }

  def verifyGrads1(grads: LGradients): Unit = {
    val da = grads.dActivationsPrev
    assertEquals(0.0, da(0, 0), PRECISION)
    assertEquals(0.52257901, da(0, 1), PRECISION)
    assertEquals(0.0, da(1, 0), PRECISION)
    assertEquals(-0.3269206, da(1, 1), PRECISION)
    assertEquals(0.0, da(2, 0), PRECISION)
    assertEquals(-0.32070404, da(2, 1), PRECISION)
    assertEquals(0.0, da(3, 0), PRECISION)
    assertEquals(-0.74079187, da(3, 1), PRECISION)

    val dw = grads.dWeights
    assertEquals(0.41010002, dw(0, 0), PRECISION)
    assertEquals(0.07807203, dw(0, 1), PRECISION)
    assertEquals(0.13798444, dw(0, 2), PRECISION)
    assertEquals(0.10502167, dw(0, 3), PRECISION)

    assertEquals(0.0, dw(1, 0), PRECISION)
    assertEquals(0.0, dw(1, 1), PRECISION)
    assertEquals(0.0, dw(1, 2), PRECISION)
    assertEquals(0.0, dw(1, 3), PRECISION)

    assertEquals(0.05283652, dw(2, 0), PRECISION)
    assertEquals(0.01005865, dw(2, 1), PRECISION)
    assertEquals(0.01777766, dw(2, 2), PRECISION)
    assertEquals(0.0135308, dw(2, 3), PRECISION)

    assertEquals(-0.22007063, grads.dBiases(0), PRECISION)
    assertEquals(0.0, grads.dBiases(1), PRECISION)
    assertEquals(-0.02835349, grads.dBiases(2), PRECISION)

  }

  def correctActivations(outputsAndActivations: Array[OutputsAndActivationsL]): Array[OutputsAndActivationsL] = {
    val zs = outputsAndActivations.map(_._1)
    val relus = zs.dropRight(1).map(dm => {
      (dm, dm.map(z => Math.max(0.0, z)))
    })
    val sigmoids = (zs.last, sigmoid(zs.last))
    relus :+ sigmoids
  }

  def getDefaultBackPropTestData(): (DenseMatrix[Double], Array[OutputsAndActivationsL], Array[LWeightsAndBiases]) = {
    val aLast = DenseMatrix((1.78862847, 0.43650985))
    val zLast = DenseMatrix((0.64667545, -0.35627076))
    val a2 = DenseMatrix((1.97611078, -1.24412333),
      (-0.62641691, -0.80376609),
      (-2.41908317, -0.92379202))
    val z2 = DenseMatrix((-0.7129932, 0.62524497),
      (-0.16051336, -0.76883635),
      (-0.23003072, 0.74505627))
    val a1 = DenseMatrix((0.09649747, -1.8634927),
      (-0.2773882, -0.35475898),
      (-0.08274148, -0.62700068),
      (-0.04381817, -0.47721803))
    val z1Unused = DenseMatrix.ones[Double](a1.rows, a1.cols)

    val oAndA = Array((z1Unused, a1), (z2, a2), (zLast, aLast))

    val y = DenseMatrix((1.0, 0.0))

    val w2 = DenseMatrix((-1.02387576, 1.12397796, -0.13191423))
    val b2 = DenseVector(-1.62328545)
    val wb2 = LWeightsAndBiases(w2, b2)
    val w1 = DenseMatrix((-1.31386475, 0.88462238, 0.88131804, 1.70957306),
      (0.05003364, -0.40467741, -0.54535995, -1.54647732),
      (0.98236743, -1.10106763, -1.18504653, -0.2056499))
    val b1 = DenseVector(1.48614836, 0.23671627, -1.02378514)
    val wb1 = LWeightsAndBiases(w1, b1)
    val wbs = Array(wb1, wb2)
    (y, oAndA, wbs)
  }

}