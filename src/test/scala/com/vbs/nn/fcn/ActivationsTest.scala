/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import org.junit.Test
import org.junit.Assert._
import ActivationFunctions._
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import breeze.linalg._
import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.ActivationParams
import com.vbs.nn.params.WeightsAndBiasesInitializer._

class ActivationsTest {

  @Test def checkDimensions(): Unit = {
    val x = DenseMatrix.ones[Double](4, 3)
    val wb = generateWeightsAndBiases(Array(4, 5), "gaussian")(0)
    val a = linearForwardL(x, wb)
    assertEquals(5, a.rows)
    assertEquals(3, a.cols)
  }
  
  @Test def testSoftmax(): Unit = {
    import ActivationsTest._
 
    val (x, wb) = inputsAndWeightsTestData
    val dm = softmaxForwardL(x, wb(0))
    assertEquals(dm._1.rows, dm._2.rows)
    assertEquals(dm._1.cols, dm._2.cols)
    val totals = for (c <- 0 until dm._2.cols) yield (sum(dm._2(::, c)))
    totals.foreach(total => assertEquals(total, 1.0, 0.0000001))
  }

  @Test def linearForwardSingleOut(): Unit = {
    val x = DenseMatrix((1.62434536, -0.61175641), (-0.52817175, -1.07296862), (0.86540763, -2.3015387))
    val w = DenseMatrix((1.74481176, -0.7612069, 0.3190391))
    val b = DenseVector((-0.24937038))
    val wb = LWeightsAndBiases(w, b)
    val a = linearForwardL(x, wb)
    assertEquals(1, a.rows)
    assertEquals(2, a.cols)
    assertEquals(3.26295337, a(0, 0), 0.0000001)
    assertEquals(-1.23429987, a(0, 1), 0.0000001)
  }

  @Test def nonLinearLayerActivation(): Unit = {
    val x = DenseMatrix((-0.41675785, -0.05626683), (-2.1361961, 1.64027081), (-1.79343559, -0.84174737))
    val w = DenseMatrix((0.50288142, -1.24528809, -1.05795222))
    val b = DenseVector((-0.90900761))
    val wb = LWeightsAndBiases(w, b)

    def checkZ(z: DenseMatrix[Double]): Unit = {
      assertEquals(3.438961335, z(0, 0), 0.0000001)
      assertEquals(-2.08938435, z(0, 1), 0.0000001)
    }

    val params = new ActivationParams()
    params.activationName = "sigmoid"

    var za = getLayerActivationFunction(params)(x, wb)
    var a = za._2
    checkZ(za._1)
    assertEquals(0.96890023, a(0, 0), 0.0000001)
    assertEquals(0.11013289, a(0, 1), 0.0000001)

    params.activationName = "relu"
    za = getLayerActivationFunction(params)(x, wb)
    a = za._2
    checkZ(za._1)
    assertEquals(3.43896131, a(0, 0), 0.0000001)
    assertEquals(0.0, a(0, 1), 0.0000001)

    params.activationName = "softmax"
    za = getLayerActivationFunction(params)(x, wb)
    a = za._2
    checkZ(za._1)
    assertEquals(1.0, a(0, 0), 0.0000001)
    assertEquals(1.0 , a(0, 1), 0.0000001)

    params.activationName = "leaky_relu"
    params.epsilon = 0.0001
    za = getLayerActivationFunction(params)(x, wb)
    a = za._2
    checkZ(za._1)
    assertEquals(3.43896131, a(0, 0), 0.0000001)
    assertEquals(0.0001, a(0, 1), 0.0000001)
  }

  @Test def invalidActivationRequests(): Unit = {
    val x = DenseMatrix((-0.41675785, -0.05626683), (-2.1361961, 1.64027081), (-1.79343559, -0.84174737))
    val w = DenseMatrix((0.50288142, -1.24528809, -1.05795222))
    val b = DenseVector((-0.90900761))
    val wb = LWeightsAndBiases(w, b)
    val params = new ActivationParams()
    params.activationName = "rubbish"

    try {
      getLayerActivationFunction(params)(x, wb)
      fail("Call should have failed with IllegalArgumentException")
    } catch {
      case e: IllegalArgumentException => assertEquals("Unsupported activation function: 'rubbish'", e.getMessage)
      case _: Throwable                => fail("Expecteding IllegalArgumentException")
    }
    params.activationName = "leaky_relu"

    try {
      getLayerActivationFunction(params)(x, wb)
      fail("Call should have failed with IllegalArgumentException")
    } catch {
      case e: IllegalArgumentException => assertEquals("requirement failed: epsilon must be > 0 for 'leaky_relu', but got 0.0", e.getMessage)
      case _: Throwable                => fail("Expecteding IllegalArgumentException")
    }
  }

  @Test def feedForward3Layer(): Unit = {
    def toParams(s: String): ActivationParams = {
      val p = new ActivationParams()
      p.activationName = s
      p
    }
    val params = Array("relu", "relu", "sigmoid").map(toParams)
    val (x, wbs) = ActivationsTest.inputsAndWeightsTestData()
    val activationFunctions = getActivationFunctions(params)
    assertEquals(params.size, activationFunctions.size)
    val feedForwardFcn = feedForward(activationFunctions)(_, _, _)

    val outputsAndActivations = feedForwardFcn(x, wbs, None)
    assertEquals(params.size, outputsAndActivations.size)

    val al = outputsAndActivations.last._2
    assertEquals(x.cols, al.cols)
    assertEquals(0.03921668, al(0, 0), 0.0000001)
    assertEquals(0.70498921, al(0, 1), 0.0000001)
    assertEquals(0.19734387, al(0, 2), 0.0000001)
    assertEquals(0.04728177, al(0, 3), 0.0000001)
  }

}

object ActivationsTest {
  def inputsAndWeightsTestData(): (DenseMatrix[Double], Array[LWeightsAndBiases]) = {
    val x = DenseMatrix((-0.31178367, 0.72900392, 0.21782079, -0.8990918),
      (-2.48678065, 0.91325152, 1.12706373, -1.51409323),
      (1.63929108, -0.4298936, 2.63128056, 0.60182225),
      (-0.33588161, 1.23773784, 0.11112817, 0.12915125),
      (0.07612761, -0.15512816, 0.63422534, 0.810655))

    val w1 = DenseMatrix((0.35480861, 1.81259031, -1.3564758, -0.46363197, 0.82465384),
      (-1.17643148, 1.56448966, 0.71270509, -0.1810066, 0.53419953),
      (-0.58661296, -1.48185327, 0.85724762, 0.94309899, 0.11444143),
      (-0.02195668, -2.12714455, -0.83440747, -0.46550831, 0.23371059))

    val b1 = DenseVector(1.38503523, -0.51962709, -0.78015214, 0.95560959)

    val w2 = DenseMatrix((-0.12673638, -1.36861282, 1.21848065, -0.85750144),
      (-0.56147088, -1.0335199, 0.35877096, 1.07368134),
      (-0.37550472, 0.39636757, -0.47144628, 2.33660781))
    val b2 = DenseVector(1.50278553, -0.59545972, 0.52834106)

    val w3 = DenseMatrix((0.9398248, 0.42628539, -0.75815703))
    val b3 = DenseVector((-0.16236698))
    val wbs = Array(LWeightsAndBiases(w1, b1), LWeightsAndBiases(w2, b2), LWeightsAndBiases(w3, b3))
    (x, wbs)

  }
}