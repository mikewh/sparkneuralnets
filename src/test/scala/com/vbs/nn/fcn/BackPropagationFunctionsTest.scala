/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import org.junit.Test
import org.junit.Assert._
import BackPropagationFunctions._
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import breeze.linalg._
import breeze.numerics._

import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.LGradients
import com.vbs.nn.params.ActivationParams
/**
 * Tests individual components of FCN back propagation 
 */
class BackPropagationFunctionsTest {
  
  val PRECISION = 0.0000001


  @Test def linearBackwardForLayer(): Unit = {
    val dZ = DenseMatrix((1.62434536, -0.61175641))
    val aPrev = DenseMatrix((-0.52817175, -1.07296862),
      (0.86540763, -2.3015387),
      (1.74481176, -0.7612069))

    val w = DenseMatrix((0.3190391, -0.24937038, 1.46210794))
    val b = DenseVector(-2.06014071)

    val wb = LWeightsAndBiases(w, b)
    val lgrads = linearBackwardL(dZ, aPrev, wb, 0.0)

    verifyGradients(lgrads, Array(0.51822968, -0.19517421, -0.40506361, 0.15255393, 2.37496825, -0.89445391),
      Array(-0.10076895, 1.40685096, 1.64992505), Array(0.50629448))
  }
  
  @Test def sigmoidBackwardForOutput(): Unit = {
    val aLast = DenseMatrix((1.78862847,  0.43650985))
    val y = DenseMatrix((1.0,  0.0))
    val dA = sigmoidBackwardOutputLayer(aLast, y)
    assertEquals(-0.5590876, dA(0, 0), PRECISION)
    assertEquals(1.77465392, dA(0, 1), PRECISION)
  }

  @Test def sigmoidBackwardForLayer(): Unit = {
    val (_, dA, outputs, _) = getDefaultBackPropTestData
    val activations = sigmoid(outputs)
    val dZ = sigmoidBackwardL(dA, (outputs, activations))
    assertEquals(-0.10414453, dZ(0, 0), PRECISION)
    assertEquals(-0.01044791, dZ(0, 1), PRECISION)
  }

  @Test def reluBackwardForLayer(): Unit = {
    val (_, dA, outputs, _) = getDefaultBackPropTestData
    val dZ = reluBackwardL(dA, (outputs, null))
    assertEquals(-0.41675785, dZ(0, 0), PRECISION)
    assertEquals(0.0, dZ(0, 1), PRECISION)
  }

  @Test def sigmoidBackPropForLayer(): Unit = {
    def verifyLgrads(lgrads: LGradients): Unit = {
     verifyGradients(lgrads, Array(0.11017994, 0.01105339, 0.09466817, 0.00949723, -0.05743092, -0.00576154),
      Array(0.10266786, 0.09778551, -0.01968084), Array(-0.05729622))
     
    }
    
    val (aPrev, dA, outputs, wb) = getDefaultBackPropTestData
    val activations = sigmoid(outputs)

    val sigmoidFcn = sigmoidBackwardL(_, _)
    verifyLgrads(backPropL(sigmoidFcn)(dA, aPrev, (outputs, activations), wb, 0.0))
    
    val params = new ActivationParams()
    params.activationName = "sigmoid"
    val fcn = getLayerBackPropFunction(params)
    verifyLgrads(fcn(dA, aPrev, (outputs, activations), wb, 0.0))
  }

  @Test def reluBackPropForLayer(): Unit = {
    def verifyLgrads(lgrads: LGradients): Unit = {
     verifyGradients(lgrads, Array(0.44090989, 0.0, 0.37883606, 0.0, -0.2298228, 0.0),
      Array(0.44513824, 0.37371418, -0.10478989), Array(-0.20837892))
     
    }
    
    val (aPrev, dA, outputs, wb) = getDefaultBackPropTestData

    val reluFcn = reluBackwardL(_, _)
    verifyLgrads(backPropL(reluFcn)(dA, aPrev, (outputs, null), wb, 0.0))
    
    val params = new ActivationParams()
    params.activationName = "relu"
    val fcn = getLayerBackPropFunction(params)
    verifyLgrads(fcn(dA, aPrev, (outputs, null), wb, 0.0))
    
    params.activationName = "leaky_relu"
    params.epsilon = 0.00001
    val leakyFcn = getLayerBackPropFunction(params)
    verifyLgrads(leakyFcn(dA, aPrev, (outputs, null), wb, 0.0))

  }

  def getDefaultBackPropTestData(): (DenseMatrix[Double], DenseMatrix[Double], DenseMatrix[Double], LWeightsAndBiases) = {
    val dA = DenseMatrix((-0.41675785, -0.05626683))
    val aPrev = DenseMatrix((-2.1361961, 1.64027081),
      (-1.79343559, -0.84174737),
      (0.50288142, -1.24528809))
    val outputs = DenseMatrix((0.04153939, -1.11792545))
    val w = DenseMatrix((-1.05795222, -0.90900761, 0.55145404))
    val b = DenseVector(2.29220801)
    val wb = LWeightsAndBiases(w, b)
    (aPrev, dA, outputs, wb)
  }

  def verifyGradients(lgrads: LGradients, daPrev: Array[Double], dw: Array[Double], db: Array[Double]): Unit = {
    lgrads.dActivationsPrev.t.toArray.zip(daPrev).foreach(estimatedActual =>
      assertEquals("dActivationsPrev", estimatedActual._1, estimatedActual._2, PRECISION))
    lgrads.dWeights.toArray.zip(dw).foreach(estimatedActual =>
      assertEquals("dWeights", estimatedActual._1, estimatedActual._2, PRECISION))
    lgrads.dBiases.toArray.zip(db).foreach(estimatedActual =>
      assertEquals("dBiases", estimatedActual._1, estimatedActual._2, PRECISION))
  }

}