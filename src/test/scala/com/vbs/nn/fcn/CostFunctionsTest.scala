/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import org.junit.Assert._
import org.junit.Assert.fail
import org.junit.Test

import com.vbs.nn.utils.MatrixUtils

import CostFunctions.getCostFunction
import breeze.linalg.DenseMatrix

class CostFunctionsTest {

  @Test def invalidCalls(): Unit = {

    try {
      getCostFunction("rubbish")
      fail("Call should have failed with IllegalArgumentException")
    } catch {
      case e: IllegalArgumentException => assertEquals("Unsupported cost function: 'rubbish'", e.getMessage)
      case _: Throwable                => fail("Expecteding IllegalArgumentException")
    }

    val y = DenseMatrix.ones[Double](4, 3)
    val al = DenseMatrix.ones[Double](5, 6)
    val cfcn = getCostFunction("crossentropy")
    try {
      cfcn(al, y)
      fail("Call should have failed with IllegalArgumentException")
    } catch {
      case e: IllegalArgumentException => assertEquals("requirement failed: last layer activations(5, 6) and targets(4, 3) do not have the same dimensions",
        e.getMessage)
      case _: Throwable => fail("Expecteding IllegalArgumentException")
    }

  }

  @Test def crossEntropySingleTargetValue(): Unit = {
    val y = DenseMatrix((1.0, 1.0, 1.0))
    val al = DenseMatrix((0.8, 0.9, 0.4))
    val cfcn = getCostFunction("crossentropy")
    val cost = cfcn(al, y)
    assertEquals(0.414931599615, cost, 0.0000000001)

  }

  @Test def crossEntropyMultiTargetValue(): Unit = {
    val y = DenseMatrix.ones[Double](4, 3)
    val al = DenseMatrix.ones[Double](4, 3) :- 0.1
    val cfcn = getCostFunction("crossentropy")
    val cost = cfcn(al, y)
    assertEquals(0.42144206263, cost, 0.0000000001)
  }

  @Test def miniBatchCrossEntropy(): Unit = {
    val al = MatrixUtils.toDenseMatrix("""
 [[0.15213189315168135 0.1524999582656275 0.1517876003792884 0.15203074629297694]
[0.060003130379032626 0.05982002764460486 0.06015042003004064 0.06002360038068875]
[0.05982921721660859 0.05935553007583599 0.05960309316412191 0.05972686364757177]
[0.14838272921320247 0.14817658844007447 0.14920908289733656 0.14861415671755426]
[0.08499621429639627 0.0843182633456915 0.08395028881234642 0.08486984218271636]
[0.28505223884496156 0.2859533879752575 0.28485637492713134 0.28435885187093995]
[0.045307310201450665 0.04550610062979364 0.04564483144736693 0.045572574686759376]
[0.06365564961815605 0.06339010558211178 0.06356342980261864 0.06350518436855355]
[0.08284674974001201 0.08322557859286268 0.08341357219734365 0.08347712348266133]
[0.01779486733849837 0.01775445944814009 0.017821306342405367 0.017821056369577634]]      
      """)

    val y = MatrixUtils.toDenseMatrix("""
[[0.0 0.0 0.0 0.0]
[0.0 0.0 0.0 0.0]
[0.0 0.0 0.0 0.0]
[0.0 1.0 0.0 0.0]
[0.0 0.0 0.0 0.0]
[1.0 0.0 0.0 0.0]
[0.0 0.0 0.0 0.0]
[0.0 0.0 1.0 1.0]
[0.0 0.0 0.0 0.0]
[0.0 0.0 0.0 0.0]]
              """)

    val cfcn = getCostFunction("crossentropytf")
    val cost = cfcn(al, y)
    assertEquals(2.1691960206473166, cost, 0.0000000001)

  }

}