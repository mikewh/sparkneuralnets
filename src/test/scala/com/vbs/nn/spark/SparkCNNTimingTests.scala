/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.spark
import org.junit.Test

import org.apache.spark.sql.SparkSession
import com.vbs.nn.CNNFCNLinkedNetwork
import com.vbs.nn.sampler.CNNMiniBatchSampler
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.PoolingLayerParameters
import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.params.Parameters
import com.vbs.nn.hsigns.HSignsDataSetLoader

class SparkCNNTimingTests {
  val sparkSession = SparkSession
    .builder()
    .appName("SparkCNNTimingTests")
    .config("spark.master", "local")
    .getOrCreate()

  @Test def feedforwardTiming(): Unit = {
    var ts = System.currentTimeMillis()

    val (xs, ys) = HSignsDataSetLoader.getTrainingData()
    println(s"Loaded ${xs.size} training samples in ${System.currentTimeMillis() - ts}ms")

    val globalParams = Parameters()
      .setIterations(1)
      .setLearningRate(0.009)
      .setAdam(0.9, 0.999, 1e-08)

    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8, true),
        ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4, true))
      .setFilterWeightsAndBiasesInitializer("static_xavier")
    CNNFCNLinkedNetwork.initialise(cnnParams)
    
    val sc = sparkSession.sparkContext
    ts = System.currentTimeMillis()

    val samples = CNNMiniBatchSampler(ys, xs, 4, 1080 / 4, "static_random", true).miniBatches().map(sample => (SparkCNN.toRDD(sc)(sample._2)).cache)
    println(s"SparkCNNTimingTests.map ${samples(0).count} samples: ${System.currentTimeMillis() - ts}ms")

    val layerParams = cnnParams.getCNNLayerParams()
    ts = System.currentTimeMillis()
    val r = samples.map(sample => SparkCNN.feedForward(sc)(sample, layerParams))
    println(s"SparkCNNTimingTests.prepare ${r.size} time: ${System.currentTimeMillis() - ts}ms")
    ts = System.currentTimeMillis()
    r.map(_.collect)
    println(s"SparkCNNTimingTests.execute time: ${System.currentTimeMillis() - ts}ms")

    
  }

}