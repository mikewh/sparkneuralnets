/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.spark
import org.junit.Test
import org.junit.Assert._

import org.apache.spark.sql.SparkSession
import com.vbs.nn.CNNFCNLinkedNetwork
import com.vbs.nn.cnn.CNNFeedForwardTest
import com.vbs.nn.cnn.CNNTestUtils
import com.vbs.nn.tf.TensorflowAdam
import com.vbs.nn.tf.TensorflowRegression

class SparkCNNTest {
  val sparkSession = SparkSession
    .builder()
    .appName("SparkCNNTest")
    .config("spark.master", "local")
    .getOrCreate()

  @Test def feedforwardOneBatch(): Unit = {
    val sc = sparkSession.sparkContext
    val xs = TensorflowRegression.loadSampleX()
    val sampleX = sc.parallelize(xs)

    val (cnnParams, _) = TensorflowAdam.loadSampleRunParams()

    CNNFCNLinkedNetwork.initialise(cnnParams)
    val layerParams = cnnParams.getCNNLayerParams()
    val cnnLayerOutputs = SparkCNN.feedForward(sc)(sampleX, layerParams).collect
    val (e1, ecnnLayerOutputs) = CNNFeedForwardTest.feedForwardCNN()
    assertEquals(ecnnLayerOutputs.size, cnnLayerOutputs.size)
    for (i <- 0 until ecnnLayerOutputs.size) {
      for (j <- 0 until ecnnLayerOutputs(i).size) {
        val mci1 = ecnnLayerOutputs(i)(j)
        val mci2 = cnnLayerOutputs(i)(j)
        CNNTestUtils.assertMCIEquals(mci1._1, mci2._1)
        CNNTestUtils.assertMCIEquals(mci1._2, mci2._2)
        
      }
    }

  }

}