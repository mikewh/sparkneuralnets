/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.spark.sampler
import org.junit.Assert._
import org.junit.Test

import org.apache.spark.sql.SparkSession
import com.vbs.nn.hsigns.HSignsDataSetLoader

class SparkCNNSamplerTest {
  
  val sparkSession = SparkSession
    .builder()
    .appName("SparkCNNTimingTests")
    .config("spark.master", "local")
    .getOrCreate()

  @Test def loadDataSet(): Unit = {
    val sc = sparkSession.sparkContext
    var ts = System.currentTimeMillis()
    val (xs, ys) = HSignsDataSetLoader.getTrainingData()
    println(s"Loaded ${xs.size} training samples in ${System.currentTimeMillis() - ts}ms")

    val yv = (for (col <- 0 until ys.cols) yield (ys(::, col)))
    val randomSampler = new SparkCNNSampler(sc)(yv, xs, 16, 5, "random")
    ts = System.currentTimeMillis()
    val samples = randomSampler.miniBatches()
    assertEquals(5, samples.size)
    println(s"created ${samples.size}  samples in ${System.currentTimeMillis() - ts}ms")
  }

}