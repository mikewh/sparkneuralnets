/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn

import org.junit.Test
import com.vbs.nn.params.Parameters
import com.vbs.nn.fcn.ActivationFunctions
import com.vbs.nn.fcn.ActivationFunctions.ActivationFunction
import com.vbs.nn.fcn.ActivationFunctions.OutputsAndActivationsL
import com.vbs.nn.sampler.MiniBatchSampler
import com.vbs.nn.params.LWeightsAndBiases
import breeze.linalg.DenseMatrix
import breeze.linalg.max
import breeze.linalg.min
import com.vbs.nn.io.CSVLoader
import com.vbs.nn.io.CSVLoaderTest

/**
 * Batch Normalization not yet complete
 */
class BatchNormTrainingTests {
  val (xs, ys) = CSVLoader.getFeatureSetAndEncodedLabels(CSVLoaderTest.SMALL_TEST_FILE, true, 10)

  class DebugFCNImplementations(params: Parameters) extends DefaultFCNImplementations(params) {

    def executeFeedForward(activationFunctions: Array[ActivationFunction])(xs: DenseMatrix[Double],
                                                                           weightsAndBiases: Array[LWeightsAndBiases],
                                                                           dropouts: Option[(Array[Double], Array[DenseMatrix[Double]])]): Array[OutputsAndActivationsL] = {
      val outputsAndActivations = ActivationFunctions.feedForward(params.activationFunctions())(xs, weightsAndBiases, dropouts)
      val za = outputsAndActivations.map(_._1).filter(_.toArray.find(_ > 1.0).isDefined)
      val r = za.map(z => (min(z), max(z)))
      r foreach println
      outputsAndActivations
    }

    override def feedForward = {
      executeFeedForward(params.activationFunctions())(_, _, _)
    }
  }

  @Test def normalizeActivations(): Unit = {
    val params = Parameters()
      .setIterations(1)
      .setLearningRate(0.001)
      .setLayerDims(784, 20, 30, 10)
      .setWeightInitializer("static_xavier")
      .setActivations("relu", "relu", "softmax")
      .setCostFunction("crossentropy")
      .setOptimizer("simple")

    val fcnImpl = new DebugFCNImplementations(params)
    params.setFCNImplementations(fcnImpl)

    val sampler =  MiniBatchSampler(ys, xs, xs.cols, xs.cols, "random")

    val lastCost = FullyConnectedNetwork.train(params, sampler)
    println(lastCost)

  }

}