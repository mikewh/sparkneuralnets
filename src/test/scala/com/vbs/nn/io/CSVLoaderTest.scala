/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.io
import org.junit.Test
import org.junit.Assert._
import java.io.File

class CSVLoaderTest {
  import CSVLoaderTest._

  @Test def loadDenseMatrix(): Unit = {
    val dm = CSVLoader.getFeatureSetDM(SMALL_TEST_FILE)
    assertEquals(785, dm.rows)
    assertEquals(11, dm.cols)
  }

  @Test def loadFeatureSetsAndlabels(): Unit = {
    val (featureSet, labels) = CSVLoader.getFeatureSetAndLabels(SMALL_TEST_FILE, true)
    assertEquals(11, labels.size)
    assertEquals(7.0, labels(0), 0.01)
    assertEquals(4.0, labels(10), 0.01)

    assertEquals(784, featureSet.rows)
    assertEquals(11, featureSet.cols)

  }

  @Test def loadFeatureSetsAndEncodedlabels(): Unit = {
    val (featureSet, labels) = CSVLoader.getFeatureSetAndEncodedLabels(SMALL_TEST_FILE, true, 10)
    assertEquals(10, labels.rows)
    assertEquals(11, labels.cols)
    assertEquals(1.0, labels(7, 0), 0.01)
    assertEquals(1.0, labels(4, 10), 0.01)

    assertEquals(784, featureSet.rows)
    assertEquals(11, featureSet.cols)

  }

  /**
   * Mainly to check normalisation
   */
  @Test def preprocessCSVFile(): Unit = {
    val (featureSet, labels) = CSVLoader.getFeatureSetAndEncodedLabels(SMALL_TEST_FILE, true, 10)
    val (ixs, ys) = CSVDataSetPreprocessor(s"${BASE_PATH}mnist/small").
      setLabelSpec(true, 10).
      setImageNormaliser().
      transform()
    assertEquals(labels, ys)
    assertEquals(featureSet :/ 255.0, ixs)

  }

}
object CSVLoaderTest {
  val BASE_PATH = new File(getClass.getResource("/").getPath).getAbsolutePath + "/"
  val SMALL_TEST_FILE = s"${BASE_PATH}mnist/small.csv"

}