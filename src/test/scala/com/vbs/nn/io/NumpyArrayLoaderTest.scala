/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.io
import org.junit.Assert._
import org.junit.Test
import com.vbs.nn.tf.TensorflowRegression
import com.vbs.nn.model.MultiChannelDataSetDM
import com.vbs.nn.cnn.params.FilterWeightsAndBiasesInitializer
import com.vbs.nn.model.MultiChannelWeightMatrixDM
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.utils.MatrixUtils


class NumpyArrayLoaderTest {

  @Test def loadMultiChannelData(): Unit = {
    val dmss = MultiChannelDataSetDM(TensorflowRegression.loadSampleX())
    assertEquals(2, dmss.size)
    assertEquals(64, dmss.height)
    assertEquals(64, dmss.width)
    assertEquals(3, dmss.channels)

  }

  /**
   * these must have been persisted using io_utils.py persistNpStrByChannel
   */
  @Test def loadMultiChannelWeights(): Unit = {
    val filterWb = FilterWeightsAndBiasesInitializer.generateFilterWeightsAndBiasesL(4, 5, 3, 8, "static_gaussian", true)
    val wts = MultiChannelWeightMatrixDM(filterWb.weights)
    assertEquals(4, wts.fHeight)
    assertEquals(5, wts.fWidth)
    assertEquals(3, wts.channelsIn)
    assertEquals(8, wts.channelsOut)

    val dmss = NumpyArrayLoader.loadMultiChannelDataSet(TensorflowRegression.PATH + "1_1_tf_simple_W3.txt")
    println(MultiChannelWeightMatrixDM(dmss).dimString)
    val edmss = TensorflowRegression.getH5MultiChannel("1_1_tf_simple_W3.txt")
    println(MultiChannelWeightMatrixDM(edmss).dimString)

  }
}

object NumpyArrayLoaderTest {
  def toMultiChannelData(s: String): DenseMatrix4d =  
    MatrixUtils.toDenseMatrix4d(s).map(NumpyArrayLoader.toMultiChannelDataItem)
}