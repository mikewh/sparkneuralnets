/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.io
import org.junit.Test
import org.junit.Assert._
import breeze.linalg.DenseMatrix
import com.vbs.nn.model.MultiChannelDataSetDMTest
import com.vbs.nn.cnn.CNNTestUtils

class MatrixIOTest {
  val PATH = "C:/Temp/MLData/sparkneuralnets/matrices/"

  @Test def persistSingleMatrix(): Unit = {
    val dm = DenseMatrix((6.0, 7.0, 8.0), (4.0, 5.0, 9.0))
    MatrixIO.persistMatrix(PATH + "test1", dm)
    val dm1 = MatrixIO.load(PATH + "test1")
    assertEquals(dm, dm1)

  }
  
  @Test def persistAndLoadDenseMatrix4d(): Unit = {
    val dmss = MultiChannelDataSetDMTest.generateFromSequence(2, 3, 4, 5).dmss
    MatrixIO.persistDenseMatrix4d(PATH + "test4d", dmss)
    val ldmss = MatrixIO.loadDenseMatrix4d(PATH + "test4d")
    for (i <- 0 until dmss.size) CNNTestUtils.assertMCIEquals(dmss(i), ldmss(i))
  }

}