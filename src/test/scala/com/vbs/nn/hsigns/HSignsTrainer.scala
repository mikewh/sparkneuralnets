/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.hsigns
import org.junit.Test
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.PoolingLayerParameters
import com.vbs.nn.model.CNNFCNModel
import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.params.Parameters
import com.vbs.nn.model.ModelReporter
import org.junit.Ignore

/**
 * Training run for a full data set
 */
@Ignore
class HSignsTrainer {

  @Test def localData(): Unit = {
    val ts = System.currentTimeMillis()
    val (xs, ys) = HSignsDataSetLoader.getTrainingData()
    println(s"Loaded ${xs.size} training samples in ${System.currentTimeMillis() - ts}ms")

    val globalParams = Parameters()
      .setIterations(100)
      .setLearningRate(0.009)
      .setAdam(0.9, 0.999, 1e-08)
      .setSamplerSpec("random", 64)

    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8, true),
        ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4, true))
      .setFilterWeightsAndBiasesInitializer("xavier")
      .createCNNSampler(ys, xs)

    val fcnParams = Parameters(globalParams)
      .setLayerDims(64, 6)
      .setLearningRate(0.009)
      .setActivations("softmax")
      .setCostFunction("crossentropy")
      .setWeightInitializer("xavier")

    val (xst, yst) = HSignsDataSetLoader.getTestData()
    println(s"Loaded ${xst.size} test samples")

    val id = CNNFCNModel.run(cnnParams, fcnParams, xs, ys, xst, yst, HSignsDataSetLoader.PATH + "model/", "localData10")
    println(s"Run $id finished")
  }

  @Test def sparkSession(): Unit = {
    val ts = System.currentTimeMillis()
    val (xs, ys) = HSignsDataSetLoader.getTrainingData()
    println(s"Loaded ${xs.size} training samples in ${System.currentTimeMillis() - ts}ms")
    val yv = (for (col <- 0 until ys.cols) yield (ys(::, col))).toSeq

    val globalParams = Parameters()
      .setIterations(2)
      .setLearningRate(0.009)
      .setAdam(0.9, 0.999, 1e-08)
      .setSamplerSpec("static_random", 4, 4)

    val cnnParams = CNNParameters(globalParams)
      .setCNNLayerParams(ConvolutionLayerParameters("same", 1, 3, 4, 8),
        PoolingLayerParameters(8, 8, true),
        ConvolutionLayerParameters("same", 1, 8, 2, 16),
        PoolingLayerParameters(4, 4, true))
      .setFilterWeightsAndBiasesInitializer("static_xavier")
      .setSparkSession("SparkHsignsTest", 20)
      .createCNNSampler(yv, xs)

    val fcnParams = Parameters(globalParams)
      .setLayerDims(64, 6)
      .setLearningRate(0.009)
      .setActivations("softmax")
      .setCostFunction("crossentropyTF")
      .setWeightInitializer("static_xavier")

    val (xst, yst) = HSignsDataSetLoader.getTestData()
    println(s"Loaded ${xst.size} test samples")

    val id = CNNFCNModel.run(cnnParams, fcnParams, xs, ys, xst, yst, HSignsDataSetLoader.PATH + "model/", "sparkSession2")
    println(s"Run $id finished")

  }

  @Test def produceReports(): Unit = {
    ModelReporter.produceReports(HSignsDataSetLoader.PATH + "model/")

  }
}