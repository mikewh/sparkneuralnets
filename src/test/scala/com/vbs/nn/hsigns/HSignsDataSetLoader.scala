/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.hsigns
import org.junit.Test
import org.junit.Ignore
import org.junit.Assert._
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import breeze.linalg.DenseMatrix
import com.vbs.nn.io.NumpyArrayPreprocessor
import com.vbs.nn.io.MatrixIO
import com.vbs.nn.cnn.CNNTestUtils
import com.vbs.nn.GlobalParams

/**
 * Used to load full data sets from Numpy strings and store them as a single array of doubles for rapid subsequent retrieval.
 */
class HSignsDataSetLoader {
  
  @Test def persistTrainingData(): Unit = {
    var ts = System.currentTimeMillis()
    val (xs, ys) = HSignsDataSetLoader.persistDataAsMatrices(s"${HSignsDataSetLoader.PATH}train")
    println(s"Transformed and persisted training data in ${System.currentTimeMillis - ts}ms")
    
    ts = System.currentTimeMillis()
    val (xst, yst) = HSignsDataSetLoader.getTrainingData()
    println(s"Loaded training data in ${System.currentTimeMillis - ts}ms")
    assertEquals(ys, yst)
    assertEquals(xs.size, xst.size)
    for (i <- 0 until xs.size) CNNTestUtils.assertMCIEquals(xs(i), xst(i))
   
  }
  
  @Ignore def persistTestData(): Unit = {
    var ts = System.currentTimeMillis()
    val (xs, ys) = HSignsDataSetLoader.persistDataAsMatrices(s"${HSignsDataSetLoader.PATH}test")
    println(s"Transformed and persisted test data in ${System.currentTimeMillis - ts}ms")
    
    ts = System.currentTimeMillis()
    val (xst, yst) = HSignsDataSetLoader.getTestData()
    println(s"Loaded test data in ${System.currentTimeMillis - ts}ms")
    assertEquals(ys, yst)
    assertEquals(xs.size, xst.size)
    for (i <- 0 until xs.size) CNNTestUtils.assertMCIEquals(xs(i), xst(i))
   
  }

}
object HSignsDataSetLoader {
  val PATH = GlobalParams.EXTERNAL_BASE_PATH + "hsigns/"

  def persistDataAsMatrices(fName: String): (DenseMatrix4d, DenseMatrix[Double]) = {
    NumpyArrayPreprocessor(fName).
      setLabelValues(6).persistDenseMatrices()
  }
   
  def getTrainingData(): (DenseMatrix4d, DenseMatrix[Double]) = {
    val xs = MatrixIO.loadDenseMatrix4d(PATH + "train_xs")
    val ys = MatrixIO.load(PATH + "train_ys")
    (xs, ys)
    
  }
   
  def getTestData(): (DenseMatrix4d, DenseMatrix[Double]) = {
    val xs = MatrixIO.loadDenseMatrix4d(PATH + "test_xs")
    val ys = MatrixIO.load(PATH + "test_ys")
    (xs, ys)
    
  }

}