/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.model
import org.junit.Assert._
import org.junit.Test
import com.vbs.nn.cnn.CNNTestUtils

class MultiChannelDataSetDMTest {
  
  @Test def fromAndToArray(): Unit = {
    val mcds = MultiChannelDataSetDMTest.generateFromSequence(2, 3, 4, 5)
    val data = mcds.toArray
    val mcds1 = MultiChannelDataSetDM(2,3,4,5, data)
    assertEquals(mcds.dimString, mcds1.dimString)
    for (i <- 0 until mcds1.size) CNNTestUtils.assertMCIEquals(mcds.dmss(i), mcds1.dmss(i))
  }
  
}

object MultiChannelDataSetDMTest {
  def generateFromSequence(size: Int, width: Int, height: Int, channels: Int): MultiChannelDataSetDM = {
    MultiChannelDataSetDM(size, width, height, channels, (1 to size * width * height * channels).toSeq.map(_.toDouble))
  }
}