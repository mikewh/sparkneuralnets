package com.vbs.nn.model
import org.junit.Assert._
import org.junit.Test
import com.vbs.nn.cnn.params.CNNParametersTest
import com.vbs.nn.CNNFCNLinkedNetwork
import com.vbs.nn.FullyConnectedNetwork

import com.vbs.nn.GlobalParams

class CNNFCNModelTest {

  @Test def persistAndLoad(): Unit = {
    val (globalParams, cnnParams, fcnParams) = CNNParametersTest.getTestParams()
    CNNFCNLinkedNetwork.initialise(cnnParams)
    FullyConnectedNetwork.initialise(fcnParams)

    val id = CNNFCNModel.persist(cnnParams, fcnParams, GlobalParams.EXTERNAL_BASE_PATH, "initial")

    val (loadedCNN, loadedFCN) = CNNFCNModel.load(GlobalParams.EXTERNAL_BASE_PATH, "initial", id)
    assertEquals(cnnParams.toString, loadedCNN.toString)
    assertEquals(fcnParams.toString, loadedFCN.toString)
  }

}
