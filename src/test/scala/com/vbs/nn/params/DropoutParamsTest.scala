/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import breeze.linalg.DenseMatrix

import org.junit.Test
import org.junit.Assert._

import com.vbs.nn.fcn.ActivationFunctions._
import com.vbs.nn.fcn.BackPropagationFunctions._

import com.vbs.nn.fcn.ActivationsTest
import com.vbs.nn.utils.MatrixUtils._
import com.vbs.nn.utils.MatrixTestUtils._
import breeze.linalg.sum

class DropoutParamsTest {

  val PRECISION = 0.0000001

  @Test def generateDropoutMatrices(): Unit = {
    val (x, weightsAndBiases) = ActivationsTest.inputsAndWeightsTestData()

    val probs = Array(0.1, 0.5)
    val dParams = DropoutParams(weightsAndBiases, probs)

    val dropouts = dParams.generateDropouts(x.cols)
    weightsAndBiases.map(_.weights).zip(dropouts).foreach(wd => {
      assertEquals(wd._1.rows, wd._2.rows)
      assertEquals(wd._2.cols, x.cols)
    })
    val kept = dropouts.map(d => sum(d) / (d.rows * d.cols))
    assertTrue(kept(0) < kept(1))

  }

  @Test def feedforwardDropoutMatrices(): Unit = {
    def getTestDropouts(): Array[DenseMatrix[Double]] = {
      val s = """[
[[ 1.0 0.0  1.0  1.0  1.0]
 [ 1.0  1.0  1.0  1.0  1.0]]
[[ 1.0  1.0  1.0 0.0  1.0]
 [ 1.0  1.0  1.0  1.0  1.0]
 [0.0 0.0  1.0  1.0 0.0]]      ]"""
      toDenseMatrixArray(s)
    }

    def toParams(s: String): ActivationParams = {
      val p = new ActivationParams()
      p.activationName = s
      p
    }
    val x = getTestInput()
    val weightsAndBiases = getWeightsAndBiases()

    val probs = Array(0.7, 0.7)

    val activationParams = Array("relu", "relu", "sigmoid").map(toParams)
    val activationFunctions = getActivationFunctions(activationParams)
    val feedForwardFcn = feedForward(activationFunctions)(_, _, _)

    val dropoutParams = Some((probs, getTestDropouts()))

    val outputsAndActivations = feedForwardFcn(x, weightsAndBiases, dropoutParams)
    val expectedA3 = toDenseMatrix(" [[ 0.36974721  0.00305176  0.04565099  0.49683389  0.36974721]]")
    assertMatricesEqual(expectedA3, outputsAndActivations.last._2, PRECISION)

    val y = toDenseMatrix("[[1 1 0 1 0]]")
    val backPropLayerFunctions = getLayerBackPropFunctions(activationParams)
    val gradients = backPropagate(backPropLayerFunctions, backwardOutputLayer)(x, y,
      outputsAndActivations, weightsAndBiases, 0.0, dropoutParams)
    println(gradients(1).dWeights)

  }
  @Test def backPropDropoutMatrices(): Unit = {
    def getTestDropouts(): Array[DenseMatrix[Double]] = {
      val s = """[
[[ 1.0 0.0  1.0  1.0  1.0]
 [ 1.0  1.0  1.0  1.0 0.0]]
[[ 1.0 0.0  1.0 0.0  1.0]
 [0.0  1.0 0.0  1.0  1.0]
 [0.0 0.0  1.0 0.0 0.0]]       ]"""
      toDenseMatrixArray(s)
    }

    def getOAndA(): Array[OutputsAndActivationsL] = {
      val z = """[
 [[-1.52855314  3.32524635  2.13994541  2.60700654 -0.75942115]
 [-1.98043538  4.1600994   0.79051021  1.46493512 -0.45506242]]
 [[ 0.53035547  8.02565606  4.10524802  5.78975856  0.53035547]
 [-0.69166075 -1.71413186 -3.81223329 -4.61667916 -0.69166075]
 [-0.39675353 -2.62563561 -4.82528105 -6.0607449  -0.39675353]]
 [[-0.7415562  -0.0126646  -5.65469333 -0.0126646  -0.7415562 ]]
]"""

      val a = """[
[[ 0.0         0.0         4.27989081  5.21401307  0.0       ]
 [ 0.0         8.32019881  1.58102041  2.92987024  0.0       ]]
[[ 1.06071093  0.0         8.21049603  0.0         1.06071093]
 [ 0.0         0.0         0.0         0.0         0.0       ]
 [ 0.0         0.0         0.0         0.0         0.0       ]]
[[ 0.32266394  0.49683389  0.00348883  0.49683389  0.32266394]]
]"""
      (toDenseMatrixArray(z).zip(toDenseMatrixArray(a))).map(za => (za._1, za._2))
    }

    def toParams(s: String): ActivationParams = {
      val p = new ActivationParams()
      p.activationName = s
      p
    }
    val x = getTestInput()
    val weightsAndBiases = getWeightsAndBiases()

    val probs = Array(0.8, 0.8)

    val activationParams = Array("relu", "relu", "sigmoid").map(toParams)
    val backPropLayerFunctions = getLayerBackPropFunctions(activationParams)

    val dropoutParams = Some((probs, getTestDropouts()))

    val outputsAndActivations = getOAndA()

    val y = toDenseMatrix("[[1 1 0 1 0]]")
    val gradients = backPropagate(backPropLayerFunctions, backwardOutputLayer)(x, y,
      outputsAndActivations, weightsAndBiases, 0.0, dropoutParams)
      
    val expectedDw2 = toDenseMatrix("""
[[ -0.00256518 -0.0009476 ]
 [ 0.0         0.0       ]
 [ 0.0         0.0       ]]""")

    assertMatricesEqual(expectedDw2, gradients(1).dWeights, PRECISION)
      
    val expectedDw1 = toDenseMatrix("""
[[ 0.00019884  0.00028657  0.00012138]
 [ 0.00035647  0.00051375  0.00021761]]""")

    assertMatricesEqual(expectedDw1, gradients(0).dWeights, PRECISION)

  }

  def getWeightsAndBiases(): Array[LWeightsAndBiases] = {
    val w = """
      weights:[[[-1.09989127 -0.17242821 -0.87785842]
 [ 0.04221375  0.58281521 -1.10061918]]
[[ 0.50249434  0.90085595]
 [-0.68372786 -0.12289023]
 [-0.93576943 -0.26788808]]
[[-0.6871727  -0.84520564 -0.67124613]]]
      """
    val b = """
      biases: [[ 1.14472371 0.90159072]
 [ 0.53035547 -0.69166075 -0.39675353]
 [-0.0126646]]
      """
    val params = Parameters(w + "|" + b)
    val weightsAndBiases = params.getLayerWeightsAndBiases().get
    weightsAndBiases
  }

  def getTestInput(): DenseMatrix[Double] = {
    val s = """
      [[ 1.62434536 -0.61175641 -0.52817175 -1.07296862  0.86540763]
 [-2.3015387   1.74481176 -0.7612069   0.3190391  -0.24937038]
 [ 1.46210794 -2.06014071 -0.3224172  -0.38405435  1.13376944]]
      """
    toDenseMatrix(s)
  }

}