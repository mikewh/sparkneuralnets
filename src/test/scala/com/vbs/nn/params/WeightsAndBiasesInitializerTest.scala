/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import org.junit.Test
import org.junit.Assert._
import WeightsAndBiasesInitializer._

class WeightsAndBiasesInitializerTest {

  @Test def validAndInvalidInitialization(): Unit = {
    var wb = generateWeightsAndBiases(Array(5, 4, 3), "gaussian")
    checkSizes(wb)
    assertTrue(wb(0).biases.forall(_ != 0.0))
    assertTrue(wb(1).biases.forall(_ != 0.0))

    wb = generateWeightsAndBiases(Array(5, 4, 3), "gaussian", true)
    checkSizes(wb)
    assertTrue(wb(0).biases.forall(_ == 0.0))
    assertTrue(wb(1).biases.forall(_ == 0.0))
    

    wb = generateWeightsAndBiases(Array(5, 4, 3), "xavier")
    checkSizes(wb)
    
    wb = generateWeightsAndBiases(Array(1,1), "xavier")
    assertEquals(1, wb.size)
    assertEquals(1, wb(0).weights.rows)
    assertEquals(1, wb(0).weights.cols)
    
    assertEquals(1, wb(0).biases.size)
 
    try {
      generateWeightsAndBiases(Array(1), "xavier")
      fail("Call should have failed with IllegalArgumentException")
    } catch {
      case e: IllegalArgumentException => assertEquals("requirement failed: Expecting a minimum of 2 layers, but got 1", e.getMessage)
      case _: Throwable                => fail("Expecteding IllegalArgumentException")
    }

    try {
      generateWeightsAndBiases(Array(5, 4, 3), "rubbish")
      fail("Call should have failed with IllegalArgumentException")
    } catch {
      case e: IllegalArgumentException => assertEquals("Unsupported weight init function: 'rubbish'", e.getMessage)
      case _: Throwable                => fail("Expecteding IllegalArgumentException")
    }

  }

  private def checkSizes(wb: Array[LWeightsAndBiases]): Unit = {
    assertEquals(2, wb.size)
    assertEquals(4, wb(0).weights.rows)
    assertEquals(5, wb(0).weights.cols)
    assertEquals(3, wb(1).weights.rows)
    assertEquals(4, wb(1).weights.cols)
    
    assertEquals(4, wb(0).biases.size)
    assertEquals(3, wb(1).biases.size)

  }

}