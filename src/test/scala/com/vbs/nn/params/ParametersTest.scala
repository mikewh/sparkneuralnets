/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params

import org.junit.Test
import org.junit.Assert._
class ParametersTest {

  @Test def createParams(): Unit = {
    val p = Parameters()
    p.setLearningRate(0.1)
    p.setLearningRate(0.2)
      .setLayerDims(4, 6, 2)
      .setWeightInitializer("xavier")
      .setCostFunction("crossentropy")
      .setActivations("relu", "relu", "sigmoid")
   var s = p.toString
   val p1 = Parameters(s)
   assertEquals(p.toString, p1.toString)
   p1.setWeightRegularizerParams("l2", 0.7)
   s = p1.toString
   val p2 = Parameters(s)
   assertEquals(p1.toString, p2.toString)
   p2.setKeepProbabilities(0.2, 0.3)
   s = p2.toString
   val p3 = Parameters(s)
   assertEquals(p2.toString, p3.toString)
   p3.setAccuracy("train", 0.94)
   p3.setAccuracy("test", 0.91)
   s = p3.toString
  }
  
  @Test def adamParams(): Unit = {
    val p = Parameters().setAdam(0.9, 0.999, 1e-8)
    assertEquals("adam", p.getOptimizer())
    assertTrue(p.getAdamParams().isDefined)
    val (beta1, beta2, epsilon) = p.getAdamParams().get
    assertEquals(0.9, beta1, 0.01)
    assertEquals(0.999, beta2, 0.0001)
    assertEquals(1e-8, epsilon, 1e-8)
    
    
  }


}