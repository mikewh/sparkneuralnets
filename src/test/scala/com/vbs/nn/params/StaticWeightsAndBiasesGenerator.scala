/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import scala.util.Random


object StaticWeightsAndBiasesGenerator {
  
 def weightsInitStaticGaussian(layerDims: Array[Int], seed: Long, zeroBias: Boolean): Array[LWeightsAndBiases] = {
    val random = new Random(seed)
    def initWeights(w: DenseMatrix[Double]): Unit = {
      for (r <- 0 until w.rows) {
        for (c <- 0 until w.cols) w(r,c) = random.nextGaussian()
      }
    }
    def initBiases(b: DenseVector[Double]): Unit = {
      for (i <- 0 until b.size) b(i) = random.nextGaussian()
    }
    val r = for (idx <- 1 until layerDims.size) yield {
      val w = DenseMatrix.zeros[Double](layerDims(idx), layerDims(idx - 1))
      initWeights(w)
      val b = DenseVector.zeros[Double](layerDims(idx))
      if (!zeroBias) initBiases(b)
      LWeightsAndBiases(w, b)
    }
    r.toArray
  
 }
 

  private def weightsInitStaticXavier(layerDims: Array[Int], seed: Long, zeroBias: Boolean): Array[LWeightsAndBiases] = {
    weightsInitStaticGaussian(layerDims, seed, zeroBias).map(lWeightsAndBiases => {
      val ratio = 2.0 / (lWeightsAndBiases.weights.rows + lWeightsAndBiases.weights.cols)
      LWeightsAndBiases(lWeightsAndBiases.weights :* ratio, lWeightsAndBiases.biases)
    })
  }
 
  
 def generateWeightsAndBiases(layerDims: Array[Int], fcnName: String, seed: Long = 1l, zeroBias: Boolean = false): Array[LWeightsAndBiases] = fcnName.toLowerCase match  {
    case "xavier"   => weightsInitStaticXavier(layerDims, seed, zeroBias)
    case "gaussian" => weightsInitStaticGaussian(layerDims, seed,  zeroBias)
    case _          => throw new IllegalArgumentException(s"Unsupported weight init function: '$fcnName'")
 }
}