layerDims:[2, 5, 2, 1]
|
learningrate:0.0007
|
iterations:10000
|
minibatchsize:64
|
sampler:full
|
weightinitializer:static_params
|
activations:[relu, relu, sigmoid]
|
costfunction:crossentropy
