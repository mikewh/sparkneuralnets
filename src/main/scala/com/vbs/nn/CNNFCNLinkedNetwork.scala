/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn

import com.vbs.nn.cnn.params.CNNParameters
import breeze.linalg.DenseMatrix

import com.vbs.nn.params.Parameters
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.CNNOptimizerParamsAdam
import com.vbs.nn.cnn.params.CNNOptimizerParamsSimple
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d

import scala.collection.mutable.ArrayBuffer
import breeze.linalg.DenseVector
import com.vbs.nn.params.OptimizerParamsMomentum
import com.vbs.nn.params.OptimizerParamsAdam
import com.vbs.nn.params.OptimizedParamsSetAdam
import com.vbs.nn.params.OptimizedParamsSetMomentum
import com.vbs.nn.cnn.params.CNNOptimizerParams
import com.vbs.nn.cnn.CNNFeedForward
import com.vbs.nn.cnn.CNNOptimizer
import com.vbs.nn.cnn.params.FilterWeightsAndBiasesInitializer
import com.vbs.nn.fcn.ActivationFunctions
import com.typesafe.scalalogging.LazyLogging

/**
 * A convolutional network, backed by a fully connected network.
 *
 * Minibatch feed forward and back propagation is implemented using one of scala .par collections (the default), Spark (if not running on a single machine) or
 * with no parallelization (not really needed, just retained for testing)
 */
object CNNFCNLinkedNetwork extends LazyLogging {

  /**
   * If filter weights/ biases have not been explicitly set, these are generated.
   *
   * If adam optimization is specified, the relevant matrices are created.
   */
  def initialise(p: CNNParameters): CNNOptimizerParams = {

    val convLayers = p.getCNNLayerParams().collect { case convLayer: ConvolutionLayerParameters => convLayer }
    p.getFilterWeightsAndBiases() match {
      case Some(filterWeightsAndBiases) => {
        require(filterWeightsAndBiases.size == convLayers.size, s"Mismatch between no of filter weights (${filterWeightsAndBiases.size} and conv layers (${convLayers.size})")
        for (i <- 0 until filterWeightsAndBiases.size) convLayers(i).setFilterWeightsAndBiases(filterWeightsAndBiases(i))
      }
      case None => {
        FilterWeightsAndBiasesInitializer.generateFilterWeightsAndBiases(convLayers, p.getFilterWeightsAndBiasesInitializer(), true)
        p.setFilterWeightsAndBiases(convLayers.map(_.getFilterWeightsAndBiases()))
      }
    }

    val optimizerParams: CNNOptimizerParams = p.getOptimizer() match {
      case "adam" => {
        val (beta1, beta2, epsilon) = p.getAdamParams().get
        CNNOptimizerParamsAdam(beta1, beta2, epsilon, convLayers.map(_.getFilterWeightsAndBiases()))
      }
      case _ => CNNOptimizerParamsSimple()
    }
    optimizerParams

  }

  def predict(cnnParams: CNNParameters, fcnParams: Parameters, xs: DenseMatrix4d): DenseMatrix[Double] = {
    initialise(cnnParams)
    FullyConnectedNetwork.initialise(fcnParams)
    val layerParams = cnnParams.getCNNLayerParams()
    val batchOutputs = CNNFeedForward.feedForwardCNNBatch(xs.par, layerParams)
    val flattenedOutput = CNNFeedForward.flattenCNNOutputs(batchOutputs)
    val outputsAndActivations = ActivationFunctions.feedForward(fcnParams.activationFunctions())(flattenedOutput, fcnParams.getLayerWeightsAndBiases().get, None)
    outputsAndActivations.last._2
  }

  def train(cnnParams: CNNParameters, fcnParams: Parameters): Double = {
    val ts = System.currentTimeMillis()
    val cnnOptimizerParams = initialise(cnnParams)
    val convLayers = cnnParams.getCNNLayerParams().collect { case convLayer: ConvolutionLayerParameters => convLayer }
    val layerParams = cnnParams.getCNNLayerParams()

    val fcnOptimizerParams = FullyConnectedNetwork.initialise(fcnParams)

    var _epochCost = 0.0
    val costs = new ArrayBuffer[Double]
    for (i <- 0 until cnnParams.getIterations()) {
      var _lastCost = 0.0
      val samples = cnnParams.miniBatches()
      samples.foreach(sample => {
        val (sampleY, sampleX) = (sample._1, sample._2)
        val cnnLayerOutputs = cnnParams.feedForwardCNNFcn()(sampleX, layerParams)
        val flattenedOutput = CNNFeedForward.flattenCNNOutputs(cnnLayerOutputs, cnnParams.getCNNFlattenBatchFcn())

        val (cost, fcnOptimizedParamsSet) = FullyConnectedNetwork.trainMiniBatch(fcnParams, fcnOptimizerParams, flattenedOutput, sampleY)
        fcnOptimizerParams.weightsAndBiases = fcnOptimizedParamsSet.weightsAndBiases
        fcnOptimizedParamsSet match {
          case opsm: OptimizedParamsSetMomentum => fcnOptimizerParams.asInstanceOf[OptimizerParamsMomentum].velocities = opsm.velocities
          case opsa: OptimizedParamsSetAdam => {
            val opa = fcnOptimizerParams.asInstanceOf[OptimizerParamsAdam]
            opa.avgGradients = opsa.avgGradients
            opa.avgSqGradients = opsa.avgSqGradients
          }
          case _ =>
        }

        _lastCost += cost / samples.size
        costs += cost

        val cnnLayerOutputsP = cnnParams.getParallel() match {
          case true  => cnnLayerOutputs.par
          case false => cnnLayerOutputs
        }

        val (actvationPrevGradients, weightsAndBiasesGradients) =
          cnnParams.backPropageCNNFcn()(sampleX, fcnOptimizedParamsSet.gradients(0), layerParams, cnnLayerOutputsP, cnnParams.getCNNUnflattenFCNOutputsFcn())

        cnnOptimizerParams match {
          case optimizerParamsSimple: CNNOptimizerParamsSimple => {
            val updatedWbs = CNNOptimizer.optimizeSimple(cnnParams, cnnOptimizerParams, weightsAndBiasesGradients)
            for (i <- 0 until convLayers.size) convLayers(i).setFilterWeightsAndBiases(updatedWbs(i))
          }
          case optimizerParamsAdam: CNNOptimizerParamsAdam => {
            val updatedWbsAndAvgs = CNNOptimizer.optimizeAdam(cnnParams, optimizerParamsAdam, weightsAndBiasesGradients)
            for (i <- 0 until convLayers.size) convLayers(i).setFilterWeightsAndBiases(updatedWbsAndAvgs(i)._1)
            optimizerParamsAdam.avgGradients = updatedWbsAndAvgs.map(_._2)
            optimizerParamsAdam.avgSqGradients = updatedWbsAndAvgs.map(_._3)

          }
        }

      })
      logger.debug(s"Cost after iteration $i: ${_lastCost}")
      _epochCost = _lastCost
    }
    val traintime = (System.currentTimeMillis() - ts).toInt
    /* TODO: Tidy up once CNNParameters and parameters have been refactored  */
    fcnParams.setWeightsAndBiases(fcnOptimizerParams.weightsAndBiases)
    fcnParams.setCosts(DenseVector(costs.toArray))
    fcnParams.setCost(_epochCost)
    fcnParams.setTrainTime(traintime)

    cnnParams.setFilterWeightsAndBiases(convLayers.map(_.getFilterWeightsAndBiases()))

    _epochCost
  }

}