/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.spark.sampler

import org.apache.spark.SparkContext
import scala.util.Random

import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.sampler.CNNSampler
import scala.collection.GenSeq
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

/**
 *
 * Note that using this to sample data locally (when there is a limited number of partitions) results in a "contains a task of very large size" warning.
 * This is because the data.sample is executed lazily, meaning that the entire data set is shipped to each worker.
 * 
 * If the data set can be fitted into memory, then use CNNMiniBatchSampler which will run considerably faster. 
 */
class SparkCNNSampler(sc: SparkContext, partitions: Int = 4)(ys: Seq[DenseVector[Double]],
                      xs: DenseMatrix4d, val batchSize: Int, val numBatches: Int, val samplerType: String) extends CNNSampler {

  val static = samplerType.trim.toLowerCase.startsWith("static_")
  val sequential = samplerType.trim.toLowerCase.endsWith("sequential")

  private val rddX = sc.parallelize(xs, partitions)

  private val rddY = sc.parallelize(ys, partitions)
  private val samples = rddY.zip(rddX).cache

  private val rnd = new Random(static match {
    case false => System.nanoTime()
    case true  => 1l
  }).nextLong()

  def mapToMiniBatch(sample: Array[(DenseVector[Double], DenseMatrix3d)]): (DenseMatrix[Double], GenSeq[DenseMatrix3d]) = {
    val labels = sample.map(_._1)
    val sampleX = sample.map(_._2).toSeq
    val sampleY = new DenseMatrix(labels(0).size, labels.size, labels.map(_.toArray).flatten)
    (sampleY, sampleX)
  }

  private def miniBatchesRandom(): Seq[(DenseMatrix[Double], GenSeq[DenseMatrix3d])] = {
    for (i <- 0 until numBatches) yield {
      mapToMiniBatch(samples.sample(true, batchSize.toDouble / xs.size, rnd).collect)
    }
  }

  private def miniBatchesSeq(): Seq[(DenseMatrix[Double], GenSeq[DenseMatrix3d])] = {
    val batchCount = Math.min(numBatches * batchSize, samples.count).toInt
    val batchSamples = samples.take(batchCount)
    for (col <- 0 until batchCount by batchSize) yield {
      mapToMiniBatch(batchSamples.drop(Math.min(batchCount - col, batchSize)))
    }
  }
  
  def miniBatches(): Seq[(DenseMatrix[Double], GenSeq[DenseMatrix3d])] = {
    if (sequential) miniBatchesSeq() else miniBatchesRandom()
  }

}