/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.spark
import org.apache.spark.SparkContext

import org.apache.spark.rdd.RDD
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.cnn.params.CNNLayerParameters
import com.vbs.nn.cnn.CNNFeedForward
import scala.collection.GenSeq
import com.vbs.nn.params.LGradients
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.CNNBackPropagate

/**
 * Spark implentation of Convolutional Nerual net
 * 
 * Each element in a mini batch is processed (feed forard and back prop) by a separate Spark task
 */
object SparkCNN {
  def feedForward(sc: SparkContext)(xs: RDD[DenseMatrix3d],
                                    layerParameters: Seq[CNNLayerParameters]): RDD[Seq[(DenseMatrix3d, DenseMatrix3d)]] = {
    val ffFcns = CNNFeedForward.feedForwardFunctions(layerParameters)
    val feedForwardOneFcn = CNNFeedForward.feedForwardOne(ffFcns)(_)
    xs.map(x => feedForwardOneFcn(x))
  }

  def toRDD(sc: SparkContext, partitions: Int = 4)(xs: GenSeq[DenseMatrix3d]): RDD[DenseMatrix3d] = 
    sc.parallelize(xs.seq, partitions)

  def feedForwardCNNBatch(sc: SparkContext, partitions: Int = 4)(xs: GenSeq[DenseMatrix3d],
                                            layerParameters: Seq[CNNLayerParameters]): GenSeq[Seq[(DenseMatrix3d, DenseMatrix3d)]] = {

    val rdd = sc.parallelize(xs.seq, partitions)
    feedForward(sc)(rdd, layerParameters).collect
  }

  def backPropageCNNBatch(sc: SparkContext, partitions: Int = 4)(xs: GenSeq[DenseMatrix3d],
                                            fcnLayer0Gradients: LGradients,
                                            layerParameters: Seq[CNNLayerParameters],
                                            cnnLayerOutputs: GenSeq[Seq[(DenseMatrix3d, DenseMatrix3d)]],
                                            unflattenFCNOutputsFcn: CNNBackPropagate.UnflattenFCNOutputsFcn = CNNBackPropagate.unflattenFCNOutputs): (CNNBackPropagate.ActvationPrevDeltas, CNNBackPropagate.Gradients) = {
    require(layerParameters.size == cnnLayerOutputs(0).size, s"Mismatch between layer parameters (${layerParameters.size}) and outputs(${cnnLayerOutputs(0).size})")
    val convLayerParams = layerParameters.collect { case confParams: ConvolutionLayerParameters => confParams }

    val fcnLayer0 = for (i <- 0 until fcnLayer0Gradients.dActivationsPrev.cols) yield (fcnLayer0Gradients.dActivationsPrev(::, i))
    val fcnLayer0Activations = sc.parallelize(fcnLayer0, partitions).cache
    val rddXs = sc.parallelize(xs.seq, partitions)
    val rddLayerOutputs = sc.parallelize(cnnLayerOutputs.seq, partitions)
    val zippedParallelArgs = rddXs.zip(fcnLayer0Activations).zip(rddLayerOutputs)

    def combine(a: (CNNBackPropagate.ActvationPrevDeltas, CNNBackPropagate.Gradients),
                b: (CNNBackPropagate.ActvationPrevDeltas, CNNBackPropagate.Gradients)): (CNNBackPropagate.ActvationPrevDeltas, CNNBackPropagate.Gradients) = {
      val combinedActivations = a._1 ++ b._1
      val aDW = a._2.map(_._1)
      val bDw = b._2.map(_._1)
      val combinedDw = for (i <- 0 until aDW.size) yield {
        aDW(i).zip(bDw(i)).map(arDw => CNNBackPropagate.add(arDw._1, arDw._2))
      }
      val combinedDb = a._2.map(_._2).
        zip(b._2.map(_._2)).
        map(db12 => db12._1 + db12._2)

      (combinedActivations, combinedDw.zip(combinedDb))
    }

    val daPrevsGradients = zippedParallelArgs.map(xs_fcnLayer0Activations_cnnLayerOutputs => {
      val x = xs_fcnLayer0Activations_cnnLayerOutputs._1._1
      val fcnLayer0Activation = xs_fcnLayer0Activations_cnnLayerOutputs._1._2
      val cnnLayerOutput = xs_fcnLayer0Activations_cnnLayerOutputs._2
      val dA = unflattenFCNOutputsFcn(fcnLayer0Activation, cnnLayerOutput)
      CNNBackPropagate.backPropagateOne(x, dA, layerParameters, cnnLayerOutput)
    }).aggregate(CNNBackPropagate.initializeFoldLeft(convLayerParams))((accum, result) => CNNBackPropagate.accumResults(accum, result), combine)

    daPrevsGradients
  }

}