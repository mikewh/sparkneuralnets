/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.spark

import com.vbs.nn.cnn.CNNFeedForward
import org.apache.spark.sql.SparkSession
import com.vbs.nn.cnn.CNNBackPropagate
import com.vbs.nn.sampler.CNNSampler
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.spark.sampler.SparkCNNSampler
import breeze.linalg.DenseVector

/**
 * All parameters required for the Spark CNN implementation
 * @param samplerPartitions The number of partitions when loading data for sampling
 */
class SparkCNNConfig(name: String, samplerPartitions: Int) {
  private val sparkSession = SparkSession
    .builder()
    .appName(name)
    .config("spark.master", "local")
    .getOrCreate()

  private val sc = sparkSession.sparkContext

  private var samplerO: Option[CNNSampler] = None

  def setSampler(ys: Seq[DenseVector[Double]],
                 xs: DenseMatrix4d, batchSize: Int, numBatches: Int,
                 samplerType: String): Unit = {
    samplerO = Some(new SparkCNNSampler(sc, samplerPartitions)(ys, xs, batchSize, numBatches, samplerType))
  }

  def getSampler(): CNNSampler = samplerO match {
    case Some(sampler) => sampler
    case None          => throw new IllegalStateException("CNNSampler has not been set on SparkCNNConfig")
  }

  /**
   * returns the number of partitions used in the CNN
   */
  def getExecutePartitions(): Int = getSampler().batchSize

  def feedForwardCNNFcn(): CNNFeedForward.FeedForwardCNNBatchFcn = SparkCNN.feedForwardCNNBatch(sc, getExecutePartitions())(_, _)

  def backPropageCNNFcn(): CNNBackPropagate.BackPropageCNNBatchFcn = SparkCNN.backPropageCNNBatch(sc, getExecutePartitions())(_, _, _, _, _)

}