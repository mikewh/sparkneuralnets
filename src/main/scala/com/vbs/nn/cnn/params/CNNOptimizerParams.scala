/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params

/**
 * Container for additional parameters required by different optimizers
 */
trait CNNOptimizerParams extends Serializable

/**
 * No additional parameters needed for simple gradient optimizer
 */
case class CNNOptimizerParamsSimple() extends CNNOptimizerParams
