/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

/**
 * Additional parameters required by the adam optimizer
 */
class CNNOptimizerParamsAdam(val beta1: Double, val beta2: Double, val epsilon: Double, 
    val initialFilterWeightsAndBiases: Seq[FilterWeightsAndBiasesL]) extends CNNOptimizerParams{

  def initializeDeltas(): Seq[FilterWeightsAndBiasesL] = {
    initialFilterWeightsAndBiases.map(filterWeightsAndBiasesL => {
      val weights = filterWeightsAndBiasesL.weights.map(mci => mci.map(dm => DenseMatrix.zeros[Double](dm.rows, dm.cols)))
      val biases = DenseVector.zeros[Double](filterWeightsAndBiasesL.biases.size)
      FilterWeightsAndBiasesL(weights, biases)
    })
  }
  
  private var _count = 0
  
  def count(): Int = {
    _count += 1
    _count
  }
  
  private var _avgGradients = initializeDeltas

  def avgGradients_=(avgGradients: Seq[FilterWeightsAndBiasesL]): Unit = {
    _avgGradients = avgGradients
  }

  def avgGradients = _avgGradients
  
  
  private var _avgSqGradients = initializeDeltas

  def avgSqGradients_=(avgSqGradients: Seq[FilterWeightsAndBiasesL]): Unit = {
    _avgSqGradients = avgSqGradients
  }

  def avgSqGradients = _avgSqGradients  
}

object CNNOptimizerParamsAdam {
  def apply(beta1: Double, beta2: Double, epsilon: Double, 
    initialFilterWeightsAndBiases: Seq[FilterWeightsAndBiasesL]) = new CNNOptimizerParamsAdam(beta1, beta2, epsilon, initialFilterWeightsAndBiases)
  
}