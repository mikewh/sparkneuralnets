/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import com.vbs.nn.cnn.Pooling

class PoolingLayerParameters(val fSize: Int, val stride: Int, val maxPool: Boolean = true) extends CNNLayerParameters with Serializable {
  val fcn: Pooling.PoolFunction = if (maxPool) Pooling.maxPool else Pooling.averagePool

  override def toString = s"layertype:pool|fsize:$fSize|stride:$stride|maxpool:$maxPool"

  private var _backPropPoolLayerFcn: Pooling.BackPropPoolLayerFcn = Pooling.backPropPoolLayer

  def setBackPropPoolLayerFcn(fcn: Pooling.BackPropPoolLayerFcn): Unit = {
    _backPropPoolLayerFcn = fcn
  }

  def getBackPropPoolLayerFcn = _backPropPoolLayerFcn
}

object PoolingLayerParameters {
  def apply(fSize: Int, stride: Int, maxPool: Boolean = true) = new PoolingLayerParameters(fSize, stride, maxPool)
}