/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import scala.util.Random

/**
 * Initializer for weights and biases used in CNN Convolutional layers
 */
object FilterWeightsAndBiasesInitializer {

  private def xavier(dms: Seq[DenseMatrix[Double]]): Seq[DenseMatrix[Double]] = {
    dms.map(dm => {
      val ratio = 2.0 / (dm.rows + dm.cols)
      dm :* ratio
    })
  }

  private def weightsInitStaticXavier(fHeight: Int, fWidth: Int, channelsIn: Int, channelsOut: Int, zeroBias: Boolean): FilterWeightsAndBiasesL = {

    val fwb = weightsInitStaticGaussianL(fHeight, fWidth, channelsIn, channelsOut, zeroBias)
    val weights = fwb.weights.map(xavier)
    FilterWeightsAndBiasesL(weights, fwb.biases)
  }

  private def weightsInitStaticGaussianL(fHeight: Int, fWidth: Int, channelsIn: Int, channelsOut: Int, zeroBias: Boolean): FilterWeightsAndBiasesL = {
    val random = new Random(1L)
    def initWeights(): DenseMatrix[Double] = {
      val w = DenseMatrix.zeros[Double](fHeight, fWidth)
      for (r <- 0 until w.rows) {
        for (c <- 0 until w.cols) w(r, c) = random.nextGaussian()
      }
      w
    }
    def initBiases(b: DenseVector[Double]): Unit = {
      for (i <- 0 until b.size) b(i) = random.nextGaussian()
    }

    val weights = for (channelOut <- 0 until channelsOut)
      yield (for (channelIn <- 0 until channelsIn)
      yield (initWeights()))

    val b = DenseVector.zeros[Double](channelsOut)
    if (!zeroBias) initBiases(b)
    FilterWeightsAndBiasesL(weights, b)
  }

  private def weightsInitXavier(fHeight: Int, fWidth: Int, channelsIn: Int, channelsOut: Int, zeroBias: Boolean): FilterWeightsAndBiasesL = {

    val fwb = weightsInitGaussianL(fHeight, fWidth, channelsIn, channelsOut, zeroBias)
    val weights = fwb.weights.map(xavier)
    FilterWeightsAndBiasesL(weights, fwb.biases)
  }

  private def weightsInitGaussianL(fHeight: Int, fWidth: Int, channelsIn: Int, channelsOut: Int, zeroBias: Boolean): FilterWeightsAndBiasesL = {
    val normal01 = breeze.stats.distributions.Gaussian(0, 1)
    val weights = for (channelOut <- 0 until channelsOut)
      yield (for (channelIn <- 0 until channelsIn)
      yield (DenseMatrix.rand(fHeight, fWidth, normal01)))
    val b = zeroBias match {
      case false => DenseVector.rand(channelsOut, normal01)
      case true  => DenseVector.zeros[Double](channelsOut)
    }
    FilterWeightsAndBiasesL(weights, b)
  }

  def generateFilterWeightsAndBiases(convLayers: Seq[ConvolutionLayerParameters],
                                     fcnName: String, zeroBias: Boolean): Unit =
    convLayers.foreach(convLayer =>
      convLayer.setFilterWeightsAndBiases(generateFilterWeightsAndBiasesL(convLayer.filterSize, convLayer.channelsIn, convLayer.channelsOut, fcnName, zeroBias)))

  def generateFilterWeightsAndBiasesL(fHeight: Int, fWidth: Int, channelsIn: Int, channelsOut: Int,
                                      fcnName: String, zeroBias: Boolean): FilterWeightsAndBiasesL = fcnName.toLowerCase match {
    case "gaussian"        => weightsInitGaussianL(fHeight, fWidth, channelsIn, channelsOut, zeroBias)
    case "xavier"          => weightsInitXavier(fHeight, fWidth, channelsIn, channelsOut, zeroBias)
    case "static_gaussian" => weightsInitStaticGaussianL(fHeight, fWidth, channelsIn, channelsOut, zeroBias)
    case "static_xavier"   => weightsInitStaticXavier(fHeight, fWidth, channelsIn, channelsOut, zeroBias)
    case _                 => throw new IllegalArgumentException(s"Unsupported weight init function: '$fcnName'")
  }

  def generateFilterWeightsAndBiasesL(fSize: Int, channelsIn: Int, channelsOut: Int,
                                      fcnName: String, zeroBias: Boolean = false): FilterWeightsAndBiasesL =
    generateFilterWeightsAndBiasesL(fSize, fSize, channelsIn, channelsOut, fcnName, zeroBias)

}