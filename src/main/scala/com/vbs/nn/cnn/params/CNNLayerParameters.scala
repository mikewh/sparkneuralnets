/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import com.vbs.nn.params.Parameters

/**
 * Holds Configuration of all layers within a CNN
 */
trait CNNLayerParameters



object CNNLayerParameters {
  
  def fromString(s: String): CNNLayerParameters = {
    val params = Parameters.toParamPairs(s).toMap
    params("layertype") match {
      case "conv" => new ConvolutionLayerParameters(params("padtype"),
        params("stride").toInt, params("channelsin").toInt, params("filtersize").toInt, params("channelsout").toInt)
      case "pool" => new PoolingLayerParameters(params("fsize").toInt, params("stride").toInt, params("maxpool").toBoolean)
    }

  }

  def toLayerParams(s: String): Seq[CNNLayerParameters] =
    s.trim.drop(1).dropRight(1).split("\\}\\{").map(fromString)
}