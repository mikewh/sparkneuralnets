/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import breeze.linalg.DenseVector

import com.vbs.nn.model.DataSetTypes.DenseMatrix4d

/**
 * Container for data generated when back propagating a minibatch through a CNN layer 
 * 
 * @param dActivationsPrev previous layer gradients for all entires in the minibatch
 * @param dWeights layer weight gradients updated for the minibatch
 * @param dBiases biases weight gradients updated for the minibatch
 */
case class CNNGradientsL (dActivationsPrev: DenseMatrix4d, dWeights: DenseMatrix4d, dBiases: DenseVector[Double])