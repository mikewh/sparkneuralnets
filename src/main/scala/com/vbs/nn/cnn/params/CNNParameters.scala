/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params

import com.vbs.nn.params.Parameters
import com.vbs.nn.params.Parameter
import com.vbs.nn.params.Simple
import com.vbs.nn.cnn.CNNBackPropagate
import com.vbs.nn.cnn.CNNFeedForward
import scala.annotation.tailrec
import com.vbs.nn.cnn.CNNToFCNInterface
import com.vbs.nn.sampler.CNNMiniBatchSampler
import scala.collection.GenSeq
import breeze.linalg.DenseMatrix
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.sampler.CNNSampler
import com.vbs.nn.spark.SparkCNNConfig
import breeze.linalg.DenseVector

/**
 * Parameters used only by CNNs
 */
class CNNParameters(p: Parameters) extends Parameters(p.m) {
  case class CNNLayerParamSet(layerParameters: Seq[CNNLayerParameters]) extends Parameter

  case class CNNFilterWeightsAndBiases(filterWeightsAndBiases: Seq[FilterWeightsAndBiasesL]) extends Parameter

  override val m = scala.collection.mutable.Map[String, Parameter]() ++= p.m

  override def toString = {
    val cnnLayerStr = m.get("cnnlayerparams") match {
      case Some(_) => CNNParameters.toSequenceBlock("cnnLayerParams", getCNNLayerParams().map(_.toString))
      case None    => ""
    }
    val fWeightsAndBiasesStr = getFilterWeightsAndBiases() match {
      case Some(fWeightsAndBiases) => CNNParameters.toSequenceBlock("filterWeightsAndBiases", fWeightsAndBiases.map(_.toString))
      case None                    => ""
    }
    s"$p|filterweightsandbiasesinitializer:${m("filterweightsandbiasesinitializer")}$cnnLayerStr$fWeightsAndBiasesStr"
  }

  private var _flattenBatchFcn: CNNFeedForward.FlattenCCNOutputsFcn = CNNToFCNInterface.flattenBatch
  private var _unflattenFCNOutputsFcn: CNNBackPropagate.UnflattenFCNOutputsFcn = CNNBackPropagate.unflattenFCNOutputs

  private var _sparkConfigO: Option[SparkCNNConfig] = None
  private var _miniBatchSamplerO: Option[CNNSampler] = None

  def getCNNLayerParams(): Seq[CNNLayerParameters] = m.get("cnnlayerparams") match {
    case Some(p: CNNLayerParamSet) => p.layerParameters
    case None                      => throw new IllegalStateException(s"Missing 'cnnlayerparams' param")
    case _                         => throw new IllegalArgumentException(s"Corrupt 'cnnlayerparams' param")
  }

  def setCNNLayerParams(layerParameters: CNNLayerParameters*): CNNParameters = {
    m += "cnnlayerparams" -> CNNLayerParamSet(layerParameters)
    this
  }

  def getCNNUnflattenFCNOutputsFcn(): CNNBackPropagate.UnflattenFCNOutputsFcn = _unflattenFCNOutputsFcn

  def setCNNUnflattenFCNOutputsFcn(fcn: CNNBackPropagate.UnflattenFCNOutputsFcn): CNNParameters = {
    _unflattenFCNOutputsFcn = fcn
    this
  }

  def getCNNFlattenBatchFcn(): CNNFeedForward.FlattenCCNOutputsFcn = _flattenBatchFcn

  def setCNNFlattenBatchFcn(fcn: CNNFeedForward.FlattenCCNOutputsFcn): CNNParameters = {
    _flattenBatchFcn = fcn
    this
  }

  def setFilterWeightsAndBiasesInitializer(id: String): CNNParameters = {
    m += "filterweightsandbiasesinitializer" -> Simple(id)
    this
  }

  def getFilterWeightsAndBiasesInitializer(): String = get[String]("filterweightsandbiasesinitializer")

  def setFilterWeightsAndBiases(filterWeightsAndBiases: Seq[FilterWeightsAndBiasesL]): CNNParameters = {
    m += "filterweightsandbiases" -> CNNFilterWeightsAndBiases(filterWeightsAndBiases)
    setZeroBiases(filterWeightsAndBiases.map(_.biases).forall(_ == 0.0))
    this
  }

  def getFilterWeightsAndBiases(): Option[Seq[FilterWeightsAndBiasesL]] = m.get("filterweightsandbiases") match {
    case Some(fwbs: CNNFilterWeightsAndBiases) => Some(fwbs.filterWeightsAndBiases)
    case None                                  => None
    case _                                     => throw new IllegalArgumentException(s"Corrupt 'filterweightsandbiases' param")
  }

  def setSparkSession(name: String, samplerPartitions: Int): CNNParameters = {
    _sparkConfigO = Some(new SparkCNNConfig(name, samplerPartitions))
    this
  }

  def createCNNSampler(ys: Seq[DenseVector[Double]], xs: DenseMatrix4d): CNNParameters = (p.getSamplerType(), p.getMiniBatchSize()) match {
    case (Some(samplerType), Some(miniBatchSize)) => {
      val numBatches = getNumBatches().getOrElse(xs.size)
      p.setNumBatches(numBatches)
      _sparkConfigO match {
        case Some(sparkConfig) => sparkConfig.setSampler(ys, xs, miniBatchSize, numBatches, samplerType)
        case _                 => throw new IllegalStateException("Sampler requires ys to be DenseMatrix[Double], but got Seq[DenseVector[Double]]")
      }
      this
    }
    case _ => throw new IllegalStateException("setSamplerSpec has not been called")

  }

  def createCNNSampler(ys: DenseMatrix[Double], xs: DenseMatrix4d): CNNParameters = (p.getSamplerType(), p.getMiniBatchSize()) match {
    case (Some(samplerType), Some(miniBatchSize)) => {
      val numBatches = getNumBatches().getOrElse(xs.size / miniBatchSize)
      p.setNumBatches(numBatches)
      _sparkConfigO match {
        case Some(sparkConfig) => throw new IllegalStateException("Spark sampler requires ys to be Seq[DenseVector[Double]], but got DenseMatrix[Double]")
        case _                 => _miniBatchSamplerO = Some(CNNMiniBatchSampler(ys, xs, miniBatchSize, numBatches, samplerType, getParallel()))
      }
      this
    }
    case _ => throw new IllegalStateException("setSamplerSpec has not been called")

  }

  def getCNNMiniBatchSampler(): CNNSampler = (_sparkConfigO, _miniBatchSamplerO) match {
    case (Some(sparkConfig), None) => sparkConfig.getSampler()
    case (None, Some(sampler))     => sampler
    case _                         => throw new IllegalStateException(s"No Sampler has been set")
  }

  def miniBatches(): Seq[(DenseMatrix[Double], GenSeq[DenseMatrix3d])] = 
    getCNNMiniBatchSampler().miniBatches()

  def feedForwardCNNFcn(): CNNFeedForward.FeedForwardCNNBatchFcn = _sparkConfigO match {
    case Some(sparkConfig) => sparkConfig.feedForwardCNNFcn()
    case _                 => CNNFeedForward.feedForwardCNNBatch
  }

  def backPropageCNNFcn(): CNNBackPropagate.BackPropageCNNBatchFcn = _sparkConfigO match {
    case Some(sparkConfig) => sparkConfig.backPropageCNNFcn()
    case _                 => CNNBackPropagate.backPropageCNNBatch
  }

}

object CNNParameters {
  def apply(p: Parameters) = new CNNParameters(p)

  def apply(s: String) = {
    val (superS, blocks) = extractSequenceBlocks(s)
    val params = scala.collection.mutable.Map[String, String](Parameters.toParamPairs(superS).toSeq: _*)
    val filterweightsandbiasesinitializer = params("filterweightsandbiasesinitializer")
    params -= "filterweightsandbiasesinitializer"

    val p = Parameters(params)
    val cnnParams = new CNNParameters(p)
    blocks.foreach(block => {
      val (name, valueStr) = block
      name match {
        case "cnnlayerparams" => {
          val layerParams = extractSequenceFromBlock(valueStr).map(CNNLayerParameters.fromString(_))
          cnnParams.setCNNLayerParams(layerParams: _*)
        }
        case "filterweightsandbiases" =>
          cnnParams.setFilterWeightsAndBiases(extractSequenceFromBlock(valueStr).map(FilterWeightsAndBiasesL.fromString(_)))
        case _ => throw new IllegalArgumentException(s"Unsupported sequence block param: '$name'")
      }
    })
    cnnParams.setFilterWeightsAndBiasesInitializer(filterweightsandbiasesinitializer)
    cnnParams

  }

  def extractSequenceFromBlock(s: String): Seq[String] =
    s.trim.drop(1).dropRight(1).split("\\}\\{")

  def toSequenceBlock(name: String, ss: Seq[String]): String = {
    s"{${name.toLowerCase}:{${ss.mkString("}{")}}}"
  }

  @tailrec def extractSequenceBlocks(s: String, nextIdx: Int = 0, blocks: Seq[(String, String)] = Nil): (String, Seq[(String, String)]) = {
    val idx = s.indexOf(":{", nextIdx)
    if (idx == -1) (s, blocks)
    else {
      val startIdx = s.lastIndexOf("{", idx)
      val endIdx = s.indexOf("}}", idx)
      if (startIdx == -1 || endIdx == -1) throw new IllegalArgumentException(s"corrupt paramString - missing block start '{' or end '}}'")
      else {
        val block = Parameters.toParamPair(s.substring(startIdx + 1, endIdx + 1).replaceAll("\\s+", " "))
        val precedingStr = s.substring(0, startIdx)
        val followingStr = s.substring(endIdx + 2)
        extractSequenceBlocks(precedingStr + followingStr, startIdx, blocks :+ block)

      }
    }
  }
}

