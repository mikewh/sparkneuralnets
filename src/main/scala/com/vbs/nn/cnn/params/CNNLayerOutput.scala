/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params

import com.vbs.nn.cnn.Convolution
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

sealed trait CNNLayerOutput

/**
 * Outputs and activations of a Convolutional layer for a minibatch
 */
case class ConvolutionLayerOutput(output: Seq[Convolution.CNNOutputsAndActivations]) extends CNNLayerOutput

/**
 * Outputs of a Pooling layer for a minibatch
 */
case class PoolingLayerOutput(output: DenseMatrix4d) extends CNNLayerOutput

/**
 * Outputs and activations of a Convolutional layer for a single entry
 */
case class OneConvolutionLayerOutput(output: Convolution.CNNOutputsAndActivations) extends CNNLayerOutput

/**
 * Outputs of a Pooling layer for a single entry
 */
case class OnePoolingLayerOutput(output: DenseMatrix3d) extends CNNLayerOutput
