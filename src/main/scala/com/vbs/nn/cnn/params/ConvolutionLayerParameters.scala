/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import com.vbs.nn.cnn.Convolution

/**
 * Configuration of a convolutional layer within a CNN
 */
class ConvolutionLayerParameters(val padType: String, val stride: Int, val channelsIn: Int, val filterSize: Int, val channelsOut: Int,
                                      val activationFcn: Convolution.ActivationFunction = Convolution.relu) extends CNNLayerParameters with Serializable {
  private var _filterWeightsAndBiases: Option[FilterWeightsAndBiasesL] = None

  override def toString =
    s"layertype:conv|padtype:$padType|stride:$stride|channelsin:$channelsIn|filtersize:$filterSize|channelsout:$channelsOut"

  private var _feedForwardLayerFcn: Convolution.FeedForwardLayerFcn = Convolution.feedForwardLayer

  private var _backPropNonLinearFcn: Convolution.BackPropNonLinearFcn = Convolution.backPropReluLayer
  private var _backPropLinearFcn: Convolution.BackPropLinearFcn = Convolution.backPropLayerLinear

  def setFilterWeightsAndBiases(filterWeightsAndBiases: FilterWeightsAndBiasesL): Unit = {
    require(channelsOut == filterWeightsAndBiases.weights.size &&
      channelsIn == filterWeightsAndBiases.weights(0).size &&
      filterSize == filterWeightsAndBiases.weights(0)(0).rows &&
      filterSize == filterWeightsAndBiases.weights(0)(0).cols,
      s"""Mismatch between layer channelsIn, filterSize, channelsOut ($channelsIn, $filterSize, $channelsOut) 
and weights (${filterWeightsAndBiases.weights(0).size}, ${filterWeightsAndBiases.weights(0)(0).rows}, ${filterWeightsAndBiases.weights.size})""")

    _filterWeightsAndBiases = Some(filterWeightsAndBiases)
    
    // TODO: Remove this (should be handled in params)
    _filterWeightsAndBiases.get.zeroiseBiases()
  }

  def setFeedForwardLayerFcn(fcn: Convolution.FeedForwardLayerFcn): Unit = {
    _feedForwardLayerFcn = fcn
  }

  def setBackPropNonLinearFcn(fcn: Convolution.BackPropNonLinearFcn): Unit = {
    _backPropNonLinearFcn = fcn
  }

  def setBackPropLinearFcn(fcn: Convolution.BackPropLinearFcn): Unit = {
    _backPropLinearFcn = fcn
  }

  def getFilterWeightsAndBiases(): FilterWeightsAndBiasesL = _filterWeightsAndBiases match {
    case Some(filterWeightsAndBiases) => filterWeightsAndBiases
    case None => throw new IllegalStateException("filterWeightsAndBiases have not been set")
  }

  def getFeedForwardLayerFcn = _feedForwardLayerFcn

  def getBackPropNonLinearFcn = _backPropNonLinearFcn

  def getBackPropLinearFcn = _backPropLinearFcn

}

object ConvolutionLayerParameters {
  
  def apply(padType: String, stride: Int, channelsIn: Int, filterSize: Int, channelsOut: Int,
      activationFcn: Convolution.ActivationFunction = Convolution.relu) = 
        new ConvolutionLayerParameters(padType, stride, channelsIn, filterSize, channelsOut,activationFcn)
  
}
