/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn.params
import breeze.linalg.DenseVector

import com.vbs.nn.utils.MatrixUtils
import com.vbs.nn.params.Parameters
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d

/**
 * Container for weights and biases for a Convolutional layer
 */
class FilterWeightsAndBiasesL(val weights: DenseMatrix4d, val biases: DenseVector[Double]) extends Serializable {
  def zeroiseBiases(): Unit = for (i <- 0 until biases.size) biases(i) = 0.0

  override def toString = s"filterweightsl:\n${MatrixUtils.matrix4dToString(weights)}\n|\nfilterbiasesl:\n${MatrixUtils.vectorToString(biases)}"
}

object FilterWeightsAndBiasesL {
  def apply(weights: DenseMatrix4d, biases: DenseVector[Double]) = new FilterWeightsAndBiasesL(weights, biases)
  
  
  def fromString(s: String): FilterWeightsAndBiasesL = {
    val wbParams = Parameters.toParamPairs(s).toMap
    fromStrings(wbParams("filterweightsl"),wbParams("filterbiasesl"))
  }
  
  def fromStrings(weights: String, biases: String): FilterWeightsAndBiasesL = 
        FilterWeightsAndBiasesL(MatrixUtils.toDenseMatrix4d(weights), MatrixUtils.toDenseVector(biases))

  
  def fromParams(params: Map[String, String]): Option[FilterWeightsAndBiasesL] = (params.get("filterweightsl"), params.get("filterbiasesl")) match {
    case (Some(weights), Some(biases)) => Some(fromStrings(weights, biases))
    case (None, None) => None
    case (_,_) => throw new IllegalArgumentException(s"Corrupt 'filterweightsl' / 'filterbiasesl' params (expecting both or neither)")
  }
}