/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn

import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.cnn.params.CNNOptimizerParams
import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.FilterWeightsAndBiasesL
import com.vbs.nn.fcn.OptimizerFunctions
import breeze.linalg.DenseVector
import com.vbs.nn.cnn.params.CNNOptimizerParamsAdam

/**
 * CNN Optimization functions. 
 * 
 * Only simple gradient descent and adam optimization currently implemented for CNNs
 */
object CNNOptimizer {

  def optimizeSimple(p: CNNParameters,
                     optimizerParams: CNNOptimizerParams,
                     gradients: CNNBackPropagate.Gradients): Seq[FilterWeightsAndBiasesL] = {
    val convLayers = p.getCNNLayerParams().collect { case convLayer: ConvolutionLayerParameters => convLayer }
    for (i <- 0 until convLayers.size) yield (optimizeSimpleL(p, convLayers(i), gradients(i)))
  }

  def optimizeSimpleL(p: CNNParameters, convLayerParams: ConvolutionLayerParameters,
                      gradientsL: (DenseMatrix4d, DenseVector[Double])): FilterWeightsAndBiasesL = {

    optimizeSimpleL(p.getLearningRate(), p.getZeroBiases(), convLayerParams.getFilterWeightsAndBiases(), gradientsL)
  }

  def optimizeSimpleL(learningRate: Double, zeroiseBiases: Boolean, filterWeightsAndBiasesL: FilterWeightsAndBiasesL,
                      gradientsL: (DenseMatrix4d, DenseVector[Double])): FilterWeightsAndBiasesL = {
    require(filterWeightsAndBiasesL.weights.size == gradientsL._1.size)
    val updatedWeightsAndBiases = for (channelOut <- 0 until filterWeightsAndBiasesL.weights.size) yield {
      val biases = new DenseVector(Array(filterWeightsAndBiasesL.biases(channelOut)))
      val dBiases = new DenseVector(Array(gradientsL._2(channelOut)))
      val mci = filterWeightsAndBiasesL.weights(channelOut)
      val dMci = gradientsL._1(channelOut)
      require(mci.size == dMci.size)
      val updatedMciWeightsBias = for (channelIn <- 0 until mci.size) yield {
        OptimizerFunctions.optimizeSimpleL(learningRate, mci(channelIn), biases, dMci(channelIn), dBiases)
      }
      updatedMciWeightsBias.map(uwb => (uwb._1, uwb._2(0)))
    }
    // The bias value for each on the incoming channels associated with a given channel out will be the same, therefore just take the first
    val updatedWeights = updatedWeightsAndBiases.map(_.map(_._1))
    val updatedBiases = zeroiseBiases match {
      case false => DenseVector(updatedWeightsAndBiases.map(_.map(_._2)).map(_(0)).toArray)
      case true  => DenseVector.zeros[Double](updatedWeightsAndBiases.map(_.map(_._2)).map(_(0)).size)
    }
    FilterWeightsAndBiasesL(updatedWeights, updatedBiases)
  }

  def optimizeAdam(p: CNNParameters,
                   opa: CNNOptimizerParamsAdam,
                   gradients: CNNBackPropagate.Gradients): Seq[(FilterWeightsAndBiasesL, FilterWeightsAndBiasesL, FilterWeightsAndBiasesL)] = {
    val convLayers = p.getCNNLayerParams().collect { case convLayer: ConvolutionLayerParameters => convLayer }
    for (i <- 0 until convLayers.size) yield {
      optimizeAdamL(p, opa, convLayers(i), gradients(i),
        opa.avgGradients(i), opa.avgSqGradients(i))
    }
  }

  def optimizeAdamL(p: CNNParameters, opa: CNNOptimizerParamsAdam,
                    convLayerParams: ConvolutionLayerParameters, gradientsL: (DenseMatrix4d, DenseVector[Double]),
                    avgGradientsL: FilterWeightsAndBiasesL,
                    avgSqGradientsL: FilterWeightsAndBiasesL): (FilterWeightsAndBiasesL, FilterWeightsAndBiasesL, FilterWeightsAndBiasesL) = {
    optimizeAdamL(p.getLearningRate(), opa.beta1, opa.beta2, opa.epsilon, opa.count(),
      convLayerParams.getFilterWeightsAndBiases(), gradientsL,
      avgGradientsL, avgSqGradientsL)
  }

  def optimizeAdamL(learningRate: Double, beta1: Double, beta2: Double, epsilon: Double, callCount: Int,
                    filterWeightsAndBiasesL: FilterWeightsAndBiasesL,
                    gradientsL: (DenseMatrix4d, DenseVector[Double]),
                    avgGradientsL: FilterWeightsAndBiasesL,
                    avgSqGradientsL: FilterWeightsAndBiasesL): (FilterWeightsAndBiasesL, FilterWeightsAndBiasesL, FilterWeightsAndBiasesL) = {

    require(filterWeightsAndBiasesL.weights.size == gradientsL._1.size)
    val updatedWeightsAndBiases = for (channelOut <- 0 until filterWeightsAndBiasesL.weights.size) yield {
      val biases = new DenseVector(Array(filterWeightsAndBiasesL.biases(channelOut)))
      val dBiases = new DenseVector(Array(gradientsL._2(channelOut)))
      val avgBiases = new DenseVector(Array(avgGradientsL.biases(channelOut)))
      val avgSqBiases = new DenseVector(Array(avgSqGradientsL.biases(channelOut)))
      val mci = filterWeightsAndBiasesL.weights(channelOut)
      val dMci = gradientsL._1(channelOut)
      val avgWeights = avgGradientsL.weights(channelOut)
      val avgSqWeights = avgSqGradientsL.weights(channelOut)
      require(mci.size == dMci.size)
      val rawFcn: OptimizerFunctions.RawAdamFunctionSignature = OptimizerFunctions.optimizeAdamRawL
      val updatedMciWeightsBias = for (channelIn <- 0 until mci.size) yield {
        rawFcn(learningRate, beta1, beta2, epsilon, callCount,
          mci(channelIn), biases, dMci(channelIn), dBiases,
          avgWeights(channelIn), avgBiases,
          avgSqWeights(channelIn), avgSqBiases)
      }
      updatedMciWeightsBias.map(uwbAndDeltas => (uwbAndDeltas._1, uwbAndDeltas._2(0),
        uwbAndDeltas._3, uwbAndDeltas._4(0),
        uwbAndDeltas._5, uwbAndDeltas._6(0)))
    }

    val updatedWeights = updatedWeightsAndBiases.map(_.map(_._1))
    val updatedBiases = DenseVector(updatedWeightsAndBiases.map(_.map(_._2)).map(_(0)).toArray)
    val ufilterWeightsAndBiasesL = FilterWeightsAndBiasesL(updatedWeights, updatedBiases)

    val avgWeights = updatedWeightsAndBiases.map(_.map(_._3))
    val avgBiases = DenseVector(updatedWeightsAndBiases.map(_.map(_._4)).map(_(0)).toArray)
    val avgWeightsAndBiasesL = FilterWeightsAndBiasesL(avgWeights, avgBiases)

    val avgSqWeights = updatedWeightsAndBiases.map(_.map(_._5))
    val avgSqBiases = DenseVector(updatedWeightsAndBiases.map(_.map(_._6)).map(_(0)).toArray)
    val avgSqWeightsAndBiasesL = FilterWeightsAndBiasesL(avgSqWeights, avgSqBiases)

    (ufilterWeightsAndBiasesL, avgWeightsAndBiasesL, avgSqWeightsAndBiasesL)
  }  
}