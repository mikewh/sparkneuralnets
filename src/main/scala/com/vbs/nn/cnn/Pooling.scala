/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

import breeze.linalg.DenseMatrix
import breeze.linalg.max
import breeze.stats.mean
import com.vbs.nn.cnn.params.PoolingLayerOutput
/**
 * Functions required to feed forward and back propagate through pooling layers of a CNN
 */
object Pooling {

  type PoolFunction = DenseMatrix[Double] => Double
  
  type BackPropPoolLayerFcn = (DenseMatrix3d,  DenseMatrix3d, Int, Int, Boolean) => DenseMatrix3d

  type BackPropPoolLayerBatchSignature = (DenseMatrix4d, DenseMatrix4d, Int, Int, Boolean) => DenseMatrix4d

  def maxPool(slicePrev: DenseMatrix[Double]): Double = max(slicePrev)

  def averagePool(slicePrev: DenseMatrix[Double]): Double = mean(slicePrev)

  def maskMaxPool(pool: DenseMatrix[Double]): DenseMatrix[Double] = {
    val maxPool = max(pool)
    val dm = DenseMatrix.zeros[Double](pool.rows, pool.cols)
    for (r <- 0 until pool.rows) {
      for(c <- 0 until pool.cols){
        if (pool(r,c) == maxPool) dm(r, c) = 1.0
      }
    }
    
    dm
  }

  def distributePool(dz: Double, rows: Int, cols: Int): DenseMatrix[Double] = {
    val avg = dz / (rows * cols)
    DenseMatrix.fill(rows, cols)(avg)
  }

  def poolLayer(input: DenseMatrix3d,
                        fSize: Int, stride: Int, fcn: PoolFunction): DenseMatrix3d = {
    val hPrev = input(0).rows
    val wPrev = input(0).cols
    def newDim(i: Int): Int = (1 + (i - fSize) / stride)
    val h = newDim(hPrev)
    val w = newDim(wPrev)
    val c = input.size
    val layerOutput = for (i <- 0 until c) yield (DenseMatrix.zeros[Double](h, w))

    for (rowStart <- 0 until h) {
      for (colStart <- 0 until w) {
        val prevSlice = input.map(dm => dm((rowStart * stride) until (rowStart * stride) + fSize, (colStart * stride) until (colStart * stride) + fSize))
        for (channel <- 0 until c) {
          layerOutput(channel)(rowStart, colStart) = fcn(prevSlice(channel))
        }
      }
    }
    layerOutput
  }

  def poolLayerBatch(prevLayerOutputs: DenseMatrix4d,
                     fSize: Int, stride: Int, fcn: PoolFunction): PoolingLayerOutput = {

    val hPrev = prevLayerOutputs(0)(0).rows
    val wPrev = prevLayerOutputs(0)(0).cols
    def newDim(i: Int): Int = (1 + (i - fSize) / stride)
    val h = newDim(hPrev)
    val w = newDim(wPrev)
    val c = prevLayerOutputs(0).size

    def poolLayerForImage(prevLayerOutput: DenseMatrix3d): DenseMatrix3d = {
      val layerOutput = for (i <- 0 until c) yield (DenseMatrix.zeros[Double](h, w))

      for (rowStart <- 0 until h) {
        for (colStart <- 0 until w) {
          val prevSlice = prevLayerOutput.map(dm => dm((rowStart * stride) until (rowStart * stride) + fSize, (colStart * stride) until (colStart * stride) + fSize))
          for (channel <- 0 until c) {
            layerOutput(channel)(rowStart, colStart) = fcn(prevSlice(channel))
          }
        }
      }
      layerOutput
    }
    PoolingLayerOutput(prevLayerOutputs.map(poolLayerForImage))
  }

  def backPropPoolLayer(dA: DenseMatrix3d, prevLayerOutput: DenseMatrix3d,
                        fSize: Int, stride: Int, maxPooling: Boolean): DenseMatrix3d = {
    val h = dA(0).rows
    val w = dA(0).cols
    val channels = dA.size

    val dAPrev = prevLayerOutput.map(dm => DenseMatrix.zeros[Double](dm.rows, dm.cols))

    for (rowStart <- 0 until h) {
      for (colStart <- 0 until w) {
        for (channel <- 0 until channels) {
          val da = dA(channel)(rowStart, colStart)
          val vStart = rowStart * stride
          val hStart = colStart * stride
          val dAPrevDelta = maxPooling match {
            case true => {
              val prevSlice = prevLayerOutput(channel)(vStart until vStart + fSize, hStart until hStart + fSize)
              maskMaxPool(prevSlice) :*= da
            }
            case false => {
              distributePool(da, fSize, fSize)
            }
          }
          for (r <- 0 until fSize) {
            for (c <- 0 until fSize) {
              val dX = dAPrev(channel)(vStart + r, hStart + c) + dAPrevDelta(r, c)
              dAPrev(channel)(vStart + r, hStart + c) = dX
            }
          }

        }
      }
    }

    dAPrev
  }

  def backPropPoolLayerBatch(dAs: DenseMatrix4d, prevLayerOutputs: DenseMatrix4d,
                             fSize: Int, stride: Int, maxPooling: Boolean): DenseMatrix4d = {
    for (i <- 0 until dAs.size) yield (backPropPoolLayer(dAs(i), prevLayerOutputs(i), fSize, stride, maxPooling))
  }

}