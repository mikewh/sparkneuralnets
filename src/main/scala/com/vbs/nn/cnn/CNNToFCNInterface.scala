/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn

import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import scala.collection.GenSeq

/**
 * Functions required to transfer data between a CCN and FCN
 */
object CNNToFCNInterface {

  def flatten(output: DenseMatrix3d): Seq[Double] = {
    for (row <- 0 until output(0).rows; col <- 0 until output(0).cols; channel <- 0 until output.size) yield (output(channel)(row, col))
  }

  def flattenBatch(outputs: GenSeq[DenseMatrix3d]): DenseMatrix[Double] = {
    val arrays = outputs.map(flatten)
    new DenseMatrix(arrays(0).size, outputs.size, arrays.flatten.toArray)
  }

  def unflatten(height: Int, width: Int, channels: Int,
                          flattened: DenseVector[Double]): DenseMatrix3d = {
    require(height * width * channels == flattened.size, 
        s"Mismatch between (height, width, channels) ($height, $width, $channels) and size of flattened data (${flattened.size})")
    val mci = for (i <- 0 until channels) yield (new DenseMatrix[Double](height, width))
    var _idx = 0
    for (row <- 0 until height; col <- 0 until width; channel <- 0 until channels) {
      mci(channel)(row, col) = flattened(_idx)
      _idx += 1
    }
    mci
  }

  def unflattenBatch(height: Int, width: Int, channels: Int, flattened: DenseMatrix[Double],
                     unflattenFcn: (Int, Int, Int, DenseVector[Double]) => DenseMatrix3d = unflatten): Seq[DenseMatrix3d] = {
    for (col <- 0 until flattened.cols) yield (unflattenFcn(height, width, channels, flattened(::, col)))
  }
  
}