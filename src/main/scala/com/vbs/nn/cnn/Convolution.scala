/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn

import com.vbs.nn.cnn.params.CNNGradientsL
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.cnn.params.CNNGradientsL

/**
 * Functions required to feed forward and back propagate through convolutional layers of a CNN
 */
object Convolution {

  type ActivationFunction = DenseMatrix3d => DenseMatrix3d

  type FeedForwardLayerFcn = (DenseMatrix3d, Padding, DenseMatrix4d, DenseVector[Double], ActivationFunction) => CNNOutputsAndActivations

  type BackPropNonLinearFcn = (DenseMatrix3d, DenseMatrix3d) => DenseMatrix3d

  type BackPropLinearFcn = (DenseMatrix3d, DenseMatrix3d, Int, Padding, DenseMatrix4d, DenseVector[Double]) => (DenseMatrix3d, DenseMatrix4d, DenseVector[Double])

  type CNNOutputsAndActivations = (DenseMatrix3d, DenseMatrix3d)

  type BackPropagationLayerBatchSignature = (DenseMatrix4d, DenseMatrix4d, DenseMatrix4d, Padding, DenseMatrix4d, DenseVector[Double]) => CNNGradientsL

  def convolveSlice(slicePrev: DenseMatrix3d, weights: DenseMatrix3d,
                    bias: Double): Double = {
    (slicePrev.zip(weights).map(spw => breeze.linalg.sum((spw._1 :* spw._2) :+= bias))).sum
  }

  def convolveLayer(prevLayerOutput: DenseMatrix3d,
                    padding: Padding,
                    filterWeights: DenseMatrix4d, filterBiases: DenseVector[Double]): DenseMatrix3d = {
    val hPrev = prevLayerOutput(0).rows
    val wPrev = prevLayerOutput(0).cols

    val fSize = filterWeights(0)(0).rows

    val (h, w) = padding.layerDims(hPrev, wPrev)
    val channels = filterWeights.size
    val layerOutput = for (i <- 0 until channels) yield (DenseMatrix.zeros[Double](h, w))
    val prevPadded = prevLayerOutput.map(padding.padInput(_))
    for (rowStart <- 0 until h by padding.strideH) {
      for (colStart <- 0 until w by padding.strideW) {
        val prevSlice = prevPadded.map(dm => dm(rowStart until rowStart + fSize, colStart until colStart + fSize))
        for (channel <- 0 until channels) {
          layerOutput(channel)(rowStart, colStart) = convolveSlice(prevSlice, filterWeights(channel), filterBiases(channel))
        }
      }
    }
    layerOutput
  }

  def convolveLayerBatch(prevLayerOutputs: DenseMatrix4d,
                         padding: Padding,
                         filterWeights: DenseMatrix4d, filterBiases: DenseVector[Double]): DenseMatrix4d =
    prevLayerOutputs.map(convolveLayer(_, padding, filterWeights, filterBiases))

  /**
   * The only activation function currently configured for a Convolutional layer
   */
  def relu(output: DenseMatrix3d): DenseMatrix3d = output.map(dm => dm.map(x => Math.max(0.0, x)))

  def feedForwardLayer(prevLayerOutput: DenseMatrix3d, padding: Padding,
                       filterWeights: DenseMatrix4d, filterBiases: DenseVector[Double],
                       activationFcn: ActivationFunction = relu): CNNOutputsAndActivations = {
    val outputs = convolveLayer(prevLayerOutput, padding, filterWeights, filterBiases)
    (outputs, activationFcn(outputs))
  }

  def backPropReluLayer(dA: DenseMatrix3d, prevLayerOutput: DenseMatrix3d): DenseMatrix3d = {
    val outputsMask = prevLayerOutput.map(dm => dm.map((x => if (x <= 0.0) 0.0 else 1.0)))
    dA.zip(outputsMask).map(dao => dao._1 :* dao._2)
  }

  def updateDAPrevSlice(dAPrevPad: DenseMatrix3d, channelWeightsDelta: DenseMatrix3d,
                        rowStart: Int, colStart: Int, fSize: Int): Unit = {
    for (channel <- 0 until dAPrevPad.size) {
      dAPrevPad(channel)(rowStart until rowStart + fSize, colStart until colStart + fSize) :+= channelWeightsDelta(channel)
    }
  }

  def zeroMCI(mci: DenseMatrix3d): DenseMatrix3d = mci.map(dm => DenseMatrix.zeros[Double](dm.rows, dm.cols))

  def backPropLayerLinear(dZ: DenseMatrix3d,
                          prevLayerOutput: DenseMatrix3d,
                          fSize: Int,
                          padding: Padding,
                          filterWeights: DenseMatrix4d,
                          filterBiases: DenseVector[Double]): (DenseMatrix3d, DenseMatrix4d, DenseVector[Double]) = {
    val dWeights = filterWeights.map(zeroMCI).toArray
    val dBiases = DenseVector.zeros[Double](filterBiases.size)

    val h = dZ(0).rows
    val w = dZ(0).cols
    val channels = dZ.size

    val prevPadded = prevLayerOutput.map(padding.padInput(_))
    val dAPrevPad = zeroMCI(prevLayerOutput).map(padding.padInput(_))
    for (rowStart <- 0 until h by padding.strideH) {
      for (colStart <- 0 until w by padding.strideW) {
        val prevSlice = prevPadded.map(dm => dm(rowStart until rowStart + fSize, colStart until colStart + fSize))
        for (channel <- 0 until channels) {

          val dZSlice = dZ(channel)(rowStart, colStart)
          val channelWeightsDelta = filterWeights(channel).map(_ :* dZSlice)
          updateDAPrevSlice(dAPrevPad, channelWeightsDelta, rowStart, colStart, fSize)

          val sliceWeightsDelta = prevSlice.map(_ :* dZSlice)
          dWeights(channel).zip(sliceWeightsDelta).foreach(mcis => mcis._1 :+= mcis._2)
          dBiases(channel) = dBiases(channel) + dZSlice
        }
      }
    }
    val depadded = dAPrevPad.map(dm => padding.dePad(dm))
    (depadded, dWeights, dBiases)
  }

  def backPropLinearLayerBatch(dZs: DenseMatrix4d, prevLayerOutputs: DenseMatrix4d,
                               padding: Padding,
                               filterWeights: DenseMatrix4d, filterBiases: DenseVector[Double]): CNNGradientsL = {

    val fSize = filterWeights(0)(0).rows

    val h = dZs(0)(0).rows
    val w = dZs(0)(0).cols
    val channels = dZs(0).size

    val dWeights = filterWeights.map(zeroMCI).toArray
    val dBiases = DenseVector.zeros[Double](filterBiases.size)

    def backPropConvLayerOne(dZ: DenseMatrix3d, prevLayerOutput: DenseMatrix3d): DenseMatrix3d = {
      val prevPadded = prevLayerOutput.map(padding.padInput(_))
      val dAPrevPad = zeroMCI(prevLayerOutput).map(padding.padInput(_))
      for (rowStart <- 0 until h by padding.strideH) {
        for (colStart <- 0 until w by padding.strideW) {
          val prevSlice = prevPadded.map(dm => dm(rowStart until rowStart + fSize, colStart until colStart + fSize))
          for (channel <- 0 until channels) {

            val dZSlice = dZ(channel)(rowStart, colStart)
            val channelWeightsDelta = filterWeights(channel).map(_ :* dZSlice)
            updateDAPrevSlice(dAPrevPad, channelWeightsDelta, rowStart, colStart, fSize)

            val sliceWeightsDelta = prevSlice.map(_ :* dZSlice)
            dWeights(channel).zip(sliceWeightsDelta).foreach(mcis => mcis._1 :+= mcis._2)
            dBiases(channel) = dBiases(channel) + dZSlice
          }
        }
      }
      val depadded = dAPrevPad.map(dm => padding.dePad(dm))
      depadded
    }

    val dActivationsPrev = for (i <- 0 until dZs.size) yield (backPropConvLayerOne(dZs(i), prevLayerOutputs(i)))
    CNNGradientsL(dActivationsPrev, dWeights, dBiases)
  }

}
