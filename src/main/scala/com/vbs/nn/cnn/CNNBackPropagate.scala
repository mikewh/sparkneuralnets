/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import scala.collection.GenSeq

import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.cnn.params.CNNLayerParameters
import com.vbs.nn.params.LGradients
import breeze.linalg.DenseVector
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.PoolingLayerParameters
import breeze.linalg.DenseMatrix
import scala.collection.mutable.ArrayBuffer

/**
 * All functions required for CNN back propagation
 */
object CNNBackPropagate {
  type Gradients = Seq[(DenseMatrix4d, DenseVector[Double])]
  type ActvationPrevDeltas = GenSeq[DenseMatrix4d]

  type UnflattenFCNOutputsFcn = (DenseVector[Double], GenSeq[(DenseMatrix3d, DenseMatrix3d)]) => DenseMatrix3d

  type BackPropageCNNBatchFcn = (GenSeq[DenseMatrix3d], LGradients, 
      Seq[CNNLayerParameters], GenSeq[Seq[(DenseMatrix3d, DenseMatrix3d)]],
      UnflattenFCNOutputsFcn) => (ActvationPrevDeltas, Gradients)

  def zeroMCI(mci: DenseMatrix3d): DenseMatrix3d = mci.map(dm => DenseMatrix.zeros[Double](dm.rows, dm.cols))

  def add(mci1: DenseMatrix3d, mci2: DenseMatrix3d): DenseMatrix3d =
    mci1.zip(mci2).map(mci12 => mci12._1 + mci12._2)

  /**
   * Used by both .par and Spark implementations of Back pro[  
   */
  def initializeFoldLeft(params: Seq[ConvolutionLayerParameters]): (ActvationPrevDeltas, Gradients) =
    (new ArrayBuffer[Seq[DenseMatrix3d]], initializeGradients(params))

  /**
   * Used by both .par and Spark implementations of Back pro[  
   */
  def initializeGradients(params: Seq[ConvolutionLayerParameters]): Gradients = {
    val filterWeightsAndBiases = params.map(p => p.getFilterWeightsAndBiases())
    filterWeightsAndBiases.map(fwb => (fwb.weights.map(zeroMCI), DenseVector.zeros[Double](fwb.biases.size)))
  }

  /**
   * Used by both .par and Spark implementations of Back pro[  
   */
  def accumResults(accum: (ActvationPrevDeltas, Gradients), result: (Seq[DenseMatrix3d], Gradients)): (ActvationPrevDeltas, Gradients) = {
    val updatedActivations = accum._1 :+ result._1
    val accumDW = accum._2.map(_._1)
    val resultDw = result._2.map(_._1)

    val combinedDw = for (i <- 0 until accumDW.size) yield {
      accumDW(i).zip(resultDw(i)).map(arDw => add(arDw._1, arDw._2))
    }

    val combinedDb = accum._2.map(_._2).
      zip(result._2.map(_._2)).
      map(db12 => db12._1 + db12._2)

    (updatedActivations, combinedDw.zip(combinedDb))
  }

  def getLayerOutput(lo: (DenseMatrix3d, DenseMatrix3d)): DenseMatrix3d = lo._2 match {
    case Nil => lo._1
    case _   => lo._2
  }

  def unflattenFCNOutputs(fcnLayer0Activations: DenseVector[Double],
                          cnnLayerOutputs: GenSeq[(DenseMatrix3d, DenseMatrix3d)]): DenseMatrix3d = {
    val lastLayer = cnnLayerOutputs.last
    val cnnLastLayer1 = getLayerOutput(lastLayer)
    CNNToFCNInterface.unflatten(cnnLastLayer1(0).rows, cnnLastLayer1(0).cols, cnnLastLayer1.size, fcnLayer0Activations)
  }

  /**
   * Back propagate a single item through all layers of the CNN
   */
  def backPropagateOne(x: DenseMatrix3d,
                       dA: DenseMatrix3d,
                       layerParameters: Seq[CNNLayerParameters],
                       cnnLayerOutputs: GenSeq[(DenseMatrix3d, DenseMatrix3d)]): (Seq[DenseMatrix3d], Gradients) = {

    val convLayerParams = layerParameters.collect { case confParams: ConvolutionLayerParameters => confParams }
    val dWeightsAndBiases = initializeGradients(convLayerParams)
    var _dALast = dA

    val convLayerCount = convLayerParams.size
    val daPrevs = new Array[DenseMatrix3d](convLayerCount)
    var _gradsIdx = convLayerCount - 1
    for (idx <- cnnLayerOutputs.size - 1 to 0 by -1) {
      layerParameters(idx) match {
        case poolParams: PoolingLayerParameters => {
          _dALast = poolParams.getBackPropPoolLayerFcn(_dALast, getLayerOutput(cnnLayerOutputs(idx - 1)), poolParams.fSize, poolParams.stride, poolParams.maxPool)
        }
        case convParams: ConvolutionLayerParameters => {
          val filterWeights = convParams.getFilterWeightsAndBiases().weights
          val filterBiases = convParams.getFilterWeightsAndBiases().biases
          val prevLayerOutput = if (idx == 0) x else getLayerOutput(cnnLayerOutputs(idx - 1))
          val padding = Padding.getPadding(prevLayerOutput, filterWeights(0),
            convParams.stride, convParams.stride, convParams.padType)
          val fSize = filterWeights(0)(0).rows
          val dWeights = dWeightsAndBiases(_gradsIdx)._1
          val dBiases = dWeightsAndBiases(_gradsIdx)._2

          val dZ = convParams.getBackPropNonLinearFcn(_dALast, cnnLayerOutputs(idx)._1)
          val (dAL, dweightsOne, dLBiasesOne) = convParams.getBackPropLinearFcn(dZ, prevLayerOutput, fSize, padding, filterWeights, filterBiases)
          _dALast = dAL

          for (channel <- 0 until dWeights.size) {
            for (i <- 0 until dWeights(channel).size) {
              dWeights(channel)(i) :+= dweightsOne(channel)(i)
            }
          }
          dBiases :+= dLBiasesOne
          daPrevs(_gradsIdx) = _dALast
          _gradsIdx -= 1
        }
      }
    }
    (daPrevs, dWeightsAndBiases)
  }

  /**
   * Back propagate a minibatch through all layers of a CNN
   */
  def backPropageCNNBatch(xs: GenSeq[DenseMatrix3d],
                          fcnLayer0Gradients: LGradients,
                          layerParameters: Seq[CNNLayerParameters],
                          cnnLayerOutputs: GenSeq[Seq[(DenseMatrix3d, DenseMatrix3d)]],
                          unflattenFCNOutputsFcn: UnflattenFCNOutputsFcn = unflattenFCNOutputs): (ActvationPrevDeltas, Gradients) = {
    require(layerParameters.size == cnnLayerOutputs(0).size, s"Mismatch between layer parameters (${layerParameters.size}) and outputs(${cnnLayerOutputs(0).size})")
    val convLayerParams = layerParameters.collect { case confParams: ConvolutionLayerParameters => confParams }
    val fcnLayer0Activations = for (i <- 0 until fcnLayer0Gradients.dActivationsPrev.cols) yield (fcnLayer0Gradients.dActivationsPrev(::, i))
    val zippedParallelArgs = (xs.zip(fcnLayer0Activations).zip(cnnLayerOutputs))

    val daPrevsGradients = zippedParallelArgs.map(xs_fcnLayer0Activations_cnnLayerOutputs => {
      val x = xs_fcnLayer0Activations_cnnLayerOutputs._1._1
      val fcnLayer0Activation = xs_fcnLayer0Activations_cnnLayerOutputs._1._2
      val cnnLayerOutput = xs_fcnLayer0Activations_cnnLayerOutputs._2
      val dA = unflattenFCNOutputsFcn(fcnLayer0Activation, cnnLayerOutput)
      backPropagateOne(x, dA, layerParameters, cnnLayerOutput)
    }).foldLeft(initializeFoldLeft(convLayerParams))((accum, result) => accumResults(accum, result))
    daPrevsGradients
  }

}