/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.fcn.NormaliserFunctions

/**
 * Multichannel data set normalisers.
 * 
 * Normalisation is done in place on the passed data set
 */

object CNNNormaliserFunctions {
  
  type CNNNormaliserFunction = DenseMatrix4d => DenseMatrix4d
  
  def normaliseImageSet(dmss: DenseMatrix4d): DenseMatrix4d = {
    dmss.foreach(dms => dms.foreach(dm => NormaliserFunctions.normaliseImage(dm)))
    dmss
  }
  
  def normaliseMean(dmss: DenseMatrix4d): DenseMatrix4d = {
    dmss.foreach(dms => dms.foreach(dm => NormaliserFunctions.normaliseMean(dm)))
    dmss
  }
}