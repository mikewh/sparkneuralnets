/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn
import scala.collection.GenSeq

import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import com.vbs.nn.cnn.params.CNNLayerParameters
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.PoolingLayerParameters
import breeze.linalg.DenseMatrix

/**
 * All functions required for CNN feed forward
 */
object CNNFeedForward {

  type FeedForwardFcn = DenseMatrix3d => (DenseMatrix3d, DenseMatrix3d)

  type FlattenCCNOutputsFcn = GenSeq[DenseMatrix3d] => DenseMatrix[Double]

  type FeedForwardCNNBatchFcn = (GenSeq[DenseMatrix3d], Seq[CNNLayerParameters]) => GenSeq[Seq[(DenseMatrix3d, DenseMatrix3d)]]

  def cnnForward(convParams: ConvolutionLayerParameters)(input: DenseMatrix3d): (DenseMatrix3d, DenseMatrix3d) = {
    val filterWeights = convParams.getFilterWeightsAndBiases().weights
    val filterBiases = convParams.getFilterWeightsAndBiases().biases
    val padding = Padding.getPadding(input, filterWeights(0),
      convParams.stride, convParams.stride, convParams.padType)
    convParams.getFeedForwardLayerFcn(input, padding, filterWeights, filterBiases, convParams.activationFcn)

  }

  def poolForward(poolParams: PoolingLayerParameters)(input: DenseMatrix3d): (DenseMatrix3d, DenseMatrix3d) =
    (Pooling.poolLayer(input, poolParams.fSize, poolParams.stride, poolParams.fcn), Nil)

  def feedForwardFunctions(layerParameters: Seq[CNNLayerParameters]): Seq[FeedForwardFcn] =
    layerParameters.map(params => params match {
      case convParams: ConvolutionLayerParameters => cnnForward(convParams)(_)
      case poolParams: PoolingLayerParameters     => poolForward(poolParams)(_)
    })

  def feedForwardOne(ffFcns: Seq[FeedForwardFcn])(x: DenseMatrix3d): Seq[(DenseMatrix3d, DenseMatrix3d)] = {
    val outputsAndActivations = new Array[(DenseMatrix3d, DenseMatrix3d)](ffFcns.size)
    outputsAndActivations(0) = ffFcns(0)(x)
    for (layerIdx <- 1 until ffFcns.size) {
      val prevOutput = outputsAndActivations(layerIdx - 1)._2 match {
        case Nil => outputsAndActivations(layerIdx - 1)._1
        case _   => outputsAndActivations(layerIdx - 1)._2
      }
      outputsAndActivations(layerIdx) = ffFcns(layerIdx)(prevOutput)
    }

    outputsAndActivations
  }

  def flattenCNNOutputs(batchOutputs: GenSeq[Seq[(DenseMatrix3d, DenseMatrix3d)]],
                        flattenCCNOutputsFcn: FlattenCCNOutputsFcn = CNNToFCNInterface.flattenBatch): DenseMatrix[Double] = {
    val lastLayer = batchOutputs.map(_.last)

    val lastLayerOutputs = lastLayer.map(output => output._2 match {
      case Nil => output._1
      case _   => output._2
    }).seq

    flattenCCNOutputsFcn(lastLayerOutputs)

  }

  /**
   * Function used for local (scala .par) feed forward processing
   */
  def feedForwardCNNBatch(xs: GenSeq[DenseMatrix3d],
                          layerParameters: Seq[CNNLayerParameters]): GenSeq[Seq[(DenseMatrix3d, DenseMatrix3d)]] = {
    val ffFcns = feedForwardFunctions(layerParameters)
    val feedForwardOneFcn = feedForwardOne(ffFcns)(_)
    xs.map(x => feedForwardOneFcn(x)).seq
  }

}