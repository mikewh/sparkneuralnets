/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.cnn

import breeze.linalg.DenseMatrix

case class Padding(pTop: Int, pLeft: Int, pBottom: Int, pRight: Int, strideH: Int, strideW: Int,
                   layerDims: (Int, Int) => (Int, Int)) {

  override def toString = s"padding($pTop, $pLeft, $pBottom, $pRight), strides ($strideH, $strideW)"

  def padInput(input: DenseMatrix[Double]): DenseMatrix[Double] = {
    val padded = DenseMatrix.zeros[Double](input.rows + pTop + pBottom, input.cols + pLeft + pRight)
    padded(pTop until pTop + input.rows, pLeft until pLeft + input.cols) := input
    padded
  }

  def dePad(input: DenseMatrix[Double]): DenseMatrix[Double] =
    input(pTop until (input.rows - pBottom), pLeft until (input.cols - pRight))

}

object Padding {
  import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

  def getValidPaddingSize(iSize: Int, fSize: Int, stride: Int): (Int, Int, Int, Int) =
    getValidPaddingSize(iSize, iSize, fSize, fSize, stride, stride)

  /**
   * See http://machinelearninguru.com/computer_vision/basics/convolution/convolution_layer.html
   */
  def getValidPaddingSize(iRows: Int, iCols: Int, fH: Int, fW: Int, strideH: Int, strideW: Int): (Int, Int, Int, Int) = {
    val outputH = Math.ceil((iRows - fH + 1) / strideH).toInt
    val outputW = Math.ceil((iCols - fW + 1) / strideW).toInt
    val pH = Math.max(((outputH - 1) * strideH + fH - iRows).toInt, 0)
    val pW = Math.max(((outputW - 1) * strideW + fW - iCols).toInt, 0)
    val pT = Math.floor(pH / 2).toInt
    val pL = Math.floor(pW / 2).toInt
    val pB = pH - pT
    val pR = pW - pL
    (pT, pL, pB, pR)
  }

  def padInput(input: DenseMatrix[Double], pad: Int): DenseMatrix[Double] = {
    val padded = DenseMatrix.zeros[Double](input.rows + 2 * pad, input.cols + 2 * pad)
    for (c <- 0 until input.cols) {
      for (r <- 0 until input.rows) {
        padded(r + pad, c + pad) = input(r, c)
      }
    }
    padded
  }

  def padInput(input: DenseMatrix[Double], pads: (Int, Int, Int, Int)): DenseMatrix[Double] = padInput(input, pads._1, pads._2, pads._3, pads._4)

  def padInput(input: DenseMatrix[Double], pTop: Int, pLeft: Int, pBottom: Int, pRight: Int): DenseMatrix[Double] = {
    val padded = DenseMatrix.zeros[Double](input.rows + pTop + pBottom, input.cols + pLeft + pRight)
    for (c <- 0 until input.cols) {
      for (r <- 0 until input.rows) {
        padded(r + pTop, c + pLeft) = input(r, c)
      }
    }
    padded
  }

  def dePad(input: DenseMatrix[Double], pad: Int): DenseMatrix[Double] = {
    val dePadded = DenseMatrix.zeros[Double](input.rows - 2 * pad, input.cols - 2 * pad)
    for (c <- 0 until dePadded.cols) {
      for (r <- 0 until dePadded.rows) {
        dePadded(r, c) = input(r + pad, c + pad)
      }
    }
    dePadded
  }

  def getLayerDim(prevLayerDim: Int, filterSize: Int, pad: Int, stride: Int): Int = ((prevLayerDim - filterSize + 2 * pad) / stride) + 1

  def getSamePaddingTensorflow(iSize: Int, fSize: Int, stride: Int): (Int, Int, Int, Int) =
    getSamePaddingTensorflow(iSize, iSize, fSize, fSize, stride, stride)

  /**
   * See http://machinelearninguru.com/computer_vision/basics/convolution/convolution_layer.html
   */
  def getSamePaddingTensorflow(iRows: Int, iCols: Int, fH: Int, fW: Int, strideH: Int, strideW: Int): (Int, Int, Int, Int) = {
    val outputH = Math.ceil(iRows / strideH).toInt
    val outputW = Math.ceil(iCols / strideW).toInt
    val pH = Math.max(((outputH - 1) * strideH + fH - iRows).toInt, 0)
    val pW = Math.max(((outputW - 1) * strideW + fW - iCols).toInt, 0)
    val pT = Math.floor(pH / 2).toInt
    val pL = Math.floor(pW / 2).toInt
    val pB = pH - pT
    val pR = pW - pL
    (pT, pL, pB, pR)
  }

  def newDimsPadded(pTop: Int, pLeft: Int, pBottom: Int, pRight: Int, fH: Int, fW: Int, strideH: Int, strideW: Int)(h: Int, w: Int): (Int, Int) = {

    val newH = ((h - fH + pTop + pBottom) / strideH) + 1
    val newW = ((w - fW + pLeft + pRight) / strideW) + 1
    (newH, newW)
  }

  def newDimsSame(h: Int, w: Int): (Int, Int) = (h, w)

  def getPadding(iSize: Int, fSize: Int, stride: Int, padType: String): Padding = padType.toLowerCase match {
    case "same" => {
      val (pTop, pLeft, pBottom, pRight) = getSamePaddingTensorflow(iSize, fSize, stride)
      Padding(pTop, pLeft, pBottom, pRight, stride, stride, newDimsSame)
    }
    case "valid" => {
      val (pTop, pLeft, pBottom, pRight) = getValidPaddingSize(iSize, fSize, stride)
      Padding(pTop, pLeft, pBottom, pRight, stride, stride, newDimsPadded(pTop, pLeft, pBottom, pRight, fSize, fSize, stride, stride))
    }
    case _ => throw new IllegalArgumentException(s"Unsupported padding type: '$padType'")
  }

  def getPadding(iH: Int, iW: Int, fH: Int, fW: Int, strideH: Int, strideW: Int, padType: String): Padding = padType.toLowerCase match {
    case "same" => {
      val (pTop, pLeft, pBottom, pRight) = getSamePaddingTensorflow(iH, iW, fH, fW, strideH, strideW)
      Padding(pTop, pLeft, pBottom, pRight, strideH, strideW, newDimsSame)
    }
    case "valid" => {
      val (pTop, pLeft, pBottom, pRight) = getValidPaddingSize(iH, iW, fH, fW, strideH, strideW)
      Padding(pTop, pLeft, pBottom, pRight, strideH, strideW, newDimsPadded(pTop, pLeft, pBottom, pRight, fH, fW, strideH, strideW))
    }
    case _ => throw new IllegalArgumentException(s"Unsupported padding type: '$padType'")
  }

  def getPadding(input: DenseMatrix3d, filter: DenseMatrix3d, strideH: Int, strideW: Int, padType: String): Padding =
    getPadding(input(0).rows, input(0).cols, filter(0).rows, filter(0).cols, strideH, strideW, padType)

}