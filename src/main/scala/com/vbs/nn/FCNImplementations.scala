/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn

import breeze.linalg.DenseMatrix
import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.fcn.ActivationFunctions
import com.vbs.nn.fcn.ActivationFunctions.OutputsAndActivationsL
import com.vbs.nn.params.Parameters
import com.vbs.nn.params.LGradients
import com.vbs.nn.fcn.BackPropagationFunctions

/**
 * Allows default implementations of feed forward and back propagation through fully connected network to be overridden (used for debugging)
 */
sealed trait FCNImplementations {
  type FeedForwardSignature = (DenseMatrix[Double], Array[LWeightsAndBiases], Option[(Array[Double], Array[DenseMatrix[Double]])]) => Array[OutputsAndActivationsL]

  type BackPropagateSignature = (DenseMatrix[Double], DenseMatrix[Double], Array[OutputsAndActivationsL], Array[LWeightsAndBiases], Double, Option[(Array[Double], Array[DenseMatrix[Double]])]) => Array[LGradients]

  def feedForward: FeedForwardSignature

  def backPropagate: BackPropagateSignature

}

class DefaultFCNImplementations(params: Parameters) extends FCNImplementations {
  def feedForward = ActivationFunctions.feedForward(params.activationFunctions())(_, _, _)
  
  def backPropagate = BackPropagationFunctions.backPropagate(params.backPropationFunctions(), BackPropagationFunctions.backwardOutputLayer) (_, _, _, _, _, _)
}