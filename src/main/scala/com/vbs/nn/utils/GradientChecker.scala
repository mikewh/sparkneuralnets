/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.utils
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.LGradients
import com.vbs.nn.fcn.ActivationFunctions._
import com.vbs.nn.fcn.CostFunctions._

import scala.annotation.tailrec
import breeze.linalg.norm
import com.vbs.nn.params.Parameters
import com.vbs.nn.params.OptimizerParams

/**
 * Can be used to verify back propagation through an FCN
 */
object GradientChecker {
  
  def gradientCheck(p: Parameters, optimizerParams: OptimizerParams, gradients: Array[LGradients],
                         xs: DenseMatrix[Double], ys: DenseMatrix[Double],
                         epsilon: Double = 1e-7, maxDiff: Double = 2e-7): Boolean = {
    gradientDifference(p, optimizerParams, gradients, xs, ys, epsilon) <= maxDiff
  }

  
  def gradientDifference(p: Parameters, optimizerParams: OptimizerParams, gradients: Array[LGradients],
                         xs: DenseMatrix[Double], ys: DenseMatrix[Double],
                         epsilon: Double): Double = {
    val paramCount = optimizerParams.weightsAndBiases.map(wb => (wb.weights.rows * wb.weights.cols + wb.biases.size)).sum
    val flattenedGradients = DenseVector(gradients.map(lGradients => lGradients.dWeights.t.toArray ++ lGradients.dBiases.toArray).flatten)
    require(paramCount == flattenedGradients.size, s"Mismatch between number of weights and biases ($paramCount) and gradients (${flattenedGradients.size})")

    def deriveCosts(signedEpsilon: Double): Array[Double] = {
      (for (idx <- 0 until paramCount) yield {
        val outputsAndActivations =
          feedForward(p.activationFunctions())(xs, applyEpsilonToElement(optimizerParams.weightsAndBiases, idx, signedEpsilon), None)
        val activationsLast = outputsAndActivations.last._2
        val (_, cost) = regularizedCost(p.costFunction(), p.weightRegularizer, optimizerParams.weightsAndBiases, activationsLast, ys)
        cost
      }).toArray

    }
    val costsPlus = DenseVector(deriveCosts(epsilon))
    val costsMinus = DenseVector(deriveCosts(-epsilon))

    val gradsApprox = (costsPlus - costsMinus) / (2 * epsilon)
    val numerator = norm(flattenedGradients - gradsApprox)

    val denominator = norm(flattenedGradients) + norm(gradsApprox)

    numerator / denominator
  }

  private[utils] def applyEpsilonToElement(weightsAndBiases: Array[LWeightsAndBiases], elemIdx: Int, epsilon: Double): Array[LWeightsAndBiases] = {
    val wbIndexes = weightsAndBiases.map(wb => (wb.weights.rows * wb.weights.cols, wb.biases.size))

    @tailrec def entryToUpdate(idx: Int = 0, accum: Int = 0): (Int, Int) = {
      val lastIdx = accum + (wbIndexes(idx)._1 + wbIndexes(idx)._2)
      if (elemIdx < lastIdx) (idx, elemIdx - accum)
      else if (idx == wbIndexes.size - 1)
        throw new IndexOutOfBoundsException(s"$elemIdx is greater than total number of params(${wbIndexes.map(wb => (wb._1 + wb._2)).sum})")
      else entryToUpdate(idx + 1, lastIdx)
    }

    val (i, offset) = entryToUpdate()
    val updatedWB = {
      val wtElems = weightsAndBiases(i).weights.rows * weightsAndBiases(i).weights.cols
      if (offset < wtElems) {
        val c = offset % weightsAndBiases(i).weights.cols
        val r = offset / weightsAndBiases(i).weights.cols
        val dm = weightsAndBiases(i).weights.copy
        dm(r, c) += epsilon
        LWeightsAndBiases(dm, weightsAndBiases(i).biases)
      } else {
        val j = offset - wtElems
        val dv = weightsAndBiases(i).biases.copy
        dv(j) += epsilon
        LWeightsAndBiases(weightsAndBiases(i).weights, dv)
      }
    }

    val returnedWAndb = weightsAndBiases.map(x => x)
    returnedWAndb(i) = updatedWB

    returnedWAndb
  }

}