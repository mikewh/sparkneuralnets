/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.utils
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.io.File

/**
 * Persist image data as a jpg
 */
object ImageUtils {

/**
 * See http://otfried.org/scala/image.html
 */
  def persist(fName: String, imageData: DenseMatrix3d): Unit = {
    val w = imageData(0).cols
    val h = imageData(0).rows
    val redDm = imageData(0).map(_.toInt) :* 65536
    val greenDm = imageData(1).map(_.toInt) :* 256
    val blueDm = imageData(2).map(_.toInt)

    val rgbDm = redDm + greenDm + blueDm
    val out = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB)
    for (x <- 0 until w) {
      for (y <- 0 until h) {
        out.setRGB(x, y, rgbDm(y, x))
      }
    }
    ImageIO.write(out, "jpg", new File(s"$fName.jpg"))
    ()
  }

}