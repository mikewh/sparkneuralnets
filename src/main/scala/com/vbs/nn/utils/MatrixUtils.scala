/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.utils
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d

object MatrixUtils {

  def oneHotEncode(labels: Seq[Double], numValues: Int): DenseMatrix[Double] = {
    val dm = DenseMatrix.zeros[Double](numValues, labels.size)
    for (c <- 0 until labels.size) {
      val r = labels(c).toInt
      require(r >= 0 && r < numValues, s"Encountered invalid label (${labels(c)}). Value must be between 0 and $numValues")
      dm(r, c) = 1.0
    }
    dm
  }

  def oneHotEncodeAsVector(labels: Seq[Double], numValues: Int): Seq[DenseVector[Double]] = {
    labels.map(l => {
      val dv = DenseVector.zeros[Double](numValues)
      val c = l.toInt
      require(c >= 0 && c < numValues, s"Encountered invalid label ($l). Value must be between 0 and $numValues")
      dv(c) = 1.0
      dv
    })
  }
  
  def oneHotDecode(dm: DenseMatrix[Double]): Seq[Double] = {
    import breeze.linalg._
    for (c <- 0 until dm.cols) yield {
      val row = dm(::, c)
      argmax(row).toDouble
    }
  }
  
  def binaryDecode(threshold: Double = 0.5)(dm: DenseMatrix[Double]): Seq[Double] = {
    require(dm.rows == 1, s"Expected 1 label value but got ${dm.rows}")
    dm.toArray.map(l => if (l > threshold) 1.0 else 0.0)
  }
  
  def toDenseMatrix(s: String): DenseMatrix[Double] = {
    toDenseMatrixInternal(cleanString(s))
  }

  def vectorToString(dv: DenseVector[Double]): String = s"[${dv.toArray.mkString(" ")}]"

  def matrixToString(dm: DenseMatrix[Double]): String = {
    val dms = (for (r <- 0 until dm.rows) yield (vectorToString(dm(r, ::).t))).mkString("\n")
    s"[$dms]"
  }

  def matrix4dToString(dmaa: DenseMatrix4d): String = {
    s"[\n${dmaa.map(matrix3dToString).mkString("\n")}\n]"
  }

  def matrix3dToString(dma: DenseMatrix3d): String =
    s"[\n${dma.map(matrixToString).mkString("\n")}\n]"

  def vectorArrayToString(dva: Array[DenseVector[Double]]): String =
    s"[\n${dva.map(vectorToString).mkString("\n")}\n]"

  def cleanString(s: String): String = s.replaceAll(",", " ")
    .replaceAll("\\s+", " ")
    .replaceAll("\\[[ ]+", "\\[").replaceAll("\\][ ]+", "\\]")
    .trim

  private[utils] def toDenseMatrixInternal(matrix: String): DenseMatrix[Double] = {
    require(matrix.startsWith("[[") && matrix.endsWith("]]"), matrix)
    val rows = matrix.drop(2).dropRight(2).split("\\]\\[")
    val m = rows.map(row => row.trim.split(" "))
    val c = m(0).size
    val r = m.size

    new DenseMatrix(c, r, m.flatten.map(_.toDouble)).t

  }

  def toDenseMatrix4d(s: String): DenseMatrix4d = {
    val matrices = cleanString(s)
    require(matrices.startsWith("[[[[") && matrices.endsWith("]]]]"), matrices)
    val ma = matrices.drop(1).dropRight(1)
      .replaceAll("\\]\\]\\]\\[\\[\\[", "\\]\\]\\]\\]\\]\\]\\[\\[\\[\\[\\[\\[")
      .split("\\]\\]\\]\\[\\[\\[")
    ma.map(toDenseMatrix3d)
  }

  def toDenseMatrix3d(s: String): DenseMatrix3d = {
    val matrices = cleanString(s)
    require(matrices.startsWith("[[[") && matrices.endsWith("]]]"), matrices)
    val ma = matrices.drop(1).dropRight(1)
      .replaceAll("\\]\\]\\[\\[", "\\]\\]\\]\\]\\[\\[\\[\\[")
      .split("\\]\\]\\[\\[")

    ma.map(toDenseMatrixInternal)
  }

  def toDenseMatrixArray(s: String): Array[DenseMatrix[Double]] = {
    val matrices = cleanString(s)
    require(matrices.startsWith("[[[") && matrices.endsWith("]]]"), matrices)
    val ma = matrices.drop(1).dropRight(1)
      .replaceAll("\\]\\]\\[\\[", "\\]\\]\\]\\]\\[\\[\\[\\[")
      .split("\\]\\]\\[\\[")

    ma.map(toDenseMatrixInternal)
  }

  def toDenseVector(s: String): DenseVector[Double] = {
    toDenseVectorInternal(cleanString(s))
  }

  def toDenseVectorArray(s: String): Array[DenseVector[Double]] = {
    val vectors = cleanString(s)
    require(vectors.startsWith("[[") && vectors.endsWith("]]"), vectors)
    val ma = vectors.drop(1).dropRight(1)
      .replaceAll("\\]\\[", "\\]\\]\\[\\[")
      .split("\\]\\[")

    ma.map(toDenseVectorInternal)
  }

  private def toDenseVectorInternal(vector: String): DenseVector[Double] = {
    require(vector.startsWith("[") && vector.endsWith("]"), vector)
    new DenseVector(vector.drop(1).dropRight(1).split(" ").map(_.toDouble))
  }

}