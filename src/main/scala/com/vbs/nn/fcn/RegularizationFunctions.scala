/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import breeze.linalg.DenseMatrix
import breeze.linalg._
import com.vbs.nn.params.RegularizationParams
import com.vbs.nn.params.LWeightsAndBiases

/**
 * L1, L2 and dropout regularization (currently only used by FCNs)
 */
object RegularizationFunctions {

  type RegularizationFunction = (Int, Double, Array[DenseMatrix[Double]]) => Double
  
  private [fcn] def l2Regularization(lambda: Double) (m: Int, cost: Double, weights: Array[DenseMatrix[Double]]): Double = {
    val l2 =  (lambda * 0.5 * Math.pow(weights.map(sum(_)).sum, 2)) / m
    cost + l2
  }
  
  private [fcn] def l1Regularization(lambda: Double) (m: Int, cost: Double, weights: Array[DenseMatrix[Double]]): Double = {
    val l1 =  (lambda * weights.map(sum(_)).sum) / m
    cost + l1
  }
  
  def getWeightRegularizer(lambda: Double, regularizationName: String): RegularizationParams = regularizationName.toLowerCase() match {
    case "l1" => RegularizationParams(lambda, l1Regularization(lambda) (_,_,_))
    case "l2" =>  RegularizationParams(lambda, l2Regularization(lambda) (_,_,_))
    case _ => throw new IllegalArgumentException(s"Unsupported regularization function: '${regularizationName}'")
  }
  
  def generateDropouts(weightsAndBiases: Array[LWeightsAndBiases], keepProbabilities: Array[Double], m: Int): Array[DenseMatrix[Double]] = {
    (for (i <- 0 until keepProbabilities.size) yield {
      val r = weightsAndBiases(i).weights.rows
      DenseMatrix.rand(r, m).map(x => if (x < (1 - keepProbabilities(i))) 0.0 else 1.0)
    }).toArray
  }
}