/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import breeze.numerics.pow
import breeze.numerics.sqrt

import com.vbs.nn.params.OptimizedParamsSimple
import com.vbs.nn.params.OptimizedParamsMomentum
import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.LGradients
import com.vbs.nn.params.OptimizedParamsSetSimple
import com.vbs.nn.params.OptimizedParamsSetMomentum
import com.vbs.nn.params.LayerDeltas
import com.vbs.nn.params.OptimizedParamsAdam
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.params.OptimizedParamsSetAdam

/**
 * All supported Optimizers, some of which are used by both CNNs and FCNs
 */
object OptimizerFunctions {

  /**
   * There are 2 underlying adam "raw" functions both of which are exposed by this api.  
   * 
   * @param learningRate
   * @param beta1
   * @param beta2
   * @param epsilon
   * @param callCount
   * @param weights
   * @param biases
   * @param dWeights
   * @param dBiases
   * @param avgGradientWeights
   * @param avgGradientBiases
   * @param avgSqGradientsWeights
   * @param avgSqGradientsBiases
   * 
   * @return Updated weights, biases, avgGradientWeights, avgGradientBiases, avgSqGradientsWeights, avgSqGradientsBiases
   * 
   */
  type RawAdamFunctionSignature = (Double, Double, Double, Double, Int, 
      DenseMatrix[Double], DenseVector[Double], 
      DenseMatrix[Double], DenseVector[Double], 
      DenseMatrix[Double], DenseVector[Double], 
      DenseMatrix[Double], DenseVector[Double]) => 
         (DenseMatrix[Double], DenseVector[Double], 
          DenseMatrix[Double], DenseVector[Double], 
          DenseMatrix[Double], DenseVector[Double])

  private [fcn] def optimizeSimpleL(learningRate: Double,
                      weightsAndBiasesL: LWeightsAndBiases,
                      gradientsL: LGradients): OptimizedParamsSimple = {
    val (uW, uB) = optimizeSimpleL(learningRate, weightsAndBiasesL.weights, weightsAndBiasesL.biases, gradientsL.dWeights, gradientsL.dBiases)
    OptimizedParamsSimple(LWeightsAndBiases(uW, uB), gradientsL)
  }

  def optimizeSimpleL(learningRate: Double,
                      weights: DenseMatrix[Double], biases: DenseVector[Double],
                      dWeights: DenseMatrix[Double], dBiases: DenseVector[Double]): (DenseMatrix[Double], DenseVector[Double]) = {
    val w = weights :- (dWeights :* learningRate)
    val b = biases :- (dBiases :* learningRate)
    (w, b)
  }

  private [fcn] def optimizeMomentumL(learningRate: Double, beta: Double,
                        weightsAndBiasesL: LWeightsAndBiases,
                        gradientsL: LGradients,
                        velocities: LayerDeltas): OptimizedParamsMomentum = {
    val uwV = (velocities.wDeltas :* beta) :+ (gradientsL.dWeights :* (1.0 - beta))
    val ubV = (velocities.bDeltas :* beta) :+ (gradientsL.dBiases :* (1.0 - beta))
    val w = weightsAndBiasesL.weights :- (uwV :* learningRate)
    val b = weightsAndBiasesL.biases :- (ubV :* learningRate)
    OptimizedParamsMomentum(LWeightsAndBiases(w, b), gradientsL, LayerDeltas(uwV, ubV))
  }

  private [fcn] def optimizeAdamL(learningRate: Double, beta1: Double, beta2: Double, epsilon: Double, callCount: Int,
                    weightsAndBiasesL: LWeightsAndBiases,
                    gradientsL: LGradients,
                    avgGradientsL: LayerDeltas,
                    avgSqGradientsL: LayerDeltas): OptimizedParamsAdam =
    optimizeAdamL(learningRate, beta1, beta2, epsilon, callCount,
      weightsAndBiasesL,
      gradientsL,
      avgGradientsL,
      avgSqGradientsL,
      optimizeAdamRawL)

  private [fcn] def optimizeAdamTFL(learningRate: Double, beta1: Double, beta2: Double, epsilon: Double, callCount: Int,
                      weightsAndBiasesL: LWeightsAndBiases,
                      gradientsL: LGradients,
                      avgGradientsL: LayerDeltas,
                      avgSqGradientsL: LayerDeltas): OptimizedParamsAdam =
    optimizeAdamL(learningRate, beta1, beta2, epsilon, callCount,
      weightsAndBiasesL,
      gradientsL,
      avgGradientsL,
      avgSqGradientsL,
      optimizeAdamTensorflowL)

  private [fcn] def optimizeAdamL(learningRate: Double, beta1: Double, beta2: Double, epsilon: Double, callCount: Int,
                    weightsAndBiasesL: LWeightsAndBiases,
                    gradientsL: LGradients,
                    avgGradientsL: LayerDeltas,
                    avgSqGradientsL: LayerDeltas,
                    rawAdamFcn: RawAdamFunctionSignature): OptimizedParamsAdam = {

    val (updatedWeights, updatedBiases,
      avgDeltasWeights, avgDeltasBiases,
      avgSqDeltasWeights, avgSqBiases) = rawAdamFcn(learningRate, beta1, beta2, epsilon, callCount,
      weightsAndBiasesL.weights, weightsAndBiasesL.biases,
      gradientsL.dWeights, gradientsL.dBiases,
      avgGradientsL.wDeltas, avgGradientsL.bDeltas,
      avgSqGradientsL.wDeltas, avgSqGradientsL.bDeltas)

    OptimizedParamsAdam(LWeightsAndBiases(updatedWeights, updatedBiases), gradientsL,
      LayerDeltas(avgDeltasWeights, avgDeltasBiases), LayerDeltas(avgSqDeltasWeights, avgSqBiases))
  }

  private [fcn] def optimizeAdamTensorflowL(learningRate: Double, beta1: Double, beta2: Double, epsilon: Double, callCount: Int,
                              weights: DenseMatrix[Double], biases: DenseVector[Double],
                              dWeights: DenseMatrix[Double], dBiases: DenseVector[Double],
                              avgGradientWeights: DenseMatrix[Double], avgGradientBiases: DenseVector[Double],
                              avgSqGradientsWeights: DenseMatrix[Double],
                              avgSqGradientsBiases: DenseVector[Double]): (DenseMatrix[Double], DenseVector[Double], DenseMatrix[Double], DenseVector[Double], DenseMatrix[Double], DenseVector[Double]) = {

    val lrt = learningRate * Math.sqrt(1.0 - Math.pow(beta2, callCount)) / (1.0 - Math.pow(beta1, callCount))

    val uwAvgGrads = (avgGradientWeights :* beta1) :+ (dWeights :* (1.0 - beta1))
    val ubAvgGrads = (avgGradientBiases :* beta1) :+ (dBiases :* (1.0 - beta1))

    val uwAvgSqGrads = (avgSqGradientsWeights :* beta2) :+ (pow(dWeights, 2) :* (1.0 - beta2))
    val ubAvgSqGrads = (avgSqGradientsBiases :* beta2) :+ (pow(dBiases, 2) :* (1.0 - beta2))

    val uwV = uwAvgGrads / (sqrt(uwAvgSqGrads) :+ epsilon)
    val ubV = ubAvgGrads / (sqrt(ubAvgSqGrads) :+ epsilon)
    val w = weights :- (uwV :* lrt)
    val b = biases :- (ubV :* lrt)

    (w, b, uwAvgGrads, ubAvgGrads, uwAvgSqGrads, ubAvgSqGrads)
  }

  def optimizeAdamRawL(learningRate: Double, beta1: Double, beta2: Double, epsilon: Double, callCount: Int,
                       weights: DenseMatrix[Double], biases: DenseVector[Double],
                       dWeights: DenseMatrix[Double], dBiases: DenseVector[Double],
                       avgGradientWeights: DenseMatrix[Double], avgGradientBiases: DenseVector[Double],
                       avgSqGradientsWeights: DenseMatrix[Double],
                       avgSqGradientsBiases: DenseVector[Double]): (DenseMatrix[Double], DenseVector[Double], DenseMatrix[Double], DenseVector[Double], DenseMatrix[Double], DenseVector[Double]) = {

    val uwAvgGrads = (avgGradientWeights :* beta1) :+ (dWeights :* (1.0 - beta1))
    val ubAvgGrads = (avgGradientBiases :* beta1) :+ (dBiases :* (1.0 - beta1))

    val moment1 = 1 - Math.pow(beta1, callCount)
    val uwAvgGrads1 = uwAvgGrads :/ moment1
    val ubAvgGrads1 = ubAvgGrads :/ moment1

    val uwAvgSqGrads = (avgSqGradientsWeights :* beta2) :+ (pow(dWeights, 2) :* (1.0 - beta2))
    val ubAvgSqGrads = (avgSqGradientsBiases :* beta2) :+ (pow(dBiases, 2) :* (1.0 - beta2))

    val moment2 = 1 - Math.pow(beta2, callCount)
    val uwAvgSqGrads1 = uwAvgSqGrads :/ moment2
    val ubAvgSqGrads1 = ubAvgSqGrads :/ moment2

    val uwV = uwAvgGrads1 / (sqrt(uwAvgSqGrads1) :+ epsilon)
    val ubV = ubAvgGrads1 / (sqrt(ubAvgSqGrads1) :+ epsilon)
    val w = weights :- (uwV :* learningRate)
    val b = biases :- (ubV :* learningRate)

    (w, b, uwAvgGrads, ubAvgGrads, uwAvgSqGrads, ubAvgSqGrads)
  }
  def optimizeSimple(learningRate: Double,
                     weightsAndBiases: Array[LWeightsAndBiases],
                     gradients: Array[LGradients]): OptimizedParamsSetSimple = {
    require(weightsAndBiases.size == gradients.size, s"Mismatch between weightsAndBiases and gradients sizes (${weightsAndBiases.size}, ${gradients.size})")
    val optimizedParams = weightsAndBiases.zip(gradients).map { case (wb, grads) => optimizeSimpleL(learningRate, wb, grads) }
    OptimizedParamsSetSimple(optimizedParams)
  }

  def optimizeMomentum(learningRate: Double, beta: Double,
                       weightsAndBiases: Array[LWeightsAndBiases],
                       gradients: Array[LGradients],
                       velocities: Array[LayerDeltas]): OptimizedParamsSetMomentum = {
    require(weightsAndBiases.size == gradients.size, s"Mismatch between weightsAndBiases and gradients sizes (${weightsAndBiases.size}, ${gradients.size})")
    val optimizedParams = (for (idx <- 0 until weightsAndBiases.size) yield {
      optimizeMomentumL(learningRate, beta, weightsAndBiases(idx), gradients(idx), velocities(idx))
    }).toArray
    OptimizedParamsSetMomentum(optimizedParams)
  }

  def optimizeAdam(learningRate: Double, beta1: Double, beta2: Double, epsilon: Double, callCount: Int,
                   weightsAndBiases: Array[LWeightsAndBiases],
                   gradients: Array[LGradients],
                   avgGradients: Array[LayerDeltas],
                   avgSqGradients: Array[LayerDeltas]): OptimizedParamsSetAdam = {
    val optimizedParams = (for (idx <- 0 until weightsAndBiases.size) yield {
      optimizeAdamL(learningRate, beta1, beta2, epsilon, callCount, weightsAndBiases(idx), gradients(idx), avgGradients(idx), avgSqGradients(idx))
    }).toArray
    OptimizedParamsSetAdam(optimizedParams)

  }

  def optimizeAdamTF(learningRate: Double, beta1: Double, beta2: Double, epsilon: Double, callCount: Int,
                     weightsAndBiases: Array[LWeightsAndBiases],
                     gradients: Array[LGradients],
                     avgGradients: Array[LayerDeltas],
                     avgSqGradients: Array[LayerDeltas]): OptimizedParamsSetAdam = {
    val optimizedParams = (for (idx <- 0 until weightsAndBiases.size) yield {
      optimizeAdamTFL(learningRate, beta1, beta2, epsilon, callCount, weightsAndBiases(idx), gradients(idx), avgGradients(idx), avgSqGradients(idx))
    }).toArray
    OptimizedParamsSetAdam(optimizedParams)

  }

  def initializeDeltas(weightsAndBiases: Array[LWeightsAndBiases]): Array[LayerDeltas] =
    weightsAndBiases.map(wb => LayerDeltas(DenseMatrix.zeros[Double](wb.weights.rows, wb.weights.cols), DenseVector.zeros[Double](wb.biases.size)))

}