/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import breeze.linalg.DenseMatrix
import breeze.stats.{ stddev, mean }

/**
 * Feature sets normalisers (not multi channel data).
 * 
 * Normalisation is done in place on the passed feature set
 */
object NormaliserFunctions {
  
  type NormaliserFunction = DenseMatrix[Double] => DenseMatrix[Double]
  
  def normaliseImage(dm: DenseMatrix[Double]): DenseMatrix[Double] = dm :/= 255.0
  
  def normaliseMean(dm: DenseMatrix[Double]): DenseMatrix[Double] = {
    val s = stddev(dm)
    val m = mean(dm)
    if (s == 0.0) dm else (dm :-= m) :/= s 
  }
  
}