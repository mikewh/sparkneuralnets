/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import breeze.linalg.DenseMatrix
import breeze.numerics._
import breeze.linalg._

import com.vbs.nn.params.RegularizationParams
import com.vbs.nn.params.LWeightsAndBiases

/**
 *
 * Cost functions
 *
 */

object CostFunctions {

  /**
   * Returns cost for output with dimensions (size of output layer, number of examples (m)).
   *
   * @param activationsLast Activations from for the last layer of the network with dimensions (size of layer, number of examples (m))
   * @param targets Labels with dimensions (size of layer, number of examples (m))
   */
  type CostFunction = (DenseMatrix[Double], DenseMatrix[Double]) => Double
  
  
  private def squaredError(activationsLast: DenseMatrix[Double], targets: DenseMatrix[Double]): Double = {
    require(activationsLast.rows == targets.rows && activationsLast.cols == targets.cols,
      s"last layer activations(${activationsLast.rows}, ${activationsLast.cols}) and targets(${targets.rows}, ${targets.cols}) do not have the same dimensions")
      sum((targets - activationsLast).map(diff => 0.5 * pow(diff, 2))) / targets.cols
  }

  /**
   * As defined in Tensorflow, used with CNN's
   */
  private def crossEntropyTF(activationsLast: DenseMatrix[Double], targets: DenseMatrix[Double]): Double = {
    require(activationsLast.rows == targets.rows && activationsLast.cols == targets.cols,
      s"last layer activations(${activationsLast.rows}, ${activationsLast.cols}) and targets(${targets.rows}, ${targets.cols}) do not have the same dimensions")
      (-sum((targets :* log(activationsLast)))) / targets.cols
  }
  
  /**
   * Appears to work better when testing standalone MNist FCN and with the gradient checker
   */
  private def crossEntropy(activationsLast: DenseMatrix[Double], targets: DenseMatrix[Double]): Double = {
    require(activationsLast.rows == targets.rows && activationsLast.cols == targets.cols,
      s"last layer activations(${activationsLast.rows}, ${activationsLast.cols}) and targets(${targets.rows}, ${targets.cols}) do not have the same dimensions")
      (-sum((targets :* log(activationsLast)) + ((1.0 - targets) :* log(1.0 - activationsLast)))) / targets.cols
  }

  def getCostFunction(id: String): CostFunction = id.toLowerCase match {
    case "crossentropytf" => crossEntropyTF
    case "crossentropy" => crossEntropy
    case "squarederror" => squaredError
    case _              => throw new IllegalArgumentException(s"Unsupported cost function: '$id'")
  }

  /**
   * Returns cost after applying any regularisation, together with reguliszation parameter lambds, set to 0 if no regularisation applied
   */
  def regularizedCost(costFunction: CostFunction, weightRegularizer: Option[RegularizationParams],
                      weightsAndBiases: Array[LWeightsAndBiases],
                      activationsLast: DenseMatrix[Double], ys: DenseMatrix[Double]): (Double, Double) = weightRegularizer match {
    case None => (0.0, costFunction(activationsLast, ys))
    case Some(regParams) =>
      (regParams.lambda, regParams.regularizationFunction(ys.cols, costFunction(activationsLast, ys), weightsAndBiases.map(_.weights)))
  }

}