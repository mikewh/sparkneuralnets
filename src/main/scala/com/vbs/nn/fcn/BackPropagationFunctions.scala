/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import breeze.linalg.DenseMatrix
import breeze.linalg._
import breeze.numerics._
import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.LGradients
import com.vbs.nn.params.ActivationParams
import ActivationFunctions._
/**
 * All functions required for back propagation through an FCN
 */

object BackPropagationFunctions {

  /**
   * Signature of the non-linear component functions
   */
  type NonLinearBackPropFcn = (DenseMatrix[Double], OutputsAndActivationsL) => DenseMatrix[Double]

  /**
   * Signature of the output layer back propagation function
   */
  type BackPropagationOutputFunction = (DenseMatrix[Double], DenseMatrix[Double]) => DenseMatrix[Double]

  /**
   * Public signature for non-output back propagation functions
   * @param dA Gradient of cost wrt current layer activations, dimensions are (current layer, number of examples)
   * @param aPrev Previous layer activations, dimensions are (previous layer, number of examples)
   * @param outputsAndActivationsL The outputs (Z) and activations for the current layer, dimensions of both are (current layer, number of examples)
   * @param weightsAndBiases Weights and Biases for the current layer
   * @param lambda Non zero if weight regularization is being used
   */
  type BackPropagationLayerFunction = (DenseMatrix[Double], DenseMatrix[Double], OutputsAndActivationsL, LWeightsAndBiases, Double) => LGradients

  def linearBackwardL(dZ: DenseMatrix[Double], aPrev: DenseMatrix[Double], weightsAndBias: LWeightsAndBiases, lambda: Double): LGradients = {
    val daPrev = weightsAndBias.weights.t * dZ
    val m = aPrev.cols.toDouble
    val dW = ((dZ * aPrev.t) :/ m) :+ (weightsAndBias.weights :* lambda / m)
    val dB = sum(dZ, Axis._1) :/ m
    LGradients(daPrev, dW, dB)
  }

  private[fcn] def sigmoidBackwardOutputLayer(activationsLast: DenseMatrix[Double], targets: DenseMatrix[Double]): DenseMatrix[Double] =
    -1.0 :* ((targets :/ activationsLast) - ((1.0 - targets) :/ (1.0 - activationsLast)))

  def backwardOutputLayer(activationsLast: DenseMatrix[Double], targets: DenseMatrix[Double]): DenseMatrix[Double] =
    activationsLast - targets

  private[fcn] def sigmoidBackwardL(dA: DenseMatrix[Double], outputsAndActivationsL: OutputsAndActivationsL): DenseMatrix[Double] = {
    val activationsL = outputsAndActivationsL._2
    dA :* (activationsL :* (1.0 - activationsL))
  }

  private[fcn] def sigmoidBackwardRecalculateL(dA: DenseMatrix[Double], outputsAndActivationsL: OutputsAndActivationsL): DenseMatrix[Double] = {
    val s = sigmoid(outputsAndActivationsL._1)
    dA :* (s :* (1.0 - s))
  }
  private[fcn] def reluBackwardL(dA: DenseMatrix[Double], outputsAndActivationsL: OutputsAndActivationsL): DenseMatrix[Double] = {
    val outputsMask = outputsAndActivationsL._1.map(x => if (x <= 0.0) 0.0 else 1.0)
    dA :* outputsMask
  }

  private[fcn] def leakyReluBackwardL(epsilon: Double)(dA: DenseMatrix[Double], outputsAndActivationsL: OutputsAndActivationsL): DenseMatrix[Double] = {
    val outputsMask = outputsAndActivationsL._1.map(x => x match {
      case 0.0 => epsilon
      case _   => if (x < 0.0) 0.0 else 1.0
    })
    dA :* outputsMask
  }
  
  private[fcn] def softmaxBackwardL(dA: DenseMatrix[Double], outputsAndActivationsL: OutputsAndActivationsL): DenseMatrix[Double] = dA
  
  private[fcn] def noneBackwardL(dA: DenseMatrix[Double], outputsAndActivationsL: OutputsAndActivationsL): DenseMatrix[Double] = {
    outputsAndActivationsL._2
  }

  /**
   * Back propagation function for a given layer, returning gradients
   *
   * @param nonLinearFcn The non-linear back prop function
   * @param dA Gradient of cost wrt current layer activations, dimensions are (current layer, number of examples)
   * @param aPrev Previous layer activations, dimensions are (previous layer, number of examples)
   * @param outputsAndActivationsL The outputs (Z) and activations for the current layer, dimensions of both are (current layer, number of examples)
   * @param weightsAndBiases Weights and Biases for the current layer
   */
  private[fcn] def backPropL(nonLinearFcn: NonLinearBackPropFcn)(dA: DenseMatrix[Double], aPrev: DenseMatrix[Double], outputsAndActivationsL: OutputsAndActivationsL,
                                                                 weightsAndBiases: LWeightsAndBiases,
                                                                 lambda: Double): LGradients = {
    val dZ = nonLinearFcn(dA, outputsAndActivationsL)
    linearBackwardL(dZ, aPrev, weightsAndBiases, lambda)
  }

  /**
   * Returns requested back propagation function for a single layer.
   *
   * @param params The function name, together with any parameters required by the back propagation function
   */
  def getLayerBackPropFunction(paramsL: ActivationParams): BackPropagationLayerFunction = paramsL.activationName.toLowerCase match {
    case "sigmoidr" => backPropL(sigmoidBackwardRecalculateL)
    case "sigmoid"  => backPropL(sigmoidBackwardL)
    case "softmax"  => backPropL(softmaxBackwardL)
    case "relu"     => backPropL(reluBackwardL)
    case "none"     => backPropL(noneBackwardL)
    case "leaky_relu" => {
      require(paramsL.epsilon > 0.0, s"epsilon must be > 0 for 'leaky_relu', but got ${paramsL.epsilon}")
      backPropL(leakyReluBackwardL(paramsL.epsilon))
    }
    case _ => throw new IllegalArgumentException(s"Unsupported activation function: '${paramsL.activationName}'")
  }

  def getOutputBackPropFunction(paramsL: ActivationParams): BackPropagationOutputFunction = paramsL.activationName.toLowerCase match {
    case "sigmoidp" => sigmoidBackwardOutputLayer
    case _          => backwardOutputLayer
  }

  /**
   * Returns all non-output layer back prop functions for the network
   */
  def getLayerBackPropFunctions(params: Array[ActivationParams]): Array[BackPropagationLayerFunction] = params.map(getLayerBackPropFunction)

  /**
   *
   */
  def backPropagate(layerFunctions: Array[BackPropagationLayerFunction],
                    outputFunction: BackPropagationOutputFunction)(inputs: DenseMatrix[Double],
                                                                   targets: DenseMatrix[Double],
                                                                   outputsAndActivations: Array[OutputsAndActivationsL],
                                                                   weightsAndBiases: Array[LWeightsAndBiases],
                                                                   lambda: Double,
                                                                   dropouts: Option[(Array[Double], Array[DenseMatrix[Double]])]): Array[LGradients] = {
    require(layerFunctions.size == weightsAndBiases.size,
      s"Mismatch between number of layers defined by layer functions (${layerFunctions.size}) and weights (${weightsAndBiases.size})")

    def applyDropout(da: DenseMatrix[Double], idx: Int): DenseMatrix[Double] = dropouts match {
      case None                        => da
      case Some((keepProbs, dropouts)) => (da :* dropouts(idx)) :/ keepProbs(idx)
    }

    val gradients = new Array[LGradients](weightsAndBiases.size)
    val activationsLast = outputsAndActivations.last._2
    val dALast = outputFunction(activationsLast, targets)
    if (gradients.size == 1) {
      gradients(0) = layerFunctions(0)(applyDropout(dALast, 0), inputs, outputsAndActivations(0), weightsAndBiases(0), lambda)
    } else {
      gradients(weightsAndBiases.size - 1) = linearBackwardL(dALast, outputsAndActivations(outputsAndActivations.size - 2)._2, weightsAndBiases.last, lambda)
      for (idx <- weightsAndBiases.size - 2 to 1 by -1) {
        val aPrev = outputsAndActivations(idx - 1)._2
        val dA = gradients(idx + 1).dActivationsPrev
        gradients(idx) = layerFunctions(idx)(applyDropout(dA, idx), aPrev, outputsAndActivations(idx), weightsAndBiases(idx), lambda)
      }
      val dA = gradients(1).dActivationsPrev
      gradients(0) = layerFunctions(0)(applyDropout(dA, 0), inputs, outputsAndActivations(0), weightsAndBiases(0), lambda)
    }

    gradients
  }

}