/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.fcn
import breeze.linalg.DenseMatrix
import breeze.linalg._
import breeze.numerics._
import com.vbs.nn.params.LWeightsAndBiases
import com.vbs.nn.params.ActivationParams

/**
 * Activation functions available in an FCN
 *
 */

object ActivationFunctions {
  /**
   * Outputs and activations for a single layer
   */
  type OutputsAndActivationsL = (DenseMatrix[Double], DenseMatrix[Double])

  /**
   * Public signature for all activation functions
   */
  type ActivationFunction = (DenseMatrix[Double], LWeightsAndBiases) => OutputsAndActivationsL

  /**
   * Returns output (Z) for a layer with dimensions (size of layer, number of examples (m)).
   *
   * @param aPrev Activations from previous layer (or input if first layer) with dimensions (size of previous layer, number of examples (m))
   * @param weightsAndBias Layer weights and biases
   */
  def linearForwardL(aPrev: DenseMatrix[Double], weightsAndBias: LWeightsAndBiases): DenseMatrix[Double] = {
    val dm = weightsAndBias.weights * aPrev
    dm(::, *) + weightsAndBias.biases
  }

  /**
   * Returns outputs and activations for sigmoid function
   */
  private def sigmoidForwardL(aPrev: DenseMatrix[Double], weightsAndBias: LWeightsAndBiases): OutputsAndActivationsL = {
    val outputs = linearForwardL(aPrev, weightsAndBias)
    (outputs, sigmoid(outputs))
  }

  /**
   * Returns outputs and activations for softmax function
   */
  private[fcn] def softmaxForwardL(aPrev: DenseMatrix[Double], weightsAndBias: LWeightsAndBiases): OutputsAndActivationsL = {
    val outputs = linearForwardL(aPrev, weightsAndBias)
    val dva = for (c <- 0 until outputs.cols) yield {
      val eInputs = exp(outputs(::, c))
      val sumE = sum(eInputs)
      eInputs.map(_ / sumE)
    }
    val activations = new DenseMatrix(outputs.rows, outputs.cols, dva.map(_.toArray).flatten.toArray)
    (outputs, activations)
  }

  /**
   * Returns outputs and activations for relu function
   */
  private[fcn] def reluForwardL(epsilon: Double)(aPrev: DenseMatrix[Double], weightsAndBias: LWeightsAndBiases): OutputsAndActivationsL = {
    val outputs = linearForwardL(aPrev, weightsAndBias)
    (outputs, outputs.map(z => Math.max(epsilon, z)))
  }

  /**
   * Returns requested activation function for a single layer.
   *
   * @param params The function name, together with any parameters required by the activation function
   */
  def getLayerActivationFunction(paramsL: ActivationParams): ActivationFunction = paramsL.activationName.toLowerCase match {
    case "none"    => softmaxForwardL
    case "sigmoid" => sigmoidForwardL
    case "softmax" => softmaxForwardL
    case "relu"    => reluForwardL(0.0)(_, _)
    case "leaky_relu" => {
      require(paramsL.epsilon > 0.0, s"epsilon must be > 0 for 'leaky_relu', but got ${paramsL.epsilon}")
      reluForwardL(paramsL.epsilon)(_, _)
    }
    case _ => throw new IllegalArgumentException(s"Unsupported activation function: '${paramsL.activationName}'")
  }

  /**
   * Returns activation functions as configured in the params
   */
  def getActivationFunctions(params: Array[ActivationParams]): Array[ActivationFunction] = params.map(getLayerActivationFunction)

  /**
   * Returns array of outputs and activations for each layer of the network
   *
   * @param activationFunctions The activation function for each layer
   * @param xs The data set with dimensions (size of input layer, number of examples (m)).
   * @param weightsAndBiases The weights and biases for each layer
   */
  def feedForward(activationFunctions: Array[ActivationFunction])(xs: DenseMatrix[Double],
                                                                  weightsAndBiases: Array[LWeightsAndBiases],
                                                                  dropouts: Option[(Array[Double], Array[DenseMatrix[Double]])]): Array[OutputsAndActivationsL] = {
    require(activationFunctions.size == weightsAndBiases.size,
      s"Mismatch between number of layers defined by activation function (${activationFunctions.size}) and weights (${weightsAndBiases.size})")

    def applyDropout(za: OutputsAndActivationsL, idx: Int): OutputsAndActivationsL = dropouts match {
      case None => za
      case Some((keepProbs, dropouts)) => {
        if (idx < keepProbs.size) (za._1, (za._2 :* dropouts(idx)) :/ keepProbs(idx))
        else za
      }
    }

    val outputsAndActivations = new Array[OutputsAndActivationsL](activationFunctions.size)
    outputsAndActivations(0) = applyDropout(activationFunctions(0)(xs, weightsAndBiases(0)), 0)
    for (layerIdx <- 1 until activationFunctions.size) {
      val aPrev = outputsAndActivations(layerIdx - 1)._2
      outputsAndActivations(layerIdx) = applyDropout(activationFunctions(layerIdx)(aPrev, weightsAndBiases(layerIdx)), layerIdx)
    }
    outputsAndActivations
  }
}