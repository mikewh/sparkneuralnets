/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.sampler
import scala.util.Random
import breeze.linalg.DenseMatrix

/**
 * Sampler for FCNs
 */
class MiniBatchSampler(ys: DenseMatrix[Double], xs: DenseMatrix[Double], val batchSize: Int, val numBatches: Int, val samplerType: String) {

  require(xs.cols == ys.cols, s"Mismatch between number of feature samples (${xs.cols}) and labels (${ys.cols})")
  val static = samplerType.trim.toLowerCase.startsWith("static_")
  val sequential = samplerType.trim.toLowerCase.endsWith("sequential")

  val batchCount = Math.min(numBatches *  batchSize, xs.cols)

  private val yxSeq = {
    for (col <- 0 until xs.cols) yield {
      (ys(::, col).toArray, xs(::, col).toArray)
    }
  }

  private val rnd = new Random(static match {
    case false => System.nanoTime()
    case true  => 1l
  })

  def miniBatches(): Seq[(DenseMatrix[Double], DenseMatrix[Double])] = {
    sequential match {
      case false => {
        val yxShuffled = rnd.shuffle(yxSeq).take(batchCount).toArray
        yxShuffled.sliding(batchSize, batchSize).toSeq.map(yxMiniBatch => {
          val (yShuffled, xShuffled) = yxMiniBatch.unzip
          (toDenseMatrix(yShuffled), toDenseMatrix(xShuffled))
        })
      }
      case true => yxSeq.take(batchCount).toArray.sliding(batchSize, batchSize).toSeq.map(yxMiniBatch => {
        val (yShuffled, xShuffled) = yxMiniBatch.unzip
        (toDenseMatrix(yShuffled), toDenseMatrix(xShuffled))
      })
    }
  }

  private def toDenseMatrix(sad: Seq[Array[Double]]): DenseMatrix[Double] = {
    new DenseMatrix(sad(0).size, sad.size, sad.flatten.toArray)
  }
}

object MiniBatchSampler {
  
  def apply(ys: DenseMatrix[Double], xs: DenseMatrix[Double], batchSize: Int, numBatches: Int, samplerType: String) = 
    new MiniBatchSampler(ys, xs, batchSize, numBatches, samplerType)

}