/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.sampler
import scala.util.Random

import breeze.linalg.DenseMatrix
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import scala.collection.GenSeq
import breeze.linalg.DenseVector

trait CNNSampler {
  val batchSize: Int
  val numBatches: Int
  val sequential: Boolean
  def miniBatches(): Seq[(DenseMatrix[Double], GenSeq[DenseMatrix3d])]
}

/**
 * Sampler for non-spark CNNs
 */
class CNNMiniBatchSampler(ys: DenseMatrix[Double],
                          xs: DenseMatrix4d, val batchSize: Int, val numBatches: Int, val samplerType: String, val par: Boolean = true) extends CNNSampler {

  require(xs.size == ys.cols, s"Mismatch between number of feature samples (${xs.size}) and labels (${ys.cols})")
  val static = samplerType.trim.toLowerCase.startsWith("static_")
  val sequential = samplerType.trim.toLowerCase.endsWith("sequential")

  val batchCount = Math.min(numBatches * batchSize, xs.size)

  private val rnd = new Random(static match {
    case false => System.nanoTime()
    case true  => 1l
  })

  private def randomMiniBatch(): (DenseMatrix[Double], GenSeq[DenseMatrix3d]) = {
    val indices = (1 to batchSize).toSeq.map(_ => rnd.nextInt(xs.size))
    val yBatch = new DenseMatrix[Double](ys.rows, batchSize)
    var _colIdx = 0
    indices.foreach(i => {
      yBatch(::, _colIdx) := ys(::, i)
      _colIdx += 1
    })
    val xBatch = indices.map(i => xs(i))
    (yBatch, if (par) xBatch.par else xBatch)
  }

  def miniBatches(): Seq[(DenseMatrix[Double], GenSeq[DenseMatrix3d])] = {
    sequential match {
      case false => (1 to numBatches).toSeq.map(_ => randomMiniBatch())
      case true => {
        val yBatch = for (col <- 0 until batchCount by batchSize) yield {
          val endIdx = Math.min(ys.cols, col + batchSize)
          ys(::, col until endIdx)
        }
        val xBatch = xs.take(batchCount).sliding(batchSize, batchSize).toSeq
        yBatch.zip(if (par) xBatch.par else xBatch)
      }
    }
  }

}

object CNNMiniBatchSampler {

  def apply(ys: Seq[DenseVector[Double]],
            xs: DenseMatrix4d, batchSize: Int, numBatches: Int, samplerType: String, par: Boolean) = {
    val labelCount = ys(0).size
    val entryCount = ys.size
    val ysDM = new DenseMatrix(labelCount, entryCount, ys.map(_.toArray).flatten.toArray)
    new CNNMiniBatchSampler(ysDM, xs, batchSize, numBatches, samplerType, par)
  }

  def apply(ys: DenseMatrix[Double],
            xs: DenseMatrix4d, batchSize: Int, numBatches: Int, samplerType: String, par: Boolean) = {
    new CNNMiniBatchSampler(ys, xs, batchSize, numBatches, samplerType, par)
  }

}