/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.model

/**
 * Persistable mutable dense matrix implementation of a multi channel data item
 */
class MultiChannelDataItemDM(dms: DataSetTypes.DenseMatrix3d) extends MultiChannelDataItem  with Serializable {
  require(!dms.isEmpty, "Empty dataitem passed")
  val channels = dms.size
  val height = dms(0).rows
  val width = dms(0).cols
  
  /* The dimensions of dmss are immutable. However the contents of the underlying matrices may be updated   */
  def toArray: Array[Double] = dms.map(_.toArray).flatten.toArray
  
  def dimString = s"height=$height, width=$width, channels=$channels"
}

object MultiChannelDataItemDM {
  def apply(dms: DataSetTypes.DenseMatrix3d) = new MultiChannelDataItemDM(dms)
}