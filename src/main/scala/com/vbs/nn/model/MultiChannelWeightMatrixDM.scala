/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.model

/**
 * Persistable mutable dense matrix implementation of filter weights used in a Convolutional layer
 */
class MultiChannelWeightMatrixDM(dmss: DataSetTypes.DenseMatrix4d) extends MultiChannelWeightMatrix {
  require(!dmss.isEmpty, "Empty weight matrix passed")
  val channelsOut = dmss.size
  val channelsIn = dmss(0).size
  val fHeight = dmss(0)(0).rows
  val fWidth = dmss(0)(0).cols
  def dimString = s"fHeight=$fHeight, fWidth=$fWidth, channelsIn=$channelsIn, channelsOut=$channelsOut"
  
}

object MultiChannelWeightMatrixDM {
  def apply(dmss: DataSetTypes.DenseMatrix4d) = new MultiChannelWeightMatrixDM(dmss)
}