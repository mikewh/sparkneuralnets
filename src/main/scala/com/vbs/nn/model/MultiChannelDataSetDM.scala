/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.model
import breeze.linalg.DenseMatrix

/**
 * Persistable mutable dense matrix implementation of a multi channel data set
 */
class MultiChannelDataSetDM(val dmss: DataSetTypes.DenseMatrix4d) extends MultiChannelDataItem with DataSet {
  require(!dmss.isEmpty, "Empty dataset passed")
  val size = dmss.size
  private val dms = MultiChannelDataItemDM(dmss(0))
  val channels = dms.channels
  val height = dms.height
  val width = dms.width

  /* The dimensions of dmss are immutable. However the contents of the underlying matrices may be updated   */
  def toArray: Array[Double] = dmss.map(MultiChannelDataItemDM(_).toArray).flatten.toArray

  def dimString = s"${dms.dimString}, size=$size"
}

object MultiChannelDataSetDM {
  def apply(dmss: DataSetTypes.DenseMatrix4d) = new MultiChannelDataSetDM(dmss)

  def apply(size: Int, width: Int, height: Int, channels: Int, data: Seq[Double]) = {
    require(size * width * height * channels == data.size, s"Mismatch between dimensions ($size, $width, $height, $channels) and data size ${data.size}")
    val itemSize = width * height
    val multiChannelItemSize = itemSize * channels
    val dmss = data.sliding(multiChannelItemSize, multiChannelItemSize).
      map(mciData => mciData.sliding(itemSize, itemSize).
        map(item => new DenseMatrix(height, width, item.toArray)).toList)
   new MultiChannelDataSetDM(dmss.toList)
  }

}