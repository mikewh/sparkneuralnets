/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.model

import breeze.linalg.DenseMatrix

object DataSetTypes {
   type DenseMatrix3d = Seq[DenseMatrix[Double]]
   type DenseMatrix4d = Seq[DenseMatrix3d]
  
}

/**
 * Used for feature vectors
 */
trait DataItem {
  val featureCount: Int
}

/**
 * The size of any data set, feature set or multi channel
 */
trait DataSet {
  val size: Int
}

/**
 * Used for multi channel feature vectors (primarily images)
 */
trait MultiChannelDataItem {

  val height: Int
  val width: Int
  val channels: Int
}

/**
 * Used for filter weights for a Convolutional layer
 */
trait MultiChannelWeightMatrix {
  val fHeight: Int
  val fWidth: Int
  val channelsIn: Int
  val channelsOut: Int
}


