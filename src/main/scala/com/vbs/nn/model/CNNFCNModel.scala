/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.model

import com.vbs.nn.model.DataSetTypes.DenseMatrix4d

import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.params.Parameters
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import java.io.BufferedWriter
import scala.io.Source
import breeze.linalg.DenseMatrix
import com.vbs.nn.CNNFCNLinkedNetwork
import com.vbs.nn.utils.MatrixUtils
import com.typesafe.scalalogging.LazyLogging

object CNNFCNModel extends LazyLogging {

  /**
   * Train and persist model, test against training and test sets and produce report
   */
  def run(cnnParams: CNNParameters, fcnParams: Parameters,
          xs: DenseMatrix4d, ys: DenseMatrix[Double],
          xst: DenseMatrix4d, yst: DenseMatrix[Double],
          path: String, name: String): Long = {
    val id = trainAndPersist(cnnParams, fcnParams, path, name)
    predictAndReport(cnnParams, fcnParams, xs, ys, xst, yst, path, name, id)
    id
  }

  /**
   * Test against training and test sets and produce reports
   */
  def predictAndReport(cnnParams: CNNParameters, fcnParams: Parameters,
                       xs: DenseMatrix4d, ys: DenseMatrix[Double],
                       xst: DenseMatrix4d, yst: DenseMatrix[Double],
                       path: String, name: String, id: Long): Unit = {
    val pys = predict(cnnParams, fcnParams, xs)
    recordAccuracy("train", fcnParams, ys, pys)

    val pyst = predict(cnnParams, fcnParams, xst)
    recordAccuracy("test", fcnParams, yst, pyst)
    val modelFname = s"$path/fcnModel_${name}_$id.mod"
    persist(modelFname, fcnParams.toString)
    logger.info(s"predictAndReport $name: training accuracy: ${fcnParams.getAccuracy("train")}, test accuracy=${fcnParams.getAccuracy("test")}")

  }

  /**
   * Train and persist model
   */
  def trainAndPersist(cnnParams: CNNParameters, fcnParams: Parameters,
                      path: String, name: String): Long = {

    val ts = System.currentTimeMillis()
    val cost = CNNFCNLinkedNetwork.train(cnnParams, fcnParams)
    logger.info(s"trainAndPersist $name: training time: ${System.currentTimeMillis() - ts}ms, final costy=$cost")
    val costs = fcnParams.getCosts().get.toArray
    val id = persist(cnnParams, fcnParams, path, name)
    val f = FullyConnectedModel.produceGraph(costs)
    f.saveas(s"$path/graph_${name}_$id.png")
    id
  }

  def predict(cnnParams: CNNParameters, fcnParams: Parameters, xs: DenseMatrix4d): DenseMatrix[Double] =
    CNNFCNLinkedNetwork.predict(cnnParams, fcnParams, xs)

  def recordAccuracy(dataSetType: String, p: Parameters, ys: DenseMatrix[Double], pys: DenseMatrix[Double]): Unit = {
    val accuracy = getAccuracy(ys, pys)
    p.setAccuracy(dataSetType, accuracy)
    p.setSampleSize(dataSetType, ys.cols)

  }

  def getAccuracy(ys: DenseMatrix[Double], pys: DenseMatrix[Double]): Double = {
    val labels = MatrixUtils.oneHotDecode(ys)
    val predictedLabels = MatrixUtils.oneHotDecode(pys)
    labels.zip(predictedLabels).filter(e => e._1 == e._2).size.toDouble / labels.size

  }

  def persist(cnnParams: CNNParameters, fcnParams: Parameters, path: String, name: String): Long = {
    val id = System.currentTimeMillis()
    persist(s"$path/cnnModel_${name}_$id.mod", cnnParams.toString)
    persist(s"$path/fcnModel_${name}_$id.mod", fcnParams.toString)
    logger.info(s"model persisted as cnn: $path/cnnModel_${name}_$id.mod, fcn: $path/fcnModel_${name}_$id.mod")

    id
  }

  private def persist(fName: String, data: String): Unit = {
    val writer = new BufferedWriter(new OutputStreamWriter((new FileOutputStream(fName))))
    writer.write(data)
    writer.flush
    writer.close

  }

  /**
   * Load models into equivalent parameters
   */
  def load(path: String, name: String, id: Long): (CNNParameters, Parameters) = {
    (CNNParameters(Source.fromFile(s"$path/cnnModel_${name}_$id.mod").toSeq.mkString),
      Parameters(Source.fromFile(s"$path/fcnModel_${name}_$id.mod").toSeq.mkString))
  }
}