/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.model
import java.io.BufferedWriter
import java.io.FileOutputStream
import java.io.OutputStreamWriter

import scala.io.Source

import com.vbs.nn.FullyConnectedNetwork
import com.vbs.nn.params.Parameters

import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import breeze.plot.Figure
import breeze.plot.plot
import com.typesafe.scalalogging.LazyLogging
import com.vbs.nn.utils.MatrixUtils

/**
 * Stand alone Fully Connected Network
 */
object FullyConnectedModel extends LazyLogging {

  /**
   * Train and persist model, test against training and test sets and produce report
   */
  def run(params: Parameters,
          ys: DenseMatrix[Double], xs: DenseMatrix[Double],
          yst: DenseMatrix[Double], xst: DenseMatrix[Double],
          path: String, name: String,
          labelDecoder: (DenseMatrix[Double]) => Seq[Double] = MatrixUtils.oneHotDecode): Long = {
    val id = trainAndPersist(params, ys, xs, path, name)
    predictAndReport(params, ys, xs, yst, xst, path, name, id, labelDecoder)
    id
  }

  def trainAndPersist(params: Parameters, ys: DenseMatrix[Double], xs: DenseMatrix[Double],
                      path: String, name: String): Long = {

    val ts = System.currentTimeMillis()
    val sampler = params.getFCNSampler(ys, xs)

    val cost = FullyConnectedNetwork.train(params, sampler)
    val costs = params.getCosts().get.toArray
    logger.info(s"trainAndPersist $name: training time: ${System.currentTimeMillis() - ts}ms, final costy=$cost")
    val id = persist(params, path, name)
    val f = FullyConnectedModel.produceGraph(costs)
    f.saveas(s"$path/graph_${name}_$id.png")
    id
  }

  /**
   * Test against training and test sets and produce reports
   */
  def predictAndReport(params: Parameters,
                       ys: DenseMatrix[Double], xs: DenseMatrix[Double],
                       yst: DenseMatrix[Double], xst: DenseMatrix[Double],
                       path: String, name: String, id: Long,
                       labelDecoder: (DenseMatrix[Double]) => Seq[Double] = MatrixUtils.oneHotDecode): Unit = {
    val pys = FullyConnectedNetwork.predict(params, xs)
    recordAccuracy("train", params, ys, pys, labelDecoder)

    val pyst = FullyConnectedNetwork.predict(params, xst)
    recordAccuracy("test", params, yst, pyst, labelDecoder)
    val modelFname = s"$path/fcnModel_${name}_$id.mod"
    persist(modelFname, params.toString)
    logger.info(s"predictAndReport $name: training accuracy: ${params.getAccuracy("train")}, test accuracy=${params.getAccuracy("test")}")

  }

  def predict(params: Parameters, xs: DenseMatrix[Double]): DenseMatrix[Double] = {
    FullyConnectedNetwork.predict(params, xs)
  }

  def recordAccuracy(dataSetType: String, p: Parameters, ys: DenseMatrix[Double], pys: DenseMatrix[Double],
      labelDecoder: (DenseMatrix[Double]) => Seq[Double] = MatrixUtils.oneHotDecode): Unit = {
    val accuracy = getAccuracy(ys, pys, labelDecoder)
    p.setAccuracy(dataSetType, accuracy)
    p.setSampleSize(dataSetType, ys.cols)
  }

  def getAccuracy(ys: DenseMatrix[Double], pys: DenseMatrix[Double],
                  labelDecoder: (DenseMatrix[Double]) => Seq[Double] = MatrixUtils.oneHotDecode): Double = {
    val labels = labelDecoder(ys)
    val predictedLabels = labelDecoder(pys)
    labels.zip(predictedLabels).filter(e => e._1 == e._2).size.toDouble / labels.size
  }

  def trainingAndTestAccuracy(p: Parameters, trainData: (DenseMatrix[Double], DenseMatrix[Double]), testData: (DenseMatrix[Double], DenseMatrix[Double]),
                              labelDecoder: (DenseMatrix[Double]) => Seq[Double]): (Double, Double) = {
    (getAccuracy(trainData._1, trainData._2, labelDecoder), getAccuracy(testData._1, testData._2, labelDecoder))
  }

  def persist(params: Parameters, path: String, name: String): Long = {
    val id = System.currentTimeMillis()
    persist(s"$path/fcnModel_${name}_$id.mod", params.toString)
    logger.info(s"model persisted as fcn: $path/fcnModel_${name}_$id.mod")
    id
  }

  def predictAndPersistLabels(params: Parameters, xs: DenseMatrix[Double], path: String, name: String, id: String,
      labelDecoder: (DenseMatrix[Double]) => Seq[Double] = MatrixUtils.oneHotDecode): String = {
    val labels = labelDecoder(predict(params, xs)).mkString("\n")
    val fName = s"$path/fcnPredictions_${name}_$id.pred"
    persist(fName, labels)
    logger.info(s"predictions persisted as $fName")
    fName
  }

  private def persist(fName: String, data: String): Unit = {
    val writer = new BufferedWriter(new OutputStreamWriter((new FileOutputStream(fName))))
    writer.write(data)
    writer.flush
    writer.close

  }

  def load(path: String, id: String): Parameters = {
    val paramString = Source.fromFile(s"$path/fcnModel_$id.mod").toSeq.mkString
    Parameters(paramString)
  }

  def produceGraph(costs: Seq[Double]): Figure = {
    val errs = DenseVector(costs.toArray)
    val iters = DenseVector((1 to costs.size).map(_.toDouble).toArray)
    val f = Figure()
    val p = f.subplot(0)

    p += plot(iters, errs)
    p.xlabel = "iters"
    p.ylabel = "errs"
    f
  }

}