/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.model
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter

import scala.io.Source
import scala.util.Sorting
import scala.util.control.NonFatal

import com.vbs.nn.params.Parameters

import breeze.linalg.DenseVector
import breeze.linalg.argmin
import breeze.plot.Figure
import breeze.plot.plot
import com.vbs.nn.cnn.params.CNNParameters
import com.vbs.nn.cnn.params.ConvolutionLayerParameters
import com.vbs.nn.cnn.params.PoolingLayerParameters

/**
 * Quick and dirty: Produce HTML reports from persisted models 
 */
object ModelReporter {

  private val HTML_TOP = """<!DOCTYPE html>
<html>
<head>
<style>
th, td {
    padding: 15px;
}
td {
  vertical-align: top;
}
table {
    border-spacing: 5px;
}
</style>
</head>
<body>

"""

  private val HTML_DETAIL_TABLE_TOP = """
<table>
  <tr>
    <th>Performance</th>
    <th>Params</th> 
    <th>Stats</th>
  </tr>"""

  private val HTML_INDEX_TABLE_TOP = """
<table>
  <tr>
    <th>Performance</th>
    <th>Id</th> 
    <th>Stats</th>
  </tr>"""

  private val HTML_BOTTOM = """</tr>
</table>
</body>
</html>"""

  object ReportOrdering extends Ordering[(String, (Parameters, Option[CNNParameters]))] {
    def compare(a: (String, (Parameters, Option[CNNParameters])), b: (String, (Parameters, Option[CNNParameters]))) = {
      def getAccuracy(params: Parameters): Double = params.getAccuracy("test") match {
        case (Some(acc)) => acc
        case None        => 0.0
      }
      (getAccuracy(a._2._1).compare(getAccuracy(b._2._1))) * -1
    }
  }

  def produceReports(path: String): Unit = {
    val models = new File(s"$path").listFiles.map(_.getName)
      .filter(f => f.endsWith(".mod") && f.startsWith("fcn"))
      .map(fName => (fName, getParameters(path, fName)))

    val modelsToReport = models.filterNot(model => {
      val reportFile = model._1.dropRight(4) + ".html"
      new File(path + reportFile).exists
    })

    modelsToReport.foreach(kv => produceReport(path, kv._1, kv._2))
    produceIndex(path, models)
  }
  
  def getParameters(path: String, fName: String): (Parameters, Option[CNNParameters]) = {
    val p = Parameters(Source.fromFile(s"$path/$fName").toSeq.mkString)
    val cnnFname = s"$path/cnn${fName.drop(3)}"
    val cnnparams = new File(cnnFname).exists match {
      case true => Some(CNNParameters(Source.fromFile(s"$cnnFname").toSeq.mkString))
      case false => None
    }
    (p, cnnparams)
  }

  def produceIndex(path: String, modelsToReport: Array[(String, (Parameters, Option[CNNParameters]))]): Unit = {
    Sorting.quickSort(modelsToReport)(ReportOrdering)

    def getModelNameAndId(fName: String): (String, String) = {
      val startIdx = fName.indexOf("_")
      val lastIdx = fName.lastIndexOf("_")
      require(startIdx > 0 && lastIdx > startIdx, s"Unexpected filename '$fName'. Expected format xxx_ModelName_modelId.html")
      val modelName = fName.dropRight(fName.size - lastIdx).drop(startIdx + 1)
      val modelId = fName.drop(lastIdx + 1).dropRight(5)
      (modelName, modelId)
    }

    val html = new StringBuilder(HTML_TOP)
      .append(HTML_INDEX_TABLE_TOP)
    modelsToReport.map(model => (model._1.dropRight(4) + ".html", model._2)).filter(model => {
      new File(path + model._1).exists
    }).map(model => {
      val (modelName, modelId) = getModelNameAndId(model._1)
      val graphFName = s"graph_${modelName}_${modelId}.png"
      html.append(s"<tr><td><img src='$graphFName' style='width:240px;height:160px;'</td>")
        .append(s"<td><a href='${model._1}'>${modelName}_${modelId}</a></td>")
      val statsSb = new StringBuilder("<td>")
      statsSb.append(produceSummaryHTML(model._2))

      model._2._1.getTrainTime() match {
        case Some(trainTime) => statsSb.append(s"trainTime=${trainTime / 1000}s</br>")
        case _               =>
      }
      statsSb.append("</td>")
      html.append(statsSb.toString)

    })
    html.append(HTML_BOTTOM)
    persist(path, "index.html", html.toString())

  }

  def produceReport(path: String, fName: String, pCnn: (Parameters, Option[CNNParameters])): Unit = {
    val(p, cnn) = pCnn
    def getModelNameAndId(): (String, String) = {
      val startIdx = fName.indexOf("_")
      val lastIdx = fName.lastIndexOf("_")
      require(startIdx > 0 && lastIdx > startIdx, s"Unexpected filename '$fName'. Expected format xxx_ModelName_modelId.mod")
      val modelName = fName.dropRight(fName.size - lastIdx).drop(startIdx + 1)
      val modelId = fName.drop(lastIdx + 1).dropRight(4)
      (modelName, modelId)
    }

    try {
      val (modelName, modelId) = getModelNameAndId()
      val graphFName = s"graph_${modelName}_${modelId}.png"
      val graphLink = persistGraph(path, graphFName, p) match {
        case true  => s"<img src='$graphFName'"
        case false => ""
      }
      val paramString = produceParamsHTML(pCnn)
      val statsString = produceStatsHTML(pCnn)
      val html = new StringBuilder(HTML_TOP)
        .append(HTML_DETAIL_TABLE_TOP)
        .append(s"<tr><td>$graphLink</td>")
        .append(s"<td>$paramString</td>")
        .append(s"<td>$statsString</td>")
        .append(HTML_BOTTOM).toString
      val reportFile = fName.dropRight(4) + ".html"
      persist(path, reportFile, html)

    } catch {
      case NonFatal(e) => {
        e.printStackTrace
        println(s"Failed to produce report for $fName:\n${e.getMessage}")
      }
    }
  }

  def produceParamsHTML(pCnn: (Parameters, Option[CNNParameters])): String = {
    val (p, cnn) = pCnn
    val paramString = new StringBuilder("<b>FCN and shared</b></br>").append(s"layerDims=${p.getLayerDims().mkString(", ")}</br>")
      .append(s"learningRate=${p.getLearningRate().toString}</br>")
      .append(s"weightInitializer=${p.getWeightInitializer()}</br>")
      .append(s"activations=${p.getActivationParams().mkString(", ")}</br>")
      .append(s"costFunction=${p.getCostFunctionName()}</br>")
      .append(s"sampler=${p.getSamplerSpec()}</br>")
    val optimizerParams = p.getOptimizer() +
      {
        p.getMomentum() match {
          case Some(momentum) => s" [beta: $momentum]"
          case None           => ""
        }
      } + {
        p.getAdamParams() match {
          case Some((beta1, beta2, epsilon)) => s" [beta1: $beta1, beta2: $beta2, epsilon: $epsilon]"
          case None                          => ""
        }
      }
    paramString.append(s"optimizer=$optimizerParams</br>")
    p.getWeightRegularizerParams() match {
      case Some((regularizer, decay)) => paramString.append(s"weightRegularizer=$regularizer [decay: $decay]</br>")
      case _                          =>
    }
    p.getKeepProbabilities() match {
      case Some(keepProbs) => paramString.append(s"dropout(keep probs)=${keepProbs.mkString(", ")}</br>")
      case _               =>
    }
    paramString.append(s"epochs=${p.getIterations().toString}</br>")
    cnn match {
      case Some(cnnParams) => paramString.append(produceCNNParamsHTML(cnnParams))
      case None =>
    }

    paramString.toString()
  }
  
  def produceCNNParamsHTML(cnnParams: CNNParameters): String = {
    val paramString = new StringBuilder("</br></br><b>CNN Layers</b></br>")
    for (i <- 0 until cnnParams.getCNNLayerParams().size) {
      val s = cnnParams.getCNNLayerParams()(i) match {
        case convLayer: ConvolutionLayerParameters => 
          s"conv, padtype=${convLayer.padType}, fSize=${convLayer.filterSize}, cIn=${convLayer.channelsIn}, cOut=${convLayer.channelsOut}, stride=${convLayer.stride}"
        case poolLayer: PoolingLayerParameters =>
          s"pool, fSize=${poolLayer.fSize}, stride=${poolLayer.stride}, maxpool=${poolLayer.maxPool}"
      }
      paramString.append(s"${i + 1}: $s</br>")
    }
    paramString.toString
    
  }

  def produceSummaryHTML(pCnn: (Parameters, Option[CNNParameters])): String = {
    val (p, cnn) = pCnn

    def getLastCost(cost: Double): String = {
      val sb = new StringBuilder(s"lastCost=$cost")
      if (cost.equals(Double.NaN)) {
        val i = p.getCosts().get.toArray.lastIndexWhere(!_.equals(Double.NaN))
        sb.append(s", lastValidCost=${p.getCosts.get(i)} at iteration $i")
      }
      sb.append("</br>")
      sb.toString
    }
    def getMinCost(costs: DenseVector[Double]): String = {
      val ca = costs.toArray
      val i = argmin(ca.filter(_ != Double.NaN))
      s"minCost=${ca(i)} at iteration $i</br>"
    }

    val statsString = new StringBuilder()
    p.getAccuracy("train") match {
      case Some(accuracy) => statsString.append(s"trainingAccuracy=$accuracy</br>")
      case _              =>
    }
    p.getAccuracy("test") match {
      case Some(accuracy) => statsString.append(s"testAccuracy=$accuracy</br>")
      case _              =>
    }
    p.getCost() match {
      case Some(cost) => statsString.append(getLastCost(cost))
      case _          =>
    }
    p.getCosts() match {
      case Some(costs) => statsString.append(getMinCost(costs))
      case _           =>
    }
    statsString.toString
  }

  def produceStatsHTML(pCnn: (Parameters, Option[CNNParameters])): String = {
    val (p, cnn) = pCnn
    val statsString = new StringBuilder(produceSummaryHTML(pCnn))
    val trainSampleSize = p.getSampleSize("train") match {
      case Some(sampleSize) => sampleSize
      case _                => 0
    }
    statsString.append(s"trainSampleSize=$trainSampleSize</br>")
    p.getSampleSize("test") match {
      case Some(sampleSize) => statsString.append(s"testSampleSize=$sampleSize</br>")
      case _                =>
    }
    val totalSamples = (p.getNumBatches(), p.getMiniBatchSize()) match {
      case (None, _)                           => trainSampleSize * p.getIterations()
      case (Some(numbatches), Some(batchSize)) => numbatches * batchSize * p.getIterations()
      case (Some(numbatches), None)            => numbatches * trainSampleSize * p.getIterations()
    }
    statsString.append(s"totalTrainingCount=$totalSamples</br>")

    p.getTrainTime() match {
      case Some(trainTime) => statsString.append(s"trainTime=${trainTime / 1000}s</br>")
      case _               =>

    }
    statsString.toString()
  }

  def persistGraph(path: String, graphFName: String, p: Parameters): Boolean = {
    if (new File(path + graphFName).exists) return true else {
      p.getCosts() match {
        case Some(costs) => {
          val iters = DenseVector((1 to costs.size).map(_.toDouble).toArray)
          val f = Figure()
          val p = f.subplot(0)

          p += plot(iters, costs)
          p.xlabel = "iters"
          p.ylabel = "errs"
          f.saveas(graphFName)
          true
        }
        case None => false
      }
    }
  }

  def persist(path: String, id: String, html: String): Unit = {
    def fName = s"$path$id"
    val writer = new BufferedWriter(new OutputStreamWriter((new FileOutputStream(fName))))
    writer.write(html)
    writer.flush
    writer.close
  }

}