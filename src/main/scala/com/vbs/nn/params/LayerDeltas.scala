/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

/**
 * Updates to weights and biases in FCNs
 */
case class LayerDeltas(wDeltas: DenseMatrix[Double], bDeltas: DenseVector[Double]) extends Serializable{
  require(wDeltas.rows == bDeltas.size, s"Mismatch between weight layer size (${wDeltas.rows}) and biases size (${bDeltas.size}")
  override def toString = s"wDeltas:\n$wDeltas\nbDeltas:\n$bDeltas"
}
