/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import scala.util.Random


/**
 * Initializer functions for FCN weights and biases
 *
 */
object WeightsAndBiasesInitializer {
  
 private def weightsInitStaticGaussian(layerDims: Array[Int], zeroBias: Boolean): Array[LWeightsAndBiases] = {
    val random = new Random(1L)
    def initWeights(w: DenseMatrix[Double]): Unit = {
      for (r <- 0 until w.rows) {
        for (c <- 0 until w.cols) w(r,c) = random.nextGaussian()
      }
    }
    def initBiases(b: DenseVector[Double]): Unit = {
      for (i <- 0 until b.size) b(i) = random.nextGaussian()
    }
    val r = for (idx <- 1 until layerDims.size) yield {
      val w = DenseMatrix.zeros[Double](layerDims(idx), layerDims(idx - 1))
      initWeights(w)
      val b = DenseVector.zeros[Double](layerDims(idx))
      if (!zeroBias) initBiases(b)
      LWeightsAndBiases(w, b)
    }
    r.toArray
  
 }
 

  private def weightsInitStaticXavier(layerDims: Array[Int], zeroBias: Boolean): Array[LWeightsAndBiases] = {
    weightsInitStaticGaussian(layerDims, zeroBias).map(lWeightsAndBiases => {
      val ratio = 2.0 / (lWeightsAndBiases.weights.rows + lWeightsAndBiases.weights.cols)
      LWeightsAndBiases(lWeightsAndBiases.weights :* ratio, lWeightsAndBiases.biases)
    })
  }

  private def weightsInitGaussian(layerDims: Array[Int], zeroBias: Boolean): Array[LWeightsAndBiases] = {
    require(layerDims.size > 1, s"Expecting a minimum of 2 layers, but got ${layerDims.size}")
    val normal01 = breeze.stats.distributions.Gaussian(0, 1)
    val r = for (idx <- 1 until layerDims.size) yield {
      val w = DenseMatrix.rand(layerDims(idx), layerDims(idx - 1), normal01)
      val b = if (!zeroBias) DenseVector.rand(layerDims(idx)) else DenseVector.zeros[Double](layerDims(idx))
      LWeightsAndBiases(w, b)
    }
    r.toArray
  }

  private def weightsInitXavier(layerDims: Array[Int], zeroBias: Boolean): Array[LWeightsAndBiases] = {
    weightsInitGaussian(layerDims, zeroBias).map(lWeightsAndBiases => {
      val ratio = 2.0 / (lWeightsAndBiases.weights.rows + lWeightsAndBiases.weights.cols)
      LWeightsAndBiases(lWeightsAndBiases.weights :* ratio, lWeightsAndBiases.biases)
    })
  }

  /**
   * Returns generated weights and biases for the layers of the network.
   *
   * @param layerDims The dimensions of each layer of the network with the first (0) being the input layer.
   * @param fcnName The name of the initializer function (gaussian or xavier)
   * @param zeroBias If True the biases for all layers will be set to 0 otherwise will be random gaussians
   */
  def generateWeightsAndBiases(layerDims: Array[Int], fcnName: String, zeroBias: Boolean = false): Array[LWeightsAndBiases] = fcnName.toLowerCase match {
    case "xavier"   => weightsInitXavier(layerDims, zeroBias)
    case "gaussian" => weightsInitGaussian(layerDims, zeroBias)
    case "static_xavier"   => weightsInitStaticXavier(layerDims, zeroBias)
    case "static_gaussian" => weightsInitStaticGaussian(layerDims, zeroBias)
    case _          => throw new IllegalArgumentException(s"Unsupported weight init function: '$fcnName'")
  }

}