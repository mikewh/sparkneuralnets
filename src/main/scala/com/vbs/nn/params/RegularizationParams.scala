/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import com.vbs.nn.fcn.RegularizationFunctions.RegularizationFunction
/**
 * Container for regularization function
 */
case class RegularizationParams(lambda: Double, regularizationFunction: RegularizationFunction)
