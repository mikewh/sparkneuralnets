/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

/**
 * Calculated gradients for a single layer of an FCN.
 *
 * @param dActivationsPrev Gradient of cost wrt previous layer activations, dimensions are (previous layer, number of examples)
 * @param dWeights Gradient of cost wrt current layer weights, dimensions are (current layer, previous layer)
 * @param dBiases Gradient of cost wrt current layer weights, dimensions are (current layer)
 *
 */
case class LGradients(dActivationsPrev: DenseMatrix[Double], dWeights: DenseMatrix[Double], dBiases: DenseVector[Double]) {
  require(dActivationsPrev.rows == dWeights.cols && dWeights.rows == dBiases.size,
    s"Mismatch between prev activations layer size (${dActivationsPrev.rows})) and weights (${dWeights.cols}) or weight layer size (${dWeights.rows}) and biases size (${dBiases.size}")
    
  override def toString = s"dActivationsPrev:\n$dActivationsPrev\ndWeights:\n$dWeights\ndBiases:\n$dBiases"

}