/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import scala.reflect.ClassTag
import scala.collection.mutable

import com.vbs.nn.fcn.ActivationFunctions.ActivationFunction
import com.vbs.nn.fcn.ActivationFunctions.getActivationFunctions
import com.vbs.nn.fcn.BackPropagationFunctions.BackPropagationLayerFunction
import com.vbs.nn.fcn.BackPropagationFunctions.getLayerBackPropFunctions
import com.vbs.nn.fcn.CostFunctions.getCostFunction
import com.vbs.nn.fcn.RegularizationFunctions.getWeightRegularizer
import com.vbs.nn.utils.MatrixUtils.cleanString
import com.vbs.nn.utils.MatrixUtils.matrix3dToString
import com.vbs.nn.utils.MatrixUtils.toDenseMatrixArray
import com.vbs.nn.utils.MatrixUtils.toDenseVector
import com.vbs.nn.utils.MatrixUtils.toDenseVectorArray
import com.vbs.nn.utils.MatrixUtils.vectorArrayToString
import com.vbs.nn.utils.MatrixUtils.vectorToString

import Parameters.toActivationParamSet
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.FCNImplementations
import com.vbs.nn.DefaultFCNImplementations
import com.vbs.nn.sampler.MiniBatchSampler
import com.typesafe.scalalogging.LazyLogging

/**
 * All parameters used by FCNs, plus any used by both CNNs amd FCNs 
 */
case class WeightInitializer(fcnName: String) extends Parameter {
  override def toString = s"$fcnName"
}

case class ActivationParamSet(activationParams: Array[ActivationParams]) extends Parameter {
  override def toString = {
    val str = activationParams.map(p => {
      p.activationName + (p.epsilon match {
        case 0.0 => ""
        case _   => s".epsilon=${p.epsilon}"
      })
    }).mkString(", ")
    s"[$str]"
  }
}

case class DMArray(dma: Array[DenseMatrix[Double]]) extends Parameter {
  override def toString = s"${matrix3dToString(dma)}"
}

case class DVArray(dva: Array[DenseVector[Double]]) extends Parameter {
  override def toString = s"${vectorArrayToString(dva)}"
}

case class DV(dv: DenseVector[Double]) extends Parameter {
  override def toString = s"${vectorToString(dv)}"
}

case class FCNImplementation(fcnImpl: FCNImplementations) extends Parameter

class Parameters(val m: mutable.Map[String, Parameter]) extends Serializable {

  override def toString = {
    m.toSeq.sortBy(_._1).map(kv => s"${kv._1}:${kv._2.toString}").mkString("\n|\n")
  }

  private [params] def getOption[T](pName: String): Option[T] = {
    val clazz = implicitly[ClassTag[Simple[T]]].runtimeClass
    m.get(pName) match {
      case Some(p) if clazz.isInstance(p) => Some(p.asInstanceOf[Simple[T]].x)
      case None                           => None
      case _                              => throw new IllegalArgumentException(s"Corrupt or missing '$pName' param")
    }
  }

  def get[T](pName: String): T = {
    val clazz = implicitly[ClassTag[Simple[T]]].runtimeClass
    m(pName) match {
      case p if clazz.isInstance(p) => p.asInstanceOf[Simple[T]].x
      case _                        => throw new IllegalArgumentException(s"Corrupt or missing '$pName' param")
    }
  }

  private [params] def getArray[T](pName: String): Array[T] = {
    val clazz = implicitly[ClassTag[ArrayParam[T]]].runtimeClass
    m(pName) match {
      case p if clazz.isInstance(p) => p.asInstanceOf[ArrayParam[T]].x
      case _                        => throw new IllegalArgumentException(s"Corrupt or missing '$pName' param")
    }
  }

  private [params] def getArrayOption[T](pName: String): Option[Array[T]] = {
    val clazz = implicitly[ClassTag[ArrayParam[T]]].runtimeClass
    m.get(pName) match {
      case Some(p) if clazz.isInstance(p) => Some(p.asInstanceOf[ArrayParam[T]].x)
      case None                           => None
      case _                              => throw new IllegalArgumentException(s"Corrupt or missing '$pName' param")
    }
  }

  private def set(name: String, value: Parameter): Parameters = {
    m += name -> value
    this
  }

  def getLearningRate(): Double = get[Double]("learningrate")
  def setLearningRate(lr: Double): Parameters = set("learningrate", Simple(lr))

  def getCostFunctionName(): String = get[String]("costfunction")
  def setCostFunction(s: String): Parameters = set("costfunction", Simple(s))

  def getWeightInitializer(): String = get[String]("weightinitializer")
  def setWeightInitializer(s: String): Parameters = set("weightinitializer", Simple(s))

  def getIterations(): Int = get[Int]("iterations")
  def setIterations(i: Int): Parameters = set("iterations", Simple(i))

  def getOptimizer(): String = get[String]("optimizer")
  def setOptimizer(s: String): Parameters = set("optimizer", Simple(s))

  def getLayerDims(): Array[Int] = getArray[Int]("layerdims")
  def setLayerDims(ia: Int*): Parameters = set("layerdims",ArrayParam(ia.toArray))
  
  def getWeights(): Option[Array[DenseMatrix[Double]]] = m.get("weights") match {
    case Some(p: DMArray) => Some(p.dma)
    case None             => None
    case _                => throw new IllegalArgumentException(s"Corrupt 'weights' param")
  }

  def getBiases(): Option[Array[DenseVector[Double]]] = m.get("biases") match {
    case Some(p: DVArray) => Some(p.dva)
    case None             => None
    case _                => throw new IllegalArgumentException(s"Corrupt 'biases' param")
  }
  def setWeightsAndBiases(weightsAndBiases: Array[LWeightsAndBiases]): Parameters = {
    m += "weights" -> DMArray(weightsAndBiases.map(_.weights))
    m += "biases" -> DVArray(weightsAndBiases.map(_.biases))
    setZeroBiases(weightsAndBiases.map(_.biases).forall(_ == 0.0))
    this
  }

  def getParallel(): Boolean = getOption[Boolean]("parallel") match {
    case Some(s) => s
    case None    => true
  }
  def setParallel(b: Boolean): Parameters = set("parallel", Simple(b))

  def getZeroBiases(): Boolean = getOption[Boolean]("zerobiases") match {
    case Some(b) => b
    case None    => false
  }
  def setZeroBiases(b: Boolean): Parameters = set("zerobiases", Simple(b))

  def setWeightRegularizerParams(weightRegularizer: String, weightDecay: Double): Parameters = {
    m += "weightregularizer" -> Simple(weightRegularizer)
    m += "weightdecay" -> Simple(weightDecay)
    this
  }
  def getWeightRegularizerParams(): Option[(String, Double)] = getOption[String]("weightregularizer") match {
    case Some(weightregularizer) => Some((weightregularizer, get[Double]("weightdecay")))
    case None                    => None
  }
  def weightRegularizer: Option[RegularizationParams] = getWeightRegularizerParams() match {
    case Some((fName, decay)) => Some(getWeightRegularizer(decay, fName))
    case None                 => None
  }

  def getKeepProbabilities(): Option[Array[Double]] = getArrayOption[Double]("keepprobabilities")
  def setKeepProbabilities(da: Double*): Parameters = set("keepprobabilities", ArrayParam(da.toArray))
  
  def removeKeepProbalities(): Parameters = {
    m.remove("keepprobabilities")
    this
  }

  def getMomentum(): Option[Double] = getOption[Double]("momentumBeta")
  def setMomentum(d: Double): Parameters = {
    m += "momentumBeta" -> Simple(d)
    setOptimizer("momentum")
  }

  def getAdamParams(): Option[(Double, Double, Double)] = {
    getOption[Double]("adambeta1") match {
      case Some(adambeta1) => Some((adambeta1, get[Double]("adambeta2"), get[Double]("adamepsilon")))
      case None            => None
    }
  }

  def setAdam(beta1: Double, beta2: Double, epsilon: Double): Parameters = {
    m += "adambeta1" -> Simple(beta1)
    m += "adambeta2" -> Simple(beta2)
    m += "adamepsilon" -> Simple(epsilon)
    setOptimizer("adam")

  }

  def setAdamTF(beta1: Double, beta2: Double, epsilon: Double): Parameters = {
    m += "adambeta1" -> Simple(beta1)
    m += "adambeta2" -> Simple(beta2)
    m += "adamepsilon" -> Simple(epsilon)
    setOptimizer("adamtf")

  }

  def getAccuracy(dataSetType: String): Option[Double] = getOption[Double](dataSetType.toLowerCase() + "accuracy")
  def setAccuracy(dataSetType: String, accuracy: Double): Unit = dataSetType.toLowerCase match {
    case ("train" | "test") => {
      m += dataSetType + "accuracy" -> Simple(accuracy)
      ()
    }
    case _ => throw new IllegalArgumentException(s"Unsupported dataset type: '$dataSetType'")
  }

  def getSampleSize(dataSetType: String): Option[Int] = getOption[Int](dataSetType.toLowerCase() + "samplesize")
  def setSampleSize(dataSetType: String, sampleSize: Int): Unit = dataSetType.toLowerCase match {
    case ("train" | "test") => {
      m += dataSetType + "samplesize" -> Simple(sampleSize)
      ()
    }
    case _ => throw new IllegalArgumentException(s"Unsupported dataset type: '$dataSetType'")
  }

  def getMiniBatchSize(): Option[Int] = getOption[Int]("minibatchsize")
  def setMiniBatchSize(i: Int): Parameters = set("minibatchsize", Simple(i))

  def getNumBatches(): Option[Int] = getOption[Int]("numbatches")
  def setNumBatches(i: Int): Parameters = set("numbatches", Simple(i))

  def getSamplerType(): Option[String] = getOption[String]("samplertype")
  def setSamplerType(s: String): Parameters = set("samplertype", Simple(s))

  def setSamplerSpec(samplerType: String, batchSize: Int, numBatches: Int): Parameters = {
    setMiniBatchSize(batchSize)
    setNumBatches(numBatches)
    setSamplerType(samplerType)
  }

  def setSamplerSpec(samplerType: String, batchSize: Int): Parameters = {
    setMiniBatchSize(batchSize)
    setSamplerType(samplerType)
  }

  def getSamplerSpec(): String = (getSamplerType(), getMiniBatchSize()) match {
    case (Some(samplerType), Some(miniBatchSize)) => {
      val numBatches = getNumBatches() match {
        case Some(numBatches) => s", numberBatches: $numBatches"
        case None             => ""
      }
      s"$samplerType [miniBatchSize: $miniBatchSize$numBatches]"
    }
    case _ => "Not Specified"
  }

  def getFCNSampler(ys: DenseMatrix[Double], xs: DenseMatrix[Double]): MiniBatchSampler = (getSamplerType(), getMiniBatchSize()) match {
    case (Some(samplerType), Some(miniBatchSize)) => {
      val numBatches = getNumBatches().getOrElse(xs.cols / miniBatchSize)
      setNumBatches(numBatches)
      MiniBatchSampler(ys, xs, miniBatchSize, numBatches, samplerType)
    }
    case _ => throw new IllegalStateException("setSamplerSpec has not been called")
  }

  def getLogInterval(): Option[Int] = getOption[Int]("loginterval")
  def setLogInterval(i: Int): Parameters = set("loginterval", Simple(i))

  def getGradientCheckInterval(): Option[Int] = getOption[Int]("gradientcheckinterval")
  def setGradientCheckInterval(i: Int): Parameters = set("gradientcheckinterval", Simple(i))

  def getTrainTime(): Option[Int] = getOption[Int]("traintime")
  def setTrainTime(i: Int): Parameters = set("traintime", Simple(i))

  def getActivationParams(): Array[ActivationParams] = m("activations") match {
    case p: ActivationParamSet => p.activationParams
    case _                     => throw new IllegalArgumentException(s"Corrupt 'WeightRegularizer' param")
  }
  def setActivations(args: String*): Parameters = {
    val str = s"[${args.mkString(" ")}]"
    m += "activations" -> toActivationParamSet(cleanString(str))
    this
  }

  def getLayerWeightsAndBiases(): Option[Array[LWeightsAndBiases]] = (getWeights(), getBiases()) match {
    case (Some(weights), Some(biases)) => {
      require(biases.size == weights.size, s"Mismatch between weights layers (${weights.size}) and biases layers (${biases.size})")
      val lb = (for (i <- 0 until weights.size) yield {
        LWeightsAndBiases(weights(i), biases(i))
      }).toArray
      Some(lb)
    }
    case (None, None) => None
    case (_, _)       => throw new IllegalArgumentException(s"Missing 'weights' or 'biases' param")
  }

  def getCost(): Option[Double] = getOption[Double]("cost")
  def setCost(d: Double): Parameters = set("cost", Simple(d))

  def getCosts(): Option[DenseVector[Double]] = m.get("costs") match {
    case Some(p: DV) => Some(p.dv)
    case None        => None
    case _           => throw new IllegalArgumentException(s"Corrupt 'costs' param")
  }
  def setCosts(costs: DenseVector[Double]): Parameters = set("costs", DV(costs))

  def activationFunctions(): Array[ActivationFunction] = getActivationFunctions(getActivationParams())

  def costFunction() = getCostFunction(getCostFunctionName())

  def backPropationFunctions(): Array[BackPropagationLayerFunction] = getLayerBackPropFunctions(getActivationParams())

  def getFCNImplementations(): FCNImplementations = m.get("fcnImplementation") match {
    case None                       => new DefaultFCNImplementations(this)
    case Some(p: FCNImplementation) => p.fcnImpl
    case _                          => throw new IllegalArgumentException(s"Corrupt 'fcnImplementation' param")
  }

  def setFCNImplementations(fcnImpl: FCNImplementations): Parameters = {
    m += "fcnImplementation" -> FCNImplementation(fcnImpl)
    this
  }

}

object Parameters extends LazyLogging {

  def apply() = new Parameters(scala.collection.mutable.Map[String, Parameter]())

  def apply(p: Parameters) = new Parameters(scala.collection.mutable.Map[String, Parameter]() ++ p.m)

  def apply(s: String) = new Parameters(toParameters(s))

  def apply(params: mutable.Map[String, String]) = new Parameters(params.map(kv => toParam(kv._1, kv._2)))

  def toMutableMap(paramPairs: Seq[(String, String)]): mutable.Map[String, Parameter] = {
    val m = paramPairs.map(kv => toParam(kv._1, kv._2))
    mutable.Map(m.toSeq: _*)
  }

  def toParameters(s: String): mutable.Map[String, Parameter] = toMutableMap(toParamPairs(s))

  def toParamPairs(s: String): Seq[(String, String)] =
    cleanString(s).split("\\|").map(s => toParamPair(s.trim))

  def toParamPair(s: String): (String, String) = {
    val splitIdx = s.indexOf(":")
    if (splitIdx < 1) throw new IllegalArgumentException(s"Missing param name : value separator: '${s.take(30)}'")
    val pName = s.take(splitIdx).toLowerCase
    val pValue = s.drop(splitIdx + 1)
    (pName, pValue)
  }

  def toParam(pName: String, pValue: String): (String, Parameter) = pName match {
    case "weights"               => (pName, DMArray(toDenseMatrixArray(pValue)))
    case "biases"                => (pName, DVArray(toDenseVectorArray(pValue)))
    case "zerobiases"            => (pName, Simple(pValue.toBoolean))
    case "activations"           => (pName, toActivationParamSet(pValue))
    case "learningrate"          => (pName, Simple(pValue.toDouble))
    case "iterations"            => (pName, Simple(pValue.toInt))
    case "minibatchsize"         => (pName, Simple(pValue.toInt))
    case "batchsize"             => (pName, Simple(pValue.toInt))
    case "numbatches"            => (pName, Simple(pValue.toInt))
    case "loginterval"           => (pName, Simple(pValue.toInt))
    case "gradientcheckinterval" => (pName, Simple(pValue.toInt))
    case "parallel"              => (pName, Simple(pValue))
    case "sampler"               => (pName, Simple(pValue))
    case "samplertype"           => (pName, Simple(pValue))
    case "optimizer"             => (pName, Simple(pValue))
    case "costfunction"          => (pName, Simple(pValue))
    case "weightinitializer"     => (pName, Simple(pValue))
    case "layerdims"             => (pName, ArrayParam(toDenseVector(pValue).toArray.map(_.toInt)))
    case "cost"                  => (pName, Simple(pValue.toDouble))
    case "costs"                 => (pName, DV(toDenseVector(pValue)))
    case "weightregularizer"     => (pName, Simple(pValue))
    case "weightdecay"           => (pName, Simple(pValue.toDouble))
    case "traintime"             => (pName, Simple(pValue.toInt))
    case "trainsamplesize"       => (pName, Simple(pValue.toInt))
    case "testsamplesize"        => (pName, Simple(pValue.toInt))
    case "trainaccuracy"         => (pName, Simple(pValue.toDouble))
    case "testaccuracy"          => (pName, Simple(pValue.toDouble))
    case "momentumbeta"          => (pName, Simple(pValue.toDouble))
    case "adambeta1"             => (pName, Simple(pValue.toDouble))
    case "adambeta2"             => (pName, Simple(pValue.toDouble))
    case "adamepsilon"           => (pName, Simple(pValue.toDouble))
    case "keepprobabilities"     => (pName, ArrayParam(toDenseVector(pValue).toArray))
    case _                       => throw new IllegalArgumentException(s"Unsupported param: '$pName'")

  }

  def toActivationParamSet(s: String): ActivationParamSet = {
    require(s.startsWith("[") && s.endsWith("]"), s)
    val ar = s.drop(1).dropRight(1).split(" ").map(pName => {
      val param = new ActivationParams()
      if (pName.toLowerCase.startsWith("leaky_relu.epsilon=")) {
        param.activationName = "leaky_relu"
        try {
          param.epsilon = pName.drop("leaky_relu.epsilon=".size).toDouble
        } catch {
          case e: NumberFormatException => {
            throw new IllegalArgumentException(s"Could not load leaky_relu activation. Missing or invalid .epsilon= in $s")
          }
        }
      } else {
        param.activationName = pName
      }
      param
    })
    ActivationParamSet(ar)

  }

}