/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params

/**
 * Optimized parameters for a single layer when using simple gradient descent in FCNs
 */
case class OptimizedParamsSimple(weightsAndBiases: LWeightsAndBiases, 
    gradients: LGradients) {
  override def toString = s"[\n$weightsAndBiases\n$gradients\n]"
}

/**
 * Optimized parameters for a single layer when using momentum in FCNs
 */
case class OptimizedParamsMomentum(weightsAndBiases: LWeightsAndBiases,
    gradients: LGradients,
    velocities: LayerDeltas)

/**
 * Optimized parameters for a single layer when using adam in FCNs
 */
case class OptimizedParamsAdam(weightsAndBiases: LWeightsAndBiases,
    gradients: LGradients,
    avgGradients: LayerDeltas,
    avgSqGradients: LayerDeltas)

sealed trait OptimizedParamsSet {
  val weightsAndBiases: Array[LWeightsAndBiases]
  val gradients: Array[LGradients]
}

/**
 * Optimized parameters for a all layers when using simple gradient descent in FCNs
 */
case class OptimizedParamsSetSimple(params: Array[OptimizedParamsSimple]) extends OptimizedParamsSet{
  val weightsAndBiases: Array[LWeightsAndBiases] = params.map(_.weightsAndBiases)
  val gradients: Array[LGradients] = params.map(_.gradients)
}

/**
 * Optimized parameters for a all layers when using momentum in FCNs
 */
case class OptimizedParamsSetMomentum(params: Array[OptimizedParamsMomentum]) extends OptimizedParamsSet{
  val weightsAndBiases: Array[LWeightsAndBiases] = params.map(_.weightsAndBiases)
  val gradients: Array[LGradients] = params.map(_.gradients)
  val velocities: Array[LayerDeltas] = params.map(_.velocities)
}

/**
 * Optimized parameters for a all layers when using adam in FCNs
 */
case class OptimizedParamsSetAdam(params: Array[OptimizedParamsAdam]) extends OptimizedParamsSet{
  val weightsAndBiases: Array[LWeightsAndBiases] = params.map(_.weightsAndBiases)
  val gradients: Array[LGradients] = params.map(_.gradients)
  val avgGradients: Array[LayerDeltas] = params.map(_.avgGradients)
  val avgSqGradients: Array[LayerDeltas] = params.map(_.avgSqGradients)
}