/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector

/**
 * Weights and Biases for a single layer of an FCN. Weight dimensions are (current layer, previous layer)
 *
 * @param weights dimensions are (current layer, previous layer)
 * @param biases dimensions are (current layer)
 *
 */
case class LWeightsAndBiases(weights: DenseMatrix[Double], biases: DenseVector[Double]) extends Serializable{
  require(weights.rows == biases.size, s"Mismatch between weight layer size (${weights.rows}) and biases size (${biases.size}")
  override def toString = s"weights:\n$weights\nbiases:\n$biases"
}