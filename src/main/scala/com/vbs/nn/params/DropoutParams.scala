/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params
import breeze.linalg.DenseMatrix

/**
 * Defines dropouts if specified for FCNs
 */
case class DropoutParams(weightsAndBiases: Array[LWeightsAndBiases], keepProbabilities: Array[Double]) {
  require(keepProbabilities.size == weightsAndBiases.size - 1, s"Number of drop out keep probabilities (${keepProbabilities.size}) must match number of layers(${weightsAndBiases.size}) - 1")

  def generateDropouts(m: Int): Array[DenseMatrix[Double]] = {
    (for (i <- 0 until keepProbabilities.size) yield {
      val r = weightsAndBiases(i).weights.rows
      DenseMatrix.rand(r, m).map(x => if (x < (1 - keepProbabilities(i))) 0.0 else 1.0)
    }).toArray
  }
}