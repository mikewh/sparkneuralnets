/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.params

import com.vbs.nn.fcn.OptimizerFunctions.initializeDeltas

/**
 * Container for updated FCN parameters
 * 
 */
sealed trait OptimizerParams extends Serializable{
  
  def weightsAndBiases: Array[LWeightsAndBiases]
  
  def weightsAndBiases_=(weightsAndBiases: Array[LWeightsAndBiases]): Unit
}

class OptimizerParamsSimple(initialWeightsAndBiases: Array[LWeightsAndBiases]) extends OptimizerParams {
  
  private var _weightsAndBiases = initialWeightsAndBiases
  
  def weightsAndBiases = _weightsAndBiases
  
  def weightsAndBiases_=(weightsAndBiases: Array[LWeightsAndBiases]): Unit = {
    _weightsAndBiases = weightsAndBiases
  }
}
object OptimizerParamsSimple {
  def apply(initialWeightsAndBiases: Array[LWeightsAndBiases]) = new OptimizerParamsSimple(initialWeightsAndBiases)
}

class OptimizerParamsMomentum(val momentum: Double, initialWeightsAndBiases: Array[LWeightsAndBiases]) extends OptimizerParams {
  
  private var _weightsAndBiases = initialWeightsAndBiases
  
  def weightsAndBiases: Array[LWeightsAndBiases] = _weightsAndBiases
  
  def weightsAndBiases_=(weightsAndBiases: Array[LWeightsAndBiases]): Unit = {
    _weightsAndBiases = weightsAndBiases
  }
  
  private var _velocities = initializeDeltas(initialWeightsAndBiases)

  def velocities_=(velocities: Array[LayerDeltas]): Unit = {
    _velocities = velocities
  }

  def velocities = _velocities
}
object OptimizerParamsMomentum {
  def apply(momentum: Double, initialWeightsAndBiases: Array[LWeightsAndBiases]) = new OptimizerParamsMomentum(momentum, initialWeightsAndBiases)
}

class OptimizerParamsAdam(val beta1: Double, val beta2: Double, val epsilon: Double, 
    initialWeightsAndBiases: Array[LWeightsAndBiases]) extends OptimizerParams {
  
  private var _weightsAndBiases = initialWeightsAndBiases
  
  def weightsAndBiases = _weightsAndBiases
  
  def weightsAndBiases_=(weightsAndBiases: Array[LWeightsAndBiases]): Unit = {
    _weightsAndBiases = weightsAndBiases
  }
  
  private var _count = 0
  
  def count(): Int = {
    _count += 1
    _count
  }
  
  private var _avgGradients = initializeDeltas(initialWeightsAndBiases)

  def avgGradients_=(avgGradients: Array[LayerDeltas]): Unit = {
    _avgGradients = avgGradients
  }

  def avgGradients = _avgGradients
  
  
  private var _avgSqGradients = initializeDeltas(initialWeightsAndBiases)

  def avgSqGradients_=(avgSqGradients: Array[LayerDeltas]): Unit = {
    _avgSqGradients = avgSqGradients
  }

  def avgSqGradients = _avgSqGradients  
}
object OptimizerParamsAdam {
  def apply(beta1: Double, beta2: Double, epsilon: Double, 
    initialWeightsAndBiases: Array[LWeightsAndBiases]) = new OptimizerParamsAdam(beta1, beta2, epsilon, initialWeightsAndBiases) 
}