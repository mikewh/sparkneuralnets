/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.io

import com.vbs.nn.cnn.CNNNormaliserFunctions
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import breeze.linalg.DenseMatrix
import com.vbs.nn.utils.MatrixUtils


/**
 * Loads normalised DenseMatrix4d from Numpy string representation, then persists for fast subsequent retrieval using MatrixIO.loadDenseMatrix4d.
 * 
 */
class NumpyArrayPreprocessor(fName: String) {
  val fNameXS = fName + "_xs"
  val fNameYS = fName + "_ys"
  
  private var normalise: CNNNormaliserFunctions.CNNNormaliserFunction = CNNNormaliserFunctions.normaliseImageSet
  
  private var labelValues: Option[Int] = None
  
  def setMeanNormaliser(): NumpyArrayPreprocessor = {
    normalise = CNNNormaliserFunctions.normaliseMean
    this
  }
  
  def setLabelValues(labelvalues: Int): NumpyArrayPreprocessor = {
    this.labelValues = Some(labelvalues)
    this
  }
  
  def transform(): (DenseMatrix4d, DenseMatrix[Double]) = labelValues match {
    case Some(lv) => {
      val xs = normalise(NumpyArrayLoader.loadMultiChannelDataSet(s"$fNameXS.txt"))
      val ys = MatrixUtils.oneHotEncode(NumpyArrayLoader.loadRaw2dMatrix(s"$fNameYS.txt").toArray, lv)
      (xs, ys)  
    }
    case None => throw new IllegalStateException(s"Transform of $fNameYS failed. No label values have been supplied")
  }
  
  def persistDenseMatrices(): (DenseMatrix4d, DenseMatrix[Double]) = {
    val (xs, ys) = transform()
    MatrixIO.persistDenseMatrix4d(fNameXS, xs)
    MatrixIO.persistMatrix(fNameYS, ys)
    (xs, ys)
  }
  
}

object NumpyArrayPreprocessor {
  def apply(fName: String) = new NumpyArrayPreprocessor(fName)
}