/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.io

import com.vbs.nn.fcn.NormaliserFunctions
import breeze.linalg.DenseMatrix

/**
 * Loads feature set and labels from a single csv file as a pair of DenseMatrices with the feature set normalised.
 * 
 * Optionally persists DenseMatrix's for fast subsequent retrieval using MatrixIO.load
 */
class CSVDataSetPreprocessor(fName: String, separator: String = ",") {
  
  private var labelSpec: Option[(Boolean, Int)] = None
  private var normalise: NormaliserFunctions.NormaliserFunction = NormaliserFunctions.normaliseMean
  
  def setLabelSpec(labelFirst: Boolean, labelValues: Int): CSVDataSetPreprocessor = {
    labelSpec = Some((labelFirst, labelValues))
    this
  }
  
  /**
   * Use if the feature set represents an image
   */
  def setImageNormaliser(): CSVDataSetPreprocessor = {
    normalise = NormaliserFunctions.normaliseImage
    this
  }
  
  def transform(): (DenseMatrix[Double], DenseMatrix[Double]) = labelSpec match {
    case Some(labelSpec) => {
      val (labelFirst, labelValues) = labelSpec
      val (featureSet, labels) = CSVLoader.getFeatureSetAndEncodedLabels(s"$fName.csv", labelFirst, labelValues, separator)
      (normalise(featureSet), labels)
      
    }
    case None => throw new IllegalStateException(s"Transform of $fName failed. No label spec has been supplied")
  }
  
  def persistDenseMatrices(): (DenseMatrix[Double], DenseMatrix[Double]) = {
    val (xs, ys) = transform()
    MatrixIO.persistMatrix(s"${fName}_xs",xs)
    MatrixIO.persistMatrix(s"${fName}_ys",ys)
    (xs, ys)
  }
  
  
}

object CSVDataSetPreprocessor {
  def apply(fName: String, separator: String = ",") = new CSVDataSetPreprocessor(fName, separator)
}