/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.io
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d
import com.vbs.nn.model.DataSetTypes.DenseMatrix3d
import scala.io.Source
import com.vbs.nn.utils.MatrixUtils
import breeze.linalg.DenseMatrix

/**
 * Loads numpy datasets which have been persisted as strings using np.array_str
 * 
 */
object NumpyArrayLoader {

  def loadRaw4dMatrix(fName: String): DenseMatrix4d = {
    val s = Source.fromFile(fName).toSeq.mkString
    MatrixUtils.toDenseMatrix4d(s)
  }
  
  def loadRaw2dMatrix(fName: String): DenseMatrix[Double] = {
    val s = Source.fromFile(fName).toSeq.mkString
    MatrixUtils.toDenseMatrix(s)
  }

  def loadMultiChannelDataSet(fName: String): DenseMatrix4d = loadRaw4dMatrix(fName).map(toMultiChannelDataItem)

  private[io] def toMultiChannelDataItem(dma: DenseMatrix3d): DenseMatrix3d = {
    val height = dma.size
    val width = dma(0).rows
    val channels = dma(0).cols
    dma.foreach(dm => require(dm.rows == width && dm.cols == channels, s"Expected (h,w,c) of ($height, $width, $channels) but got ($height, ${dm.rows}, ${dm.cols})"))

    val imageData = for (channel <- 0 until channels) yield {
      val dm = new DenseMatrix[Double](height, width)
      for (r <- 0 until height) {
        for (c <- 0 until width) dm(r, c) = dma(r)(c, channel)
      }
      dm
    }
    imageData
  }
 
}