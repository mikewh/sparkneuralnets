/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.io

import breeze.linalg.DenseMatrix
import java.io.FileOutputStream
import java.nio.channels.FileChannel
import java.nio.ByteBuffer
import java.io.RandomAccessFile
import com.vbs.nn.model.DataSetTypes.DenseMatrix4d

import com.vbs.nn.model.MultiChannelDataSetDM

/**
 * Quick and dirty mechanism to persist data as sequence of doubles for quicker subsequent retrieval
 */
object MatrixIO {

  def persistMatrix(fName: String, dm: DenseMatrix[Double]): Unit = {
    val out = new FileOutputStream(fName + ".bdm")
    val file = out.getChannel()
    val len = dm.rows * dm.cols
    val buf = ByteBuffer.allocate(2 * 4 + 8 * len)
    buf.putInt(dm.rows)
    buf.putInt(dm.cols)
    val arr = dm.toArray
    for (i <- 0 until arr.size) buf.putDouble(arr(i))
    buf.flip
    file.write(buf)
    out.flush()
    file.close
    out.close
  }

  def load(fName: String): DenseMatrix[Double] = {
    val file = new RandomAccessFile(fName + ".bdm", "r")
    val inChannel = file.getChannel()
    val buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
    buffer.load()
    val rows = buffer.getInt
    val cols = buffer.getInt
    val arr = (for (i <- 0 until (buffer.limit - 8) / 8) yield (buffer.getDouble)).toArray
    new DenseMatrix(rows, cols, arr)
  }
  
  def persistDenseMatrix4d(fName: String, dmss: DenseMatrix4d): Unit = {
    val mcds = MultiChannelDataSetDM(dmss)
    val data = mcds.toArray
    val out = new FileOutputStream(fName + ".bdmi")
    val file = out.getChannel()
    val buf = ByteBuffer.allocate(4 * 4 + 8 * data.size)
    buf.putInt(mcds.size)
    buf.putInt(mcds.height)
    buf.putInt(mcds.width)
    buf.putInt(mcds.channels)
    for (i <- 0 until data.size) buf.putDouble(data(i))
    buf.flip
    file.write(buf)
    out.flush()
    file.close
    out.close
  }
  
  def loadDenseMatrix4d(fName: String): DenseMatrix4d = {
    val file = new RandomAccessFile(fName + ".bdmi", "r")
    val inChannel = file.getChannel()
    val buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
    buffer.load()
    val size = buffer.getInt
    val height = buffer.getInt
    val width = buffer.getInt
    val channels = buffer.getInt
    val data = (for (i <- 0 until (buffer.limit - 4 * 4) / 8) yield (buffer.getDouble)).toArray
    file.close
    inChannel.close
    MultiChannelDataSetDM(size, width, height, channels, data).dmss
    
  }

}