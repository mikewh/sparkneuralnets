/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn.io

import scala.io.Source
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.utils.MatrixUtils

/**
 * Loads feature sets and (optionally) labels from a CSV file info DenseMatrix and DenseVector
 *
 */
object CSVLoader {

  def loadAsArrays(fName: String, separator: String = ","): Seq[Array[Double]] = {
    val data = Source.fromFile(fName).getLines
      .filter(line => !(line.isEmpty))
      .map(line => line.split(separator).map(_.toDouble)).toSeq

    require(!data.isEmpty, s"$fName contains no valid data")
    val setSize = data(0).size
    require(data.forall(entry => entry.size == setSize), s"$fName contains records with differing numbers of elements")

    data
  }
  
  private [io] def toFeatureSet(data: Seq[Array[Double]]): DenseMatrix[Double] = {
    val cols = data.size
    val rows = data(0).size
    new DenseMatrix[Double](rows, cols, data.flatten.toArray)
  }
  
  private [io] def extractFeatureSetAndLabels(fName: String, labelFirst: Boolean, 
      separator: String = ","): (Seq[Array[Double]], Seq[Double]) = {
    loadAsArrays(fName, separator).map(entry => {
      labelFirst match {
        case true => (entry.tail, entry.head)
        case false => (entry.dropRight(1), entry.last)
      }
    }).unzip
  }

  /**
   * The label must be either the first or last element of each entry
   */
  def getFeatureSetAndLabels(fName: String, labelFirst: Boolean, separator: String = ","): (DenseMatrix[Double], DenseVector[Double]) = {
    val (featureSet, labels) = extractFeatureSetAndLabels(fName, labelFirst, separator)
    (toFeatureSet(featureSet), new DenseVector(labels.toArray))
  }
  
  def getFeatureSetAndEncodedLabels(fName: String, labelFirst: Boolean, labelValues: Int, 
      separator: String = ","): (DenseMatrix[Double], DenseMatrix[Double]) = {
    val (featureSet, labels) = extractFeatureSetAndLabels(fName, labelFirst, separator)
    (toFeatureSet(featureSet), MatrixUtils.oneHotEncode(labels.toArray, labelValues))
  }
    

  /**
   * Used if the underlying CSV contains only the feature set and no labels
   */
  def getFeatureSetDM(fName: String, separator: String = ","): DenseMatrix[Double] = toFeatureSet(loadAsArrays(fName, separator))

}