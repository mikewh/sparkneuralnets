/*
 * Produced by by Virtual Business Systems: www.virtual-businesses.com email scala@virtual-businesses.com
 * 
 * This is demonstration code not intended for commercial use. It is made available WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * If it is used, this notice must be retained.
 */
package com.vbs.nn
import scala.collection.mutable.ArrayBuffer
import com.typesafe.scalalogging.LazyLogging

import com.vbs.nn.fcn.CostFunctions._
import com.vbs.nn.fcn.OptimizerFunctions._
import com.vbs.nn.params.OptimizedParamsSet
import com.vbs.nn.params.OptimizedParamsSetMomentum
import com.vbs.nn.params.Parameters
import com.vbs.nn.sampler.MiniBatchSampler

import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import com.vbs.nn.fcn.RegularizationFunctions
import com.vbs.nn.params.OptimizerParams
import com.vbs.nn.params.OptimizerParamsSimple
import com.vbs.nn.params.OptimizerParamsMomentum
import com.vbs.nn.params.WeightsAndBiasesInitializer
import com.vbs.nn.params.OptimizerParamsAdam
import com.vbs.nn.params.OptimizedParamsSetAdam

import com.vbs.nn.utils.GradientChecker._
import com.vbs.nn.fcn.ActivationFunctions

/**
 * A stand alone fully connected network.
 *
 * Each minibatch is defined by Breese Dense matrices, one for the featureset and one for the labelset.
 *
 * No Spark implementation for the FCN is currently defined.
 *
 */
object FullyConnectedNetwork extends LazyLogging {

  def initialise(p: Parameters): OptimizerParams = {
    val weightsAndBiases = p.getLayerWeightsAndBiases() match {
      case Some(wAndB) => wAndB
      case None => {
        val wb = WeightsAndBiasesInitializer.generateWeightsAndBiases(p.getLayerDims(), p.getWeightInitializer())
        p.setWeightsAndBiases(wb)
        wb
      }
    }

    val optimizerParams: OptimizerParams = p.getOptimizer() match {
      case "momentum" => OptimizerParamsMomentum(p.getMomentum().get, weightsAndBiases)
      case "adam" | "adamtf" => {
        val (beta1, beta2, epsilon) = p.getAdamParams().get
        OptimizerParamsAdam(beta1, beta2, epsilon, weightsAndBiases)

      }
      case _ => OptimizerParamsSimple(weightsAndBiases)
    }
    optimizerParams

  }

  private[nn] def trainMiniBatch(p: Parameters, optimizerParams: OptimizerParams,
                                 xs: DenseMatrix[Double], ys: DenseMatrix[Double]): (Double, OptimizedParamsSet) = {
    val fcnImplementations = p.getFCNImplementations()
    val weightsAndBiases = optimizerParams.weightsAndBiases
    val dropouts = p.getKeepProbabilities() match {
      case Some(probs) => Some((probs, RegularizationFunctions.generateDropouts(weightsAndBiases, probs, xs.cols)))
      case _           => None
    }

    val outputsAndActivations = fcnImplementations.feedForward(xs, weightsAndBiases, dropouts)
    val activationsLast = outputsAndActivations.last._2
    val (lambda, cost) = regularizedCost(p.costFunction(), p.weightRegularizer, weightsAndBiases, activationsLast, ys)
    val gradients = fcnImplementations.backPropagate(xs, ys,
      outputsAndActivations, weightsAndBiases, lambda, dropouts)

    val updatedOptimizedParams = optimizerParams match {
      case _: OptimizerParamsSimple => optimizeSimple(p.getLearningRate(), weightsAndBiases, gradients)
      case opm: OptimizerParamsMomentum => {
        optimizeMomentum(p.getLearningRate(), opm.momentum,
          weightsAndBiases, gradients, opm.velocities)
      }
      case opa: OptimizerParamsAdam => {
        val adamOptimizerFCN = p.getOptimizer() match {
          case "adam"   => optimizeAdam _
          case "adamtf" => optimizeAdamTF _
        }
        adamOptimizerFCN(p.getLearningRate(), opa.beta1, opa.beta2, opa.epsilon, opa.count(),
          weightsAndBiases, gradients, opa.avgGradients, opa.avgSqGradients)
      }
    }
    (cost, updatedOptimizedParams)
  }

  def trainImpl(p: Parameters, sampler: MiniBatchSampler): (Double, OptimizedParamsSet) = {
    require(p.getIterations() > 0, "train called with 0 iterations")
    val ts = System.currentTimeMillis()

    val weightsAndBiases = p.getLayerWeightsAndBiases() match {
      case Some(wAndB) => wAndB
      case None        => WeightsAndBiasesInitializer.generateWeightsAndBiases(p.getLayerDims(), p.getWeightInitializer())
    }
    val costs = new ArrayBuffer[Double]

    val optimizerParams: OptimizerParams = p.getOptimizer() match {
      case "momentum" => OptimizerParamsMomentum(p.getMomentum().get, weightsAndBiases)
      case "adam" => {
        val (beta1, beta2, epsilon) = p.getAdamParams().get
        OptimizerParamsAdam(beta1, beta2, epsilon, weightsAndBiases)

      }
      case _ => OptimizerParamsSimple(weightsAndBiases)
    }
    var iter = 0
    var cost = Double.MaxValue
    var optimizedParams: OptimizedParamsSet = null
    while (iter < p.getIterations()) {
      val miniBatches = sampler.miniBatches()
      for (i <- 0 until miniBatches.size) {
        val (ys, xs) = miniBatches(i)
        val costAndOptimizedParams = trainMiniBatch(p, optimizerParams, xs, ys)
        cost = costAndOptimizedParams._1
        optimizedParams = costAndOptimizedParams._2
        optimizerParams.weightsAndBiases = optimizedParams.weightsAndBiases
        optimizedParams match {
          case opsm: OptimizedParamsSetMomentum => optimizerParams.asInstanceOf[OptimizerParamsMomentum].velocities = opsm.velocities
          case opsa: OptimizedParamsSetAdam => {
            val opa = optimizerParams.asInstanceOf[OptimizerParamsAdam]
            opa.avgGradients = opsa.avgGradients
            opa.avgSqGradients = opsa.avgSqGradients
          }
          case _ =>
        }
        if (p.getLogInterval().isDefined) {
          if (costs.size % p.getLogInterval().get == 0) {
            costs += costAndOptimizedParams._1
          }
        } else costs += costAndOptimizedParams._1
        p.getGradientCheckInterval() match {
          case Some(gradientCheckInterval) => {
            if (i == 0 && iter % gradientCheckInterval == 0 && !gradientCheck(p, optimizerParams, optimizedParams.gradients, xs, ys)) {
              throw new RuntimeException(s"gradientCheck failed with cost ${costAndOptimizedParams._1} on iteration $iter}")
            }
          }
          case _ =>
        }
      }
      iter += 1
    }
    p.setWeightsAndBiases(optimizerParams.weightsAndBiases)
    p.setCosts(DenseVector(costs.toArray))
    p.setCost(cost)
    val traintime = (System.currentTimeMillis() - ts).toInt
    p.setTrainTime(traintime)
    logger.info(s"Training complete in $traintime ms")
    (cost, optimizedParams)

  }

  /**
   * Used when running a stand alone FCN
   */
  def train(p: Parameters, sampler: MiniBatchSampler): Double = {
    val (cost, optimizedParams) = trainImpl(p, sampler)
    cost

  }

  def predict(p: Parameters, xs: DenseMatrix[Double]): DenseMatrix[Double] = {
    val outputsAndActivations = ActivationFunctions.feedForward(p.activationFunctions())(xs, p.getLayerWeightsAndBiases().get, None)
    outputsAndActivations.last._2
  }

}