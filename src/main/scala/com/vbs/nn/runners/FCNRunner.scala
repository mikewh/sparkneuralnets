package com.vbs.nn.runners

import java.util.Properties
import com.typesafe.scalalogging.LazyLogging

import com.vbs.nn.params.Parameters
import java.io.FileInputStream
import scala.collection.JavaConverters._
import com.vbs.nn.io.CSVLoader
import com.vbs.nn.model.FullyConnectedModel
import com.vbs.nn.utils.MatrixUtils
import breeze.linalg.DenseMatrix

object FCNRunner extends LazyLogging {
  def main(args: Array[String]): Unit = {
    require(args.length == 1, "Run with single argument giving config path")
    try {
      val prop = new Properties()
      prop.load(new FileInputStream(args(0)))
      val pMap = prop.asScala
      def getString(name: String): String = pMap.get(name) match {
        case None    => throw new IllegalArgumentException(s"Missing property '$name'")
        case Some(s) => s
      }
      val op = getString("op").toLowerCase
      val fileName = getString("file")
      val oTestFileName = pMap.get("testFile")
      val modelPath = getString("modelPath")
      val modelName = getString("modelName")
      val labelFirst = pMap.get("labelFirst") match {
        case None    => true
        case Some(s) => s.toBoolean
      }
      val oId = pMap.get("id")
      val labelDecoder: (DenseMatrix[Double]) => Seq[Double] = MatrixUtils.binaryDecode(0.1)
      pMap -= "file"
      pMap -= "testFile"
      pMap -= "modelPath"
      pMap -= "modelName"
      pMap -= "id"
      pMap -= "op"
      (op, oId) match {
        case ("train", _) => {
          val params = Parameters(pMap)
          val (xs, ys) = CSVLoader.getFeatureSetAndLabels(fileName, labelFirst)
          oTestFileName match {
            case None => {
              val id = FullyConnectedModel.trainAndPersist(params, ys.toDenseMatrix, xs, modelPath, modelName)
              logger.info(s"FCNRunner train $fileName completed with id $id")
            }
            case Some(testFileName) => {
              val (xst, yst) = CSVLoader.getFeatureSetAndLabels(testFileName, labelFirst)
              val id = FullyConnectedModel.run(params, ys.toDenseMatrix, xs, yst.toDenseMatrix, xst, modelPath, modelName, labelDecoder)
              logger.info(s"FCNRunner train / test on $fileName / $testFileName completed with id $id")

            }
          }
        }
        case ("predict", Some(id)) => {
          val xs = CSVLoader.getFeatureSetDM(fileName)
          val params = FullyConnectedModel.load(modelPath, s"${modelName}_$id")
          FullyConnectedModel.predictAndPersistLabels(params, xs, modelPath, modelName, id, labelDecoder)
        }
        case ("predict", None) => {
          throw new IllegalArgumentException(s"Missing property 'id'")
        }
        case (uop, _) => {
          throw new IllegalArgumentException(s"Unsupported 'op' property. Expected 'train' or 'predict' but got '$uop'")
        }
      }
    } catch {
      case e: Exception => logger.error("FCNRunner failed", e)
    }

  }

}